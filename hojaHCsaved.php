<?
/*
$pac_id=31880;
$_GET["pac_id"]=$pac_id;


//Creando archivos
$control = fopen("backup.html","w+");
if($control == false){
  die("No se ha podido crear el archivo.");
}

//Iniciando el archivo html
fwrite($control,'<html><body bgcolor="#ffffff">');*/

//Adicionando datos personales
$table = new my_DB();
$query = "SELECT * FROM paciente  WHERE pac_id=$pac_id";
$table->search($query);
$obj = $table->sql_fetch_object();

$query = "SELECT * FROM datos_per_pac  WHERE pac_id=$pac_id";
$table->search($query);
$dpp = $table->sql_fetch_object();

?>
<html  xmlns="http://www.w3.org/1999/xhtml">
<head>
<STYLE type="text/css">
	.t1{
		color:red;
		font:normal small-caps bold 16pt Arial, Helvetica, Geneva, Swiss, SunSans-Regular;
		text-decoration: none;
		text-align: center;
	}
	.t2{
		color:black;
		font:normal normal bold 14pt Arial, Helvetica, Geneva, Swiss, SunSans-Regular;
		text-decoration: none;
		text-align: center;
	}
	.t3{
		color:black;
		font:normal normal bold 12pt  Arial,Helvetica,Geneva,Swiss,SunSans-Regular;
		text-decoration: none;
	}
	.tablaallbordes td{
		border: 1px solid #000;
	}

	body{
		color:green;
		font:normal normal bold 11pt  Arial,Helvetica,Geneva,Swiss,SunSans-Regular;
		text-decoration: none;
		text-align: center;
	}
	.listado {
		color: #000;
		font-size:11px;
		font-family: Arial,Helvetica,Geneva,Swiss,SunSans-Regular;
		font-weight:bold;
		border:double;
		border-color:#F00;
		min-width:100%;
	
		-moz-border-radius:7px;
		-webkit-border-radius:7px;
		-ms-border-radius:7px;
		border-radius:7px;
		margin:3px;
	
	
		-webkit-box-shadow: 3px 3px 20px #000;
		-webkit-border-radius: 5px;
		-moz-box-shadow: 3px 3px 20px #000;
		-moz-border-radius: 5px;
		-ms-box-shadow: 3px 3px 20px #000;
		-ms-border-radius: 5px;
		box-shadow: 3px 3px 20px #000;
		border-radius: 5px;
	}
	
	.listado tr.cabecera td{
		background-color: #F00;
		color:#FFF;
		text-align: center;
		/*white-space:;*/
		/*vertical-align: baseline;
		letter-spacing: normal;
		word-spacing: normal;
		white-space: normal;*/
	}
	
	.par td{
		background-color: #CCC;
	}
	
	.impar td{
		background-color: #FFF;
	}
	ul.sub1 {
		list-style:square;
		
	}

 </STYLE>
<title><?="$obj->pac_nombres $obj->pac_apellidos"?></title>
</head>


<body>

<?//include("cabecerahc.php");?>


<hr align="center">
<table width="100%">
	<tr>
		<td class="t1" colspan="4" align="center">IDENTIFICACION DEL PACIENTE<br /><br /></td>
	</tr>
	<tr>
		<td class="t2" colspan="4" align="center"><br /><br />Datos B&aacute;sicos<br /><br /></td>
	</tr>
	<tr>
		<td colspan="2" align="left"><span class="t3">No. Historia C&iacute;nica:</span> &nbsp;&nbsp;<?=$obj->pac_numeroIdentificacion?></td>
		<td colspan="2" align="left"><span class="t3">Fecha de ingreso:</span>&nbsp;&nbsp;<?=$obj->pac_fechaIngreso?></td>
	</tr>
	<tr>
		<td colspan="2" align="left"><span class="t3">Nombres y Apellidos :&nbsp;&nbsp;</span><?="$obj->pac_nombres $obj->pac_apellidos"?></td>
		<td colspan="2"></td>
	</tr>
	<tr>
		<td colspan="2" align="left"><span class="t3">Identificaci&oacute;n :&nbsp;&nbsp;</span><? 
			$query="select * from tipo_documento
			where tid_id = '$obj->tid_id'";							
			$table->search($query); 
			if ($table->nfound>= 1) {
				$data=$table->sql_fetch_object();
				?><?=$data->tid_tipo?>
			<?}?><?=$obj->pac_numeroIdentificacion?></td>
		<td colspan="2" align="left"><span class="t3">Expedida en: &nbsp;&nbsp; </span><? 
			$query="select * from ciudad
			where ciu_id = '$obj->pac_lugarExpedicion'";							
			$table->search($query); 
			if ($table->nfound>= 1) {
				$data=$table->sql_fetch_object();
				?><?=$data->ciu_nombreCiudad?>
			<?}?></td>
	</tr>
	<tr>
		<td colspan="2" align="left"><span class="t3">Tipo de Paciente : &nbsp;&nbsp;</span><? 
			$query="select * from tipo_paciente
			where tip_id = '$obj->tip_id'";							
			$table->search($query); 
			if ($table->nfound>= 1) {
				$data=$table->sql_fetch_object();
				?><?=$data->tip_categoriaPaciente?>
			<?}?></td>
		<td colspan="2" align="left"><span class="t3">&Aacute;rea de Atenci&oacute;n:&nbsp;&nbsp;</span><? 
			$query="select * from area_atencion
			where ara_id = '$obj->ara_id'";							
			$table->search($query); 
			if ($table->nfound>= 1) {
				$data=$table->sql_fetch_object();
				?><?=$data->ara_nombreArea?>
			<?}?></td>
	</tr>
</table>

<br/><br/><br/>

<table width="80%" align="center">
	<tr>
		<td class="t2" colspan="4" align="center">Datos Personales<br/><br/><br/></td>
	</tr>
	<tr>
		<td colspan="4" align="center">
			<table class="tablaallbordes" cellspacing="0" cellpadding="0">
				<tr>
					<td class="t3" width="25%">Fecha Nacimiento:</td>
					<td>&nbsp;&nbsp;<?=$dpp->dpp_fechaNacimiento?></td>
					<td class="t3" width="15%">Lugar:</td>
					<td>&nbsp;&nbsp;<? 
						$query="select * from ciudad
							where ciu_id = '$dpp->dpp_lugarNacimiento'";							
						$table->search($query); 
						if ($table->nfound>= 1) {
							$data=$table->sql_fetch_object();
							?><?=$data->ciu_nombreCiudad?><?}?></td>
				</tr>
				<tr>
					<td class="t3">Edad : </td>
					<td>&nbsp;&nbsp;<?=calcular_edad($dpp->dpp_fechaNacimiento)?> <? 
						$query="select * from unidad_medida_edad
						where ume_id = '$dpp->ume_id'";							
						$table->search($query); 
						if ($table->nfound>= 1) {
							$data=$table->sql_fetch_object();
							?><?=$data->ume_nombreUnidad?>
						<?}?>&nbsp;</td>
					<td class="t3">Sexo : </td>
					<td>&nbsp;&nbsp;<?if($dpp->dpp_sexo=='t'){echo "Masculino";
																				}else echo" Feminino";?>  </td>
				</tr>
				<tr>
					<td class="t3">Dir residencia :</td>
					<td>&nbsp;&nbsp;<?=$obj->pac_direccionResidencia?></td>
					<td class="t3">Municipio :</td>
					<td>&nbsp;&nbsp;<? 
						$query="select * from ciudad
						where ciu_id = '$obj->ciu_id'";							
						$table->search($query); 
						if ($table->nfound>= 1) {
							$data=$table->sql_fetch_object();
							?><?=$data->ciu_nombreCiudad?><?}?></td>
				</tr>
				<tr>
					<td class="t3"  width="10%">Zona Residencial : </td>
					<td>&nbsp;&nbsp;<? 
						$query="select * from zona_residencial
						where zor_id = '$dpp->zor_id'";							
						$table->search($query); 
						if ($table->nfound>= 1) {
							$data=$table->sql_fetch_object();
							?><?=$data->zor_nombreZona?><?}?></td>
					<td class="t3">Estrato : </td>
					<td>&nbsp;&nbsp;<?if($dpp->pdp_estrato==-1){echo "Sin Registrar";
						}elseif($dpp->pdp_estrato==1){ echo"1";
						}elseif($dpp->pdp_estrato==2){ echo"2";
						}elseif($dpp->pdp_estrato==3){ echo"3";
						}elseif($dpp->pdp_estrato==4){ echo"4";
						}elseif($dpp->pdp_estrato==5){ echo"5";
						}elseif($dpp->pdp_estrato==6) echo"6";
						?></td>
				</tr>
				<tr>
					<td class="t3"  width="10%">Tel&eacute;fonos:</td>
					<td>&nbsp;&nbsp;<?=$obj->pac_telefonos?></td>
					<td class="t3">E-mail : </td>
					<td>&nbsp;&nbsp;<?=$dpp->dpp_email?></td>
				</tr>
				<tr>
					<td class="t3" width="10%">Grupo Sanguineo: </td>
					<td>&nbsp;&nbsp;<? 
						$query="select * from grupo_sanguineo
							where grs_id = '$dpp->grs_id'";							
						$table->search($query); 
						if ($table->nfound>= 1) {
							$data=$table->sql_fetch_object();
							?><?=$data->grs_nombregrupo?><?}?></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td class="t3" width="10%">Ocupaci&oacute;n : </td>
					<td>&nbsp;&nbsp;<?=$dpp->dpp_ocupacionInstitucion?></td>
					<td class="t3">Empresa : </td>
					<td>&nbsp;&nbsp;<?=$dpp->dpp_nombreInstitucion?></td>
				</tr>
				<tr>
					<td class="t3" width="10%">Direcci&oacute;n Emp :</td>
					<td>&nbsp;&nbsp;<?=$dpp->dpp_direccionInstitucion?></td>
					<td class="t3">Tel&eacute;fono Emp : </td>
					<td>&nbsp;&nbsp;<?=$dpp->dpp_telefonoInstitucion?></td>
				</tr>
				<tr>
					<td class="t3" width="10%">Estado Civil: </td>
					<td>&nbsp;&nbsp;<? 
						$query="select * from estado_civil
							where est_id = '$dpp->est_id'";							
						$table->search($query); 
						if ($table->nfound>= 1) {
							$data=$table->sql_fetch_object();
							?><?=$data->est_nombreEstado?>
						<?}?></td>
					<td></td>
					<td></td>
				</tr>
				<tr><?if($dpp->est_id!=1){?>
					<td class="t3" width="10%">Nom. Conyugue : </td>
					<td>&nbsp;&nbsp;<?=$dpp->dpp_nombreConyuge?></td>
					<td class="t3">Direcci&oacute;n : </td>
					<td>&nbsp;&nbsp;<?=$dpp->dpp_dirConyuge?></td>
				</tr>
				<tr>
					<td class="t3" width="10%">Ocupaci&oacute;n :</td>
					<td>&nbsp;&nbsp;<?=$dpp->dpp_ocupConyuge?></td>
					<td class="t3">Tel&eacute;fono </td>
					<td>&nbsp;&nbsp;<?=$dpp->dpp_telConyuge?></td>
			<?}?></tr>
			</table>
		</td>
	</tr>
	<tr height="3%">
		<td width="20%" height="3%"></td>
		<td width="25%" height="3%"></td>
		<td width="35%" height="3%"></td>
		<td width="20%" height="3%"></td>
	</tr>
	<tr>
		<td class="t2" colspan="4" width="20%"><br /><br />Referencias<br /><br /> </td>
	</tr>
	<tr>
		<td colspan="4">
			<table width="100%" border="0" cellspacing="1" cellpadding="2" align="center">
				<tr>
					<td class="t3"  width="10%">Persona  allegada :</td>
					<td width="30%">&nbsp;&nbsp;<?=$dpp->dpp_personaMasAllegada?></td>
					<td class="t3" width="15%">Parentesco :</td>
					<td width="40%">&nbsp;&nbsp;<?=$dpp->dpp_parentescoPersonaAllegada?></td>
				</tr>
				<tr>
					<td class="t3" width="10%">Direcci&oacute;n : </td>
					<td width="30%">&nbsp;&nbsp;<?=$dpp->dpp_direccionPersonaAllegada?></td>
					<td class="t3" width="15%">Tel&eacute;fono :</td>
					<td width="40%">&nbsp;&nbsp;<?=$dpp->dpp_telefonoPersonaAllegada?></td>
				</tr>
			</table>
		</td>
	</tr>

	<tr>
		<td class="t2" colspan="4"><br /><br />Datos de los Padres<br /><br /></td>
	</tr>
	<tr>
		<td colspan="4">
			<table width="100%" border="0" cellspacing="1" cellpadding="2" align="center">
				<tr>
					<td class="t3" width="10%">Nom Pap&aacute; : </td>
					<td  width="30%">&nbsp;&nbsp;<?=$dpp->dpp_nombrePadre?></td>
					<td class="t3" width="15%">Ocupaci&oacute;n : </td>
					<td width="40%">&nbsp;&nbsp;<?=$dpp->dpp_ocupacionPadre?></td>
				</tr>
				<tr>
					<td class="t3" width="10%">Empresa :</td>
					<td  width="30%">&nbsp;&nbsp;<?=$dpp->dpp_institucionPadre?></td>
					<td class="t3" width="15%">Direcci&oacute;n : </td>
					<td width="40%">&nbsp;&nbsp;<?=$dpp->dpp_dirPadre?></td>
				</tr>
				<tr>
					<td class="t3" width="10%">Tel&eacute;fonos :</td>
					<td width="30%">&nbsp;&nbsp;<?=$dpp->dpp_telefonoInstitucionPadre?></td>
					<td  width="15%"></td>
					<td width="40%"></td>
				</tr>
				<tr>
					<td class="t3" width="10%">Nom Mam&aacute; : </td>
					<td width="30%">&nbsp;&nbsp;<?=$dpp->dpp_nombreMadre?></td>
					<td class="t3" width="15%">Ocupaci&oacute;n : </td>
					<td width="40%">&nbsp;&nbsp;<?=$dpp->dpp_ocupacionMadre?></td>
				</tr>
				<tr>
					<td class="t3" width="10%">Empresa :</td>
					<td width="30%">&nbsp;&nbsp;<?=$dpp->dpp_institucionMadre?></td>
					<td class="t3" width="15%">Direcci&oacute;n : </td>
					<td width="40%">&nbsp;&nbsp;<?=$dpp->dpp_dirMadre?></td>
				</tr>
				<tr>
					<td class="t3" width="10%">Tel&eacute;fonos :</td>
					<td width="30%">&nbsp;&nbsp;<?=$dpp->dpp_telefonoInstitucionMadre?></td>
					<td width="15%"></td>
					<td width="40%"></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="t2" colspan="4"><br /><br />Sistema de Seguridad Social<br /><br /></td>
	</tr>
	<tr>
		<td colspan="4">
			<table width="100%" border="0" cellspacing="1" cellpadding="2" align="center">
				<tr>
					<td class="t3" width="10%">E.P.S : </td>
					<td  width="30%">&nbsp;&nbsp;<? 
							$query="select * from eps
								where eps_id = '$dpp->eps_id'";							
							$table->search($query); 
							if ($table->nfound>= 1) {
								$data=$table->sql_fetch_object();
								?><?=$data->eps_nombreEps?><?}?></td>
					<td class="t3" width="15%"> I.P.S : </td>
					<td  width="40%">&nbsp;&nbsp;<? 
							$query="select * from ips
								where ips_id = '$dpp->ips_id'";							
							$table->search($query); 
							if ($table->nfound>= 1) {
								$data=$table->sql_fetch_object();
								?><?=$data->ips_nombreIps?><?}?></td>
				</tr>
				<tr>
					<td class="t3" width="10%">Plan de Atenci&oacute;n:</td>
					<td  width="30%">&nbsp;&nbsp;<? 
							$query="select * from plan_atencion
								where pla_id = '$dpp->pla_id'";							
							$table->search($query); 
							if ($table->nfound>= 1) {
								$data=$table->sql_fetch_object();
								?><?=$data->pla_nombrePlan?> <?}?></td>
					<td class="t3" width="15%">M&eacute;dico Tratante:</td>
					<td  width="40%">&nbsp;&nbsp;<?=$dpp->dpp_medicoTratante?></td>
				</tr>
				<tr>
					<td class="t3"  width="10%">Tipo Vinculacion :</td>
					<td  width="30%">&nbsp;&nbsp;<? 
							$query="select * from tipo_vinculacion
								where tvu_id = '$dpp->tvu_id'";							
							$table->search($query); 
							if ($table->nfound>= 1) {
								$data=$table->sql_fetch_object();
								?><?=$data->tvu_nombreVinculacion?><?}?></td>
					<td class="t3" width="15%">Remitido Por :</td>
					<td  width="40%">&nbsp;&nbsp;<?=$dpp->dpp_remitidoPor?></td>
				</tr>
				<tr>
					<td class="t3" width="10%">Regimen Afiliaci&oacute;n:</td>
					<td  width="30%">&nbsp;&nbsp;<? 
							$query="select * from regimen_afiliacion
								where rea_id = '$dpp->rea_id'";							
							$table->search($query); 
							if ($table->nfound>= 1) {
								$data=$table->sql_fetch_object();
								?><?=$data->rea_nombre?><?}?></td>
					<td class="t3" width="15%">Tel&eacute;fono:</td>
					<td  width="40%">&nbsp;&nbsp;<?=$dpp->dpp_telefonoMedicoTratante?></td>
				</tr>
				<tr>
					<td colspan="3"><span class="t3">Cooperativa a la que pertenece :&nbsp;&nbsp;</span><? 
							$query="select * from cooperativa
								where coo_id = '$dpp->coo_id'";							
							$table->search($query); 
							if ($table->nfound>= 1) {
								$data=$table->sql_fetch_object();
								?><?=$data->coo_nombre?><?}?></td>
					<td width="40%"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>


<?/*ANAMNESIS*/?>
<?php
	$query= "SELECT * FROM paciente WHERE pac_id=$pac_id";
	$table->search($query);
	$paciente = $table->sql_fetch_object();
	$pac = new historia($pac_id);
	$obj = $pac->data;
?>
	<table width="100%" border="0" cellspacing="2" cellpadding="0" align="center">
		<tr>
			<td colspan="3" align="center" class="t1"><br><br/><hr><br/>ANAMNESIS<br/><br/></td>
		</tr>
		<tr>
			<td width="10%"></td>
			<td align="left" colspan="2"><span  class="t3">Fecha de Elaboraci&oacute;n Anamnesis:</span><?php echo $obj->hic_fechaAnamnesis;?></td>
		</tr>
		<tr>
			<td  class="t2" colspan="3"><br/><br/>Antecedentes M&eacute;dicos<br/><br/></td>
		</tr>
		<tr>
			<td width="10%"></td>
			<td colspan="2" align="left"><span  class="t3">Motivo de Consulta :(Descripci&oacute;n en t&eacute;rminos del paciente)</span><br/>	
			<?php echo $obj->hic_anamMotivoConsulta;?></td>
		</tr>
		<tr>
			<td width="10%"></td>
			<td colspan="2" align="left"><br><span  class="t3">Historia de la Enfermedad Actual:</span><br><?php echo $obj->hic_anamHistoriaEnfActual; ?></td>
		</tr>
	</table>




<?/*ANTECEDENTES SOCIALES*/?>


<?php
	$query= "SELECT * FROM antecedente WHERE pac_id=$pac_id";
	$table->search($query);
	$ant = $table->sql_fetch_object();
	$pac = new nino( $pac_id);
	$obj= $pac->data;

	
	$query= "SELECT * FROM paciente WHERE pac_id=$pac_id";
	$table->search($query);
	$paciente = $table->sql_fetch_object();
?>
	<table width="100%" border="0" cellspacing="2" cellpadding="0" align="center">
		<tr>
			<td class="t1" colspan="3" align="center"><br/><hr><br/>ANTECEDENTES GENERALES<br><br></td>
		</tr>
		<tr>
			<td class="t2" colspan="3"><br/><br/>Antecedentes Sociales<br><br></td>
		</tr>
		<tr>
			<td width="10%"></td>
			<td align="left" colspan="2"><span  class="t3">Fecha Ingreso Antecedente:</span><?=$ant->ant_fechaIngresoAnt?></td>
		</tr>
		<tr>
			<td width="10%"></td>
			<td align="left" colspan="2"><span  class="t3">Entorno Socio-Econ&oacute;mico y Familiar :</span></td>
		</tr>
		<tr>
			<td width="10%"></td>
			<td align="left" colspan="2"   width="100%"><?=$ant->ant_entornoSocioeconomico?><br /><br /></td>
		</tr>
	</table>

	<table width="100%" border="0" cellspacing="2" cellpadding="0" align="center">

<?/*ANTECEDENTES MEDICOS PERSONALES*/?>		
		<tr>
			<td class="t2" colspan="5" align="center"><br><br>Antecedentes M&eacute;dicos Generales<br><br></td>
		</tr>
		<tr>
			<td width="10%"></td>
			<td colspan="4" align="left"><span class="t3">Fecha Ingreso Antecedente:</span> <?=$ant->ant_fechaIngresoAnt1?><br /><br /></td>
		</tr>
		<tr>
			<td width="10%"></td>
			<td align="left" colspan="4">Seg&uacute;n las edades cronol&oacute;gicas:</td>
		</tr>
		<tr>
			<td width="10%"></td>
			<td class="t3" align="left" colspan="4"><ul><li>Prenatal :</li></ul></td>
		</tr>
		<tr>
			<td width="10%"></td>
			<td colspan="2"><span class="t3">Enfermedades relevantes durante el embarazo:</span><br>
				<table width="100%" border="0" cellspacing="1" cellpadding="2">
					<tr>
						<td>&nbsp;&nbsp;<?=$ant->ant_antMedPrenatalEnferEmbarazo?></td>
					</tr>
				</table>
			</td>
			<td colspan="2"><span class="t3">Medicamentos tomados durante el embarazo:</span><br>
				<table width="100%" border="0" cellspacing="1" cellpadding="2">
					<tr>
						<td>&nbsp;&nbsp;<?=$ant->ant_medicamentosTomadosEmb?></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td width="10%"></td>
			<td class="t3" align="left" colspan="4"><br><ul><li>Natal :</li></ul></td>
		</tr>
		<tr>
			<td width="10%"></td>
			<td align="left" colspan="4"><span  class="t3">Tipo de parto:</span> &nbsp;&nbsp;<?  $query="select * from parto  where par_id = '$ant->par_id'";
				$table->search($query);
				//echo $query;
				if ($table->nfound>= 1) {
					$data=$table->sql_fetch_object();
					?><?=$data->par_tipoParto?> <?}?>
			</td>
		</tr>
		<tr>
			<td width="10%"></td>
			<td align="left" colspan="4"><span class="t3">Emplearon f&oacute;rceps en el parto:</span>&nbsp; <?=($ant->ant_antMedNatalEmplearonForceps =="t")?"Si":"No"?>
			</td>
		</tr>
		<tr>
			<td width="10%"></td>
			<td align="left" colspan="4"><span class="t3">El ni&ntilde;o naci&oacute; cian&oacute;tico:</span>&nbsp;&nbsp; <?=($ant->ant_ninoCianotico =="t")?"Si":"No"?>
			</td>
		</tr>
		<tr>
			<td width="10%"></td>
			<td class="t3" align="left" colspan="4"><br><ul><li>Postnatal :</li></ul></td>
		</tr>
		<tr>
			<td width="10%"></td>
			<td colspan="3" style="border: 1px solid;"><span class="t3">Fecha &uacute;ltimo ex&aacute;men m&eacute;dico:</span> (DD/MM/AAAA)&nbsp;&nbsp;<?=$ant->ant_antMedPostnatalFechaUltEx?></td>
			<td width="10%"></td>
		</tr>

		<tr>
			<td width="10%"></td>
			<td class="t3" width="35%">Motivo de &uacute;ltimo ex&aacute;men m&eacute;dico:</td>
			<td width="10%"></td>
			<td class="t3" width="35%">Qu&eacute; tratamiento m&eacute;dico actual tiene:</td>
			<td width="10%"></td>
		</tr>
		<tr>
			<td width="10%"></td>
			<td width="35%" style="border: 1px solid;"><?=$ant->ant_motivoUltimoExamen?></td>
			<td width="10%"></td>
			<td width="35%" style="border: 1px solid;"><?=$ant->ant_tratamientoMedicoActual?></td>
			<td width="10%"></td>
		</tr>
		
		<tr>
			<td width="10%"></td>
			<td class="t3" colspan="3" width="35%">Medicamentos tomados actualmente :</td>
			<td width="10%"></td>
		</tr>
		<tr>
			<td width="10%"></td>
			<td colspan="4" style="border: 1px solid;"><?=$ant->ant_medicTomActual?></td>
		</tr>
	</table>
<br /><br /><br />
	<table width="80%" border="0" cellspacing="2" cellpadding="0" align="center">

<?/*ENFERMEDADES PROPIAS DE LA NIÑEZ Y ADOLESCENTE*/?>
		<?
			$query= "SELECT * FROM enf_nino WHERE pac_id=$pac_id";
			$table->search($query);
			$obj = $table->sql_fetch_object();
			
			
			$query = "SELECT * FROM ninoxcie p, cie d WHERE enf_id=$obj->enf_id AND p.cie_id = d.cie_id";
			$table->sql_query($query);
		?>	
		<tr>
			<td class="t2" colspan="3" align="center">Enfermedades Propias de la Ni&ntilde;ez y la Adolescencia<br><br></td>
		</tr>	
		<tr>
			<td colspan="3" align="left">
				Listado de Enfermedades Diagnosticadas Algunas vez<br>
				al Paciente Durante su Ni&ntilde;ez<br><br></td>
		</tr>
		<tr>
			<td><span class="t3">Fecha Ingreso Antecedente:</span> <?=$obj->enf_fechaElaboracion?></td>
		</tr>
		<tr>
			<td>
				<table class="listado">
					<tr class="cabecera">
						<td>C&oacute;digo internacional<br />de la enfermedad</td>
						<td>Nombre de la enfermedad</td>
					</tr>
					<? 
					$num=0;
					while ($cie = $table->sql_fetch_object()){ $num = ($num) ? 0 : 1;?>
					<tr <?=($num)?' class="par"':' class="impar"'?>>
						<td><?=$cie->cie_codigo?></td>
						<td>&nbsp;<?=$cie->cie_descripcion?></td>
					</tr>
					<?} ?>
				</table>
			</td>
		</tr>
		<tr>
			<td align="left"><span  class="t3">COMENTARIOS :</span>&nbsp;&nbsp;<?=$obj->enf_comentario?></td>
		</tr>
	</table>
<br /><br /><br />
	<table width="80%" border="0" cellspacing="2" cellpadding="0" align="center">

<?/*ENFERMEDADES PROPIAS DEL ADULTO Y ADULTO MAYOR*/?>
		<?
			$query= "SELECT * FROM enf_adulto WHERE pac_id=$pac_id";
			$table->search($query);
			$obj = $table->sql_fetch_object();

			$query = "SELECT * FROM adultoxcie p, cie d WHERE adu_id=$obj->adu_id AND p.cie_id = d.cie_id";
			$table->sql_query($query);
		?>	
		<tr>
			<td class="t2" colspan="3" align="center">Enfermedades Propias Del Adulto y Del Adulto Mayor<br><br></td>
		</tr>	
		<tr>
			<td colspan="3" align="left">
				Listado de Enfermedades Diagnosticadas Algunas vez<br>
				al Paciente Durante su Adultez<br><br></td>
		</tr>
		<tr>
			<td><span class="t3">Fecha de Elaboraci&oacute;n: <?=$obj->adu_fechaElaboracion?></span></td>
		</tr>
		<tr>
			<td>
				<table class="listado">
					<tr class="cabecera">
						<td>C&oacute;digo internacional<br>
								de la enfermedad</td>
						<td>Nombre de la enfermedad</td>
					</tr>
					<?
					$num=0;
					while ($cie = $table->sql_fetch_object()){ ?>
					<tr <?=($num)?' class="par"':' class="impar"'?>>
						<td><?=$cie->cie_codigo?></td>
						<td>&nbsp;<?=$cie->cie_descripcion?></td>
					</tr>
					<?} ?>
				</table>
			</td>
		</tr>
		<tr>
			<td align="left"><span class="t3">COMENTARIOS :</span>&nbsp;&nbsp;<?=$obj->adu_comentario?></td>
		</tr>
	</table>
<br /><br /><br />
	<table width="80%" border="0" cellspacing="2" cellpadding="0" align="center">

<?/*ANTECEDENTES MEDICOS FAMILIARES*/?>
		<?
			$query= "SELECT * FROM enf_familia WHERE pac_id=$pac_id";
			$table->search($query);
			$obj = $table->sql_fetch_object();

			$query = "SELECT * FROM familiaxcie p, cie d WHERE fam_id=$obj->fam_id AND p.cie_id = d.cie_id";
			$table->sql_query($query);
		?>	
		<tr>
			<td class="t2" colspan="3" align="center">Antecedentes M&eacute;dicos Familiares<br><br></td>
		</tr>
		<tr>
			<td colspan="3" align="left">
				Listado de Enfermedades Diagnosticadas a un Familiar<br>
				del Paciente<br><br></td>
		</tr>
		<tr>
			<td><span class="t3">Fecha de Elaboraci&oacute;n:</span> <?=$obj->fam_fechaElaboracion?></td>
		</tr>
		
		<tr>
			<td>
				<table class="listado">
					<tr class="cabecera">
						<td>C&oacute;digo internacional<br></td>
						<td>Nombre de la enfermedad</td>
					</tr>
					<? while ($cie = $table->sql_fetch_object()){ $num = ($num) ? 0 : 1;?>
					<tr <?=($num)?' class="par"':' class="impar"'?>>
						<td><?=$cie->cie_codigo?></td>
						<td>&nbsp;<?=$cie->cie_descripcion?></td>
					</tr>
					<?} ?>
				</table>
			</td>
		</tr>
		<tr>
			<td align="left"><span class="t3">COMENTARIOS :&nbsp;&nbsp;</span><?=$obj->fam_comentario?></td>
		</tr>
	</table><br /><br />
	<table width="80%" border="0" cellspacing="2" cellpadding="0" align="center">

<?/*ANTECEDENTES ESTOMATOLOGICOS U ODONTOLOGICOS*/?>
		<?
		$pac = new adulto( $pac_id);
		$obj=$pac->data;
		?>	
		<tr>
			<td class="t2" colspan="3" align="center">Antecedentes Estomatol&oacute;gicos u Odontol&oacute;gicos<br><br></td>
		</tr>
		<tr>
			<td><span class="t3">Fecha de Elaboraci&oacute;n:</span> <?=$ant->ant_fechaIngresoAnt4?></td>
		</tr>
		<tr>
			<td colspan="3">
				<table width="70%" border="0" cellspacing="1" cellpadding="2" align="center">
					<tr>
						<td class="t3"  width="15%">&nbsp;&nbsp;Prevenci&oacute;n :</td>
						<td  width="25%">&nbsp;&nbsp;<?=($ant->ant_antOdonPrevencion=="t")?"SI":"NO"?></td>
						<td class="t3" width="20%">&nbsp;&nbsp;Pr&oacute;tesis :</td>
						<td  width="10%">&nbsp;&nbsp;<?=($antj->ant_protesis=="t")?"SI":"NO"?></td>
					</tr>
					<tr>
						<td class="t3" width="15%">&nbsp;&nbsp;Operatoria :</td>
						<td  width="25%">&nbsp;&nbsp;<?=($ant->ant_operatoria=="t")?"SI":"NO"?></td>
						<td class="t3"  width="20%">&nbsp;&nbsp;Ortodoncia :</td>
						<td  width="10%">&nbsp;&nbsp;<?=($ant->ant_ortodoncia=="t")?"SI":"NO"?></td>
					</tr>
					<tr>
						<td class="t3" width="15%">&nbsp;&nbsp;Periodoncia :</td>
						<td  width="25%">&nbsp;&nbsp;<?=($ant->ant_periodoncia=="t")?"SI":"NO"?></td>
						<td class="t3" width="20%">&nbsp;&nbsp;Ortopedia :</td>
						<td  width="10%">&nbsp;&nbsp;<?=($ant->ant_ortopedia=="t")?"SI":"NO"?></td>
					</tr>
					<tr>
						<td class="t3"  width="15%">&nbsp;&nbsp;Endodoncia :</td>
						<td  width="25%">&nbsp;&nbsp;<?=($ant->ant_endodoncia=="t")?"SI":"NO"?></td>
						<td class="t3"  width="20%">&nbsp;&nbsp;Cirug&iacute;a Oral :</td>
						<td  width="10%">&nbsp;&nbsp;<?=($ant->ant_cirugiaOral=="t")?"SI":"NO"?></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="3"><br><span class="t3">Observaciones  :</span> <br>
				<table width="100%" border="0" cellspacing="1" cellpadding="2">
					<tr>
						<td>&nbsp;&nbsp;<?=$ant->ant_comentariosAntOdo?></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<table width="100%" border="0" cellspacing="1" cellpadding="2">
					<tr>
						<td colspan="3">
							<br><span class="t3">Fecha &uacute;ltima visita al odont&oacute;logo:</span> (DD/MM/AA)&nbsp; <?=$ant->ant_antOdonFechaVisitaOdont?>
							<br>
							</td>
					</tr>
							<tr>
						<td colspan="3" valign="top" width="35%"><span class="t3">Motivo de &uacute;ltima visita:</span><br>
							<table width="100%" border="0" cellspacing="1" cellpadding="2">
								<tr>
									<td>&nbsp;&nbsp;<?=$ant->ant_motivo?></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td coolspan="3" valign="top" width="35%"><span class="t3"><br />Ha presentado alguna reacci&oacute;n desfavorable  hacia el tratamiento odontol&oacute;gico?:</span> <?=($ant->ant_reaccionDesfavorableTratam=="t")?"SI":"NO"?><br>
									<br><span class="t3">
									Observaciones:</span><br>
									<table width="100%" border="0" cellspacing="1" cellpadding="2">
										<tr>
											<td>&nbsp;&nbsp;<?=$ant->ant_observacionesOdontologicas?></td>
										</tr>
									</table>
						</td>
					</tr>
					<tr height="8%">
						<td width="35%" height="8%"></td>
						<td width="10%" height="8%"></td>
						<td valign="top" width="35%" height="8%"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table><br /><br />
	<table width="80%" border="0" cellspacing="2" cellpadding="0" align="center">
<?/*ANTECEDENTES TOXICOS - ALERGICOS - QUIRURGICOS - HEMORRAGIAS*/?>	
		<tr>
			<td class="t2" colspan="3" align="center">Antecedentes T&oacute;xicos - Al&eacute;rgicos - Quir&uacute;rgicos - Hemorragias<br><br></td>
		</tr>
		<tr>
			<td><span class="t3">Fecha de Elaboraci&oacute;n:</span> <?=$ant->ant_fechaIngresoAnt5?></td>
		</tr>
		<tr>
			<td colspan="3">Ha tenido alguna vez reacciones al&eacute;rgicas a :</td>
		</tr>
		<tr>
			<td colspan="3"><br>
				<table width="70%" border="0" cellspacing="1" cellpadding="2">
				<tr>
					<td class="t3"  width="20%">&nbsp;&nbsp;Anest&eacute;sico Local :</td>
					<td  width="13%">&nbsp;&nbsp;<?=($ant->ant_sufreAlergiaAnestesicolocal=="t")?"SI":"NO"?></td>
				</tr>
				<tr>
					<td class="t3" width="20%">&nbsp;&nbsp;Penicilina :</td>
					<td  width="13%">&nbsp;&nbsp;<?=($ant->ant_alergiaPenicilina=="t")?"SI":"NO"?></td>
				</tr>
				<tr>
					<td class="t3" width="20%">&nbsp;&nbsp;Otros medicamentos o comidas :</td>
					<td  width="13%">&nbsp;&nbsp;<?=($ant->ant_alergiaDrogasComidas=="t")?"SI":"NO"?></td>
				</tr>
				</table>
			</td>
		</tr>
		<tr>
				<td colspan="3"><br><span class="t3">
					Observaciones :</span><br>
					<table width="80%" border="0" cellspacing="1" cellpadding="2">
						<tr>
							<td>&nbsp;&nbsp;<?=$ant->ant_comentarioAntAler?></td>
						</tr>
					</table>
				</td>
		</tr>
		<tr>
			<td class="t2" colspan="3" align="center"><br>
				<br>
				H&aacute;bitos</td>
		</tr>
		<tr>
			<td colspan="2"><br>Presenta alguna de las siguientes situaciones:<br>
				<br>
			</td>
			<td width="45%"></td>
		</tr>
		<tr>
			<td align="right" width="45%">
				<table width="90%" border="0" cellspacing="1" cellpadding="2">
					<tr>
						<td class="t3" width="60%">Rechina los dientes :</td>
						<td align="center"  width="30%"><?=($ant->ant_rechinaDientes=="t")?"SI":"NO"?></td>
					</tr>
					<tr>
						<td class="t3" width="60%">Se chupa los dedos :</td>
						<td align="center"  width="30%"><?=($ant->ant_chupaDedo=="t")?"SI":"NO"?></td>
					</tr>
					<tr>
						<td class="t3" width="60%">Se come las u&ntilde;as</td>
						<td align="center"  width="30%"><?=($ant->ant_comeUnas=="t")?"SI":"NO"?></td>
					</tr>
					<tr>
						<td class="t3" width="60%">Respira por la boca :  </td>
						<td align="center"  width="30%"><?=($ant->ant_respiraBoca=="t")?"SI":"NO"?></td>
					</tr>
					<tr>
						<td class="t3" width="60%">Apieta los Dientes :</td>
						<td align="center"  width="30%"><?=($ant->ant_aprietaDiente=="t")?"SI":"NO"?></td>
					</tr>
				</table>
			</td>
			<td width="10%"></td>
			<td valign="top" width="45%"><span  class="t3">Otros h&aacute;bitos orales :</span><br>
				<table width="100%" border="0" cellspacing="1" cellpadding="2">
					<tr>
						<td>&nbsp;&nbsp;<?=$ant->ant_otrosHabitos?></td>
					</tr>
				</table>
			</td>
		</tr>

	</table><br /><br />


<?/*REVISION POR SISTEMA*/?>

<?

$query= "SELECT * FROM paciente WHERE pac_id=$pac_id";
$table->search($query);
$paciente = $table->sql_fetch_object();
	

$pac = new historia($pac_id);
$obj = $pac->data;
?>
<hr>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td class="t1" colspan="3" align="center"><br>REVISION POR SISTEMA<br>
			<br>
		</td>
	</tr>
	<tr>
		<td class="t3" width="45%"><ul><li>Nervioso :</li></ul></td>
		<td width="10%"></td>
		<td class="t3" width="45%"><ul><li>Genito Urinario :</li></ul></td>
	</tr>
	<tr>
		<td rowspan="7">
			<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
				<tr>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Ansiedad :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_rxsNerviosoAnsiedad=="t")?"SI":"NO"?></td>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Depresi&oacute;n :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_depresion=="t")?"SI":"NO"?></td>
				</tr>
				<tr>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Irritabilidad :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_irritabilidad=="t")?"SI":"NO"?></td>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Lipotimias :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_lipotimias=="t")?"SI":"NO"?></td>
				</tr>
				<tr>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Sincopes :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_sincopes=="t")?"SI":"NO"?></td>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Epilepsia :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_epilepsias=="t")?"SI":"NO"?></td>
				</tr>
				<tr>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Convulsiones :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_convulsiones=="t")?"SI":"NO"?></td>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Neuralgias:</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_neuralgia=="t")?"SI":"NO"?></td>
				</tr>
				<tr>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Paresias :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_paresias=="t")?"SI":"NO"?></td>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Par&aacute;lisis:</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_paralisis=="t")?"SI":"NO"?></td>
				</tr>
				<tr>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Cefaleas :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_cefaleas=="t")?"SI":"NO"?></td>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Migra&ntilde;a :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_migrana=="t")?"SI":"NO"?></td>
				</tr>
				<tr>
					<td class="t3" colspan="3" bgcolor="white">&nbsp;&nbsp;Accidente cerebro vascular :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_accidenteCerebroVascular=="t")?"SI":"NO"?></td>
				</tr>
				<tr>
					<td class="t3" colspan="3" bgcolor="white">Otro accidente Nervioso:</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_nerviosoOtro=="t")?"SI":"NO"?></td>
				</tr>
			</table>
		</td>
		<td width="10%"></td>
		<td rowspan="4">
			<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
				<tr>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Infecciones Vaginales :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_rxsGenitoUrinarioInfeccion=="t")?"SI":"NO"?></td>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Embarazo :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_embarazo=="t")?"SI":"NO"?></td>
				</tr>
				<tr>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Insuficiencia Renal :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_insuficienciaRenal=="t")?"SI":"NO"?></td>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Abortos :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_abortos=="t")?"SI":"NO"?></td>
				</tr>
				<tr>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Cistitis :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_cistitis=="t")?"SI":"NO"?></td>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Planificacion :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_planificacion=="t")?"SI":"NO"?></td>
				</tr>
				<tr>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Dismenorrea :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_dismenorrea=="t")?"SI":"NO"?></td>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;ETS :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_ets=="t")?"SI":"NO"?></td>
				</tr>
				<tr>
					<td class="t3" colspan="3" bgcolor="white">&nbsp;&nbsp;Otras : </td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_genitoOtro=="t")?"SI":"NO"?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="10%"></td>
	</tr>
	<tr>
		<td width="10%"></td>
	</tr>
	<tr>
		<td width="10%"></td>
	</tr>
	<tr>
		<td width="10%"></td>
		<td class="t3" width="45%">&nbsp;&nbsp;Observaciones :</td>
	</tr>
	<tr>
		<td width="10%"></td>
		<td width="45%">
			<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
				<tr>
					<td bgcolor="white">&nbsp;&nbsp;<?=$obj->hic_obsGenito?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="10%"></td>
		<td class="t3" width="45%"><ul><li>Endocrino :</li></ul></td>
	</tr>
	<tr>
		<td class="t3" width="45%">&nbsp;&nbsp;Observaciones :</td>
		<td width="10%"></td>
		<td rowspan="3">
			<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
				<tr>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Enanismo :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_rxsEndocrinoEnanismo=="t")?"SI":"NO"?></td>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Cretinismo :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_cretinismo=="t")?"SI":"NO"?></td>
				</tr>
				<tr>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Gigantismo :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_gigantismo=="t")?"SI":"NO"?></td>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Deabetes :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_diabetes=="t")?"SI":"NO"?></td>
				</tr>
				<tr>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Acromegalia :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_acromegalia=="t")?"SI":"NO"?></td>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Otras : </td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_endocrinoOtro=="t")?"SI":"NO"?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td rowspan="2">&nbsp;&nbsp;<?=$obj->hic_obsNervioso?><br /><br /><br />
		</td>
		<td width="10%"></td>
	</tr>
	<tr>
		<td width="10%"></td>
	</tr>
	<tr>
		<td width="45%"><span class="t3"><ul><li>Respiratorio :</li></ul></span></td>
		<td width="10%"></td>
		<td class="t3" width="45%">&nbsp;&nbsp;Observaciones :</td>
	</tr>
	<tr>
		<td rowspan="4">
			<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
				<tr>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Tos :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_rxsRespiratorioTos=="t")?"SI":"NO"?></td>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Hemoptisis :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_hemoptisis=="t")?"SI":"NO"?></td>
				</tr>
				<tr>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Disnea :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_disnea=="t")?"SI":"NO"?></td>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Asma :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_asma=="t")?"SI":"NO"?></td>
				</tr>
				<tr>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Rin&iacute;tis:</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_rinitis=="t")?"SI":"NO"?></td>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;TBC :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_tbc=="t")?"SI":"NO"?></td>
				</tr>
				<tr>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Pulmonares :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_pulmonares=="t")?"SI":"NO"?></td>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Micosis :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_micosis=="t")?"SI":"NO"?></td>
				</tr>
				<tr>
					<td class="t3" colspan="3" bgcolor="white">&nbsp;&nbsp;Otras : </td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_resOtro=="t")?"SI":"NO"?></td>
				</tr>
			</table>
		</td>
		<td width="10%"></td>
		<td width="45%">
			<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
				<tr>
					<td bgcolor="white">&nbsp;&nbsp;<?=$obj->hic_obsEndocrino?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="10%"></td>
		<td class="t3" width="45%"><ul><li>Osteo Articular :</li></ul></td>
	</tr>
	<tr>
		<td width="10%"></td>
		<td rowspan="3">
			<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
				<tr>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Artritis :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_rxsOsteoarticularArtritis=="t")?"SI":"NO"?></td>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Artrosis :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_artrosis=="t")?"SI":"NO"?></td>
				</tr>
				<tr>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Espasmos :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_espasmos=="t")?"SI":"NO"?></td>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Dolores Articulares :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_doloresArticulares=="t")?"SI":"NO"?></td>
				</tr>
				<tr>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Tics :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_tics=="t")?"SI":"NO"?></td>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Otras : </td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_osteoOtro=="t")?"SI":"NO"?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="10%"></td>
	</tr>
	<tr>
		<td class="t3" width="45%">&nbsp;&nbsp;Observaciones :</td>
		<td width="10%"></td>
	</tr>
	<tr>
		<td rowspan="2">
			<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
				<tr>
					<td bgcolor="white">&nbsp;&nbsp;<?=$obj->hic_obsRespiratorio?></td>
				</tr>
			</table>
		</td>
		<td width="10%"></td>
		<td class="t3" width="45%">&nbsp;&nbsp;Observaciones :</td>
	</tr>
	<tr>
		<td width="10%"></td>
		<td rowspan="2">
			<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
				<tr>
					<td bgcolor="white">&nbsp;&nbsp;<?=$obj->hic_obsOsteoArt?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="t3" width="45%"><br /><br /><ul><li>&nbsp;&nbsp;Cardiovascular :</li></ul></td>
		<td width="10%"></td>
	</tr>
	<tr>
		<td rowspan="4">
			<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
				<tr>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Arritmias :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_rxsCardiovascularArritmias=="t")?"SI":"NO"?></td>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Cianosis :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_cianosis=="t")?"SI":"NO"?></td>
				</tr>
				<tr>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Anasarca :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_anasarca=="t")?"SI":"NO"?></td>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;HTA :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_hta=="t")?"SI":"NO"?></td>
				</tr>
				<tr>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Cardiopat&iacute;as Congenitas :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_cardiopatiasCongenitas=="t")?"SI":"NO"?></td>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Varices :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_varices=="t")?"SI":"NO"?></td>
				</tr>
				<tr>
					<td class="t3" colspan="3" bgcolor="white">&nbsp;&nbsp;Otras :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_cardioOtro=="t")?"SI":"NO"?></td>
				</tr>
			</table>
		</td>
		<td width="10%"></td>
		<td class="t3" width="45%"><ul><li>Piel :</li></ul></td>
	</tr>
	<tr>
		<td width="10%"></td>
		<td rowspan="2">
			<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
				<tr>
					<td class="t3" bgcolor="white" width="50%">Erupciones Cut&aacute;neas :</td>
					<td bgcolor="white" width="10%">&nbsp;&nbsp;<?=($obj->hic_rxsPielErupcionesCut=="t")?"SI":"NO"?></td>
					<td class="t3" bgcolor="white" width="30%">Psoriasis :</td>
					<td bgcolor="white" width="10%">&nbsp;&nbsp;<?=($obj->hic_psoriasis=="t")?"SI":"NO"?></td>
				</tr>
				<tr>
					<td class="t3" bgcolor="white">Micosis :</td>
					<td bgcolor="white">&nbsp;&nbsp;<?=($obj->hic_micosis1=="t")?"SI":"NO"?></td>
					<td class="t3" bgcolor="white">Otras : </td>
					<td bgcolor="white">&nbsp;&nbsp;<?=($obj->hic_pielOtro=="t")?"SI":"NO"?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="10%"></td>
	</tr>
	<tr>
		<td width="10%"></td>
		<td class="t3" width="45%">&nbsp;&nbsp;Observaciones :</td>
	</tr>
	<tr>
		<td class="t3" width="45%">&nbsp;&nbsp;Observaciones :</td>
		<td width="10%"></td>
		<td rowspan="2">
			<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
				<tr>
					<td bgcolor="white">&nbsp;&nbsp;<?=$obj->hic_obsPiel?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td rowspan="2">
			<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
				<tr>
					<td bgcolor="white">&nbsp;&nbsp;<?=$obj->hic_obsCardiovascular?></td>
				</tr>
			</table>
		</td>
		<td width="10%"></td>
	</tr>
	<tr>
		<td width="10%"></td>
		<td class="t3" width="45%"><br><br><ul><li>Inmunol&oacute;gico</li></ul></td>
	</tr>
	<tr>
		<td class="t3" width="45%"><ul><li>Gastroinstestinales :</li></ul></td>
		<td width="10%"></td>
		<td rowspan="5">
			<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
				<tr>
					<td class="t3" colspan="3" bgcolor="white">&nbsp;&nbsp;Enf. Autoinmunol&oacute;gicas (Deabetes Juvenil, Artritis Reumatoidea, Anemia, etc):</td>
					<td bgcolor="white" width="10%">&nbsp;&nbsp;<?=($obj->hic_rxsInmAut=="t")?"SI":"NO"?></td>
				</tr>
				<tr>
					<td class="t3" colspan="3" bgcolor="white">&nbsp;&nbsp;Enf. Complejas Inmunol&oacute;gicas (Hepatitis Viral, Malaria, etc.) :</td>
					<td bgcolor="white" width="10%">&nbsp;&nbsp;<?=($obj->hic_rxsInmCom=="t")?"SI":"NO"?></td>
				</tr>
				<tr>
					<td class="t3" colspan="3" bgcolor="white">&nbsp;&nbsp;Enf. de Inmunodeficiencia (Sida, etc.)</td>
					<td bgcolor="white" width="10%">&nbsp;&nbsp;<?=($obj->hic_rxsInmDef=="t")?"SI":"NO"?></td>
				</tr>
				<tr>
					<td class="t3" colspan="3" bgcolor="white">&nbsp;&nbsp;Alergias :</td>
					<td bgcolor="white" width="10%">&nbsp;&nbsp;<?=($obj->hic_rxsInmAle=="t")?"SI":"NO"?></td>
				</tr>
				<tr>
					<td class="t3" colspan="3" bgcolor="white">&nbsp;&nbsp;Otras : </td>
					<td bgcolor="white" width="10%">&nbsp;&nbsp;<?=($obj->hic_inmuOtro=="t")?"SI":"NO"?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td rowspan="3">
			<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
				<tr>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Diarrea :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_rxsGastrointestinalDiarrea=="t")?"SI":"NO"?></td>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Parasitos :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_parasitos=="t")?"SI":"NO"?></td>
				</tr>
				<tr>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Gastritis :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_gastritis=="t")?"SI":"NO"?></td>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Ulceras :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_ulceras=="t")?"SI":"NO"?></td>
				</tr>
				<tr>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Hepatitis :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_hepatitis=="t")?"SI":"NO"?></td>
					<td class="t3" bgcolor="white" width="30%">&nbsp;&nbsp;Ictericia :</td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_ictericia=="t")?"SI":"NO"?></td>
				</tr>
				<tr>
					<td class="t3" colspan="3" bgcolor="white">&nbsp;&nbsp;Otras : </td>
					<td bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_gastroOtro=="t")?"SI":"NO"?></td>
				</tr>
			</table>
		</td>
		<td width="10%"></td>
		<td width="10%"></td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td class="t3" width="45%">
			<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
				<tr>
					<td bgcolor="white">&nbsp;&nbsp;Observaciones :</td>
				</tr>
			</table></td>
		<td width="10%"></td>
	</tr>
	<tr>
		<td rowspan="2">
			<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
				<tr>
					<td bgcolor="white">&nbsp;&nbsp;<?=$obj->hic_obsGastrointestinal?></td>
				</tr>
			</table>
		</td>
		<td width="10%"></td>
		<td class="t3" width="45%">&nbsp;&nbsp;Observaciones :</td>
	</tr>
	<tr>
		<td width="10%"></td>
		<td rowspan="2">
			<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
				<tr>
					<td bgcolor="white">&nbsp;&nbsp;<?=$obj->hic_rxsObsInm?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="45%"></td>
		<td width="10%"></td>
	</tr>
</table>


<?/*Examen Físico General*/?>
<hr>
<table width="80%" border="0" cellspacing="2" cellpadding="0" align="center">
	<tr>
		<td class="t1" colspan="3" align="center"><br>EXAMEN FISICO GENERAL<br>
			<br>
		</td>
	</tr>
	<tr>
		<td colspan="3" align="left"><span class="t3">Fecha de Ingreso :</span> <?=$obj->hic_fechaElaboracionEfg?>
		</td>
	</tr>
	<tr>
		<td class="t3" colspan="2" align="left"><ul><li>Caracteristicas F&iacute;sicas:</li></ul></td>
		<td width="10%"></td>
	</tr>
	<tr>
		<td width="10%"></td>
		<td class="t3" width="25%" align="right">Contextura :</td>
		<td width="35%"><?if($obj->hic_contextura==1){echo "Grande";
			}elseif($obj->hic_contextura==2){echo "Mediana";
			}elseif($obj->hic_contextura==3){echo "Peque&ntilde;a";}?>
	</tr>
	<tr>
		<td width="10%"></td>
		<td class="t3" align="right">Picos de Crecimiento (&uacute;ltimamente)  :</td>
		<td width="35%"><?if($obj->hic_picosCrecimiento==1){ echo "Estable";
			}elseif($obj->hic_picosCrecimiento==2){ echo "Acelerado";
			}elseif($obj->hic_picosCrecimiento==3){ echo "Decreciente";}?></td>
	</tr>
	<tr>
		<td width="10%"></td>
		<td class="t3" width="25%" align="right">Apariencia Nutricional :</td>
		<td width="35%"> <?=($obj->hic_aparienciaNutricionalNormal =="t")?"Normal":"Anormal"?>  </td>
	</tr>
	<tr>
		<td width="10%"></td>
		<td class="t3" width="25%" align="right">Orientaci&oacute;n en tiempo y espacio :</td>
		<td width="35%"><?=($obj->hic_caracteristicasFisicasOrien =="t")?"Normal":"Anormal"?> </td>
	</tr>
</table>
<br />
<br />

<table width="80%" border="0" cellspacing="2" cellpadding="0" align="center">
	<tr>
		<td class="t3" colspan="3" align="left"><ul><li>Signos Vitales:</li></ul><br><br></td>
		<td width="15%"></td>
		<td width="20%"></td>
	</tr>
	<tr>
		<td width="10%"></td>
		<td class="t3" width="7%">Peso :</td>
		<td width="35%"><?=$obj->hic_exFiSignosVitalesPeso?> kg</td>
		<td class="t3" width="15%">Tension Arterial :</td>
		<td width="20%"><?=$obj->hic_tensionArterial?> mm/hg</td>
	</tr>
	<tr>
		<td width="10%"></td>
		<td class="t3" width="7%">Talla :</td>
		<td width="35%"><?=$obj->hic_talla?>Cms</td>
		<td class="t3" width="15%">Frecuencia Respiratoria :</td>
		<td width="20%"><?=$obj->hic_frecuenciaRespiratoria?> p/min</td>
	</tr>
	<tr>
		<td width="10%"></td>
		<td class="t3" width="7%">Pulso :</td>
		<td width="35%"><?=$obj->hic_pulso?> p/min</td>
		<td class="t3" width="15%">Temperatura :</td>
		<td width="20%"><?=$obj->hic_temperatura?> &ordm;C</td>
	</tr>

	</table><br /><br />
	<table width="80%" border="0" cellspacing="2" cellpadding="0" align="center">
	<tr>
		<td class="t3"><ul><li>Observaciones :</li></ul></td>
		</tr>
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="1" cellpadding="2">
				<tr>
					<td>&nbsp;&nbsp;<?=$obj->hic_exaFisObs?></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br />
<br />


<?/*EXAMEN ESTOMATOLOGICO*/?>
<table width="80%" border="0" cellspacing="2" cellpadding="0" align="center">

	<tr>
		<td colspan="3" align="center"><hr><br><span class="t1">EXAMEN ESTOMATOLOGICO EXTRA ORAL<br></span><span class="t2">(vista ant, lat, Regi&oacute;n cervicofacial, parotidea, simetria)</span>
			<br>
		</td>
	</tr>
	<tr>
		<td colspan="3" align="left"><br /><br /><span class="t3">Fecha de Ingreso :</span> <?=$obj->hic_fechaElaboracionEfg?>
		</td>
	</tr>

</table>
<br />
<table width="80%" border="0" cellspacing="2" cellpadding="0" align="center">
	<tr>
		<td align="left"><span class="t3">Tipo de Perfil :</span>&nbsp;&nbsp;<?if($obj->hic_estomExtraOralTipoPerfil==1){
														echo "C&oacute;ncavo";
														}
														elseif($obj->hic_estomExtraOralTipoPerfil==2){
														echo "Convexo";
														}
														elseif($obj->hic_estomExtraOralTipoPerfil==3){
														echo "Recto";}?></td>
	</tr>
	<tr>
		<td class="t3" align="left"><br><ul><li> ATM Palpaci&oacute;n Lat. Post. Boca Abierta. Boca Cerrada :</li></ul></td>
	</tr>
	<tr>
		<td>&nbsp;&nbsp;<?=$obj->hic_atm?></td>
	</tr>
	<tr>
		<td align="left"><span class="t3"><ul><li>Regi&oacute;n Submandibular : Ganglios - Glandulas Salivares :</li></ul></span>
		</td>
	</tr>
	<tr>
		<td>&nbsp;&nbsp;<?=$obj->hic_regionSubmandibular?></td>
	</tr>
	<tr>
		<td align="left"><ul><li><span class="t3">Cuello: (M&uacute;sculos - Cadenas Ganglionares):</span></li></ul>
		</td>
	</tr>
	<tr>
		<td>&nbsp;&nbsp;<?=$obj->hic_cuello?></td>
	</tr>
	<tr>
		<td class="t3" align="left"><ul><li>Regi&oacute;n Labial:</li></ul></td>
	</tr>
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="1" cellpadding="2">
				<tr>
					<td valign="top" width="5%"></td>
					<td class="t3" valign="top" width="20%">Labio Superior :</td>
					<td colspan="4">&nbsp;&nbsp;<?=$obj->hic_labioSup?></td>
				</tr>
				<tr height="3%">
					<td width="5%" height="3%"></td>
					<td width="20%" height="3%"></td>
					<td width="20%" height="3%"></td>
					<td width="20%" height="3%"></td>
					<td width="20%" height="3%"></td>
					<td width="15%" height="3%"></td>
				</tr>
				<tr>
					<td valign="top" width="5%"></td>
					<td class="t3" valign="top" width="20%">Labio Inferior :</td>
					<td colspan="4">
						<table width="100%" border="0" cellspacing="1" cellpadding="2">
							<tr>
								<td  width="15%">&nbsp;&nbsp;<?=$obj->hic_labioInf?></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr height="3%">
					<td width="5%" height="3%"></td>
					<td width="20%" height="3%"></td>
					<td width="20%" height="3%"></td>
					<td width="20%" height="3%"></td>
					<td width="20%" height="3%"></td>
					<td width="15%" height="3%"></td>
				</tr>
			</table>
			<p></p>
		</td>
	</tr>
	<tr>
		<td class="t3" align="left"><ul><li>Comentarios Regi&oacute;n Labial : (Comisura, Tonicidad, Selle labial, Otros).</li></ul></td>
	</tr>
	<tr>
		<td>&nbsp;&nbsp;<?=$obj->hic_comentariosExtraOral?></td>
	</tr>
</table>
<br />
<br />

<?/*EXAMEN ESTOMATOLOGICO*/?>

<hr>

<table width="80%" border="0" cellspacing="2" cellpadding="0" align="center">

	<tr>
		<td class="t1" colspan="3" align="center"><br>EXAMEN ESTOMATOLOGICO INTRA ORAL<br><br>
		</td>
	</tr>
	<tr>
		<td olspan="3" align="left"><span class="t3">Fecha de Ingreso :</span> <?=$obj->hic_fechaElaboracionEfg?></td>
	</tr>
	<tr>
		<td>
			<ul>
				<li><span class="t3">Lengua :</span>
					<ul>
						<li>Tama&ntilde;o :<?=$obj->hic_lenguaTam?>
						</li>
						<li>Movilidad:<?=$obj->hic_lenguaMov?>
						</li>
						<li>Forma:<?=$obj->hic_lenguaFor?>
						</li>
					</ul>
				</li>
			</ul>
			
			
			<ul>
				<li>
					<span class="t3">PALADAR</span>
					<ul>
						<li><span class="t3">Paladar Duro :</span>
							<ul>
								<li>Profundidad: &nbsp;&nbsp;<?=$obj->hic_paladarPro?></li>
								<li>Color mucosa palatina :&nbsp;&nbsp;<?=$obj->hic_paladarColor?></li>
								<li>Ancho: <?=$obj->hic_paladarAncho?></li>
								<li>Torus :&nbsp;&nbsp;<?=$obj->hic_paladarTorus?></li>
								<li>Rugas: &nbsp;&nbsp;<?=$obj->hic_paladarRugas?></li>
							</ul>
						</li>
						<li><span class="t3">Paladar Blando :</span>
							<ul>
								<li>Clasificaci&oacute;n :<?=$obj->hic_paladarClasif?></li>
								<li>Submucosa :<?=$obj->hic_paladarSubmucosa?></li>
								<li>&Uacute;vula :<?=$obj->hic_paladarUvula?></li>
							</ul>
						</li>
					</ul>
					<ul>
						<li><span class="t3">Comentarios Paladar:</span>
							<ul>
								<li>
									<?=$obj->hic_paladarComentario?>
								</li>
							</ul>
						</li>
					</ul>
				</li>
			</ul>
		</td>
		<td width="10%">
		</td>
		<td>
			<ul><li><span class="t3">Mucosa Vestibular(Mucosas, surco, frenillos, otros):</span><?=$obj->hic_estomIntraOralVestibulo?></li></ul>
			
			
			<ul><li><span class="t3">Piso de Boca(Frenillos- Carunculas Salivares, Glandulas, Otros):</span><?=$obj->hic_pisoBoca?></li></ul>
			
			
			<ul><li><span class="t3">Funci&oacute;n Glandular(Calidad - Consistencia salivar, Otros):</span><?=$obj->hic_funcionGlandular?></li></ul>
			
			
			<ul><li><span class="t3">Funci&oacute;n Glandular(Calidad - Consistencia salivar, Otros):</span><?=$obj->hic_rebordesAlveolares?></li></ul>
			
			<ul><li><span class="t3">Dientes(N&uacute;mero, tipo arcada, tipo de oclusi&oacute;n, mal posici&oacute;n, plano oclusal, dimensi&oacute;n  oclusal,  relaci&oacute;n l&iacute;nea media dental y anat&oacute;mica, otros):</span><?=$obj->hic_diente?></li></ul>
		</td>
	</tr>
</table>

<br />
<br />
<?/*PERIODONTOGRAMAS*/?>
<hr>
<table width="80%" border="0" cellspacing="2" cellpadding="0" align="center">
<tr>
	<td class="t1">PERIODONTOGRAMAS<br/></td>
</tr>
<?
	$per = new periodontograma();
	$result = $per->buscar();
	$data = array();
      	while($object = $table->sql_fetch_object())
		array_push($data,$object);
?>



	<? foreach($data as $obj){
	?>
	<tr>
		<td><?
			$query= "select  MAX(oeh_id) FROM odontologos_encargados_historia WHERE pac_id='$paciente->pac_id'";
			$table->search($query); 
			$maxo=$table->sql_fetch_object();
			//echo $query;
				$query="select * from usuario u,  odontologos_encargados_historia  d where u.usu_id=d.usu_id and d.pac_id = '$paciente->pac_id' and oeh_id='$maxo->max' order by oeh_id ASC";

				$table->search($query); 
				//echo $query;
				if ($table->nfound>= 1) {
					$data=$table->sql_fetch_object();
					?><?=$data->usu_nombres." ".$data->usu_apellidos?> <?}?></td>
		
	</tr>
	<tr>
		<td>
			<?
		$per_id=$obj->per_id;
		$query= "SELECT * FROM periodontograma WHERE per_id=$per_id";
		$table->search($query);
		$perioid= $table->sql_fetch_object();
		if ($perioid->per_reevaluacion!=0){
			$opc="periodontograma";
			$s_opc="visualizar";
			include("./modules/$opc/$s_opc.php");
		}
			?>
		</td>
	</tr>
	<? } ?>
</table>






<?/*DIAGNOSTICOS*/?>


<?

	$pac = new paciente($pac_id);
	$obj_pac = $pac->data;

	$dia = new diagnostico();
	$result = $dia->buscar();	
	$numdiag=0;
	$data = array();
	$opc="diagnostico";
	$s_opc="visualizar";
	while($object = $table->sql_fetch_object())
		array_push($data,$object);
?>
<hr>
<table width="80%" border="0" cellspacing="2" cellpadding="0" align="center">
<tr>
	<td class="t1">DIAGNOSTICOS<br/></td>
</tr>
	<? 
	$num=0;
	foreach($data as $obj){
	$numdiag++;
		$num = ($num) ? 0 : 1;
		
	?>
	<?
	$dia_id=$obj->dia_id;
	$query= "SELECT * FROM diagnostico WHERE dia_id=$dia_id";
	$table->search($query);
	$diag = $table->sql_fetch_object();
	?>
		<tr>
			<td class="t2" align="left">Diagn&oacute;stico No. <?=$numdiag?><br /><br /></td>
		</tr>
		<tr>
			<td align="left"><span class="t3">Fecha de elaboraci&oacute;n :</span>
				<?=$diag->dia_fechaElaboracion?>
			</td>
		</tr>
		<tr>
			<td align="left"><span class="t3">Odont&oacute;logo :</span>
				<? 
				$query="select * from usuario u,odontologos_encargados_historia d where u.usu_id=d.usu_id and d.pac_id = '$obj_pac->pac_id' order by oeh_id ASC";	
				$table->search($query); 
				if ($table->nfound>= 1) {
					$data1=$table->sql_fetch_object();
					echo($data1->usu_nombres." ".$data1->usu_apellidos);
				}?>
			</td>
		</tr>
		<tr align="left">
			<td><span class="t3">Docente:</span>
			<? 													
			$query="select * from usuario u,docentes_encargados_historia d where u.usu_id=d.usu_id and d.pac_id='$obj_pac->pac_id' order by deh_id ASC";
			$table->search($query); 
			if ($table->nfound>= 1) {
				$data1=$table->sql_fetch_object();
				echo($data1->usu_nombres." ".$data1->usu_apellidos);
			}?></td>
		</tr>
		<tr>
			<td>
				<table class="listado">
					<tr class="cabecera">
						<td>C&oacute;digo del Diagn&oacute;stico</td>
						<td>Descripci&oacute;n</td>
						<td>Diente afectado</td>
						<td>Superficie afectada</td>
					</tr>
					<? 
					$query = "SELECT * FROM diaxcod m, codigos_diagnostico t WHERE dia_id=$dia_id AND m.cod_id = t.cod_id";
					$table->sql_query($query);
					$num=0;
					while( $codigos_diag = $table->sql_fetch_object() ){ 
						$num = ($num) ? 0 : 1; ?>
					<tr <?=($num)?' class="par"':' class="impar"'?>>
						<td><?=$codigos_diag->cod_codigoDiagnostico ?></td>
						<td><?=$codigos_diag->cod_descripcionDiagnostico ?></td>
						<td><?=$codigos_diag->dxc_diente?></td>
						<td><?=$codigos_diag->dxc_superficie?></td>
					</tr>
					<?}  // end while sql_fetch_object ?>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<?if($diag->tdi_id>1){?>
				<table width="100%" border="0" cellspacing="1" cellpadding="2">
					<tr>
						<td class="t3" width="20%">Plan de tratamiento :</td>
						<td>&nbsp;&nbsp;<?=$diag->dia_planTratamiento?></td>
					</tr>
					<tr>
						<td class="t3" width="20%">Pron&oacute;stico General :</td>
						<td>&nbsp;&nbsp;<?=$diag->dia_pronosticoGeneral?></td>
					</tr>
					<tr>
						<td class="t3" width="20%">Comentarios Examinador : </td>
						<td>&nbsp;&nbsp;<?=$diag->dia_comentarioExaminador?></td>
					</tr>
				</table><?}else{?>
				<table width="80%" border="0" cellspacing="1" cellpadding="2">
					<tr>
						<td class="t3" width="20%">Ayudas Diagn&oacute;sticas :</td>
						<td>&nbsp;&nbsp;<?=$diag->dia_ayudasDiagnosticas?></td>
					</tr>
					<tr>
						<td class="t3" width="20%">Observacion General :</td>
						<td>&nbsp;&nbsp;<?=$diag->dia_observacionGeneral?></td>
					</tr>
				</table><?}?>
				<br /><br />
			</td>
		</tr>
	<? } ?>
</table>

<?/*EVOLUCION*/?>
<?
	//Listar
	$evolucion = new evolucion();
	$result = $evolucion->buscar();
	$data = array();
	$numevo=0;
       	while($object = $table->sql_fetch_object()){
		array_push($data,$object);
	}
	
?>
<span class="t1"><br/><br/><hr><br/><br/>EVOLUCIONES<br/>
</span>

	<?php foreach($data as $midata){
	$numevo++;
	?>
<br><br><span class="t2">Evoluci&oacute;n No.<?=$numevo?></span><br>
<table width="80%" border="0" align="center">
	<tr>
		<td colspan="2" align="left"><br><br>
		<?
			$evo_id=$midata->evo_id;

			$query= "SELECT * FROM evolucion WHERE evo_id=$evo_id";
			$table->search($query);
			$obj = $table->sql_fetch_object();
		?>
		</td>
	</tr>
	<tr>
		<td width="30%"></td>
		<td align="left"><span class="t3">Fecha de Elaboraci&oacute;n de evoluci&oacute;n:</span><?php echo $obj->evo_fechaElaboracion?></td>
	</tr>
	<tr>
		<td></td>
		<?$query="SELECT * FROM usuario WHERE usu_id=$obj->usu_id";
		$table->search($query);
		$usu_evo = $table->sql_fetch_object();?>
		<td align="left"><span class="t3">Usuario en Evoluci&oacute;n:</span><?php echo ($usu_evo->usu_nombres.$usu_evo->usu_apellidos.". (Id.:".$usu_evo->usu_numeroIdentificacion)?>)</td>
	</tr>
	<tr>
		<td></td>
		<td align="left"><span class="t3">Docente :</span>
		<?php 
			$query= "select  MAX(deh_id) FROM docentes_encargados_historia WHERE pac_id='$paciente->pac_id'";
			$table->search($query); 
			$maxd=$table->sql_fetch_object();
			$query="select * from usuario u,  docentes_encargados_historia  d   
			where u.usu_id=d.usu_id and d.pac_id = '$paciente->pac_id' and deh_id='$maxd->max' order by deh_id ASC";							
			$table->search($query); 
			if ($table->nfound>= 1) {
				$data1=$table->sql_fetch_object();
				echo $data1->usu_nombres." ".$data1->usu_apellidos;
			}?>
		</td>
	</tr>
	<tr>
		<td class="t3" colspan="2">Datos de la Consulta:<br /></td>
	</tr>
	<tr>
		<td colspan="2">
			<table  width="80%" border="1" cellspacing="1" cellpadding="2"  align="center">
				<tr>
					<td class="t3" width="28%">Tipo Consulta:</td>
					<td>&nbsp;&nbsp;<?php 
						$query="select * from tipo_consulta where tco_id = '$obj->tco_id'";
						$table->search($query); 
						if ($table->nfound>= 1) {
							$data1=$table->sql_fetch_object();
							echo $data1->tco_nombreConsulta;
						}?>
					</td>
				</tr>
				<tr>
					<td class="t3">Causa Consulta :</td>
					<td>&nbsp;&nbsp;<?php  
						$query="select * from causa_consulta where cac_id = '$obj->cac_id'";							
						$table->search($query); 
						if ($table->nfound>= 1) {
							$data1=$table->sql_fetch_object();
							echo $data1->cac_nombreCausa;
						}?>
					</td>
				</tr>
				<tr>
					<td class="t3">Ambito del procedimiento  :</td>
					<td>&nbsp;&nbsp;<?php 
						$query="select * from ambito_procedimiento where amp_id = '$obj->amp_id'";							
						$table->search($query); 
						if ($table->nfound>= 1) {
							$data1=$table->sql_fetch_object();
							echo $data1->amp_nombreAmbito;
						}?></td>
				</tr>
				<tr>
					<td class="t3">Finalidad del procedimiento:</td>
					<td>&nbsp;&nbsp;<?php  
						$query="select * from finalidad_procedimiento where fip_id = '$obj->fip_id'";							
						$table->search($query); 
						if ($table->nfound>= 1) {
							$data1=$table->sql_fetch_object();
							echo $data1->fip_nombreFinalidad;
						}?></td>
				</tr>
				<tr>
					<td class="t3">Forma del Acto Quir&uacute;rgico  : </td>
					<td>&nbsp;&nbsp;<?php  
						$query="select * from forma_acto_quirurgico where faq_id = '$obj->faq_id'";							
						$table->search($query); 
						if ($table->nfound>= 1) {
							$data1=$table->sql_fetch_object();
							echo $data1->faq_nombreForma;
						}?></td>
				</tr>
				<tr>
					<td class="t3">Orden de servicio No.  : </td>
					<td>&nbsp;&nbsp;<?php echo $obj->evo_numeroAutorizacion;?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="10%"></td>
		<td class="t3" align="left">Comentarios :&nbsp;</td>
	</tr>
	<tr>
		<td width="10%"></td>
		<td><?php echo $obj->evo_comentario?></td>
	</tr>
	<?
 		$query = "SELECT * FROM procxevo p, procedimiento d WHERE evo_id=$evo_id AND p.pto_id = d.pto_id";
 		$table->sql_query($query);
	?>
	<tr>
		<td class="t3" colspan="2" align="left"><br><br>Listado de Procedimientos Realizados:<br /><br /></td>
	</tr>
	<tr>
		<td colspan="2">
			<table  class="listado">
				<tr class="cabecera">
					<td>C&oacute;digo del Procedimiento</td>
					<td>Nombre</td>
					<td>No. Procedimientos</td>
					<td>Diente</td>
					<td>Superficie</td>
				</tr>
				<?php 
					$num=0;
					while ($procedimiento = $table->sql_fetch_object()){
						$num = ($num) ? 0 : 1;?>
				<tr <?= ($num) ? ' class="par"' :  'class="impar"' ?>>
					<td><?php echo $procedimiento->pto_codigoProcedimiento?></td>
					<td>&nbsp;<?php echo $procedimiento->pto_nombreProcedimiento?></td>
					<td>&nbsp;<?php echo $procedimiento->pxe_cantidadProcedimientos?></td>
					<td>&nbsp;<?php echo $procedimiento->pxe_diente?></td>
					<td>&nbsp;<?php if($procedimiento->pxe_incisal=="t"){echo "Incisal";}?>&nbsp; 
						<?php if($procedimiento->pxe_mesial=="t"){echo "Mesial";}?>&nbsp;
						<?php if($procedimiento->pxe_distal=="t"){echo "Distal";} ?>&nbsp;
						<?php if($procedimiento->pxe_lingual=="t"){echo "Lingual";} ?>&nbsp;
						<?php if($procedimiento->pxe_oclusal=="t"){echo "Oclusal";} ?>&nbsp;
						<?php if($procedimiento->pxe_vestibular=="t"){echo "Vestibular";} ?>&nbsp;
						<?php if($procedimiento->pxe_palatino=="t"){echo "Palatino";} ?></td>
				</tr>
				<?php }  ?>
			</table>
		</td>
	</tr>
</table>
	<?php } ?>
</body>
</html>




