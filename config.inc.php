<?php
/**********************************************************
Archivo de configuracion de administrador de la aplicacion
**********************************************************/
// Paths
$images_path = "./images";
$modules_path = "./modules";
if (!isset ($include_path)) // xmlrpcser
   $include_path = "./includes";

// Variables de la base de datos (se deben actulizar en la declaracion de la BD >> abajo
$dbhost = "localhost";
$dbname = "Odontologia";
$dbuser = "hbonilla";
$dbpass = "0d0nt0l0g1aS144P";
//$dbpass = "0d0nt0l0g1aS144P";

///********************* Variables del sitio ***********************/

//  Includes
require ("$include_path/FormValidator.class.php");
require ("$include_path/base_class.inc.php");
require ("$include_path/pg_sql_class.inc.php");
require ("$include_path/pg_dbsearcher_class.inc.php");
require ("$include_path/sql_class.inc.php"); //Calendario
require ("$include_path/dbsearcher_class.inc.php"); // Calendario
require ("$include_path/funciones.inc.php");
require ("$include_path/session_pgsql.inc.php");

require ("$include_path/paciente.class.php");
require ("$include_path/adulto.class.php");
require ("$include_path/nino.class.php");
require ("$include_path/pagos.class.php");
require ("$include_path/usuario.class.php");
require ("$include_path/urgencias.class.php");
require ("$include_path/control_materiales.class.php");
require ("$include_path/presupuesto.class.php");
require ("$include_path/historia.class.php");
require ("$include_path/procedimiento.class.php");
require ("$include_path/materiales.class.php");
require ("$include_path/docente.class.php");
require ("$include_path/pedido.class.php");
require ("$include_path/evolucion.class.php");
require ("$include_path/periodontograma.class.php");
require ("$include_path/odontograma.class.php");
require ("$include_path/abastecimiento.class.php");
require ("$include_path/diagnostico.class.php");
require ("$include_path/horarios.class.php");
require ("$include_path/codDiagnostico.class.php");
require ("$include_path/rips.class.php");
require ("$include_path/apertura.class.php");
require ("$include_path/antecedente.class.php");
require ("$include_path/cita.class.php");
require ("$include_path/logs.class.php");
require ("$include_path/estado_cuenta.class.php");
require ("$include_path/enf_nino.class.php");
require ("$include_path/enf_adulto.class.php");
require ("$include_path/enf_familia.class.php");
require ("$include_path/laboratorio.class.php");
require ("$include_path/radiologia.class.php");
require ("$include_path/comunitaria.class.php");
require ("$include_path/auxiliares.class.php");
require ("$include_path/empleado.class.php");
require ("$include_path/cita_odont.class.php");
require ("$include_path/pagos1.class.php");
require ("$include_path/saldo.class.php");
require ("$include_path/vencimiento.class.php");
require ("$include_path/bodega.class.php");

// Configuracion DB ( aplicacion )
        Class my_DB extends pg_db_searcher {
                        var $sqlhost = "localhost";
                        var $sqluser = "hbonilla";
                        //var $sqlpass = "0d0nt0l0g1aS144P";
			var $sqlpass = "0d0nt0l0g1aS144P";
                        var $sqldb = "Odontologia";
                        var $halt_on_error=FALSE;
                        var $max_pages=20;
                        var $max=20;

                        function my_DB(){
                                $this->pg_db_searcher("my_DB");
                        }
        }
// Configuracion DB ( calendario )
        Class my_DB2 extends db_searcher {
                        var $sqlhost = "localhost";
                       	var $sqluser = "webcalendar";
                        var $sqlpass = "c4l3ndario";
                        var $sqldb = "webcalendar";
                        var $halt_on_error=FALSE;
                        var $max_pages=20;
                        var $max=20;

                        function my_DB(){
                                $this->db_searcher("my_DB");
                        }
        }
//Constantes de permisos
define ("ROOT", 1);
define ("ODONTOLOGO", 2);
define ("AUXILIAR", 4);
define ("ALMACENISTA", 8);
define ("SECRETARIA", 16);
?>
