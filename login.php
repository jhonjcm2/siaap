<?php
include("config.inc.php");
$table = new my_db;
standar_header();

if ($envio) {
    $miusuario = new usuario();
    /*
    echo "<pre>";
    print_r($table);
    echo "</pre>";
    */
    
    if($miusuario->validar($login,$password)){
       session_start();
       session_register("SESSION");
       $SESSION["ip"] = $REMOTE_ADDR;
       $SESSION["user"] = $miusuario;
      // print_r ($GLOBALS);
       header("Location: index.php");
     }else
       $mess = "<p><b>El usuario <span class=\"red\">NO</span> se encuentra en la base de datos</b></p>";
}
 
?>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title>Ingreso al SIAAP</title>
	<script language="JavaScript1.2"><!--
		function verificar_ingreso(aform) {
			// validar que no esten vacios los campos
		
			if (aform.login.value == "") {
				alert("Por favor escriba su Login");
				aform.login.focus();
				return false;
			}
			if (aform.password.value == "") {
				alert("Por favor escriba su Password");
				aform.password.focus();
				return false;
			}
		}
		// -->
	</script>
	<link href="estiloLogon.css" rel="stylesheet" type="text/css">
	</head>	
<body bgcolor="white">
	<div class="login">
		<div class="logo">
			<img src="images/Escuela-2_1.jpg" alt="logo Odontologia UV">
		</div>
	
		<div class="formulario">
		<table>
			<tr><td align="center">
				<div class="mensaje">
					<?
							if($mess){
								//echo 'Login o contrase&ntilde;a incorrecta:',$mess,'<br/>';
								echo 'Login o contrase&ntilde;a incorrecta.<br/>';
							}else{
								//echo '&nbsp;<br/>';
							}
					?>
					Ingrese su login y contrase&ntilde;a<br/>
				</div><br/></td></tr>
			<tr><td></td></tr>
			<tr><td>
				<div class="campos">
					<form name="form" action="<?=$PHP_SELF?>" method="post" onSubmit="return verificar_ingreso(form)">
						<table>
							<tr><td><span class="etiquetaInput">Login</span></td><td><input type="text" 
							name="login"    value="" size="25"/></td></tr>
							<tr><td><span class="etiquetaInput">Pass </span></td><td><input type="password" 
							name="password" value="" size="25"/></td></tr>
						</table>
						<div align="right" style="float:right">
							<input name="envio" type="submit" value="Entrar">
						</div>
					</form>
				</div></td></tr>
		</table>
		<span class="univalle">
			<hr color="#FF0000">
			<center>
			Universidad del Valle - Sede San Fernando<br/>
			Escuela de Odontolog&iacute;a<br/>
			Cali - Colombia<br/>
			</center>
		</span>
	</div>
</div>

	</body>
</html>
