<?
/*********************************************************************/
// CLASE: control de materiales
// Proposito: Realizar el control de materiales en las clinicas.
// Ultima modificacion: 22 de mayo de 2003
// /******************************************************************/
class Bodega{
	//var $cantidadSacada;
	//var $fechaSacada;
	var $canfelo;

	/** :) Modifica las unidades despachadas por nuevaUD*/
	function tomaUDesp($bod_id,$mat_id,$nuevaUD){
		$tablex = new my_DB();
		$query="UPDATE matxbod SET \"mxb_unidadesDespachadas\"='$nuevaUD' WHERE bod_id=$bod_id AND mat_id=$mat_id";
		$result=$tablex->sql_query($query);
	}

	function puedePasarse($mat_id, $cantidad, $bod_id_in, $bod_id_out){
		
		$result=$this->puedeSacarMaterial($bod_id_out,$mat_id,$cantidad);//Sacamos material de la bodega
		if(strcasecmp($result["mess_cod"],"info")==0){
			return Array("mess"=>'Si se puede', "mess_cod" => 'info');
		}else{
			return $result;
		}
	}
	/** Pasa material de una bodega a otra*/
	function pasar($mat_id, $cantidad, $bod_id_in, $bod_id_out){
		$this->actualizarCantidadEnBodega($bod_id_in,$mat_id);
		$this->actualizarCantidadEnBodega($bod_id_out,$mat_id);
		
		$result=$this->sacarMaterial($bod_id_out,$mat_id,$cantidad);//Sacamos material de la bodega
		$canfelox=$this->canfelo;
		if(strcasecmp($result["mess_cod"],"info")==0){
			foreach($canfelox as $n){
				$this->agregarMaterial($bod_id_in,$mat_id,$n[0],$n[1],$n[2]);
				//echo $n;
				//echo "<br>mat:".$mat_id." cant:".$n[0]." fecha:".$n[1]." lote:".$n[2]."<br>";
			}
			return Array("mess"=>'Paso exitoso de bodega', "mess_cod" => 'info');
		}else{
			return $result;
		}
	}


	/** Despacha un control de materiales Saca dosis de material de una bodega segun lo que diga el control de materiales*/
	function despacharCM($com_id, $bod_id){
		$tablex = new my_DB();
		$tablex2 = new my_DB();
		$query = "SELECT * FROM matxcom WHERE com_id=$com_id";
		$tablex->sql_query($query);
		/*$bod_id=$user->datos; 
		$bod_id=$this->data->bod_id;*/
		$mess_cod="info";
		//Recorremos todos los materiales de ese control de materiales
		while($obj=$tablex->sql_fetch_object()){
			$mat_id=$obj->mat_id;//Guardamos el material
			
			/***** Valido que el material exista en el almacen ****/
			//Buscamos el material en la bodega
			$query = "SELECT * FROM materiales m,  matxbod b WHERE b.mat_id=$mat_id AND m.mat_id=$mat_id AND b.bod_id=$bod_id";
			$tablex2->sql_query($query);
			$row2 = $tablex2->sql_fetch_object();
	
			/*******************************************************/
			//MODIFICA LA CANTIDAD DE MATERIAL SIN TENER EN CUENTA EL VENCIMIENTO
			$dosisXu=$row2->mat_unidades;//UNIDADES 
			$cantMat=$row2->mxb_cantidadMaterial;//CANTIDAD DE MATERIAL 
			$despachadas=$row2->mxb_unidadesDespachadas;//UNIDADES DESPACHADAS
			$pedidas=$obj->mxc_cantidadMateriales;//UNIDADES PEDIDAS
	
			$totalDosis=$dosisXu*$cantMat-$despachadas;//TOTAL UNIDADES EXISTENTES
			$resta=$totalDosis-$pedidas;//RESTA ENTRE TOTAL Y LAS Q PIDEN
			//Si la resta es mayor o igual a cero se puede suplir la dosis
			if($resta>=0){
				$nuevaCantMat=ceil($resta/$dosisXu);
				$nuevaDespa=($nuevaCantMat-$resta/$dosisXu)*$dosisXu;
				//Actulizamos la bodega
				if($nuevaCantMat>=0){
					$res=$this->sacarMaterial($bod_id,$mat_id,$cantMat-$nuevaCantMat);
					$this->tomaUDesp($bod_id,$mat_id,$nuevaDespa);
					$mess.=$res["mess"];
					if(strcasecmp($res["mess_cod"],"alert")==0)
						$mess_cod=$res["mess_cod"];
				}
			}else{
				$query="SELECT * FROM materiales WHERE mat_id=$mat_id";
				$tablex2->sql_query($query);
				$row = $tablex2->sql_fetch_object();
				$mess .= "<br>No hay suficientes existencias del material ".$row->mat_descripcion." <br>";
				$mess_cod="alert";
			}
			if(strcasecmp($res["mess_cod"],"alert")!=0){
				$query="UPDATE matxcom SET mxc_despachado='t' WHERE mxc_id=$obj->mxc_id";
				$tablex2->sql_query($query);
			}
			//*******************************************************/
		}
		return Array("mess"=>$mess, "mess_cod" => $mess_cod);
	}
	/** Mete a la bodega un material teniendo en cuenta la fecha de vencimiento y el lote.
	Afecta a matxbod y vencimiento_material*/
	function agregarMaterial($bod_id, $mat_id, $vmat_cantidad, $vmat_fecha='00-00-00' ,$lote='_'){
		$tablex = new my_DB();
		$lote=strtoupper($lote);
		//Consultamos si el material por bodega existe
		$query = "SELECT * FROM matxbod WHERE mat_id=$mat_id AND bod_id=$bod_id";
		$tablex->sql_query($query);
		$existe=false;
		//Si el material existe en bodega y tienen fecha de vencimiento
		if ($tablex->nfound > 0){
			$row = $tablex->sql_fetch_object();
			$cantMat=$row->mxb_cantidadMaterial + $vmat_cantidad;
			$query="UPDATE matxbod SET \"mxb_cantidadMaterial\"=$cantMat, \"mxb_saldoInicial\"= $cantMat, \"mxb_fechaAbastecimiento\"='". date('Y-m-d', time())."' WHERE  mat_id=$mat_id AND bod_id=$bod_id";
			//echo "<br>Update: ".$query;
			$result = $tablex->sql_query($query);
			$existe=true;
		}else{
			$query="INSERT INTO matxbod (\"mxb_cantidadMaterial\", \"mxb_saldoInicial\", \"mxb_fechaAbastecimiento\", \"mxb_fechaActual\", mat_id, bod_id) VALUES ($vmat_cantidad, $vmat_cantidad, '". date('Y-m-d', time())."', '" . date('Y-m-d', time())."', $mat_id, $bod_id)";
			//echo "<br>Insert: ".$query;
			$result = $tablex->sql_query($query);
			/*********************/
			$query = "SELECT * FROM matxbod WHERE mat_id=$mat_id AND bod_id=$bod_id";
			$tablex->sql_query($query);
			$row = $tablex->sql_fetch_object();
			/*********************/
		}
		//Consultamos si en esa bodega existe el material con esa fecha y ese lote
		$query="SELECT * FROM vencimiento_material WHERE vmat_fecha='$vmat_fecha' AND vmat_lote='$lote' AND mxb_id=$row->mxb_id";
		$tablex->search($query);
			//echo "<br>Select VM: ".$query;
		
		$registro=0;
		$nuevaCantidad=0;
		//Si no existe se crea
		if(($tablex->nfound)<=0){
			//Creamos una nueva fecha
			$query="INSERT INTO vencimiento_material (\"vmat_fecha\", \"vmat_cantidad\", \"mxb_id\", \"vmat_lote\") 
			VALUES ('$vmat_fecha',0,$row->mxb_id,'$lote')";
			$tablex->sql_query($query);
			//Buscamos ese registro
			$query="SELECT * FROM vencimiento_material WHERE vmat_fecha='$vmat_fecha' AND vmat_lote='$lote' AND mxb_id=$row->mxb_id";
			$tablex->search($query);
			//$registro = $tablex->sql_fetch_object();
		}
		$registro = $tablex->sql_fetch_object();
		//se actualiza
		//Actualizamos la cantidad de materiales con esa fecha de vencimiento
		$nuevaCantidad=$registro->vmat_cantidad+$vmat_cantidad;//Calcula la cantidad
		$query="UPDATE vencimiento_material SET \"vmat_cantidad\"=$nuevaCantidad WHERE vmat_id=$registro->vmat_id";
		$tablex->search($query);
		//if(!$existe)
		$this->actualizarCantidadEnBodega($bod_id,$mat_id);
		//Como se que existe el maerial por bodega ahora debo agregar es por fecha
		//Consultamos si en esa bodega existe el material con esa fecha y ese lote
		return Array('mess'=>'Se agrego el material a la bodega '.$bod_id,'mess_cod'=>'info');
	}
	/** :) Saca de la bodega el material mas viejo cierta cantidad si tiene fechas. Si no tiene fechas saca la cantidad directamente a la bodega. Afecta matxbod y vencimiento_material*/
	function sacarMaterial($bod_id,$mat_id,$cantidad){
		//unset($this->cantidadSacada);
		//unset($this->fechaSacada);
		unset($this->canfelo);
		
		$tablex = new my_DB();
		$tablex2 = new my_DB();
		//Actualizamos la cantidad segun los que tienen fecha
		$cantMat=$this->actualizarCantidadEnBodega($bod_id,$mat_id);
		//Las posibilidades son: 
		//1.verificar que la cantidad exista para poder sacarla
		//Si hay con fecha y la cantidad que hay es mayor que lo que se pide
		if($cantMat && $cantMat>=$cantidad){
			//2.Seleccionamos todos los materiales de la bodega y los ordenamos por fecha (old a new)
			$query = "SELECT * FROM matxbod mxb,vencimiento_material vm WHERE NOT vm.vmat_cantidad=0 AND mxb.mat_id=$mat_id AND mxb.bod_id=$bod_id AND vm.mxb_id=mxb.mxb_id ORDER BY vm.vmat_fecha ASC,vm.vmat_id ASC";
			$tablex->search($query);
			//echo "<b>El query es:$query<br>Registros encontrados:".$tablex->nfound."<b><BR>";
			//Si se encontraron registros
			if($tablex->nfound>0)
				while($cantidad>0){
					$dato = $tablex->sql_fetch_object();
					if(!$dato){
						//echo "No hay dato";
						break;
					}
					//echo "<br>Condicion:$cantidad<$dato->vmat_cantidad";
					//Si la cantidad es menor que lo que hay se puede suplir
					if($cantidad < $dato->vmat_cantidad){
						//$this->cantidadSacada[]=$cantidad;
						//$this->fechaSacada[]=$dato->vmat_fecha;
						$this->canfelo[]=array($cantidad,$dato->vmat_fecha,$dato->vmat_lote);
						//Guardamos cuanto va a quedar en bodega
						$nuevaCantidad=$dato->vmat_cantidad-$cantidad;
						$query="UPDATE vencimiento_material SET \"vmat_cantidad\"='$nuevaCantidad' WHERE vmat_id=$dato->vmat_id";
						$tablex2->sql_query($query);
						//echo "<b><br>primer q:$query<br></b>";
						$cantidad=0;
						//echo "_";
						break;
					}else{
						//$this->cantidadSacada[]=$dato->vmat_cantidad;
						//$this->fechaSacada[]=$dato->vmat_fecha;
						$this->canfelo[]=array($dato->vmat_cantidad,$dato->vmat_fecha,$dato->vmat_lote);
						//Como la cantidad no se puede suplir se disminuye la cantidad y se deja en cero la de la bodega
						$cantidad=$cantidad-$dato->vmat_cantidad;
						//eLIMINAMOS EL REGISTRO DE ESE MATERIAL
						$query="DELETE FROM vencimiento_material WHERE vmat_id=$dato->vmat_id";
						//echo "<b><br>segundo  q:$query<br></b>";
						$tablex2->sql_query($query);
						//echo "*";
					}
				}
			$this->actualizarCantidadEnBodega($bod_id,$mat_id);
			$result= array("mess"=>"Salieron las unidades de material exitosamente(tienen vencimiento) <br>", "mess_cod"=>"info");
		}else{
			//Si NO hay materiales con fecha de vencimiento
			if(!$cantMat){
				$query="SELECT * FROM matxbod WHERE bod_id=$bod_id AND mat_id=$mat_id";
				//echo "<br>NO hay materiales con fecha de vencimiento<br>";
				//echo "$query<br>";
				$tablex->sql_query($query);
				//Si existe el material por bodega
				if($row = $tablex->sql_fetch_object()){
					$cantMat=$row->mxb_cantidadMaterial - $cantidad;
					//echo "El material existe, hay:$row->mxb_cantidadMaterial qudar&aacute;n $cantMat<br>";
					if($cantMat>=0){
						$query="UPDATE matxbod SET \"mxb_cantidadMaterial\"=$cantMat, \"mxb_saldoInicial\"= $cantMat, \"mxb_fechaAbastecimiento\"='". date('Y-m-d', time())."' WHERE  mat_id=$mat_id AND bod_id=$bod_id";
						$result = $tablex->sql_query($query);
						//echo "La cantidad se puede suplir<br>$query<br>";
					}else{
						//echo "La cantidad no se puede suplir<br>";
						$result= array("mess"=>"No existe el material por bodega <br>", "mess_cod"=>"alert");
					}
				}else{
					//echo "El material en la bodega no existe <br>";
					$result = array ("mess"=>"El material en la bodega no existe <br>", "mess_cod"=>"alert");
				}
			}
			//Si hay materiales con fecha pero no alcanzan
			else{
				"La cantidad de material no alcanza, hay $cantMat y se requieren $cantidad<br>";
				$result = array ("mess"=>"No hay suficiente material <br>", "mess_cod"=>"alert");
			}
		}
		return $result;
	}

	
	/** :) Suma los materiales que tengan fecha y con esto actualiza la cantidad total q hay en bodega y retorna la suma,
	si no hay alguno con fecha no hace nada y retorna null */
	function actualizarCantidadEnBodega2($mxb_id){
		$tablex = new my_DB();
		$query = "SELECT SUM(vmat_cantidad) FROM vencimiento_material WHERE mxb_id=$mxb_id";
		//echo "El query es:".$query."<br>";
		$tablex->search($query);
		$suma = $tablex->sql_fetch_object()->sum;
		//echo "La suma es:".$suma."<br>";
		if($suma){
			$query="UPDATE matxbod SET \"mxb_cantidadMaterial\"=$suma, \"mxb_saldoInicial\"=$suma, \"mxb_fechaAbastecimiento\"='" .date('Y-m-d', time())."' WHERE  mxb_id=$mxb_id";
			//echo "El query update es:".$query."<br>";
			if($suma==0){
				$query="DELETE FROM matxbod WHERE mxb_id=$mxb_id";
			}
			$tablex->sql_query($query);
			return $suma;
		}else{
			return null;
		}
	}

	
	/** :) Suma los materiales que tengan fecha y con esto actualiza la cantidad total q hay en bodega y retorna la suma,
	si no hay alguno con fecha no hace nada y retorna null */
	function actualizarCantidadEnBodega($bod_id,$mat_id){
		$tablex = new my_DB();
		$query = "SELECT SUM(vmat_cantidad) FROM vencimiento_material vm,matxbod mxb WHERE mxb.bod_id=$bod_id AND mxb.mat_id=$mat_id AND vm.mxb_id=mxb.mxb_id";
		//echo "El query es:".$query."<br>";
		$tablex->search($query);
		$registrosuma=$tablex->sql_fetch_object();
		$suma = $registrosuma->sum;
		
		//echo "La suma es:".$suma."<br>";
		//if($suma){
			//echo "El query update es:".$query."<br>";
			
			/*if(!$suma || $suma==0){
				$query="DELETE  FROM matxbod WHERE bod_id=$bod_id AND mat_id=$mat_id";
			}else{
				$query="UPDATE matxbod SET \"mxb_cantidadMaterial\"=$suma, \"mxb_saldoInicial\"=$suma, \"mxb_fechaAbastecimiento\"='" .date('Y-m-d', time())."' WHERE  bod_id=$bod_id AND mat_id=$mat_id";
			}*/
			if(!$suma || $suma==0){
				$suma=0;
			}
			//echo ".....Suma:$suma:$query";
			$query="UPDATE matxbod SET \"mxb_cantidadMaterial\"=$suma, \"mxb_saldoInicial\"=$suma, \"mxb_fechaAbastecimiento\"='" .date('Y-m-d', time())."' WHERE  bod_id=$bod_id AND mat_id=$mat_id";

			//echo '<br>',$query;
			$tablex->sql_query($query);
			return $suma;
		//}else{
			//return false;
		//}
	}

	/*function actualizarBodega($mxb_id,$cantidad,$vmat_id=-1){
		if($vmat_id>=0){
			//Cambiamos lo que hay con esa fecha de vencimiento
			$query='UPDATE vencimiento_material SET "vmat_cantidad"='.$cantidad.' WHERE vmat_id='.$vmat_id;
			$tablex->sql_query($query);
			//echo $query."<BR>";
			//Consultamos la suma de todos los que tienen fecha de vencimiento y almacenamos en $reg
			$query="SELECT SUM(vmat_cantidad), COUNT(vmat_cantidad) FROM vencimiento_material WHERE mxb_id=".$mxb_id;
			$tablex->search($query);
			//echo $query."<BR>";
			$reg=$tablex->sql_fetch_object();
			if($reg){
				//Actualizamos lo que hay en la bodega sumando todo lo que tenga fecha
				$query='UPDATE matxbod SET "mxb_cantidadMaterial"='.$reg->sum.' , "mxb_saldoInicial"='.$reg->sum.' WHERE mxb_id='.$mxb_id;
				$tablex->sql_query($query);
				//echo $query."<BR>";
			}
		}else{
			//Actualizamos solo lo que hay en bodega
			$query='UPDATE matxbod SET "mxb_cantidadMaterial"='.$cantidad.', "mxb_saldoInicial"='.$cantidad.' WHERE mxb_id='.$mxb_id;
			$tablex->sql_query($query);
			//echo $query."<BR>";
		}
	}*/

	function puedeSacarMaterial($bod_id,$mat_id,$cantidad){
		echo $mat_id."----";
		//unset($this->cantidadSacada);
		//unset($this->fechaSacada);
		
		$tablex = new my_DB();
		$tablex2 = new my_DB();
		//Actualizamos la cantidad segun los que tienen fecha
		$cantMat=$this->actualizarCantidadEnBodega($bod_id,$mat_id);
		echo $cantidad."=".$cantMat."--->>".$bod_id."-".$mat_id;
		//Las posibilidades son: 
		//1.verificar que la cantidad exista para poder sacarla
		//Si hay con fecha y la cantidad que hay es mayor que lo que se pide
		if($cantMat && $cantMat>=$cantidad){
			//2.Seleccionamos todos los materiales de la bodega y los ordenamos por fecha (old a new)
			$query = "SELECT * FROM matxbod mxb,vencimiento_material vm WHERE NOT vm.vmat_cantidad=0 AND mxb.mat_id=$mat_id AND mxb.bod_id=$bod_id AND vm.mxb_id=mxb.mxb_id ORDER BY vm.vmat_fecha ASC,vm.vmat_id ASC";
			$tablex->search($query);
			//echo "<b>El query es:$query<br>Registros encontrados:".$tablex->nfound."<b><BR>";
			//Si se encontraron registros
			if($tablex->nfound>0)
				while($cantidad>0){
					$dato = $tablex->sql_fetch_object();
					if(!$dato){
						//echo "No hay dato";
						break;
					}
					//echo "<br>Condicion:$cantidad<$dato->vmat_cantidad";
					//Si la cantidad es menor que lo que hay se puede suplir
					if($cantidad < $dato->vmat_cantidad){
						//Guardamos cuanto va a quedar en bodega
						$nuevaCantidad=$dato->vmat_cantidad-$cantidad;
						//echo "<b><br>primer q:$query<br></b>";
						$cantidad=0;
						//echo "_";
						break;
					}else{
						//Como la cantidad no se puede suplir se disminuye la cantidad y se deja en cero la de la bodega
						$cantidad=$cantidad-$dato->vmat_cantidad;
					}
				}
			$result= array("mess"=>"Salieron las unidades de material exitosamente(tienen vencimiento) <br>", "mess_cod"=>"info");
			//print_r($result);
		}else{
			//Si NO hay materiales con fecha de vencimiento
			if(!$cantMat){
				$query="SELECT * FROM matxbod WHERE bod_id=$bod_id AND mat_id=$mat_id";
				//echo "<br>NO hay materiales con fecha de vencimiento<br>";
				//echo "$query<br>";
				$tablex->sql_query($query);
				//Si existe el material por bodega
				if($row = $tablex->sql_fetch_object()){
					$cantMat=$row->mxb_cantidadMaterial - $cantidad;
					//echo "El material existe, hay:$row->mxb_cantidadMaterial qudar&aacute;n $cantMat<br>";
					if($cantMat>=0){
						$result= array("mess"=>"OK <br>", "mess_cod"=>"info");
						//echo "La cantidad se puede suplir<br>$query<br>";
					}else{
						//echo "La cantidad no se puede suplir<br>";
						$result= array("mess"=>"No existe el material por bodega <br>", "mess_cod"=>"alert", "query"=>$query);
					}
				}else{
					//echo "El material en la bodega no existe <br>";
					$result = array ("mess"=>"El material en la bodega no existe <br>", "mess_cod"=>"alert", "query"=>$query);
				}
			}
			//Si hay materiales con fecha pero no alcanzan
			else{
				"La cantidad de material no alcanza, hay $cantMat y se requieren $cantidad<br>";
				$result = array ("mess"=>"No hay suficiente material <br>", "mess_cod"=>"alert", "query"=>$query);
			}
		}
			//print_r($result);
		return $result;
	}
}
?>
