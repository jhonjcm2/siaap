<?
/************************************************************************/
// CLASE: periodontograma
// Proposito: almacenar la informacion periodontal del paciente.
// Ultima modificacion: Octubre 2003
// /*********************************************************************/
class periodontograma{
  var $fv;
  var $data;
	
  function periodontograma($id=-1){
    global $table;
    
    $this->fv = new FormValidator;
    if ($id >= 0){
    	$query = "SELECT * 
        	     FROM periodontograma
        	     WHERE per_id='$id' ";
	$table->sql_query($query);
    	$this->data = $table->sql_fetch_object();
    }
    
  }

  /**********************************************************/
  // Proposito: Creacion del periodontograma 
  // return: arreglos con resultados de la creacion.
  /**********************************************************/
  function crear(){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;
      
  
//   if ( 0 /*$this->fv->isError()*/ ) {
if ( 0 ){
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    }else {
    	 $per_movilidadSuperior=serialize($per_movilidadSuperior);
    	 $per_vestibularSuperiorMargen1=serialize($per_vestibularSuperiorMargen1);
    	 $per_vesSupBolsa1=serialize($per_vesSupBolsa1);
    	 $per_vesSupVitalidad =serialize($per_vesSupVitalidad);
    	 $per_vesSupFurcacion =serialize($per_vesSupFurcacion);
    	 $per_vesSupL_m_g =serialize($per_vesSupL_m_g);
    	 $per_vesSupNic =serialize($per_vesSupNic);
    	 $per_palatinoMargen1 =serialize($per_palatinoMargen1);
    	 $per_palBolsa1 =serialize($per_palBolsa1);
    	 $per_palFurcacion =serialize($per_palFurcacion);
    	 $per_palPronostico =serialize($per_palPronostico);
    	 $per_palNic =serialize($per_palNic);
    	 $per_movilidadInferior =serialize($per_movilidadInferior);
    	 $per_vestibularInferiorMargen1 =serialize($per_vestibularInferiorMargen1);
    	 $per_vesInfBolsa1 =serialize($per_vesInfBolsa1);
    	 $per_vesInfVitalidad =serialize($per_vesInfVitalidad);
    	 $per_vesInfFurcacion =serialize($per_vesInfFurcacion);
    	 $per_vesInfL_m_g =serialize($per_vesInfL_m_g);
    	 $per_vesInfNic =serialize($per_vesInfNic);
    	 $per_lingualMargen1 =serialize($per_lingualMargen1);
    	 $per_linBolsa1 =serialize($per_linBolsa1);
    	 $per_linFurcacion =serialize($per_linFurcacion);
    	 $per_linPronostico =serialize($per_linPronostico);
    	 $per_linL_m_g =serialize($per_linL_m_g);
	 $per_linNic =serialize($per_linNic);
	 
         $fields = array ("per_id", "pac_id", "per_fecha", "per_fechaReevaluacion", "per_movilidadSuperior", "per_reevaluacion", 
         				"per_vestibularSuperiorMargen1", "per_vesSupMargen2", "per_vesSupBolsa1", 
         				"per_vesSupBolsa2", "per_vesSupVitalidad", "per_vesSupFurcacion","per_vesSupL_m_g", "per_vesSupNic", 
         				"per_palatinoMargen1", "per_palMargen2", "per_palBolsa1", "per_palBolsa2", "per_palFurcacion", 
         				"per_palPronostico", "per_palNic", "per_movilidadInferior", "per_vestibularInferiorMargen1", 
         				"per_vesInfMargen2", "per_vesInfBolsa1", "per_vesInfBolsa2", "per_vesInfVitalidad", 
         				"per_vesInfFurcacion",  "per_vesInfL_m_g", "per_vesInfNic", "per_lingualMargen1", "per_linMargen2", 
         				"per_linBolsa1", "per_linBolsa2", "per_linFurcacion", "per_linPronostico", "per_linL_m_g", "per_linNic") ;
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "periodontograma" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "<b>No se pudo adicionar el periodontograma por un fallo en el sistema. Por favor comun&iacute;quese con el administrador.</b>";
	$mess_cod = "alert";
	echo $query;	
	}else{
	    logs::crear("periodontograma", "crear", $query);
	    $mess = "<b>El periodontograma $login fu&eacute; adicionado con &eacute;xito</b> "; 
	    $mess_cod = "info";
	 } 
    }
        return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }


/********************* Buscar un periodontograma******************/

function buscar(){
    global $table, $_POST ,$_GET;
/*
    foreach($_POST as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($_GET as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);*/



    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

             $fields = array ("pac_id", "per_fecha", "per_fechaReevaluacion", "per_reevaluacion",
             "per_movilidadSuperior", "per_vestibularSuperiorMargen1", "per_vesSupMargen2", 
             "per_vesSupBolsa1", "per_vesSupBolsa2", "per_vesSupVitalidad", "per_vesSupFurcacion", 
             "per_vesSupL_m_g", "per_palatinoMargen1", "per_palMargen2", "per_palBolsa1", 
             "per_palBolsa2", "per_palFurcacion", "per_palPronostico", "per_movilidadInferior", 
             "per_vestibularInferiorMargen1", "per_vesInfMargen2", "per_vesInfBolsa1", "per_vesInfBolsa2", 
             "per_vesInfVitalidad", "per_vesInfFurcacion", "per_vesInfL_m_g", "per_lingualMargen1", 
             "per_linMargen2", "per_linBolsa1", "per_linBolsa2", "per_linFurcacion", "per_linPronostico", 
             "per_linL_m_g") ;
							      
       // Arreglos para datos del query y el url para paginacion

      $where_array = array();
     $query = 'SELECT * FROM  "periodontograma" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($where_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($where_array))
	$query .= " WHERE ".implode(", ",$where_array);
	
      	$result=$table->search($query);
	
	if (!$result){
		$mess = "No hay registros que concuerden con su busqueda.";
		$mess_cod = "alert";		
	}else{
	       $mess = "el periodontograma $login fue adicionado con &eacute;xito "; 
	    	$s_opc = "info_usuario";
	 }
	//echo ("BUSCAR:".$query."<br>");
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }

/****************************Ingresar Datos Reevaluacion****************************/

 function actualizar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

 $query= "SELECT * FROM periodontograma WHERE per_id=$pre_id";
   $table->search($query);
   $per= $table->sql_fetch_object();
if ($pre_id=""){
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    }else {
		$per_vesSupMargen2=serialize($per_vesSupMargen2);
		$per_vesSupBolsa2=serialize($per_vesSupBolsa2);
		$per_vesSupNic2=serialize($per_vesSupNic2);
		$per_palMargen2=serialize($per_palMargen2);
		$per_palBolsa2=serialize($per_palBolsa2);
		$per_palNic2=serialize($per_palNic2);
		$per_vesInfMargen2=serialize($per_vesInfMargen2);
		$per_vesInfBolsa2=serialize($per_vesInfBolsa2);
		$per_vesInfNic2=serialize($per_vesInfNic2);
		$per_linMargen2=serialize($per_linMargen2);
		$per_linBolsa2=serialize($per_linBolsa2);
		$per_linNic2=serialize($per_linNic2);

		$fields = array ("pac_id", "per_fechaReevaluacion", "per_reevaluacion",
				             "per_movilidadSuperior", "per_vestibularSuperiorMargen1", "per_vesSupMargen2", 
				             "per_vesSupBolsa1", "per_vesSupBolsa2", "per_vesSupNic2", "per_vesSupVitalidad", "per_vesSupFurcacion", 
				             "per_vesSupL_m_g", "per_palatinoMargen1", "per_palMargen2", "per_palBolsa1", 
				             "per_palBolsa2", "per_palNic2", "per_palFurcacion", "per_palPronostico", "per_movilidadInferior", 
				             "per_vestibularInferiorMargen1", "per_vesInfMargen2", "per_vesInfBolsa1", "per_vesInfBolsa2", "per_vesInfNic2",
				             "per_vesInfVitalidad", "per_vesInfFurcacion", "per_vesInfL_m_g", "per_lingualMargen1", 
				             "per_linMargen2", "per_linBolsa1", "per_linBolsa2", "per_linNic2", "per_linFurcacion", "per_linPronostico", 
				             "per_linL_m_g") ;
     

       
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "periodontograma" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE per_id = '$per_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo adicionar la reevaluaci&oacute;n por un fallo en el sistema. Por favor comun&iacute;quese con el administrador.";
		$mess_cod = "alert";		
	}else{
	       logs::crear("periodontograma", "actualizar", $query);
	 	$mess = "<center><b>La reevaluaci&oacute;n periodontal  $login fu&eacute; adicionada con &eacute;xito</b>"; 
	    	$mess_cod = "info";
	    	$s_opc = "busqueda";
	 } 
	}
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }
  
}

?>
