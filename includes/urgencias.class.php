<?
/***************************************************************************/
// CLASE: urgencias
// Proposito: Manejar la informacion de una Urgencia odontologica.
// Ultima modificacion: Octubre de 2003
// /************************************************************************/
class urgencias{
  var $fv;
  var $data;
	
  function urgencias($id=-1){
  global $table;

    $this->fv = new FormValidator;
    if ($id >= 0){
    	$query = "SELECT * 
        	     FROM urgencias
        	     WHERE urg_id='$id' ";
	$table->sql_query($query);
    	$this->data = $table->sql_fetch_object();
    }

  }

  /**********************************************************/
  // Proposito: Creacion de la historia de una urgencia 
  // return: arreglos con resultados de la creacion.
  /**********************************************************/
  function crear(){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);


    foreach($_GET as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      
      $this->fv->isWithinRange("cac_id","Seleccione la causa de la consulta",0,99);
      $this->fv->isEmpty("urg_historiaEnfermedadActual", "Debe insertar la historia de la enfermedad actual");      

if ($this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    }else {
    
	/*  se pasa a la quota a bytes */
         $fields = array ("urg_id", "pac_id", "cac_id", "urg_historiaEnfermedadActual", "urg_antMedOdontologicos", "urg_remision", "urg_fecha");
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "urgencias" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "<b>No se pudo adicionar la urgencia por un fallo en el sistema. Comuniquese con el administrador del sistema.</b>";
	$mess_cod = "alert";		
	}else{
	    logs::crear("urgencias", "crear", $query);
	    $mess = "<b>La datos fueron adicionados con &eacute;xito.</b> "; 
	    $mess_cod = "info";
	    //$s_opc = "busqueda";
	 } 
	  
      }
   
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }


/********************* Buscar una urgencia******************/

function buscar(){
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($_GET as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);



             $fields = array ( "urg_id", "pac_id", "cac_id", "urg_historiaEnfermedadActual", "urg_antMedOdontologicos", 
                                     "urg_remision");
							      
       // Arreglos para datos del query y el url para paginacion

      $where_array = array();
     $query = 'SELECT * FROM  "urgencias" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($where_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($where_array))
	$query .= " WHERE ".implode(", ",$where_array);
	
      	$result=$table->search($query);
	
	if (!$result){
		$mess = "<b>No hay registros que concuerden con su busqueda.</b>";
		$mess_cod = "alert";		
	}else{
	       $mess = "<b>Datos encontrados</b>"; 
	 	$mess_cod = "info";	
	    	
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }

 
}
?>
