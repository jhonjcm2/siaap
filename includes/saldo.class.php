<?php
/**********************************************************/
// CLASE: saldo
// Proposito: Crear saldos negativos y averiguar el saldo neto.
// Ultima modificacion: Noviembre  del 2011
// /**********************************************************/
class Saldo{
	var $fv;
	var $data;
	var $saldoNegativoPaciente=50;
	var $pagoTotalPaciente=40;
	var $saldoNetoPaciente=30;
	var $paciente;
	function Saldo($id=-1){
		global $table;
		$this->fv = new FormValidator;
		if ($id >= 0){
			$query = "SELECT * FROM pagos WHERE pag_id='$id' ";
			$table->sql_query($query);
			$this->data = $table->sql_fetch_object();
		}
	}
	
	/*Funcion que calcula los saldos del paciente,
		devuelve 1 si el paciente existe
		devuelve -1 si el paciente no existe por lo que sus saldos son cero*/
	function calcularSaldos($idPaciente){
		global $table;
		$this->paciente=null;
		$query = 'SELECT * FROM paciente WHERE pac_id='.$idPaciente;
		$table->search($query);
		//Si existe el paciente
		if($miPaciente=$table->sql_fetch_object()){
			//echo $miPaciente->pac_nombres;
			$this->paciente=array("nombres"=>$miPaciente->pac_nombres, "apellidos"=>$miPaciente->pac_apellidos,"pac_id"=>$idPaciente, "pac_numeroIdentificacion"=>$miPaciente->pac_numeroIdentificacion);/**/
			//echo $this->paciente["pac_numeroIdentificacion"];
			//para calcular la suma de los saldos de un paciente usamos SELECT SUM(sn.sn_valor) as valor FROM evolucion evo, saldo_negativo sn WHERE evo.pac_id=3944 AND sn.evo_id=evo.evo_id
			$query = "SELECT SUM(sn.sn_valor) as valor FROM evolucion evo, saldo_negativo sn WHERE evo.pac_id=".$idPaciente." AND sn.evo_id=evo.evo_id";
			$table->search($query);
			$datoSN = $table->sql_fetch_object();
	
			//para calcular la suma de los pagos en efectivo de un paciente usamos SELECT SUM(CAST(pag."pag_valorEfectivo" as int)) as valor FROM pagos pag WHERE pag.pac_id=3944
			$query = 'SELECT SUM(CAST(pag."pag_valorEfectivo" as int)) as valor FROM pagos pag WHERE pag_estado=false AND pag.pac_id='.$idPaciente;
			$table->search($query);
			$datoSEfectivo = $table->sql_fetch_object();
	
			//para calcular la suma de los cheques SELECT SUM(CAST(pch."pch_valorCheque" as int)) as valor FROM pagos pag, pago_cheque pch WHERE pag.pag_id=pch.pag_id AND pag.pac_id=3944		
			$query = 'SELECT SUM(CAST(pch."pch_valorCheque" as int)) as valor FROM pagos pag, pago_cheque pch WHERE pag.pag_estado=false AND pag.pag_id=pch.pag_id AND pag.pac_id='.$idPaciente;
			$table->search($query);
			$datoSCheque = $table->sql_fetch_object();
	
			//para calcular la suma de los debitos SELECT SUM(CAST(pde."pde_valor" as int)) as valor FROM pagos pag, pago_debito pde WHERE pag.pag_id=pde.pag_id AND pag.pac_id=3944
			$query = 'SELECT SUM(CAST(pde."pde_valor" as int)) as valor FROM pagos pag, pago_debito pde WHERE pag.pag_estado=false AND pag.pag_id=pde.pag_id AND pag.pac_id='.$idPaciente;
			$table->search($query);
			$datoSDebito = $table->sql_fetch_object();
	
			//para calcular la suma de los creditos SELECT SUM(CAST(pcr."pcr_valor" as int)) as valor FROM pagos pag, pago_credito pcr WHERE pag.pag_id=pcr.pag_id AND pag.pac_id=3944
			$query = 'SELECT SUM(CAST(pcr."pcr_valor" as int)) as valor FROM pagos pag, pago_credito pcr WHERE pag.pag_estado=false AND pag.pag_id=pcr.pag_id AND pag.pac_id='.$idPaciente;
			$table->search($query);
			$datoSCredito = $table->sql_fetch_object();
	
			//Calculamos el saldo neto
			$saldoNeto = $datoSEfectivo->valor + $datoSCheque->valor + $datoSDebito->valor + $datoSCredito->valor - $datoSN->valor;

			$this->saldoNegativoPaciente=($datoSN->valor)?$datoSN->valor:0;
			$this->pagoTotalPaciente=($datoSEfectivo->valor + $datoSCheque->valor + $datoSDebito->valor + $datoSCredito->valor)?$datoSEfectivo->valor + $datoSCheque->valor + $datoSDebito->valor + $datoSCredito->valor:0;
			$this->saldoNetoPaciente=$this->pagoTotalPaciente-$this->saldoNegativoPaciente;
			return 1;
		}else{
			return -1;
		}
	}

	//Funcion temporal
	function crearSaldoNegativoArbitrario($sn_valor,$pac_id,$usu_id,$descripcion=""){
		global $table;
		//nextval(('"evolucion_evo_id_seq"'::text)::regclass)
		$nquery="select last_value from evolucion_evo_id_seq";
		$objEvo = new evolucion();
		////////////////////////
			$tco_id=890203;
			$cac_id=3;
			$amp_id=1;
			$fip_id=1;
			$faq_id=1;
			$pto_id=1;
			$pxe_cantidadProcedimientos=1;
		////////////////////////
		$retEvo=$objEvo->crear2($pac_id,890203, 3, 1, 1, 1, 1, 1,$usu_id,$descripcion);
		if($retEvo["mess_cod"]=="info"){
			$table->search($nquery);
			$evo_id=$table->sql_fetch_object()->last_value;
			
			//Debo guardar el dato en saldo negativo
				$nquery= 'INSERT INTO "saldo_negativo" ("evo_id", "sn_valor" ) VALUES (';
				$nquery.= " $evo_id, $sn_valor)";
				$resultsn=$table->sql_query($nquery);
				if (!$resultsn){
					$mess = "Fallo adicionando saldo negativo (saldo de $sn_valor). Favor contacte al Administrador del Sistema.";
					$mess_cod = "alert";
				}else{
					logs::crear("saldo_negativo", "crearSaldoNegativo", $nquery);
					$mess = "El saldo negativo y la evolucion fueron; adicionados con &eacute;xito ";
					$mess_cod = "info";
				}
			return array( "mess"=>$mess, "mess_cod"=>$mess_cod, "s_opc"=>$s_opc, "username"=>$login );
		}else{
			return $retEvo;
		}
		
	}



	//Crea un saldo negativo para una evolucion y lo relaciona con el usuario, este se debe hacer luego de una evolucion.
	function crearSaldoNegativo($usu_id,$evo_id){
		global $table;
		/*Ejemplo de uso del formValidator
		$this->fv->resetErrorList();
		$this->fv->isWithinRange("pag_codigoIng","Seleccione un codigo de ingreso",0,9999); */
		/*/Mirar que tipo de odontologo es
		//Hacer consulta de los odontologos
			$query = "SELECT * FROM odontologos WHERE usu_id=$usu_id";
			$table->search($query);
		//Guardamos el odontologo y el tipo
			$miodontologo = $table->sql_fetch_object();
			$tipoOdontologo = $miodontologo->odl_tipoOdontologo;
			$nquery="SELECT SUM((CAST(proc.";
		//Dependiendo del tipo de odontologo le escojo un valor
			if($tipoOdontologo==1){
				$nquery.='"pto_valorEstPre"';
			}else if($tipoOdontologo==2){
				$nquery.='"pto_valorEstPost"';
			}else if($tipoOdontologo==3){
				$nquery.='"pto_valorAsit"';
			}else if($tipoOdontologo==4){
				$nquery.='"pto_valorEsp"';
			}else if($tipoOdontologo==5){
				$nquery.='"pto_valorHigOral"';
			}
			$nquery.=' as int))*(CAST(pxe."pxe_cantidadProcedimientos" as int))) as valor';
		//Escojo las tablas y las restricciones (From y Where)
			$nquery.=" FROM procxevo pxe, evolucion evo, procedimiento proc ";
			$nquery.=" WHERE evo.evo_id=$evo_id AND pxe.evo_id=evo.evo_id AND proc.pto_id=pxe.pto_id";/**/
			
			
			$query='SELECT SUM(pxe."pxe_valorUnitario"*(CAST(pxe."pxe_cantidadProcedimientos" as int))) as valor
			FROM procxevo pxe, evolucion evo, procedimiento proc 
			WHERE evo.evo_id='.$evo_id.' AND pxe.evo_id=evo.evo_id AND proc.pto_id=pxe.pto_id';
		//Hago el query y obtengo el valor del saldo de esa evolución
			$table->search($query);
			$registroValor= $table->sql_fetch_object();
			$saldoNegativoPaciente=($registroValor->valor)?$registroValor->valor:0;
		//Debo guardar el dato en saldo negativo
			$nquery= 'INSERT INTO "saldo_negativo" ("evo_id", "sn_valor" ) VALUES (';
			$nquery.= "$evo_id , $saldoNegativoPaciente)";
			//echo "<br>Este el de SN<br>$nquery<br>";
			//$nquery.= ($saldoNegativoPaciente)?"$saldoNegativoPaciente)":0;
			
			$resultsn=$table->sql_query($nquery);
			if (!$resultsn){
				$mess = "Fallo adicionando saldo negativo (saldo de $saldoNegativoPaciente). Favor contacte al Administrador del Sistema.";
				$mess_cod = "alert";
			}else{
				logs::crear("saldo_negativo", "crearSaldoNegativo", $nquery);
				$mess = "El procedimiento fu&eacute; adicionado con &eacute;xito ";
				$s_opc = "info_usuario";
				//Adicionamos el valor de cada procedimiento
				//$query="SELECT * FROM procxevo "
			}
		return array( "mess"=>$mess, "mess_cod"=>$mess_cod, "s_opc"=>$s_opc, "username"=>$login );
	}
	
	//Retorna el saldo total del paciente
	function dameSaldoPaciente($idPaciente=0){
		global $table;
		//para calcular la suma de los saldos de un paciente usamos SELECT SUM(sn.sn_valor) as valor FROM evolucion evo, saldo_negativo sn WHERE evo.pac_id=3944 AND sn.evo_id=evo.evo_id
		$query = "SELECT SUM(sn.sn_valor) as valor FROM evolucion evo, saldo_negativo sn WHERE evo.pac_id=".$idPaciente." AND sn.evo_id=evo.evo_id";
		$table->search($query);
		$datoSN = $table->sql_fetch_object();

		//para calcular la suma de los pagos en efectivo de un paciente usamos SELECT SUM(CAST(pag."pag_valorEfectivo" as int)) as valor FROM pagos pag WHERE pag.pac_id=3944
		$query = 'SELECT SUM(CAST(pag."pag_valorEfectivo" as int)) as valor FROM pagos pag WHERE pag.pac_id='.$idPaciente;
		$table->search($query);
		$datoSEfectivo = $table->sql_fetch_object();

		//para calcular la suma de los cheques SELECT SUM(CAST(pch."pch_valorCheque" as int)) as valor FROM pagos pag, pago_cheque pch WHERE pag.pag_id=pch.pag_id AND pag.pac_id=3944		
		$query = 'SELECT SUM(CAST(pch."pch_valorCheque" as int)) as valor FROM pagos pag, pago_cheque pch WHERE pag.pag_id=pch.pag_id AND pag.pac_id='.$idPaciente;
		$table->search($query);
		$datoSCheque = $table->sql_fetch_object();

		//para calcular la suma de los debitos SELECT SUM(CAST(pde."pde_valor" as int)) as valor FROM pagos pag, pago_debito pde WHERE pag.pag_id=pde.pag_id AND pag.pac_id=3944
		$query = 'SELECT SUM(CAST(pde."pde_valor" as int)) as valor FROM pagos pag, pago_debito pde WHERE pag.pag_id=pde.pag_id AND pag.pac_id='.$idPaciente;
		$table->search($query);
		$datoSDebito = $table->sql_fetch_object();

		//para calcular la suma de los creditos SELECT SUM(CAST(pcr."pcr_valor" as int)) as valor FROM pagos pag, pago_credito pcr WHERE pag.pag_id=pcr.pag_id AND pag.pac_id=3944
		$query = 'SELECT SUM(CAST(pcr."pcr_valor" as int)) as valor FROM pagos pag, pago_credito pcr WHERE pag.pag_id=pcr.pag_id AND pag.pac_id='.$idPaciente;
		$table->search($query);
		$datoSCredito = $table->sql_fetch_object();

		//Calculamos el saldo neto
		$saldoNeto = $datoSEfectivo->valor + $datoSCheque->valor + $datoSDebito->valor + $datoSCredito->valor - $datoSN->valor;

		return $saldoNeto;
	}

}
?>
