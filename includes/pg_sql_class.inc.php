<?

//require ("$include_path/base_class.inc.php");



// MySQL Object

// Apadtado a Objeto de Postgres

class pg_sql_db extends base {



        // Class properties



        var $sqlhost;                                        // Host donde esta alojado   PostGres

        var $sqluser;                                        // Usuario de Acceso a PostGres

        var $sqlpass;                                        // Password de acceso a PostGres

        var $sqlport="5432";                                          // Puerto del servidor para PostGres

        var $sqldb;                                          // nombre de la base de datos

        var $sql_link;                          // Link a la db

        var $sql_result;                        // Pointer al resultado luego de un query

        var $sql_data;                                // Pointer al resultado de un fetch

        var $sql_row;                                    // Numero del registro en la lista de resultados

        var $sql_max_row;                                // Numero total de registros. Evita recalcular

        var $sql_error;

        var $sql_errno;

        var $halt_on_error = FALSE;





        // constructor



        function pg_sql_db($type="pg_sql_db") {

                $this->base($type);

                $this->sql_connect();

                return;

        }





        // database connection functions



        function sql_pconnect() {

                        $this->sql_link = pg_Connect("user=$this->sqluser password=$this->sqlpass host=$this->sqlhost port=$this->sqlport dbname=$this->sqldb");

                        if ($this->sql_link) {

                                $this->sql_errno = 0;

                                $this->sql_error = "OK";

                                if ($this->halt_on_error) {

                                        $this->halt("sql_connect");

                                }

                        } else {

                                $this->errno();

                                $this->error();

                                if ($this->halt_on_error) {

                                        $this->halt("sql_pconnect");

                                        }

                                }

                        return;

        } // end sql_pconnect



        function queryerror($method="") {

                        if ($this->sql_result) {

                                $this->sql_errno = 0;

                                $this->sql_error = "OK";

                                return TRUE;

                        } else {

                                $this->errno();

                                $this->error();

                                if ($this->halt_on_error) {

                                        $this->halt($method);

                                }

                        }

                        return FALSE;

        }



        function misc_error($method=""){

                $this->errno();

                $this->error();

                if ($this->sql_errno && $this->halt_on_error) {

                        $this->halt($method);

                }

        }



        function sql_connect() {
                        $this->sql_link = pg_connect("user=$this->sqluser port=$this->sqlport dbname=$this->sqldb password=$this->sqlpass host=localhost") or die('No Conecto '.pg_last_error());
                                        
                        //$this->sql_link = pg_Connect("user=$this->sqluser  password=$this->sqlpass port=$this->sqlport dbname=$this->sqldb");

                        if (!$this->sql_link) {

                                $this->sql_errno = -10000;

                                $this->sql_error = "No se pudo establecer la conexi&oacute;n con el servidor";

                                if ($this->halt_on_error) {

                                        $this->halt("sql_connect");

                                }

                        }



                        $this->misc_error("sql_connect");

                        return $this->sql_link;

        } // end sql_connect



        // override to adapt it to site GUI



        function halt($string="") {

                $error = "## <b>Error:</b> ## ".$this->object_type."::".$string." ".$this->sql_error;

                print($error);

                exit;

        }  // end halt()



        function sql_close() {

                $this->sql_result = pg_close($this->sql_link);

                $this->queryerror();

                return $this->sql_result;

        }



// Error management functions

// En postgres solo existen mensajes de error, y no numeros.



        function error() {

                $this->sql_error = pg_errormessage($this->sql_link);

                return $this->sql_error;

        }



        function errno() {



                $this->sql_errno = pg_errormessage($this->sql_link);

                return $this->sql_errno;



        }

        // Query and result management functions



// Al trabajar con postgres, se agrega que reinicie el sql_row tras cada consulta

// Se inicia la variable max_row al numero maximo de registros, para no tener que reconsultar num_rows

        function sql_query($query) {
                $this->sql_result = pg_Exec($this->sql_link,$query);

                $this->sql_row=0;

                $this->queryerror("sql_query");
                $this->sql_max_row = $this->sql_num_rows() - 1;

                return $this->sql_result;

        }





        function sql_free_result() {



                $this->sql_result = pg_FreeResult($this->sql_result);

                $this->queryerror("sql_free_result");

                return;

        }



// Cada que busque, se incrementa el sql_row para pasar al siguiente registro



        function sql_fetch_row() {



                if ($this->sql_row > $this->sql_max_row)

                   return 0;

                $this->sql_data = pg_fetch_row($this->sql_result,$this->sql_row);

                $this->sql_row++;

                $this->misc_error("sql_fetch_row");

                return $this->sql_data;




        }



        function sql_fetch_object() {



                if ($this->sql_row > $this->sql_max_row)

                   return 0;

                $this->sql_data = pg_fetch_object($this->sql_result,$this->sql_row);

                $this->sql_row++;

                $this->misc_error("sql_fetch_object");

                return $this->sql_data;



        }



        function sql_fetch_array() {



                if ($this->sql_row > $this->sql_max_row)

                   return 0;

                $this->sql_data = pg_fetch_array($this->sql_result,$this->sql_row);

                $this->sql_row++;

                $this->misc_error("sql_fetch_array");

                return $this->sql_data;



        }



        function sql_num_rows() {



                $result = pg_numrows($this->sql_result);

                $this->misc_error("sql_num_rows");

                return $result;



        }



        // Miscellaneous functions



// En postgres no existe el seek, pero se cambia el sql_row :)

        function sql_data_seek($row) {

                   $this->sql_row=$row;

                $this->misc_error("sql_data_seek");

                return result;



        }



        function sql_affected_rows() {



                $result = pg_cmdtuples($this->sql_result);

                $this->misc_error("sql_affected_rows");

                return $result;



        }



        function sql_num_fields() {



                return pg_numfields($this->sql_result);



        }



        function sql_fieldname($field) {



                return pg_fieldname($this->sql_result,$field);



        }





        // fetch_field no existe en postgres

        function sql_fetch_field($offset=0) {

                $result = mysql_fetch_field($this->sql_result,$offset);



                if (!$result) {

                        $this->error();

                        $this->errno();

                        if ($this->halt_on_error) {

                                $this->halt("sql_fetch_field");

                        }

                } else {

                        $this->sql_errno = 0;

                        $this->sql_error = "OK";

                }

                return $result;

        }


	function sql_next_id($seq="") {

		$query = "SELECT nextval('$seq'::text) 
				as next_id";
		$this->sql_query($query);
		$row = $this->sql_fetch_object();
		$id=$row->next_id;
		return $id;	
	}




} // end class definition



?>
