<?
/**********************************************************/
// CLASE: Comunitaria
// Proposito: registrar cada asignacion de citas.
// Ultima modificacion: Octubre de 2006
// /**********************************************************/
class comunitaria{
  var $fv;
  var $data;
	
  function comunitaria($id=-1){
    global $table;
    
    $this->fv = new FormValidator;
    if ($id >= 0){
    	$query = "SELECT * 
        	     FROM cita
        	     WHERE cit_id='$id' ";
	$table->sql_query($query);
    	$this->data = $table->sql_fetch_object();
    }
    
  }

  /**********************************************************/
  // Proposito: Almacenar entidades asociadas al area de comunitaria
  // return: arreglos con resultados de la creacion.
  /**********************************************************/
  function crear(){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;
 
    $this->fv->resetErrorList();
    $this->fv->isWithinRange("cen_tipo","Seleccione un tipo de entidad",2,99);
    $this->fv->isEmpty("cen_nombre","Ingrese el nombre de la entidad");
    
   if ( $this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    }else {
      $query = 'SELECT * 
                FROM cmt_entidad
                WHERE "cen_id "='.$cen_id;
      $table->search($query);
    
      if($table->nfound) {
	$mess = "una entidad con ese id se encuentra registrada.";
	$mess_cod = "alert";
	}
      else {
	/*  se pasa a la quota a bytes */
	//$cit_fecha="$fn_anio-$fn_mes-$fn_dia";
         $fields = array ("cen_id", "usu_id", "cen_fechaIngreso", "cen_tipo", "cen_nombre", "cen_ubi", "cen_tel", "cen_director") ;
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "cmt_entidad" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "<center><b>No se pudo registrar la entidad por un fallo en el sistema</b><br> Por favor comuniquese con el administrador.</b></center>".$query;
	$mess_cod = "alert";		
	}else{
	 //   logs::crear("cita", "crear", $query);	
	    $mess = "<b>La entidad fue registrada con &eacute;xito</b><br>
	    Haga click <a href='$PHP_SELF?opc=comunitaria&s_opc=entidad'>Aqu&iacute;</a> para registrar otra entidad ";
	    $mess_cod="info";
	    $s_opc = "info_usuario";
	 } 
	  
      }
    }
     return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }


  /**********************************************************/
  // Proposito: Almacenar Datos obtenidos en las entidades Escuelas asociadas al area de comunitaria
  // return: arreglos con resultados de la creacion.
  /**********************************************************/
  function crear1(){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;
 
    $this->fv->resetErrorList();
    $this->fv->isWithinRange("ces_periodo","Seleccione un periodo",1,99);
    $this->fv->isWithinRange("ces_ano","Selecciones el a�o del Periodo",1,99);
    
   if ( $this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    }else {
      $query = "SELECT * 
                FROM cmt_escuela
                WHERE cen_id =$cen_id and ces_periodo=$ces_periodo and ces_ano=$ces_ano";
      $table->search($query);
    
      if($table->nfound) {
	$mess = "<center><b>Los Datos de esa Escuela para ese periodo YA fueron ingresados</b><br>
	Haga click <a href='$PHP_SELF?opc=comunitaria&s_opc=cmtescuela'>Aqu&iacute;</a> para ingresar los datos de otra Escuela</center>";
	$mess_cod = "alert";
	}
      else {
	/*  se pasa a la quota a bytes */
	//$cit_fecha="$fn_anio-$fn_mes-$fn_dia";
         $fields = array ("ces_id", "usu_id", "ces_fechaIngreso", "cen_id", "ces_periodo", "ces_ano", "ces_promocion", "ces_prevencion", "ces_tratamiento", "ces_caries",
         			   "ces_terminado", "ces_ipbIni", "ces_ipbFin", "ces_obs") ;
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "cmt_escuela" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "<center><b>No se pudo registrar los datos de la entidad por un fallo en el sistema</b><br> Por favor comuniquese con el administrador.</b></center>".$query;
	$mess_cod = "alert";		
	}else{
	 //   logs::crear("cita", "crear", $query);	
	    $mess = "<b>Los datos de la entidad fue registrados con &eacute;xito</b><br>
	    Haga click <a href='$PHP_SELF?opc=comunitaria&s_opc=cmtescuela'>Aqu&iacute;</a> para ingresar los datos de otra Escuela ";
	    $mess_cod="info";
	    $s_opc = "info_usuario";
	 } 
	  
      }
    }
     return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }

/**********************************************************/
  // Proposito: Almacenar Datos obtenidos en las entidades ESELADERA  ANCIANATOS  asociadas al area de comunitaria
  // return: arreglos con resultados de la creacion.
  /**********************************************************/
  function crear2(){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;
 
    $this->fv->resetErrorList();
    $this->fv->isWithinRange("cla_periodo","Seleccione un periodo",1,99);
    $this->fv->isWithinRange("cla_ano","Selecciones el a�o del Periodo",1,99);
    
   if ( $this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    }else {
      $query = "SELECT * 
                FROM cmt_ladacn
                WHERE cen_id =$cen_id and cla_periodo=$cla_periodo and cla_ano=$cla_ano";
      $table->search($query);
    
      if($table->nfound) {
	$mess = "<center><b>Los Datos de esa Entidad para ese periodo YA fueron ingresados</b><br>
	Haga click <a href='$PHP_SELF?opc=comunitaria&s_opc=cmtladanc'>Aqu&iacute;</a> para ingresar los datos de otra Escuela</center>";
	$mess_cod = "alert";
	}
      else {
	/*  se pasa a la quota a bytes */
	//$cit_fecha="$fn_anio-$fn_mes-$fn_dia";
         $fields = array ("cla_id", "cla_fechaIngreso", "usu_id", "cen_id", "cla_periodo", "cla_ano", 
         "cla_continuidad", "cla_preventivo",  "cla_amalgamas",  "cla_resinas", "cla_ionomeros", 
         "cla_prevencion",  "cla_endodoncia",  "cla_cirugia", "cla_protesis",  "cla_sellantes",  "cla_totalPacientes",
         "cla_historia",  "cla_urgencia",  "cla_obs") ;
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "cmt_ladacn" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "<center><b>No se pudo registrar los datos de la entidad por un fallo en el sistema</b><br> Por favor comuniquese con el administrador.</b></center>".$query;
	$mess_cod = "alert";		
	}else{
	 //   logs::crear("cita", "crear", $query);	
	    $mess = "<b>Los datos de la entidad fue registrados con &eacute;xito</b><br>
	    Haga click <a href='$PHP_SELF?opc=comunitaria&s_opc=cmtladanc'>Aqu&iacute;</a> para ingresar los datos de otra Escuela ";
	    $mess_cod="info";
	    $s_opc = "info_usuario";
	 } 
	  
      }
    }
     return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }




  /************** Modificar entidad****************/
  
  function modificar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    
    $fields = array ("cen_id", "usu_id", "cen_fechaIngreso", "cen_tipo", "cen_nombre", "cen_ubi", "cen_tel", "cen_director") ;
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "cmt_entidad" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE cen_id = '$cen_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "<b>No se pudo modificar la Entidad por un fallo en el sistema. Comuniquese con el administrador</b>". $query;
		$mess_cod = "alert";		
	}else{
	       logs::crear("cita", "modificar", $query);
	 	$mess = "<b>La entidad fu&eacute; modificada con &eacute;xito</b>"; 
	 	$mess_cod="info";
	} 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }
 

}
?>
