<?
/**********************************************************/
// CLASE: empeado
// Proposito: Realizar tareas de manejo de usuarios.
// Ultima modificacion: Octubre 2003
// /**********************************************************/
class empleado{
  var $fv;
  var $data;	

  function empleado($id=-1){
    global $table;
    
    $this->fv = new FormValidator;
    if ($id >= 0){
    	$query = "SELECT * 
        	     FROM empleado 
        	     WHERE emp_id='$id' ";
	$table->sql_query($query);
    	$this->data = $table->sql_fetch_object();
    }
    
  }

  /**********************************************************/
  // Proposito: Creacion unica de un empleado
  // return: arreglos con resultados de la creacion.
  /**********************************************************/
  function crear(){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;

    foreach($_GET as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;
      
        $this->fv->resetErrorList();	 
      /* $this->fv->isEqualsString("usu_password","usu_cPassword","La clave y la confirmaci&oacute;n no coinciden.");
       $this->fv->isWithinRange("tiu_id","Seleccione el tipo de Usuario",0,99);
       $this->fv->isEmpty("usu_nombres", "Debe insertar los nombres del usuario");
       $this->fv->isEmpty("usu_apellidos", "Debe insertar los apellidos del usuario");
       $this->fv->isWithinRange("tid_id","Seleccione el tipo de Documento",0,99);
       $this->fv->isEmpty("usu_telefono", "Debe insertar el numero telef&oacute;nico del usuario");
      
       //$this->fv->isWithinRange("odl_tipoOdontologo","Seleccione el tipo de Odont&oacute;logo",0,99);
       */
       $this->fv->isNumber("emp_salario", "Ingrese solo n&uacute;meros, sin puntos y sin signos");
       $this->fv->isNumber("emp_gastosrep", "Ingrese solo n&uacute;meros, sin puntos y sin signos");
       //$this->fv->isEmailAddress("emp_mail", "Ingrese una direcci&oacute;n de correo electr&oacute;nico valida");
      	
     if ($this->fv->isError() ) {
        $mess = $this->fv->getMessage();
        $mess_cod = "alert";
            //include ("modules/usuario/default.php");
        } 
 
      else {
	/*  se pasa a la quota a bytes */
	
	$fields = array ("usu_id", "emp_fechanac", "emp_lugarnac", "emp_direccion", "emp_tel", "emp_fax", "emp_mail", "emp_tipovin", "emp_fechavin", "emp_categoria", "emp_salario", "emp_gastosrep",  "emp_investiga", "emp_areaint" );
	
   
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array( );
      $values_array = array( );
     $query = 'INSERT INTO "empleado" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	   array_push($fields_array,' "'.$v.'" ');
	   array_push($values_array," '".${$v}."' ");
	   }

      if(sizeof($fields_array)){
	   $query .= implode(", ",$fields_array);
	   $query .= ") VALUES (".implode(", ",$values_array) ;
	   }
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	   $mess = "<center><b>No se pudo adicionar el empleado por un fallo en el sistema</b></center>".$query;
	   $mess_cod = "alert";
	   $s_opc="default";
	   }
	else{
			    
	    $mess = "<center><b>El Empleado fu&eacute; adicionado con &eacute;xito </b><br></center>
	    Haga click <a href='$PHP_SELF?opc=usuario&s_opc=default'>&nbsp;&nbsp;<b>aqu&iacute;</b></a> para ingresar otro Usuario";
	    $mess_cod = "info";
	    $s_opc = "busqueda";
	    } 
	  
      }
   
    //$mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login); 
 
}

  /**********************************************************/
  // Proposito: Creacion estudios empleado
  // return: arreglos con resultados de la creacion.
  /**********************************************************/
  function crearese(){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;

    foreach($_GET as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;
      
       /* $this->fv->resetErrorList();	 
       $this->fv->isEqualsString("usu_password","usu_cPassword","La clave y la confirmaci&oacute;n no coinciden.");
       $this->fv->isWithinRange("tiu_id","Seleccione el tipo de Usuario",0,99);
       $this->fv->isEmpty("usu_nombres", "Debe insertar los nombres del usuario");
       $this->fv->isEmpty("usu_apellidos", "Debe insertar los apellidos del usuario");
       $this->fv->isWithinRange("tid_id","Seleccione el tipo de Documento",0,99);
       $this->fv->isEmpty("usu_telefono", "Debe insertar el numero telef&oacute;nico del usuario");
       $this->fv->isEmpty("usu_numeroIdentificacion", "Debe insertar el numero de Identificaci&oacute;n del usuario");
       //$this->fv->isWithinRange("odl_tipoOdontologo","Seleccione el tipo de Odont&oacute;logo",0,99);
       //$this->fv->isNumber("usu_numeroIdentificacion", "Debe insertar solo n&uacute;meros");
      	*/
      	
     if ($this->fv->isError() ) {
        $mess = $this->fv->getMessage();
        $mess_cod = "alert";
            //include ("modules/usuario/default.php");
        } 
 
      else {
	/*  se pasa a la quota a bytes */
	
	$fields = array ("emp_id", "ese_tipo", "ese_titulo", "ese_institucion", "ese_ano", "ese_duracion", "ese_enfasis");
	
   
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array( );
      $values_array = array( );
     $query = 'INSERT INTO "estudio_emp" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	   array_push($fields_array,' "'.$v.'" ');
	   array_push($values_array," '".${$v}."' ");
	   }

      if(sizeof($fields_array)){
	   $query .= implode(", ",$fields_array);
	   $query .= ") VALUES (".implode(", ",$values_array) ;
	   }
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	   $mess = "<center><b>No se pudo adicionar el estudio por un fallo en el sistema</b></center>".$query;
	   $mess_cod = "alert";
	   $s_opc="default";
	   }
	else{
			    
	    $mess = "<center><b>El estudio fu&eacute; adicionado con &eacute;xito </b><br></center>
	    Haga click <a href='$PHP_SELF?opc=usuario&s_opc=default'>&nbsp;&nbsp;<b>aqu&iacute;</b></a> para ingresar otro Usuario";
	    $mess_cod = "info";
	    $s_opc = "busqueda";
	    } 
	  
      }
   
    //$mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login); 
 
}


  /**********************************************************/
  // Proposito: Creacion observaciones al  empleado
  // return: arreglos con resultados de la creacion.
  /**********************************************************/
  function crearobs(){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;

    foreach($_GET as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;
      
       /* $this->fv->resetErrorList();	 
       $this->fv->isEqualsString("usu_password","usu_cPassword","La clave y la confirmaci&oacute;n no coinciden.");
       $this->fv->isWithinRange("tiu_id","Seleccione el tipo de Usuario",0,99);
       $this->fv->isEmpty("usu_nombres", "Debe insertar los nombres del usuario");
       $this->fv->isEmpty("usu_apellidos", "Debe insertar los apellidos del usuario");
       $this->fv->isWithinRange("tid_id","Seleccione el tipo de Documento",0,99);
       $this->fv->isEmpty("usu_telefono", "Debe insertar el numero telef&oacute;nico del usuario");
       $this->fv->isEmpty("usu_numeroIdentificacion", "Debe insertar el numero de Identificaci&oacute;n del usuario");
       //$this->fv->isWithinRange("odl_tipoOdontologo","Seleccione el tipo de Odont&oacute;logo",0,99);
       //$this->fv->isNumber("usu_numeroIdentificacion", "Debe insertar solo n&uacute;meros");
      	*/
      	
     if ($this->fv->isError() ) {
        $mess = $this->fv->getMessage();
        $mess_cod = "alert";
            //include ("modules/usuario/default.php");
        } 
 
      else {
	/*  se pasa a la quota a bytes */
	
	$fields = array ("emp_id", "oxe_fecha", "oxe_tipo", "oxe_descripcion" );
	
   
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array( );
      $values_array = array( );
     $query = 'INSERT INTO "obsxemp" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	   array_push($fields_array,' "'.$v.'" ');
	   array_push($values_array," '".${$v}."' ");
	   }

      if(sizeof($fields_array)){
	   $query .= implode(", ",$fields_array);
	   $query .= ") VALUES (".implode(", ",$values_array) ;
	   }
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	   $mess = "<center><b>No se pudo adicionar La observaci&oacute;n por un fallo en el sistema</b></center>".$query;
	   $mess_cod = "alert";
	   $s_opc="default";
	   }
	else{
			    
	    $mess = "<center><b>El estudio fu&eacute; adicionado con &eacute;xito </b><br></center>
	    Haga click <a href='$PHP_SELF?opc=usuario&s_opc=default'>&nbsp;&nbsp;<b>aqu&iacute;</b></a> para ingresar otro Usuario";
	    $mess_cod = "info";
	    $s_opc = "busqueda";
	    } 
	  
      }
   
    //$mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login); 
 
}

 /**********************************************************/
  // Proposito:Actualizar datos  de un empleado
  // return: arreglos con resultados de la actualizacion.
  /**********************************************************/
  function modificar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

$this->fv->resetErrorList();	 

       $this->fv->isNumber("emp_salario", "Ingrese solo n&uacute;meros, sin puntos y sin signos");
       $this->fv->isNumber("emp_gastosrep", "Ingrese solo n&uacute;meros, sin puntos y sin signos");
    //   $this->fv->isEmailAddress("emp_mail", "Ingrese una direcci&oacute;n de correo electr&oacute;nico valida");
      	
     if ($this->fv->isError() ) {
        $mess = $this->fv->getMessage();
        $mess_cod = "alert";
           
        } 
 
      else {
   
  $fields = array ("usu_id", "emp_fechanac", "emp_lugarnac", "emp_direccion", "emp_tel", "emp_fax", "emp_mail", "emp_tipovin", "emp_fechavin", "emp_categoria", "emp_salario", "emp_gastosrep",  "emp_investiga", "emp_areaint" );
					      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "empleado" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE emp_id = '$emp_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "<b>No se pudo actualizar los datos del empleado por un fallo en el sistema</b>".$query;
		$mess_cod = "alert";		
	}else{
	       logs::crear("usuario", "modificar", $query);
	 	$mess = "<b>Los cambios $login fueron realizados con &eacute;xito</b> ".$query; 
	    	$mess_cod ="info";
	    	$s_opc = "info";
	 } 
	  }
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }

/**** Validar usuarios ****/
function validar($usuario, $password){
	global $table;
	
	$encriptado = md5($password);
	
	$query="SELECT * FROM usuario WHERE usu_login='$usuario' AND  usu_password='$encriptado' AND \"usu_estadoUsuario\" = 't'";
       
       $table->search($query);	
       
       if ($table->nfound){
            $this->datos=$table->sql_fetch_object();
            return 1;
       } else{
              logs::crear("usuario", "validar", $query);
       	$mess="Usuario o Login incorrecto, por favor verifique que las mayusculas no esten activadas";
       	$mess_cod="alert";
       	return 0;
       }       
//    return array("mess"=>$mess.$query,"mess_cod"=>$mess_cod,"datos"=>$datos);
	
}


/**** Validar usuarios ****/
function validarUsuario($usuario, $password, $pac_id, $tipo=0, $tipo1=0){
	global $table;
	
	$encriptado = md5($password);
	
	$query="SELECT * FROM usuario WHERE usu_login='$usuario' AND  usu_password='$encriptado' AND tiu_id=$tipo";
       
       $table->search($query);	
       
       if ($table->nfound){
            $this->datos=$table->sql_fetch_object();
            $valido=1;
       } else{
              logs::crear("usuario", "validarUsuario", $query);
       	$mess="Usuario o Login incorrecto, por favor verifique que las may&uacute;sculas no est&eacute;n activadas";
       	$mess_cod="alert";
       	$valido=0;
       }       
   return array("mess"=>$mess,"mess_cod"=>$mess_cod,"valido"=>$valido, "usu_id"=>$this->datos->usu_id, "usu_nombres"=>$this->datos->usu_nombres,"usu_apellidos"=>$this->datos->usu_apellidos);
	
}


/********************* Buscar usuarios******************/

function buscar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);



             $fields = array ( "tid_id", "tiu_id", "bod_id", "usu_estadoUsuario", "usu_nombres", "usu_apellidos", "usu_numeroIdentificacion",
	                               "usu_lugarExpedicion", "usu_direccion", "usu_telefono", "usu_login");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'SELECT  * FROM  "usuario" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($where_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($where_array))
	$query .= " WHERE ".implode(", ",$where_array);
	
      	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No hay registros que concuerden con su busqueda".$query;
		$mess_cod = "alert";		
	}else{
	      	$mess = "El usuario $login fue adicionado con &eacute;xito ".$query; 
	    	$s_opc = "info_usuario";
	 } 
	return $result;
   // return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }


  /************** Modificar Usuarios ****************/
  function modificarusu(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

     /********************/

    $fields = array ( "tid_id", "tiu_id", "bod_id", "usu_estadoUsuario", "usu_nombres", "usu_apellidos", "usu_numeroIdentificacion",
	                       "usu_lugarExpedicion", "usu_direccion", "usu_telefono", "usu_login", "usu_password");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "usuario" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE usu_id = '$usu_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "<b>No se pudo adicionar el usuario por un fallo en el sistema</b>";
		$mess_cod = "alert";		
	}else{
	       logs::crear("usuario", "modificar", $query);
	 	$mess = "<b>Los cambios $login fueron adicionado con &eacute;xito</b> "; 
	    	$mess_cod ="info";
	    	$s_opc = "info";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }
  
  /*****************Cambio de password*********************/
  
  function cambiarclave($usu_login){
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;

    foreach($_GET as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;
      
      $this->fv->resetErrorList();	 
      $this->fv->isEqualsString("usu_password","usu_passwordC","La clave y la confirmaci&oacute;n no coinciden.");
      $this->fv->isMoreThanSize("usu_password",6,"La clave debe contener al menos 6 caracteres..");
      
      	
     if ($this->fv->isError() ) {
        $mess = $this->fv->getMessage();
        $mess_cod = "alert";
        } 
    else {
	      if(!$this->validar($usu_login,$password)) {
		    $mess = "<center><b>La Contrase&ntilde;a es inv&aacute;lida </b></center>";
		    $mess_cod = "alert";
		    }
	      else {
		
			$usu_password = md5($usu_password);    
			// Arreglos para datos del query y el url para paginacion
			$set_array = array();
			$query = "UPDATE  usuario SET usu_password='$usu_password' WHERE usu_login = '$usu_login'";
			
			$result=$table->sql_query($query);
			
			if (!$result){
			  $mess = "<b>No se pudo modificar la clave por un fallo en el sistema.<br> Por favor contacte al administrador del sistema</b>".$query;
			  $mess_cod = "alert";		
			}else{
			  logs::crear("usuario", "cambiarClave", $query);
			  $mess = "<b>La clave fu&eacute; modificada con &eacute;xito</b> "; 
			  $mess_cod ="info";
			  $s_opc = "info";
			} 
		  
			//$mess .= $query;
			//return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
	
	      }
    }
     return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }
}
?>