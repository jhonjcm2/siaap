<?
/**************************************************************************/
// CLASE: odontograma
// Proposito: almacena los datos del odontograma de un paciente.
// Ultima modificacion: octubre de 2003
// /************************************************************************/
class odontograma{
  var $fv;
  var $data;
	
  function odontograma($id=-1){
    global $table;
    
    $this->fv = new FormValidator;
    if ($id >= 0){
    	$query = "SELECT * 
        	     FROM odontograma
        	     WHERE odg_id='$id' ";
	$table->sql_query($query);
    	$this->data = $table->sql_fetch_object();
    }
    
  }

  /**********************************************************/
  // Proposito: Creacion del odontograma
  // return: arreglos con resultados de la creacion.
  /**********************************************************/
  function crear(){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;
      $odg_fecha=date("Y-m-d",time());
      //$odg_fechaFinal=date("Y-m-d",time());
  
if ( 0 ){
      $mess = $this->fv->getMessage();
     	$mess_cod = "alert";
     	}else {
    	
 		$fields = array("odg_id", "tio_id", "pac_id", "pto_id", "odg_mapa1", "odg_mapa2", "odg_idInicial",
         				"odg_fecha", "odg_fechaFinal", "odg_comentarios", "odg_mapa3", 
         				"odg_mapa4", "odg_mapa5", "odg_mapa6", "odg_mapa7", "odg_mapa8");
							      
       	// Arreglos para datos del query y el url para paginacion
      		$fields_array = array();
      		$values_array = array();
      		$query = 'INSERT INTO "odontograma" (';
      		// Adicionando los campos para el query
      		foreach($fields as $v)
			if(${$v}!=""){
	  		array_push($fields_array,' "'.$v.'" ');
	  		array_push($values_array," '".${$v}."' ");
			}

      		if(sizeof($fields_array)){
			$query .= implode(", ",$fields_array);
			$query .= ") VALUES (".implode(", ",$values_array) ;
		}
		$query .=")";
	
		$result=$table->sql_query($query);
		if (!$result){
		$mess = "No se pudo adicionar el odontograma por un fallo en el sistema. Contacte al Administrador del Sistema";
		$mess_cod = "info";		
		}else{
		       logs::crear("odontograma", "crear", $query);
	    		$mess = "El odontograma $login fue adicionado con &eacute;xito ".$query; 
	    		$mess_cod = "info";
	 		} 
   		
	}
	return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
}


/********************* Buscar un odontograma******************/

function buscar(){
    global $table, $_POST ,$_GET;

    foreach($_POST as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($_GET as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);



        $fields = array("odg_id", "tio_id", "pac_id", "pto_id", "odg_mapa1", "odg_mapa2", "odg_idInicial",
         				"odg_fecha", "odg_fechaFinal", "odg_comentarios", "odg_mapa3", 
         				"odg_mapa4", "odg_mapa5", "odg_mapa6", "odg_mapa7", "odg_mapa8")  ;
							      
       // Arreglos para datos del query y el url para paginacion

      $where_array = array();
     $query = 'SELECT * FROM  "odontograma" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($where_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($where_array))
	$query .= " WHERE ".implode(" AND ",$where_array);
	
      	$result=$table->search($query);
	
	if (!$result){
		$mess = "No hay registros que concuerden con su busqueda.";
		$mess_cod = "alert";		
	}else{
	       $mess = "Este es el resultado de su b&uacute;squeda "; 
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }

/****************************Ingresar Datos Reevaluación****************************/

 function actualizar($odg_idc=0){
    global $table;

    foreach($_POST as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($_GET as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

  $odg_id=($odg_idc)?$odg_idc:$odg_id;
 
   $query= "SELECT * FROM odontograma WHERE odg_id=$odg_id";
   $table->search($query);
   $odg= $table->sql_fetch_object();
if ($odg_id==""){

      $mess = "No se pudo crear el odontograma por un fallo en el sistema";
      $mess_cod = "alert";
    }else {
    	$mapa1=unserialize($odg->odg_mapa1);
    	$mapa2=unserialize($odg->odg_mapa2);
    	$mapa3=unserialize($odg->odg_mapa3);
    	$mapa4=unserialize($odg->odg_mapa4);
    	$mapa5=unserialize($odg->odg_mapa5);
    	$mapa6=unserialize($odg->odg_mapa6);
    	$mapa7=unserialize($odg->odg_mapa7);
    	$mapa8=unserialize($odg->odg_mapa8);
    	if (isset($diente)){
	    	$nummapa = substr($diente,0,1);
	    	$numdiente=substr($diente,1,1);
	    	$odg_mapa= "odg_mapa$nummapa";
	    	$mapanum="mapa$nummapa";
	    	$odg_mapanum="odg_mapa$nummapa";
	    	${$mapanum}=unserialize($odg->$odg_mapa);
	    	$medio=($numdiente<=3)?$incisal:$oclusal;
	    	if($convencion==3 || $convencion==4){
	    	   $palatino+=$palatino;
	    	   $mesial+=$mesial;
	    	   $vestibular+=$vestibular;
	    	   $distal+=$distal;
	    	   $medio+=$medio;
	    	   $lingual+=$lingual;
	    	   $incisal+=$incisal;
	    	}
	    	if(${$mapanum}){
		  if($nummapa==1 || $nummapa==5)
		    ${$mapanum}[$diente]="$convencion,$vestibular,$mesial,$palatino,$distal,$medio";
		  if($nummapa==2 || $nummapa==6)
		    ${$mapanum}[$diente]="$convencion,$vestibular,$distal,$palatino,$mesial,$medio";
		  if($nummapa==8 || $nummapa==4)
		    ${$mapanum}[$diente]="$convencion,$lingual,$mesial,$vestibular,$distal,$medio";
		  if($nummapa==7 || $nummapa==3)
		    ${$mapanum}[$diente]="$convencion,$lingual,$distal,$vestibular,$mesial,$medio";
		  }

	    	else{
		  if($nummapa==1 || $nummapa==5)
		    $mapa[$diente]="$convencion,$vestibular,$mesial,$palatino,$distal,$medio";
		  if($nummapa==2 || $nummapa==6)
		    $mapa[$diente]="$convencion,$vestibular,$distal,$palatino,$mesial,$medio";
		  if($nummapa==8 || $nummapa==4)
		    $mapa[$diente]="$convencion,$lingual,$mesial,$vestibular,$distal,$medio";
		  if($nummapa==7 || $nummapa==3)
		    $mapa[$diente]="$convencion,$lingual,$distal,$vestibular,$mesial,$medio";
		  //$mapa[$diente]="$convencion";
		  ${$mapanum}=$mapa;
	    	}
	    	${$odg_mapanum}=serialize(${$mapanum});
	}
	
	$fields = array("tio_id", "pac_id", "pto_id", "odg_mapa1", "odg_mapa2", 	"odg_fecha", "odg_idInicial",
        				"odg_fechaFinal", "odg_comentarios", "odg_mapa3", 	"odg_mapa4",
        				 "odg_mapa5", "odg_mapa6", "odg_mapa7", "odg_mapa8")  ;

       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "odontograma" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE odg_id = '$odg_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo actualizar el paciente por un fallo en el sistema, por Favor llame a Holmes y nathalia que hicieron la aplicacion.".$query;
		$mess_cod = "alert";		
	}else{
	       logs::crear("odontograma", "actualizar", $query);
	 	$mess = "<center><b>El odontograma fue actualizado con &eacute;xito</b><br></center>".$query; 
	    	$mess_cod = "info";
	    	$s_opc = "busqueda";
	 } 
	}
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }
  
/************** Borrar un odontograma ****************/
 
  function borrar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

     
     	$fields = array ("tio_id", "pto_id", "odg_mapa1", "odg_mapa2", 
         				"odg_fecha", "odg_fechaFinal", "odg_comentarios", "odg_mapa3", 
         				"odg_mapa4", "odg_mapa5", "odg_mapa6", "odg_mapa7", "odg_mapa8");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'DELETE FROM "odontograma" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE odg_id =$odg_id";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo cancelar el odontograma por un fallo en el sistema, Comun&iacute;quese con el administrador.".$query;
		$mess_cod = "alert";		
	}else{
	       logs::crear("odontograma", "borrar", $query);
	 	$mess = "<center><b>El odontograma $login fu&eacute; cancelado </b><br>
	 	Vuelva a ingresarlo.</center> "; 
	    	$mess_cod = "info";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }
  
  
/******************************************/    
 function validar(){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
     

    foreach($_GET as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      
      
	$query= 'SELECT * FROM odontograma WHERE "pac_id"='.$pac_id.' AND tio_id=1';
   	$table->search($query);
   	$odon = $table->sql_fetch_object();
   	if($table->nfound){
   		$mess = "<b>El paciente ya tiene odontograma inicial.</b>";
   		$mess_cod = "info";
   		$destino="0";
		$query= 'SELECT * FROM odontograma WHERE "pac_id"='.$pac_id.' AND tio_id=2';
	   	$table->search($query);
	   	$odon = $table->sql_fetch_object();
	   	if($table->nfound) {
			$mess ="<b>El paciente ya tiene odontograma de desarrollo.</b>";
			$mess_cod = "info";
      			$destino="1";
      		}
      		
      	}else{
      	              logs::crear("odontograma", "validar", $query);
      			if(!$table->nfound) {
				$mess ="<b>El paciente no tiene odontograma.</b>";
				$mess_cod = "info";
      				$destino="2";
      			}
     		}
      	
       return array("mess"=>$mess,"mess_cod"=>$mess_cod,"destino"=>$destino );
	}
}
?>