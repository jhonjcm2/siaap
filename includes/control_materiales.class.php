<?
/*********************************************************************/
// CLASE: control de materiales
// Proposito: Realizar el control de materiales en las clinicas.
// Ultima modificacion: 22 de mayo de 2003
// /******************************************************************/
class control_materiales{
  var $fv;
  var $data;
	
  function control_materiales($id=-1){
  global $table;
    
    $this->fv = new FormValidator;
    if ($id >= 0){
    	$query = "SELECT * 
        	     FROM control_materiales
        	     WHERE com_id='$id' ";
	$table->sql_query($query);
    	$this->data = $table->sql_fetch_object();
    }
    
  }

  /**********************************************************/
  // Proposito: Creacion del control de materiales
  // return: arreglos con resultados de la creacion.
  /**********************************************************/
  function crear(){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;

    foreach($_GET as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;
      
      $this->fv->resetErrorList();
      $this->fv->isEmpty("mat_id", "Seleccione un material para agregar");
      $this->fv->isNumber("mxc_cantidadMateriales","Ingrese una cantidad de materiales v&aacute;lida",1,1000000);
      
      
   if ( $this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    }else {
      $query = 'SELECT * 
                FROM control_materiales
                WHERE "com_id"='.$com_id;
      $table->search($query);
    
      if($table->nfound) {
	$mess = "Un control de materiales con ese documento ya se encuentra registrado en el sistema, por favor escoja otro.";
	$mess_cod = "alert";
	$s_opc = "adicionar_usuario";
      }
      else {
	/*  se pasa a la quota a bytes */
	// $com_fechaElaboracion="$fn_anio-$fn_mes-$fn_dia";
         $fields = array ("com_id", "pac_id", "bod_id", "com_fecha", "com_comentario", "com_devolucion", 
                                       "com_autorizado", "com_despachado", "com_fechaElaboracion");
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "control_materiales" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "<b>No se pudo adicionar el control de materiales por un fallo en el sistema, Contacte al administrador.</b>";
	$mess_cod = "alert";		
	}else{
	    logs::crear("logs", "crear", $query);
	    $mess = "<b>El control de materiales fue adicionado con &eacute;xito. </b> "; 
	    $mess_cod = "info";
	    $s_opc = "busqueda";
	 } 
	  
      }
    }
    $mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }

  /********************* Buscar un control de materiales******************/

function buscar(){
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($_GET as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);


             $fields = array ( "pac_id", "bod_id", "com_fecha", "com_comentario", "com_devolucion","com_autorizado", "com_despachado");

       // Arreglos para datos del query y el url para paginacion

      $where_array = array();
     $query = 'SELECT * FROM  "control_materiales" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($where_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($where_array))
	$query .= " WHERE ".implode(" AND ",$where_array);
	
      	$result=$table->search($query);
	
	if (!$result){
		$mess = "No hay registros que concuerden con su busqueda.".$query;
		$mess_cod = "alert";		
	}else{
	       //logs::crear("logs", "buscar", $query);
	 	$mess = "El control de materiales fue adicionado con &eacute;xito ".$query.$table->nfound; 
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }

/***********almacenar en la tabla matxcom********************/


function agregarMate($com_id=0){

global $table, $_POST, $_GET;

	foreach($_POST as $k=>$v)
		//${$k} = base::dispelMagicQuotes($v);
		${$k} = $v;

	foreach($_GET as $k=>$v)
		// ${$k} = base::dispelMagicQuotes($v);
		${$k} = $v;


	// if ( 0 /*$this->fv->isError()*/ ) {
	if ( 0 ){
		$mess = $this->fv->getMessage();
		$mess_cod = "alert";
	}else {
		/*  se pasa a la quota a bytes */
		$fields = array ("mat_id", "com_id", "mxc_cantidadMateriales");
	
	// Arreglos para datos del query y el url para paginacion
		$fields_array = array();
		$values_array = array();
		$query = 'INSERT INTO "matxcom" (';
	// Adicionando los campos para el query
		foreach($fields as $v)
			if(${$v}!=""){
				array_push($fields_array,' "'.$v.'" ');
				array_push($values_array," '".${$v}."' ");
			}
	
		if(sizeof($fields_array)){
			$query .= implode(", ",$fields_array);
			$query .= ") VALUES (".implode(", ",$values_array) ;
		}
		$query .=")";
	
		$result=$table->sql_query($query);
	
		if (!$result){
			$mess = "No se pudo adicionar el material por un fallo en el sistema, Contacte al Administrador del Sistema.";
			$mess_cod = "alert";
		}else{
			logs::crear("logs", "agregarMate", $query);
			$mess = "<b>Un control de materiales $login fue  adicionado con &eacute;xito</b>";
			$mess_cod = "info";
		}
	}
	//$mess .= $query;
	return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
}
/** :) Permite actualizar una bodega al despachar un material *************************/
function despachar($bod_id)
{
	global $table, $_GET, $_POST;
	
	$table2 = new my_DB();
		
	foreach($_POST as $k=>$v)
		${$k} = base::dispelMagicQuotes($v);

	foreach($_GET as $k=>$v)
		${$k} = base::dispelMagicQuotes($v);

	$bodega=new Bodega();
	return $bodega->despacharCM($com_id, $bod_id);
}

/** Permite actualizar una bodega al recibir un material****************/
    function recibir()
    {
       global $table, $_GET, $_POST;
       
       $table2 = new my_DB();
        
	foreach($_POST as $k=>$v)
		${$k} = base::dispelMagicQuotes($v);

	foreach($_GET as $k=>$v)
		${$k} = base::dispelMagicQuotes($v);
		
	$query = "SELECT * FROM matxcom WHERE  com_id=$com_id";
	$table->sql_query($query);
	//$bod_id=$this->data->bod_id;
	$mess="";
	
	while($obj=$table->sql_fetch_object()){
		$mat_id=$obj->mat_id;
		
		/***** Valido que el material exista en el almacen ****/
		
		$query = "SELECT * FROM matxbod WHERE mat_id=$mat_id AND bod_id=$bod_id";
	       $table2->sql_query($query);
 	       $row2 = $table2->sql_fetch_object();
		if (!$row2){
			$query="SELECT * FROM materiales WHERE mat_id=$mat_id";
			$table2->sql_query($query);
			$row = $table2->sql_fetch_object();
			$mess .= "El material no est&aacute; registrado en esta bodega ".$row->mat_descripcion." <br>";
			$mess_cod="alert";
		}
		else{
		       //logs::crear("logs", "recibir", $query);
			$resta = $row2->mxb_cantidadMaterial + $obj->mxc_cantidadMateriales;
  		      	
  		      	/**** Bajo los materiales de la bodega de origen ==1 ****/
  		      	
			//$cantMat=$row2->mxb_cantidadMaterial - $obj->mxc_cantidadMaterial;
			$query="UPDATE matxbod SET \"mxb_cantidadMaterial\"=$resta WHERE  mat_id=$mat_id AND bod_id=$bod_id";
			$result = $table2->sql_query($query);
		}
	}
	return Array("mess"=>$mess, "mess_cod" => $mess_cod); 
    }
    

    
 /************** Borrar un control de materiales ****************/
 
  function borrar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

     /********************/

    $fields = array ( "pac_id", "bod_id", "com_fecha", "com_comentario", "com_devolucion", 
                            "com_fechaAutorizar", "com_autorizado", "com_despachado", "com_tipo");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'DELETE FROM "control_materiales" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       /*if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);*/
	
      $query .=  " WHERE com_id =$com_id";

	$result=$table->sql_query($query);
//echo "<br>".$query."<br>";

/***********Borra el material por control*******/
$fields = array ( "mxc_id","mat_id","com_id","mxc_cantidadMateriales");

       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'DELETE FROM "matxcom" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       /*if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);*/
	
      $query .=  " WHERE com_id =$com_id";
//echo "<br>".$query."<br>";
	$result=$table->sql_query($query);
/*******************/
	
	if (!$result){
		$mess = "No se pudo cancelar el control de materiales por un fallo en el sistema, Comun&iacute;quese con el administrador.";
		$mess_cod = "alert";		
	}else{
	       //logs::crear("logs", "borrar", $query);
	 	$mess = "El control de materiales $login fu&eacute; eliminado con &eacute;xito "; 
	    	$mess_cod = "info";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }

/***********borrar de la tabla matxcom un control de materiales**************/

function borrarMate($com_id=0){

global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;


//   if ( 0 /*$this->fv->isError()*/ ) {
if ( 0 ){
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
      }
      else {
        /*  se pasa a la quota a bytes */
         $fields = array ("mat_id", "com_id", "mxc_cantidadMateriales");

       // Arreglos para datos del query y el url para paginacion
      	$fields_array = array();
      	$values_array = array();
     	$query = 'DELETE FROM "matxcom" ';
     	// Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE com_id = '$com_id'";

	$result=$table->sql_query($query);
    
        if (!$result){
        $mess = "No se pudo cancelar el control de material por un fallo en el sistema, Contacte al Administrador del Sistema.";
        $mess_cod = "alert";
        }else{
              //logs::crear("logs", "borrarMate", $query);
            	$mess = "<center><b>El control de materiales $login fu&eacute; cancelado</b><br>
            	Vuelva a ingresarlo</center>";
	 	$mess_cod = "info";
         }

      }
    //$mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }

 /************** Borrar una devolucion de materiales  antes de ser acentada****************/
 
  function borrar1(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

     /********************/

    $fields = array ( "pac_id", "bod_id", "com_fecha", "com_comentario", "com_devolucion", 
                            "com_fechaAutorizar", "com_autorizado", "com_despachado", "com_tipo");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'DELETE FROM "control_materiales" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE com_id =$com_id";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo cancelar el control de materiales por un fallo en el sistema, Comun&iacute;quese con el administrador.";
		$mess_cod = "alert";		
	}else{
	       logs::crear("control_materiales", "borrar1", $query);
	 	$mess = "El control de materiales $login fu&eacute; eliminado con &eacute;xito ".$query; 
	    	$mess_cod = "info";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }

/***********borrar de la tabla matxcom  una devolucion de materiales***************/

function borrarMate1($com_id=0){

global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;


//   if ( 0 /*$this->fv->isError()*/ ) {
if ( 0 ){
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
      }
      else {
        /*  se pasa a la quota a bytes */
         $fields = array ("mat_id", "com_id", "mxc_cantidadMateriales");

       // Arreglos para datos del query y el url para paginacion
      	$fields_array = array();
      	$values_array = array();
     	$query = 'DELETE FROM "matxcom" ';
     	// Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE com_id = '$com_id'";

	$result=$table->sql_query($query);
    
        if (!$result){
        $mess = "No se pudo cancelar el control de material por un fallo en el sistema, Contacte al Administrador del Sistema.";
        $mess_cod = "alert";
        }else{
              logs::crear("control_materiales", "borrarMate1", $query);
            	$mess = "<center><b>La devoluci&oacute;n de materiales $login fu&eacute; cancelada</b><br>
            	Vuelva a ingresarla</center>";
	 	$mess_cod = "info";
         }

      }
    //$mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }


 /*************Funciones que manejan el inventario de las diferentes áreas *********************************************/
function crearcma(){	
	global $table, $_POST, $_GET;
	
	foreach($_POST as $k=>$v)
	${$k} = base::dispelMagicQuotes($v);
	//${$k} = $v;
	
	foreach($_GET as $k=>$v)
	${$k} = base::dispelMagicQuotes($v);
	//${$k} = $v;
	
	$this->fv->resetErrorList();
	
	$this->fv->isNumber("mxa_cantidad","Ingrese una cantidad de materiales v&aacute;lida",1,1000000);
		
	
	if ( $this->fv->isError() ) {
		$mess = $this->fv->getMessage();
		$mess_cod = "alert";
	}else {
		$query = 'SELECT * FROM control_mat_area WHERE "cma_id"='.$cma_id;
		$table->search($query);
	
		if($table->nfound) {
			$mess = "Un control de materiales con ese documento ya se encuentra registrado en el sistema, por favor escoja otro.";
			$mess_cod = "alert";
			$s_opc = "adicionar_usuario";
		}
		else {
			/*  se pasa a la quota a bytes */
			$fields = array ("usu_id", "pac_id", "cma_fecha");
										
			// Arreglos para datos del query y el url para paginacion
			$fields_array = array();
			$values_array = array();
			$query = 'INSERT INTO "control_mat_area" (';
			// Adicionando los campos para el query
			foreach($fields as $v)
				if(${$v}!=""){
				array_push($fields_array,' "'.$v.'" ');
				array_push($values_array," '".${$v}."' ");
			}
			
			if(sizeof($fields_array)){
				$query .= implode(", ",$fields_array);
				$query .= ") VALUES (".implode(", ",$values_array) ;
			}
			$query .=")";
			
			$result=$table->sql_query($query);
				
			if (!$result){
				$mess = "<b>No se pudo adicionar el control de materiales por un fallo en el sistema, Contacte al administrador.</b>";
				$mess_cod = "alert";		
			}else{
				logs::crear("logs", "crear", $query);
				$mess = "<b>El control de materiales fue adicionado con &eacute;xito. </b> "; 
				$mess_cod = "info";
				$s_opc = "busqueda";
			}
		}
	}
	$mess .= $query;
	return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
}
/***********almacenar en la tabla matxcma********************/


function agregarmxa($cma_id=0){

global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;


//   if ( 0 /*$this->fv->isError()*/ ) {
if ( 0 ){
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
      }
      else {
        /*  se pasa a la quota a bytes */
         $fields = array ("mat_id", "cma_id", "mxa_cantidad");

       // Arreglos para datos del query y el url para paginacion
      	$fields_array = array();
      	$values_array = array();
     	$query = 'INSERT INTO "matxcma" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
        if(${$v}!=""){
          array_push($fields_array,' "'.$v.'" ');
          array_push($values_array," '".${$v}."' ");
        }

      if(sizeof($fields_array)){
        $query .= implode(", ",$fields_array);
        $query .= ") VALUES (".implode(", ",$values_array) ;
        }
        $query .=")";

        $result=$table->sql_query($query);

        if (!$result){
        $mess = "No se pudo adicionar el material por un fallo en el sistema, Contacte al Administrador del Sistema.";
        $mess_cod = "alert";
        }else{
            logs::crear("logs", "agregarMate", $query);
            $mess = "<b>Un control de materiales $login fue  adicionado con &eacute;xito</b>";
	     $mess_cod = "info";
         }

      }
    //$mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }

 /************************permite actualizar una bodega al utilizar o gastar  un material*************************/
    function actualizarInv($bod_id, $cma_id)
    {
       global $table, $_GET, $_POST;
       
       $table2 = new my_DB();
        
	foreach($_POST as $k=>$v)
		${$k} = base::dispelMagicQuotes($v);

	foreach($_GET as $k=>$v)
		${$k} = base::dispelMagicQuotes($v);
		
	$query = "SELECT * FROM materiales t, matxcma m, matxbod b WHERE t.mat_id=m.mat_id and  m.mat_id=b.mat_id and cma_id=$cma_id AND bod_id=$bod_id";
	$table2->search($query);
	while($obj=$table2->sql_fetch_object()){
	//$mess = $query;
	
	
	/***** Valido que el material exista en el almacen ****/
		$unidad=$obj->mxb_cantidadMaterial*$obj->mat_unidades;
		$unidadR=$unidad-$obj->mxb_unidadesDespachadas;
		$gasto=$unidadR-$obj->mxa_cantidad;
		$unidadDes=$obj->mxb_unidadesDespachadas+$obj->mxa_cantidad;
		//$mess = $gasto;
 	     	if($gasto>=0){
 	     	
 	        /**** Bajo los materiales de la bodega de origen ==1 ****/
  		
  		$query="UPDATE matxbod SET \"mxb_unidadesDespachadas\"=$unidadDes WHERE  mat_id=$obj->mat_id AND bod_id=$bod_id";
			$result = $table2->sql_query($query);
			
			if($unidadDes>=$obj->mat_unidades){
			$cantidad=$obj->mxb_cantidadMaterial-1;
			$unidadDes1=$unidadDes-$obj->mat_unidades;
			//$mess = $unidadDes1;
			$query="UPDATE matxbod SET \"mxb_cantidadMaterial\"=$cantidad,  \"mxb_unidadesDespachadas\"=$unidadDes1 WHERE  mat_id=$obj->mat_id AND bod_id=$bod_id";
			$result = $table2->sql_query($query);
			
  		}
  	
	 	}else{
			$query="SELECT * FROM materiales WHERE mat_id=$obj->mat_id";
			$table2->sql_query($query);
			$row = $table2->sql_fetch_object();
			$mess .= "No hay suficientes existencias del material ".$row->mat_descripcion." <br>";
			$mess_cod="alert";
		}
	
	}
	
	$mess = "<b>Su Inventario Fue Actualizado con &Eacute;xito</b>";
	$mess_cod = "info";
	
	return Array("mess"=>$mess, "mess_cod" => $mess_cod); 
    
    }

/***********borrar de la tabla control_mat_area  un control antes de ser acentado***************/

function borrarcma($cma_id=0){

global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;


//   if ( 0 /*$this->fv->isError()*/ ) {
if ( 0 ){
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
      }
      else {
        /*  se pasa a la quota a bytes */
         $fields = array ("cma_id", "cma_fecha", "usu_id");

       // Arreglos para datos del query y el url para paginacion
      	$fields_array = array();
      	$values_array = array();
     	$query = 'DELETE FROM "control_mat_area" ';
     	// Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE cma_id = '$cma_id'";

	$result=$table->sql_query($query);
    
        if (!$result){
        $mess = "No se pudo cancelar el control de material por un fallo en el sistema, Contacte al Administrador del Sistema.".$query;
        $mess_cod = "alert";
        }else{
              logs::crear("control_materiales", "borrarMate1", $query);
            	$mess = "<center><b>El control de materiales $login fu&eacute; cancelado</b><br>
            	Vuelva a ingresarla</center>";
	 	$mess_cod = "info";
         }

      }
    //$mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }

/***********borrar de la tabla matxcma un control de materiales***************/

function borrarmxa($cma_id=0){

global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;


//   if ( 0 /*$this->fv->isError()*/ ) {
if ( 0 ){
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
      }
      else {
        /*  se pasa a la quota a bytes */
         $fields = array ("mat_id", "cma_id", "mxa_cantidad");

       // Arreglos para datos del query y el url para paginacion
      	$fields_array = array();
      	$values_array = array();
     	$query = 'DELETE FROM "matxcma" ';
     	// Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE cma_id = '$cma_id'";

	$result=$table->sql_query($query);
    
        if (!$result){
        $mess = "No se pudo cancelar el control de material por un fallo en el sistema, Contacte al Administrador del Sistema.".$query;
        $mess_cod = "alert";
        }else{
              logs::crear("control_materiales", "borrarMate1", $query);
            	$mess = "<center><b>El control de materiales $login fu&eacute; cancelado</b><br>
            	Vuelva a Ingresarlo</center>";
	 	$mess_cod = "info";
         }

      }
    //$mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }

/************** Actualiza un control de materiales
			cuando la cantidad descargada sobrepasa la actual****************/
 
  function actualizarmxa(){
    global $table;

   foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      
      
       $this->fv->resetErrorList();	
     //  $this->fv->isEmpty("enf_comentario","Ingrese algun comentario");
      
    if ($this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    } else {
    
    
    $fields = array ("cma_id", "mat_id", "mxa_cantidad");
							      
       // Arreglos para datos del query y el url para paginacion
     
     $set_array = array();
     $query = 'UPDATE  "matxcma" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE mxa_id = '$mxa_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo adicionar el comentario por un fallo en el sistema, Por favor comun&iacute;quese con el administrador.".$query;
		$mess_cod = "alert";		
	}else{
	       logs::crear("diagnostico", "actualizar", $query);
	 	$mess = "<b>El Comentario  $login fue adicionado con &eacute;xito.</b>".$query; 
	    	$mess_cod = "alert";
	    	//$s_opc = "busqueda";
	 } 
      
    }
   return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }

/************** Actualiza un control de materiales
			cuando la cantidad no s la adecuada****************/
 
  function editarCom($com_id){
    global $table;

   foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      
      
       $this->fv->resetErrorList();	
     //  $this->fv->isEmpty("enf_comentario","Ingrese algun comentario");
      
    if ($this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    } else {
    
    
    $fields = array ("com_id", "mat_id", "mxc_cantidadMateriales");
							      
       // Arreglos para datos del query y el url para paginacion
     
     $set_array = array();
     $query = 'UPDATE  "matxcom" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE mxc_id = '$mxc_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo adicionar el comentario por un fallo en el sistema, Por favor comun&iacute;quese con el administrador.".$query;
		$mess_cod = "alert";		
	}else{
	       logs::crear("diagnostico", "actualizar", $query);
	 	$mess = "<b>El material fue modificado con &eacute;xito.</b>"; 
	    	$mess_cod = "alert";
	    	//$s_opc = "busqueda";
	 } 
      
    }
   return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }

/***********borrar de la tabla matxcom  una devolucion de materiales***************/

function borrarmxc($com_id){

global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;


//   if ( 0 /*$this->fv->isError()*/ ) {
if ( 0 ){
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
      }
      else {
        /*  se pasa a la quota a bytes */
         $fields = array ("mat_id", "com_id", "mxc_cantidadMateriales");

       // Arreglos para datos del query y el url para paginacion
      	$fields_array = array();
      	$values_array = array();
     	$query = 'DELETE FROM "matxcom" ';
     	// Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE com_id = '$com_id' and mxc_id='$mxc_id'";

	$result=$table->sql_query($query);
    
        if (!$result){
        $mess = "No se pudo cancelar el control de material por un fallo en el sistema, Contacte al Administrador del Sistema.";
        $mess_cod = "alert";
        }else{
              logs::crear("control_materiales", "borrarMate1", $query);
            	$mess = "<center><b>El material $login fu&eacute; cancelado</b><br>
            	Vuelva a ingresarla</center>";
	 	$mess_cod = "info";
         }

      }
    //$mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }


}
?>
