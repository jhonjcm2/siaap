<?
/**********************************************************/
// CLASE: Datos_rips
// Proposito: manejo y genraci�n de los rips.
// Ultima modificacion: oct de 2003
// /**********************************************************/
class rips{
  var $fv;
  var $data;
	
  function rips($id=-1){
    global $table;
    
    $this->fv = new FormValidator;
    if ($id >= 0){
    	$query = "SELECT * 
        	     FROM datos_rips 
        	     WHERE dar_id='$id' ";
	$table->sql_query($query);
    	$this->data = $table->sql_fetch_object();
    }
    
  }

  /**********************************************************/
  // Proposito: Ingreso de los datos que hacen parte del rips (Viene del formulario)
  // return: arreglos con resultados de la creacion.
  /**********************************************************/
  function crear(){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;
      

//   if ( 0 /*$this->fv->isError()*/ ) {
if ( 0 ){
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    }else {
      $query = 'SELECT *
                FROM datos_rips
                WHERE "dar_id"='.$dar_id;
      $table->search($query);
    
      if($table->nfound) {
	$mess = "<center>Un un reporte con esa identificaci&oacute;n ya se encuentra registrado en el sistema<br><br> <b>por favor ingrese otro.</b></center>";
	$mess_cod = "alert";
      }
      else {
if($f_dia && $f_mes && $f_anio)
       $dar_fechaInicialPeriodoFact="$f_dia-$f_mes-$f_anio";
if($ff_dia && $ff_mes && $ff_anio)
      $dar_fechaFinalPeriodoFact="$ff_dia-$ff_mes-$ff_anio";
if($fn_dia && $fn_mes && $fn_anio)
       $dar_fechaFactura="$fn_dia-$fn_mes-$fn_anio";
	/*  se pasa a la quota a bytes */
         $fields = array ("dar_id", "ips_id", "eps_id", "dar_numeroFactura", "dar_fechaFactura", "dar_fechaElaboracionRips", 
         "dar_fechaInicialPeriodoFact", "dar_fechaFinalPeriodoFact", "usu_id"); 

       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "datos_rips" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "<center><b>No se pudieron adicionar los datos por un fallo en el sistema,</b><br> Hable con el administrador";
	$mess_cod = "alert";		
	}else{
	    $mess = "<center><b>Los datos $login fueron adicionados con &eacute;xito </b><br><br></center>";
	   
	   
	    $mess_cod = "info";
	    //$s_opc = "info_usuario";
	 } 
	  
      }
    }
   $mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }



  /**********************************************************/
  // Proposito: Ingreso de los datos que hacen parte del rips (Viene del formulario)
  // return: arreglos con resultados de la creacion.
  /**********************************************************/
  function ap(){
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;
      

//   if ( 0 /*$this->fv->isError()*/ ) {
if ( 0 ){
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    }else {

	/*  se pasa a la quota a bytes */
         $fields = array ("dar_id", "ips_id", "eps_id", "dar_numeroFactura", "dar_fechaFactura", "dar_fechaElaboracionRips", 
         "dar_fechaInicialPeriodoFact", "dar_fechaFinalPeriodoFact", "usu_id"); 

       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
     $query = 'SELECT
     					"eps_nombreEps", "eps_codigoEps", "ips_nombreIps", "ips_codigoIps", usu_nombres, 
     					usu_apellidos, "usu_numeroIdentificacion",  "pro_nombreEspecialidad", "tid_tipo", 
     					"pac_numeroIdentificacion", evo_fecha, "evo_numeroAutorizacion", "cod_codigoDiagnostico", 
     					amp_id, fip_id, faq_id, "pto_codigoProcedimiento", "pto_valorProfesional", pxe.*
     		      FROM 
     		      			paciente pac, procxevo pxe, odontologos odo, evolucion evo, tipo_documento tid,
     					diagnostico dia, eps, ips,  usuario usu, procedimiento pto, profesional pro,
     					codigos_diagnostico cod, diaxcod dxc
     		      WHERE 
     		      			dia.pac_id=pac.pac_id AND pac.eps_id=eps.eps_id AND ips.ips_id=pac.ips_id AND
     		      			evo.pac_id=pac.pac_id AND evo.usu_id=usu.usu_id AND usu.usu_id=odo.usu_id AND
     		      			pro.odl_id=odo.odl_id AND pxe.evo_id=evo.evo_id AND pxe.pto_id=pto.pto_id AND
     		      			tid.tid_id=pac.tid_id AND dxc.dia_id=dia.dia_id AND cod.cod_id=dxc.cod_id AND 
     		      			dxc.dxc_diente=pxe.pxe_diente';
      // Adicionando los campos para el query
	
	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "<center><b>No se pudieron adicionar los datos por un fallo en el sistema,</b><br> Hable con el administrador";
		$mess_cod = "alert";		
	}else{
		$mess = "<center><b>Los datos $login fueron adicionados con &eacute;xito </b><br><br></center>";
	   
	   
	    $mess_cod = "info";
	    //$s_opc = "info_usuario";
	 } 
    }
   $mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }


}
?>
