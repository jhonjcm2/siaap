<?
/**********************************************************/
// CLASE: Logs 
// Proposito: Almacenar los diferentes reportes que se puedan presentar.
// Ultima modificacion: Oct de 2003
// /**********************************************************/
class logs{
  var $fv;
  var $data;
	
  function logs($log_id=0){
  global $table;
    
    $this->fv = new FormValidator;
    if ($log_id){
    	$query = "SELECT * 
        	     FROM logs
        	     WHERE log_id='$log_id' ";
	$table->sql_query($query);
    	$this->data = $table->sql_fetch_object();
    }
    
  }

  /**********************************************************/
  // Proposito: Almacenar los reportes generados por el uso de la aplicacion
  // return: arreglos con resultados de la creacion.
  /**********************************************************/
  function crear( $log_contexto, $log_accion, $log_query, $log_fecha, $usu_id){	
    global $table;
     
     $query = 'INSERT INTO "logs" ( "log_contexto", "log_accion", "log_query", "log_fecha", "usu_id") VALUES '." ('$log_contexto', '$log_accion', '$log_query', '$log_fecha', '$usu_id')";
	
	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "Fallo al insertar log, por Favor contacte a el Administrador.";
		$mess_cod = "alert";		
	}else{
	    //logs::crear("logs", "crear", $query);
	    $mess = "<b>Log insertado</b> "; 
	    $mess_cod = "info";
	 } 
 
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }

  /********************* Buscar una apertura******************/

function buscar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);


          $fields = array ("log_id", "log_contexto", "log_accion", "log_query", "log_fecha", "usu_id");
							      
       // Arreglos para datos del query y el url para paginacion

      $where_array = array();
     $query = 'SELECT * FROM  "logs" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($where_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($where_array))
	$query .= " WHERE ".implode(", ",$where_array);
	
      	$result=$table->search($query);
	
	if (!$result){
		$mess = "No hay registros que concuerden con su busqueda.".$query;
		$mess_cod = "alert";		
	}else{
	 	$mess = "".$query.$table->nfound; 
	    	$mess_cod = "info";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }
}
?>
