<?
/**************************************************************************************************/
// CLASE: Diagnostico
// Proposito: Almacenar los diferentes Diagnosticos que pueda presentar un paciente.
// Ultima modificacion: Octubre de 2003
// /***********************************************************************************************/
class diagnostico{
  var $fv;
  var $data;
	
  function diagnostico($pac_id=0){
  global $table;
    
    $this->fv = new FormValidator;
    if ($pac_id){
    	$query = "SELECT * 
        	     FROM diagnostico
        	     WHERE dia_id='$dia_id' ";
	$table->sql_query($query);
    	$this->data = $table->sql_fetch_object();
    }
    
  }

  /*****************************************************************/
  // Proposito: Almacenar los diagnosticos  de un paciente
  // return: arreglos con resultados de la creacion.
  /*****************************************************************/
  function crear(){	
  global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;

    foreach($_GET as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;
      
      $this->fv->resetErrorList();
      $this->fv->isWithinRange("tdi_id","Seleccione el tipo de diagn&oacute;stico",0,99);  
      $this->fv->isEmpty("cod_id","Seleccione un diagn&oacute;stico para agregar");
                
	   if ($this->fv->isError() ) {
	      $mess = $this->fv->getMessage();
	      $mess_cod = "alert";
	   }else {
      
			/*se pasa a la quota a bytes */
		         $fields = array ("dia_id", "pac_id", "tdi_id", "dia_fechaElaboracion");
									      
		       // Arreglos para datos del query y el url para paginacion
		      $fields_array = array();
		      $values_array = array();
		     $query = 'INSERT INTO "diagnostico" (';
		      // Adicionando los campos para el query
		      foreach($fields as $v)
			if(${$v}!=""){
			  array_push($fields_array,' "'.$v.'" ');
			  array_push($values_array," '".${$v}."' ");
		       }
	
	       if(sizeof($fields_array)){
			$query .= implode(", ",$fields_array);
			$query .= ") VALUES (".implode(", ",$values_array) ;
		       }
		$query .=")";
		
		$result=$table->sql_query($query);
		
		if (!$result){
			$mess = "No se pudo adicionar el diagn&oacute;stico por un fallo en el sistema, Por favor comuniquese con el administrador.";
			$mess_cod = "alert";		
		        }else{
		           logs::crear("diagnostico", "crear", $query);
			    $mess = "El diagn&oacute;stico fu&eacute; adicionado con &eacute;xito "; 
			    $mess_cod = "info";
			    //$s_opc = "busqueda";
	                  } 
	  
      }
 
    //$mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }


  /********************* Buscar un diagnostico******************/

function buscar(){
    global $table;

    /*foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);*/

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;


             $fields = array ( "dia_id", "pac_id", "tdi_id", "dia_fechaElaboracion");
							      
       // Arreglos para datos del query y el url para paginacion

      $where_array = array();
     $query = 'SELECT * FROM  "diagnostico" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($where_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($where_array))
	$query .= " WHERE ".implode(", ",$where_array);
	
      	$result=$table->search($query);
	
	if (!$result){
		$mess = "No hay registros que concuerden con su busqueda.";
		$mess_cod = "alert";		
	}else{
	       $mess = "La b&uacute;squeda fu&eacute; realizada con &eacute;xito."; 
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }

/************************************************************/
function agregarDiag($dia_id=0){


global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;
      
      
      $this->fv->resetErrorList();
      $this->fv->isEmpty("cod_id","Seleccione un diagn&oacute;stico para agregar");
      

   if ( $this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
      }
      else {
        /*  se pasa a la quota a bytes */
         $fields = array ("dxc_id", "dia_id", "cod_id","dxc_diente", "dxc_superficie");

       // Arreglos para datos del query y el url para paginacion
      	$fields_array = array();
      	$values_array = array();
     	$query = 'INSERT INTO "diaxcod" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
        if(${$v}!=""){
          array_push($fields_array,' "'.$v.'" ');
          array_push($values_array," '".${$v}."' ");
        }

      if(sizeof($fields_array)){
        $query .= implode(", ",$fields_array);
        $query .= ") VALUES (".implode(", ",$values_array) ;
        }
        $query .=")";

        $result=$table->sql_query($query);

        if (!$result){
        $mess = "No se pudo adiccionar el diagn&oacute;stico por un fallo en el sistema. Por favor contacte al administrador del sistema.";
        $mess_cod = "alert";
        }else{
              logs::crear("diagnostico", "agregarDiag", $query);
              $mess = "<b>el diagn&oacute;stico $login fu&eacute; adicionado con &eacute;xito</b>";
	 	$mess_cod = "info";
         }

      }
   // $mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }


/************** creacion segundo formulario****************/
  function actualizar(){
    global $table;

   foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      
      
       $this->fv->resetErrorList();	
       $this->fv->isEmpty("dia_ayudasDiagnosticas","Ingrese las ayudas diagn&oacute;sticas");
      
    if ($this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    } else {
    
    
    $fields = array ("dia_ayudasDiagnosticas", "dia_observacionGeneral");
							      
       // Arreglos para datos del query y el url para paginacion
     
     $set_array = array();
     $query = 'UPDATE  "diagnostico" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE dia_id = '$dia_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo adicionar el diagn&oacute;stico por un fallo en el sistema, Por favor comun&iacute;quese con el administrador.";
		$mess_cod = "alert";		
	}else{
	       logs::crear("diagnostico", "actualizar", $query);
	 	$mess = "<b>Los datos del diagn�stico $login fueron adicionados con &eacute;xito.</b>"; 
	    	$mess_cod = "info";
	    	//$s_opc = "busqueda";
	 } 
      
    }
   return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }

/************** creacion tercer formulario****************/
  function actualizarDef(){
    global $table;

   foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      
      
       $this->fv->resetErrorList();	
       $this->fv->isEmpty("dia_planTratamiento","Ingrese el plan de tratamiento");
       $this->fv->isEmpty("dia_pronosticoGeneral","Ingrese el pronostico general");
       $this->fv->isEmpty("dia_comentarioExaminador","Ingrese los comentarios del examinador");
      
    if ($this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    } else {
    
    
    $fields = array ("dia_planTratamiento", "dia_pronosticoGeneral", "dia_comentarioExaminador");
							      
       // Arreglos para datos del query y el url para paginacion
     
     $set_array = array();
     $query = 'UPDATE  "diagnostico" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE dia_id = '$dia_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo adicionar el diagn&oacute;stico por un fallo en el sistema, Por favor comun&iacute;quese con el administrador.".$query;
		$mess_cod = "alert";		
	}else{
	       logs::crear("diagnostico", "actualizar", $query);
	 	$mess = "<b>Los datos del diagn�stico $login fueron adicionados con &eacute;xito.</b>"; 
	    	$mess_cod = "info";
	    	//$s_opc = "busqueda";
	 } 
      
    }
   return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }

 /************** Borrar un diagn�stico antes de acentar su creacion****************/
 
  function borrar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

     /********************/

    $fields = array ("dia_id", "pac_id", "tdi_id", "dia_fechaElaboracion");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'DELETE FROM "diagnostico" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE dia_id =$dia_id";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo cancelar el diagn&oacute;stico por un fallo en el sistema, Comun&iacute;quese con el administrador.";
		$mess_cod = "alert";		
	}else{
	      	$mess = "El diagn&oacute;stico fu&acute;e cancelado con &eacute;xito ".$query; 
	    	$mess_cod = "info";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }

/***********borrar de la tabla matxcom  un control de materiales***************/
function borrarDiag($dia_id=0){


global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;


//   if ( 0 /*$this->fv->isError()*/ ) {
if ( 0 ){
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
      }
      else {
        /*  se pasa a la quota a bytes */
         $fields = array  ("dxc_id", "dia_id", "cod_id","dxc_diente");

       // Arreglos para datos del query y el url para paginacion
      	$fields_array = array();
      	$values_array = array();
     	$query = 'DELETE FROM "diaxcod" ';
     	// Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE dia_id = '$dia_id'";

	$result=$table->sql_query($query);
    
        if (!$result){
        $mess = "No se pudo cancelar el diagn&oacute; por un fallo en el sistema, Contacte al Administrador del Sistema.";
        $mess_cod = "alert";
        }else{
              logs::crear("diagnostico", "borrarDiag", $query); 
            	$mess = "<center><b>El diagn&oacute;stico $login fu&eacute; cancelado</b><br>
            	Vuelva a ingresarlo</center>";
	 	$mess_cod = "info";
         }

      }
    //$mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }


}
?>
