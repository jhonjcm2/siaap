<?
/**********************************************************/
// CLASE: evoluci�n
// Proposito: Realizar la evoluci�n del paciente.
// Ultima modificacion: octubre de 2003
// /**********************************************************/
class laboratorio{
  var $fv;
  var $data;
	
  function laboratorio($id=-1){
    global $table;
    
    $this->fv = new FormValidator;
    if ($id >= 0){
    	$query = "SELECT * 
        	     FROM orden_lab
        	     WHERE old_id='$id' ";
	$table->sql_query($query);
    	$this->data = $table->sql_fetch_object();
    }
    
  }
/**********************************************************/
  function crearpld(){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;
      
      
      	$this->fv->resetErrorList();
    	$this->fv->isEmpty("pld_codigo", "Debe ingresar el c&oacutedigo del procedimiento");
      	$this->fv->isEmpty("pld_descripcion", "Debe ingresar nombre del procedimiento");
      	$this->fv->isEmpty("pld_tipo", "Debe ingresar el tipo de procedimiento");
      	$this->fv->isNumber("pld_valor","Ingrese un valor valido");
             
   if ( $this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    }else {
      $query = 'SELECT * 
                FROM procedimiento_lab
                WHERE "pld_codigo"='.$pld_codigo;
      $table->search($query);
    
      if($table->nfound) {
	$mess = "Un procedimiento con ese c&oacute;digo ya se encuentra registrado en el sistema, por favor escoja otro.";
	$mess_cod = "alert";
	$s_opc = "adicionar_usuario";
      }
      else {
	/*  se pasa a la quota a bytes */
	// $old_fechaElaboracion="$fn_anio-$fn_mes-$fn_dia";
         $fields = array ("pld_codigo", "pld_descripcion", "pld_tipo", "pld_valor");
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "procedimiento_lab" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "<b>No se pudo adicionar el procedimiento por un fallo en el sistema, Comuniquese con el administrador del sistema.".$query;
	$mess_cod = "alert";		
	}else{
	    logs::crear("evolucion", "crear", $query);
	    $mess = "El procedimiento fu&eacute; adicionada con &eacute;xito "; 
	    $mess_cod = "info";
	 } 
	  
      }
    }
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }

/**********************************************************/
  function crearRld(){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;
      
      
      	$this->fv->resetErrorList();
  /*  	$this->fv->isEmpty("pld_codigo", "Debe ingresar el c&oacutedigo del procedimiento");
      	$this->fv->isEmpty("pld_descripcion", "Debe ingresar nombre del procedimiento");
      	$this->fv->isEmpty("pld_tipo", "Debe ingresar el tipo de procedimiento");
      	$this->fv->isNumber("pld_valor","Ingrese un valor valido");*/
             
   if ( $this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    }else {
      /*$query = 'SELECT * 
                FROM procedimiento_lab
                WHERE "pld_codigo"='.$pld_codigo;
      $table->search($query);
    
      if($table->nfound) {
	$mess = "Un procedimiento con ese c&oacuetedigo ya se encuentra registrado en el sistema, por favor escoja otro.";
	$mess_cod = "alert";
	$s_opc = "adicionar_usuario";
      }
      else {*/
	/*  se pasa a la quota a bytes */
	// $old_fechaElaboracion="$fn_anio-$fn_mes-$fn_dia";
         $fields = array ("rld_fecha");
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "recibo_rld" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "<b>No se pudo adicionar el procedimiento por un fallo en el sistema, Comuniquese con el administrador del sistema.".$query;
	$mess_cod = "alert";		
	}else{
	    logs::crear("laboratorio", "crear", $query);
	    $mess = "El procedimiento fu&eacute; adicionada con &eacute;xito "; 
	    $mess_cod = "info";
	 } 
	  
      }
  //  }
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }

  /**********************************************************/
  // Proposito: Creacion de la orden de laboratorio dental
  // return: arreglos con resultados de la creacion.
  /**********************************************************/
  function crear(){
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;
      
      
      $this->fv->resetErrorList();
     /* $this->fv->isWithinRange("tco_id", "Seleccione el tipo de Consulta",890203,890704);
      $this->fv->isWithinRange("cac_id", "Seleccione la Causa de la Consulta",0,99);
      $this->fv->isWithinRange("amp_id", "Seleccione el &aacute;mbito del Procedimiento",0,99);
      $this->fv->isWithinRange("fip_id", "Seleccione la Finalidad del Procedimiento",0,99); 
      $this->fv->isWithinRange("faq_id", "Seleccione la forma del Acto Quir&uacute;rgico relacionada",0,99);*/
      $this->fv->isEmpty("pld_id", "Seleccione un procedimiento para agregar");
      $this->fv->isNumber("oxp_canPld","Ingrese un n&uacute;mero de procedimientos v�lido",1,1000000);
             
   if ( $this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    }else {
      $query = 'SELECT * 
                FROM orden_lab
                WHERE "old_id"='.$old_id;
      $table->search($query);
    
      if($table->nfound) {
	$mess = "Una orden de laboratorio con ese documento ya se encuentra registrado en el sistema, por favor escoja otro.";
	$mess_cod = "alert";
	$s_opc = "adicionar_usuario";
      }
      else {
	/*  se pasa a la quota a bytes */
	// $old_fechaElaboracion="$fn_anio-$fn_mes-$fn_dia";
         $fields = array ("old_id", "pac_id", "usu_id", "old_fechaElaboracion");
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "orden_lab" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "<b>No se pudo adicionar La Orden de Laboratorio por un fallo en el sistema, Comuniquese con el administrador del sistema.".$query;
	$mess_cod = "alert";		
	}else{
	    logs::crear("evolucion", "crear", $query);
	    $mess = "La Orden de Laboratorio fu&eacute; adicionada con &eacute;xito "; 
	    $mess_cod = "info";
	 } 
	  
      }
    }
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }


/*******************Crear una evolucion de una Orden de Lab***************************************/
  function evolucionar($eol_id){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;
      
      
      $this->fv->resetErrorList();
     /* $this->fv->isWithinRange("tco_id", "Seleccione el tipo de Consulta",890203,890704);
      $this->fv->isWithinRange("cac_id", "Seleccione la Causa de la Consulta",0,99);
      $this->fv->isWithinRange("amp_id", "Seleccione el &aacute;mbito del Procedimiento",0,99);
      $this->fv->isWithinRange("fip_id", "Seleccione la Finalidad del Procedimiento",0,99); 
      $this->fv->isWithinRange("faq_id", "Seleccione la forma del Acto Quir&uacute;rgico relacionada",0,99);*/
      $this->fv->isEmpty("eol_evolucion", "Seleccione un procedimiento para agregar");
  //    $this->fv->isNumber("oxp_canPld","Ingrese un n&uacute;mero de procedimientos v�lido",1,1000000);
             
   if ( $this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    }else {
      $query = 'SELECT * 
                FROM evolucion_lab
                WHERE "eol_id"='.$eol_id;
      $table->search($query);
    
      if($table->nfound) {
	$mess = "Una orden de laboratorio con ese documento ya se encuentra registrado en el sistema, por favor escoja otro.";
	$mess_cod = "alert";
	$s_opc = "adicionar_usuario";
      }
      else {
	/*  se pasa a la quota a bytes */
	
         $fields = array ("old_id", "usu_id", "eol_fechaEntrega", "eol_fechaRecibo", "eol_evolucion");
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "evolucion_lab" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "<b>No se pudo adicionar La Evoluci&oacute;n de la Orden de Laboratorio por un fallo en el sistema, Comuniquese con el administrador del sistema.".$query;
	$mess_cod = "alert";		
	}else{
	    logs::crear("evolucion", "crear", $query);
	    $mess = "La evoluci&oacute;n de la Orden de Laboratorio fu&eacute; adicionada con &eacute;xito "; 
	    $mess_cod = "info";
	 } 
	  
      }
    }
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }



/********************* Buscar una Orden de Laboratorio******************/

function buscar(){
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($_GET as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);


             $fields = array ( "pac_id", "old_fechaElaboracion", "old_obs",  
                                       "old_autoriza", "old_fechaAutoriza");
							      
       // Arreglos para datos del query y el url para paginacion

      $where_array = array();
     $query = 'SELECT * FROM  "orden_lab" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($where_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($where_array))
	$query .= " WHERE ".implode(" AND ",$where_array);
	
      	$result=$table->search($query);
	
	if (!$result){
		$mess = "No hay registros que concuerden con su busqueda.".$query;
		$mess_cod = "alert";		
	}else{
	       //logs::crear("logs", "buscar", $query);
	 	$mess = "El control de materiales fue adicionado con &eacute;xito ".$query.$table->nfound; 
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }

/********************* Buscar Las evoluciones de una Orden de Laboratorio******************/

function buscarEvo(){
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($_GET as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);


             $fields = array ( "old_id", "usu_id","eol_fechaEntrega", "eol_fechaRecibo",  
                                       "eol_docAutoriza", "eol_labAutoriza");
							      
       // Arreglos para datos del query y el url para paginacion

      $where_array = array();
     $query = 'SELECT * FROM  "evolucion_lab" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($where_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($where_array))
	$query .= " WHERE ".implode(" AND ",$where_array);
	
      	$result=$table->search($query);
	
	if (!$result){
		$mess = "No hay registros que concuerden con su busqueda.".$query;
		$mess_cod = "alert";		
	}else{
	       //logs::crear("logs", "buscar", $query);
	 	$mess = "El control de materiales fue adicionado con &eacute;xito ".$query.$table->nfound; 
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }


/***************************************************************/
function agregarPld($old_id){


global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;
      
      $this->fv->resetErrorList();
      

   if ( $this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
      }
      else {
        /*  se pasa a la quota a bytes */
         $fields = array ("oxp_id", "pld_id", "old_id", "oxp_canPld", "oxp_valor");

       // Arreglos para datos del query y el url para paginacion
      	$fields_array = array();
      	$values_array = array();
     	$query = 'INSERT INTO "oldxpld" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
        if(${$v}!=""){
          array_push($fields_array,' "'.$v.'" ');
          array_push($values_array," '".${$v}."' ");
        }

      if(sizeof($fields_array)){
        $query .= implode(", ",$fields_array);
        $query .= ") VALUES (".implode(", ",$values_array) ;
        }
        $query .=")";

        $result=$table->sql_query($query);

        if (!$result){
        $mess = "No se pudo adicionar el procedimiento por un fallo en el sistema, por Favor contacte al Administrador del Sistema.";
        $mess_cod = "alert";
        }else{
            logs::crear("evolucion", "agregarProc", $query);
            $mess = "El procedimiento fu&eacute; adicionado con &eacute;xito ";
            $s_opc = "info_usuario";
         }

      }
    //$mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }

/************** Borrar una orden de laboratorio antes de ser autorizada ****************/
 
  function borrar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    
     	$fields = array ("old_id", "pac_id", "usu_id", "old_fechaElaboracion", "old_obs");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'DELETE FROM "orden_lab" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	//$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE old_id =$old_id";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo cancelar La orden por un fallo en el sistema, Comun&iacute;quese con el administrador.".$query;
		$mess_cod = "alert";		
	}else{
	       logs::crear("evolucion", "borrar", $query);
	 	$mess = "La orden fue cancelada con &eacute;xito "; 
	    	$mess_cod = "info";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }

/***********borrar de la tabla oldxpld una orden****************/
function borraroxp($old_id=0){


global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;


//   if ( 0 /*$this->fv->isError()*/ ) {
if ( 0 ){
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
      }
      else {
        /*  se pasa a la quota a bytes */
         $fields = array ("oxp_id", "pld_id", "old_id", "oxp_canPld");

       // Arreglos para datos del query y el url para paginacion
      	$fields_array = array();
      	$values_array = array();
     	$query = 'DELETE FROM "oldxpld" ';
     	// Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE old_id = '$old_id'";

	$result=$table->sql_query($query);
    
        if (!$result){
        $mess = "No se pudo cancelar la orden por un fallo en el sistema, Contacte al Administrador del Sistema.";
        $mess_cod = "alert";
        }else{
              logs::crear("evolucion", "borrarProc", $query);
            	$mess = "<center><b>La orden fu&eacute; cancelada</b><br>
            	Vuelva a ingresarla</center>";
	 	$mess_cod = "info";
         }

      }
    //$mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }

/***********borrar de la tabla oldxpld un procedimiento de una orden****************/
function borrarPld(){


global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;


//   if ( 0 /*$this->fv->isError()*/ ) {
if ( 0 ){
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
      }
      else {
        /*  se pasa a la quota a bytes */
         $fields = array ("oxp_id", "pld_id", "old_id", "oxp_canPld");

       // Arreglos para datos del query y el url para paginacion
      	$fields_array = array();
      	$values_array = array();
     	$query = 'DELETE FROM "oldxpld" ';
     	// Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE old_id = '$old_id' AND oxp_id=$oxp_id";

	$result=$table->sql_query($query);
    
        if (!$result){
        $mess = "No se pudo cancelar la orden por un fallo en el sistema, Contacte al Administrador del Sistema.";
        $mess_cod = "alert";
        }else{
              logs::crear("evolucion", "borrarProc", $query);
            	$mess = "<center><b>La orden fu&eacute; cancelada</b><br>
            	Vuelva a ingresarla</center>";
	 	$mess_cod = "info";
         }

      }
    //$mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }


/******************Agrega las observaciones a la orden de laboratorio****/    

function actualizar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

		
		$this->fv->resetErrorList();
		
		//$this->fv->isEmpty("old_obs","ingrese las observaciones de la orden de laboratorio");
		
		
		
		 if ( $this->fv->isError() ) {
		      $mess = $this->fv->getMessage();
		      $mess_cod = "alert";
		    } else {


		$fields = array ("old_obs", "old_legalizado");
             
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "orden_lab" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE old_id='$old_id' ";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "<b>No se pudo adicionar los datos por un fallo en el sistema, por favor comuniquese con el administrador.$query</b>";
		$mess_cod = "alert";		
	}else{
	       logs::crear("historia", "actualizar", $query);
	 	$mess = "<b>Los datos $login fueron adicionados con &eacute;xito.</b> "; 
	    	$mess_cod = "info";
	    	$s_opc = "info_usuario";
	 } 
	  
    
  }
return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
}

 /************** Modificar un procedimiento de laboratorio****************/
  
  function modificar1(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
	
	$this->fv->resetErrorList();
    	$this->fv->isEmpty("pld_codigo", "Debe ingresar el c&oacutedigo del procedimiento");
    	$this->fv->isEmpty("pld_tipo", "Debe ingresar el tipo de procedimiento");
      	$this->fv->isEmpty("pld_descripcion", "Debe iingresar la descripci&oacuten del procedimiento ");
  	$this->fv->isNumber("pld_valor", "Debe insertar el valor del procedimiento ");
   if ( $this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    }else {
    
    $fields = array ("pld_codigo", "pld_tipo", "pld_descripcion", "pld_valor") ;
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "procedimiento_lab" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE pld_id = '$pld_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "<b>No se pudo modificar el procedimiento por un fallo en el sistema. Comuniquese con el administrador</b>";
		$mess_cod = "alert";		
	}else{
		$mess = "<center><b>El procedimiento fu&eacute; editado con &eacute;xito</b><br>
		 Haga click <a href='$PHP_SELF?opc=laboratorio&s_opc=listar2'>Aqu&iacute;</a> para regresar al listado</center>"; 
	 	$mess_cod="info";
	} 
	}  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }

/******************modifica la cantidad de procedimientos solitados****/    

function modificar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

		
		$this->fv->resetErrorList();
		
		//$this->fv->isEmpty("oxpd_obs","ingrese las observaciones de la orden de laboratorio");
		
		
		
		 if ( $this->fv->isError() ) {
		      $mess = $this->fv->getMessage();
		      $mess_cod = "alert";
		    } else {


		$fields = array ("oxp_canPld");
             
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "oldxpld" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE oxp_id='$oxp_id' and old_id ='$old_id' ";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "<b>No se pudo adicionar los datos por un fallo en el sistema, por favor comuniquese con el administrador.$query</b>";
		$mess_cod = "alert";		
	}else{
	       logs::crear("historia", "actualizar", $query);
	 	$mess = "<b>Los datos $login fueron modificados con &eacute;xito.</b> "; 
	    	$mess_cod = "info";
	    	$s_opc = "info_usuario";
	 } 
	  
    
  }
return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
}


}
?>
