<?
/**********************************************************/
// CLASE: usuario
// Proposito: Realizar tareas de manejo de usuarios.
// Ultima modificacion: Dic 17, 2002
// /**********************************************************/
class paciente{
  var $fv;
	
  function paciente(){
//    $this->fv = new FormValidator;
  }

  /**********************************************************/
  // Proposito: Creacion unica de un usuarios (Viene del formulario)
  // return: arreglos con resultados de la creacion.
  /**********************************************************/
  function crear(){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;
      
 /*   $this->fv->resetErrorList();	 
 
    $this->fv->isEmpty("login","Falta el Campo Login.");
    $this->fv->isRegExp("login","^[[:alpha:]][-._[:alnum:]]+$","El Login s&oacute;lo puede contener punto( . ) , underscore( _ ) &oacute;<br>guion( - ), comenzar por una letra y sin espacios en blanco.");
    $this->fv->isEmpty("nombre","Falta el Campo Nombre.");
    $this->fv->issizeString("password",4,"La clave debe ser de 4 caracteres o m&aacute;s.");
    $this->fv->isEqualsString("password","cpassword","La clave y la confirmaci&oacute;n no coinciden.");
*/

 
//    if ( 0 /*$this->fv->isError()*/ ) {
   if ( 0 ){
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    } else {
      $query = 'SELECT pac_nombres, pac_apellidos 
                FROM paciente 
                WHERE "pac_numeroIdentificacion"='.$pac_numeroIdentificacion;
      $table->search($query);
    
      if($table->nfound) {
	$mess = "Un paciente con ese documento ya se encuentra registrado en el sistema, por favor escoja otro.";
	$mess_cod = "alert";
	$s_opc = "adicionar_usuario";
      }
      else {
	/*  se pasa a la quota a bytes */
         $fields = array ("pac_id", "tid_id", "ciu_id", "eps_id", "ips_id", "grs_id", "ara_id", "tip_id", "zor_id", "pac_personaElabora","pac_fechaIngreso",
							    "pac_numeroIdentificacion", "pac_lugarExpedicion", "pac_nombres", "pac_apellidos", "pac_direccionResidencia",
							     "pac_telefonos", "pac_sexo", "pac_lugarNacimiento", "pac_edad","pac_fechaNacimiento", "pac_email", "pac_estrato", "pac_personaMasAllegada",
							      "pac_parentescoPersonaAllegada", "pac_direccionPersonaAllegada", "pac_telefonoPersonaAllegada");
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "paciente" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "No se pudo adicionar el paciente por un fallo en el sistema, por Favor llame a Holmes y nathalia que hicieron la aplicacion.";
	$mess_cod = "alert";		
	}else{
	    $mess = "El usuario $login fue adicionado con &eacute;xito ".$query; 
	    $s_opc = "info_usuario";
	 } 
	  
      }
    }

    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }

  /************** Modificar Usuarios ****************/
  function Modificar(){
    global $DB,$vsetuserquota,$site,$factor,$valias;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    $mess_cod = "info";
    $domain = $site["default_domain"];
    $nueva_ou = $dependencia;
  	
    if ($quota=="-1")
      $quotas = "NOQUOTA";
    else
      $quotas=$quota*$factor;
      
    $execute=$vsetuserquota." ".$username."@".$domain." ".$quotas;
    $sitema=exec($execute,$parametros,$codigo); 
    $query ="UPDATE usuario 
            SET publico='$publico', 
            id_ou='$nueva_ou' 
            WHERE id_usuario='$id_usuario'";
    $desplegar_ou = $nueva_ou;
    $DB->sql_query($query);
  
  
    /*****************************************************************
	    Ahora se modifica el password segun est� activo o no
          se actualiza el password con los ultimos 13 caracteres mas un * o espacio en blanco dependiendo
	   del campo activo 
    *******************************************************************/
    $query = "select activo from usuario where id_usuario='$id_usuario' ";
    $DB->sql_query($query);
    $act = $DB->sql_fetch_object();

    if($act->activo!=$activo){
      if($activo) {
	$query = "SELECT passwd 
                  FROM usuario 
                  WHERE id_usuario='$id_usuario'";
	$DB->sql_query($query);
	$row = $DB->sql_fetch_object();
	$row->passwd =  substr(1,strlen($row->passwd));
	$query = "UPDATE usuario 
                  SET activo='$activo',
                  passwd='".$row->passwd."' 
                  WHERE id_usuario='$id_usuario'";
	$DB->sql_query($query);	
	$execute = "$vmoduser  -x $username@$domain";
	exec($execute,$arreglo,$codigo);
	if(!$codigo)
	  $mess = "El usuario ha sido activado!!<br>";
	else{
	  $mess_cod = "error";
	  $mess = "Problemas con la activaci&acute;n del usuario<br>Por favor contacte al administrador" ;
	}
      }else{
	$query = "SELECT passwd 
                  FROM usuario 
                  WHERE id_usuario='$id_usuario'";
	$DB->sql_query($query);
	$row = $DB->sql_fetch_object();
	
	$query = "UPDATE usuario 
                  SET activo='$activo',
                  passwd='*".$row->passwd."' 
                  WHERE id_usuario='$id_usuario'";
	$DB->sql_query($query);	
	
	// -d ( set no password changing flag )
     // ( set no=> pop(-p), web mail(-w), imap access(-i) )
	$execute = "$vmoduser -dpwi $username@$domain";
	exec($execute,$arreglo,$codigo);
	if(!$codigo)
	  $mess = "El usuario ha sido inactivado!!<br>";
	else{
	  $mess_cod = "error";
	  $mess = "Problemas con la inactivaci&oacute;n del usuario<br>Por favor contacte al administrador" ;
	}
	$query = "";
      }
    }

    if($alias=="del"){
      $datos = explode("@",$aliasNameD);
      $query =  "DELETE FROM valias 
				 WHERE alias='$datos[0]' 
				 AND domain='$datos[1]'
				 AND valias_line='$username@$domain'" ;
		
      $DB->sql_query($query);
    }elseif($alias == "add"){
			
      /******** Se valida que no exista buzon igual que el alias ********/
      $query = "SELECT pw_name 
				FROM vpopmail 
				WHERE pw_name='$new_alias' 
				AND pw_domain = '$domain' ";
      $DB->search($query); 
      if($DB->nfound)
	$mess .= "<li>Existe el buz&oacute;n $new_alias@$domain<br>Por favor escoja otro nombre</li>";
			
      $query = "SELECT * 
                     FROM valias 
     	                WHERE alias='$new_alias' 
     		          AND domain='$domain'
     		          AND valias_line='$username@$domain'";
      $DB->search($query);

      if($DB->nfound ) 
	$mess .= "El alias para el usuario $username@$domain ya existe en el sistema.";
       
      if($mess != ""){
	$mess = "<div align=left>&nbsp;&nbsp;<b>Se Presentaron los siguientes errores:</b><ul>".$mess."</ul></div>";
	$mess_cod = "alert";
      } else {
       	$execute = $valias." ".$new_alias."@".$site["default_domain"];
	$execute .= " -i $username@".$site["default_domain"];//echo $execute;
	exec($execute,$arreglo,$codigo);
		
	if(!$codigo)
	  $mess = "El alias fue creado con &eacute;xito<br>";
	else{
	  $mess = "Problemas eliminando el Alias,<br> por favor contacte al adminstrador";
	  $mess_cod = "error";
	}//fin else (codigo)
      }//fin else (mess)
    }//fin alias (add)


    if (!$DB->sql_errno && !$codigo){ 
      $query = "UPDATE usuario 
                SET navega='$navega',
                admin='$admin' 
                WHERE id_usuario='$id_usuario'";
      $DB->sql_query($query);
	  
    }

    if (!$DB->sql_errno && !$codigo){ 
      $mess .= "El usuario fue Modificado";
	
    }else {
      $mess .= "Error: No se pudo modificar el usuario";
      $mess_cod = "error";
    }
  
    
    $s_opc = ($s_opc)? $s_opc : "info_usuario";
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }

  /** Guardar los datos personales del usuario (Contenido Directorio) ****/
  function Guardar_Datos(){
    global $DB;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    $query = "UPDATE personal 
              SET nombre='$nombre',
              documento='$documento',
              direccion='$direccion',
              telefono='$telefono',
              fecha_nac='$ano-$mes-$dia',
              email='$email',
              celular='$celular',
              homepage='$homepage',
              sexo='$sexo' 
              WHERE id='$id_usuario'";
    $DB->sql_query($query);// echo $query;
    
    $query = "SELECT username,dominio 
              FROM usuario 
              WHERE id_usuario='$id_usuario' ";
    $DB->sql_query($query);
    $row = $DB->sql_fetch_object();
    $query = "UPDATE vpopmail 
              SET pw_gecos='$nombre' 
              WHERE pw_name='".$row->username."' 
              AND pw_domain='".$row->dominio."' ";
    $DB->sql_query($query);
    
    $result = $this->Datos();
    $result["mess"] = "Los cambios fueron realizados con exito!!" . $result["mess"] ;
    $result["mess_cod"] = "info";
    return $result;
  }

  /*** Buscar los datos personales para mostrarlos ****/
  function Datos(){
    global $DB;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    $query = "SELECT u.*,p.* 
              FROM personal p,usuario u 
              WHERE u.id_usuario='$id_usuario' 
              AND u.id_usuario = p.id";
    $DB->sql_query($query);//echo $query;
    if(!($dato = $DB->sql_fetch_object()))
      $dato = $id_usuario;
    $s_opc = "datos_personales";

    return array("s_opc"=>$s_opc,"dato"=>$dato);
  }

}
?>