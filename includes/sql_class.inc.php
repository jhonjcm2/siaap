<?
/* sql_class.inc : Abr. 4 2000: Felipe Borrero  */

// require ("$INC_PATH/base_class.inc");

// MySQL Object

class sql_db extends base {

	// Class properties

	var $sqlhost;					// Host donde esta alojado MySQL
	var $sqluser;					// Usuario de Acceso a MySQL
	var $sqlpass;					// Password de acceso a MySQL
	var $sqldb;  					// nombre de la base de datos
	var $sql_link;  			// Link a la db
	var $sql_result;			// Pointer al resultado luego de un query
	var $sql_data;				// Pointer al resultado de un fetch
	var $sql_row;					
	var $sql_error;
	var $sql_errno;
	var $halt_on_error = FALSE;
	

	// constructor
/*
	function sql_db($type="sql_db") {
		$this->base($type);
		$this->sql_connect($this->sqlhost,$this->sqluser,$this->sqlpass);
		if (!$this->sql_errno) {
			$this->sql_select_db();
		}
		return;
	}*/

	// database connection functions
 
	function sql_pconnect() {

			$this->sql_link = mysql_pconnect($this->sqlhost,$this->sqluser,$this->sqlpass);
			if (!$this->sql_link) {
				$this->sql_errno = 0;
				$this->sql_error = "OK";
			} else {
				$this->errno();
				$this->error();
				if ($this->halt_on_error) {
					$this->halt("sql_pconnect");
				}
			}
			return;
	} // end sql_pconnect

	function queryerror($method="") {
			if ($this->sql_result) {
				$this->sql_errno = 0;
				$this->sql_error = "OK";
				return TRUE;
			} else {
				$this->errno();
				$this->error();
				if ($this->halt_on_error) {
					$this->halt($method);
				}
			}
			return FALSE;
	}

	function misc_error($method=""){
		$this->errno();
		$this->error();
		if ($this->sql_errno && $this->halt_on_error) {
			$this->halt($method);
		}
	}
	
	function sql_connect() {
			
			$this->sql_link = mysql_connect($this->sqlhost,$this->sqluser,$this->sqlpass);
			if (!$this->sql_link) {
				$this->sql_errno = -10000;
				$this->sql_error = "No se pudo establecer la conexi&oacute;n con la base de datos";
				if ($this->halt_on_error) {
					$this->halt("sql_connect");
				}
			}
			$this->misc_error("sql_connect");
			return $this->sql_link;
	} // end sql_connect
	
	
	function halt($string="") {
		$error = "## <b>Error</b> ".$this->sql_errno."; <b>".$this->object_type."</b>::".$string." ".$this->sql_error;
		$error .= "<br>\n## <b>File: ".$PHP_SELF."<br>\n";
		print($error);
		exit;
	}  // end halt()

// Error management functions

	function error() {

		$this->sql_error =  mysql_error();
		return $this->sql_error;
	}

	function errno() {
	
		$this->sql_errno = mysql_errno();
		return $this->sql_errno;
	
	}
	// Query and result management functions
/*
	function sql_query($query) {
		$this->sql_result = mysql_query($query,$this->sql_link);
		$this->queryerror("sql_query");
		return $this->sql_result;
	}
*/

	function sql_free_result() {

		$this->sql_result = mysql_free_result($this->sql_result);
		$this->queryerror("sql_free_result");
		return;
	}

	function sql_fetch_row() {
	
		$this->sql_data = mysql_fetch_row($this->sql_result);
		$this->misc_error("sql_fetch_row");
		return $this->sql_data;
	
	}

	function sql_fetch_object() {
		$this->sql_data = mysql_fetch_object($this->sql_result);
		$this->misc_error("sql_fetch_object");
		return $this->sql_data;
	
	}

	function sql_fetch_array() {
	
		$this->sql_data = mysql_fetch_array($this->sql_result);
		$this->misc_error("sql_fetch_array");
		return $this->sql_data;
	
	}


	function sql_num_rows() {
	
		$result = mysql_num_rows($this->sql_result);
		$this->misc_error("sql_fetch_array");
		return $result;
		
	}

	// Miscellaneous functions

	function sql_data_seek($row) {
	
		$result = mysql_data_seek($this->sql_result,$row);
		$this->misc_error("sql_data_seek");
		return result;
		
	}

	function sql_affected_rows() {

		$result = mysql_affected_rows($this->sql_result);
		$this->misc_error("sql_affected_rows");
		return $result;

	}

	function sql_num_fields() {

		return mysql_num_fields($this->sql_result);

	}

	function sql_fieldname($field) {

		return mysql_fieldname($this->sql_result,$field);	

	}

	function sql_select_db() {
		$result = mysql_select_db($this->sqldb,$this->sql_link);
		if (!$result) {
			$this->error();
			$this->errno();
			if ($this->halt_on_error) {
				$this->halt();
			}
		} else {
			$this->sql_errno = 0;
			$this->sql_error = "OK";
		}
		return $this->sql_errno;
	}
	
	function sql_fetch_field($offset=0) {
		$result = mysql_fetch_field($this->sql_result,$offset);
		if (!$result) {
			$this->error();
			$this->errno();
			if ($this->halt_on_error) {
				$this->halt("sql_fetch_field");
			}
		} else {
			$this->sql_errno = 0;
			$this->sql_error = "OK";
		}
		return $result;
	}
	
	function sql_field_values($table,$fieldname,$type=TRUE) {
	/*	Retorna un array con los posibles valores de un enum o set en MySQL
			$table es el nombre de la tabla donde esta el campo
			$fieldname es el nombre del campo
			$type es el tipo de campo ENUM=TRUE, SET=FALSE                     
			
			PRECAUCION
			Este metodo afecta el valor de $this->sql_result. Si se quiere conservar
			el resultado de una consulta anterior se debe crear una nueva instancia
			del objeto y realizar la consulta																					*/
			
  	$values[0] = "";
  	$query = "SHOW 
  							FIELDS
  				  	FROM 
  				  		$table";
  	$this->sql_query($query);
  	while ($this->sql_fetch_object()) {
   		if ($this->sql_data->Field == $fieldname)  
   			$dato=$this->sql_data->Type;
  	}
  	if ($type) {
  		$dato=ereg_replace("^enum\(","",$dato);
  	} else {
  		$dato = ereg_replace("^set\(","",$dato);
  	}
  	$dato=ereg_replace("\)","",$dato);
  	$dato=ereg_replace("'","",$dato);
  	$values=split(",",$dato);
  	return $values;
	}
	
	function sql_close() {
		if ($this->sql_link) {
			$result = mysql_close($this->sql_link);
			$this->misc_error("sql_close");
		}
		return $result;

	}
	
	
	//  este metodo retorna el ultimo ID insertado en la conexion
	function sql_insert_id(){
		$result = mysql_insert_id($this->sql_link);
		$this->misc_error("sql_insert_id");
		return $result;
	}
	
	function sql_next_id($table,$key) {
		$sem_key = 1;
		$newkey = 0;
		//  validar los parametros de entrada
		if (empty($table) || empty($key)) {
			$this->sql_errno = 302;
			$this->sql_error = "sql_next_id:: Tabla o llave invalidas";
			return $newkey;
		}
		// obtener el semaforo
		$asem = sem_get($sem_key);
		// colocar el semaforo en ON
		sem_acquire($asem);
		// 	Este codigo solo se ejecuta si el semaforo esta OFF
		$query = "SELECT MAX($key) as maximo FROM $table";
		$result = $this->sql_query($query);
		if ($result) {
			$row = $this->sql_fetch_object();
			// Calcula el nuevo ID
			$newkey = $row->maximo + 1;
			$query = "INSERT into $table ($key) VALUES ('$newkey')";
			$newrecord = $this->sql_query($query);
			if (!$newrecord) {
				// se presento error, apagar el semaforo. $table queda igual
				$newkey = 0;  // el id es invalido. 
				$this->sql_errno = 301;
				$this->sql_error = "sql_next_id:: No se pudo generar la nueva llave";																
			} else {
				$this->sql_errno = 0;
				$this->sql_error = "";
			} // end if not newrecord
		}  else {
				$this->sql_errno = 303;
				$this->sql_error =  "sql_next_id:: No se pudo leer la llave actual";
		} // end if result
		//  se libera el semaforo
		sem_release($asem);
		return $newkey;
	}

} // end class definition
?>
