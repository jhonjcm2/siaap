<?
/*************************************************************/
// CLASE: horarios
// Proposito: registrar los horarios de cada odontologo.
// Ultima modificacion: octubre de 2003
// /**********************************************************/
class horarios{
  var $fv;
  var $data;
	
  function horarios($id=-1){
    global $table;
    
    $this->fv = new FormValidator;
    if ($id >= 0){
    	$query = "SELECT * 
        	     FROM horarios
        	     WHERE hor_id='$id' ";
	$table->sql_query($query);
    	$this->data = $table->sql_fetch_object();
    }
    
  }

  /******************************************************************/
  // Proposito: ingresar al sistema cada uno de los horarios
  // return: arreglos con resultados de la creacion.
  /******************************************************************/
  function crear(){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;

    foreach($_GET as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;
      
      
   $this->fv->resetErrorList();
   $this->fv->isWithinRange("tcl_id","Seleccione el tipo de cl&iacute;nica",0,99);
   $this->fv->isWithinRange("hor_ubiClinica","Seleccione la ubicaci&oacute;n de la cl&iacute;nica",0,99);
   $this->fv->isWithinRange("hor_horaInicial","Seleccione la hora inicial de la cita",0,99);
   $this->fv->isWithinRange("hor_horaFinal","Seleccione la hora final de la cita",0,99);
   $this->fv->isWithinRange("hor_diaSemana","Seleccione el dia de la cita",0,99);
 
 
   if ( $this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    }else {
      $query = 'SELECT * 
                FROM horarios
                WHERE "hor_id "='.$hor_id;
      $table->search($query);
    
      if($table->nfound) {
	$mess = "Un horario en esa fecha ya se encuentra registrado en el sistema, por favor escoja otro.";
	$mess_cod = "alert";
	}
      else {
	/*  se pasa a la quota a bytes */
         $fields = array ("hor_id", "tcl_id", "usu_id", "hor_horaInicial", "hor_horaFinal", "hor_diaSemana", "hor_ubiClinica");
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "horarios" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "<center><b>No se pudo adicionar el horario por un fallo en el sistema</b><br> Por favor comuniquese con el administrador.</center>".$query;
	$mess_cod = "alert";		
	}else{
	    logs::crear("horarios", "crear", $query);
	    $mess = "<center><b>El horario fu&eacute; adicionado con &eacute;xito</b><br>Contin&uacute;e ingresando su horario"; 
	    $mess_cod="info";
	    $s_opc = "info_usuario";
	 } 
	  
      }
    }
  // $mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }


/********************* Buscar un horario******************/

function buscar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);



             $fields = array ("hor_id", "tcl_id", "usu_id", "hor_horaInicial", "hor_horaFinal", "hor_diaSemana", "hor_ubiClinica");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'SELECT * FROM  "horarios" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($where_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($where_array))
	$query .= " WHERE ".implode(", ",$set_array);
	
      	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No hay registros que concuerden con su busqueda".$query;
		$mess_cod = "alert";		
	}else{
	       $mess = "El horario fue adicionado con &eacute;xito ".$query; 
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess.$query,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }

/******************Agregar Horario ********************/
function agregarHor($hor_id=0){


global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

   /*$this->fv->resetErrorList();
   $this->fv->isWithinRange("tcl_id","Seleccione el tipo de cl&iacute;nica",0,99);
   $this->fv->isWithinRange("hor_ubiClinica","Seleccione la ubicaci&oacute;n de la cl&iacute;nica",0,99);
   $this->fv->isWithinRange("hor_horaInicial","Seleccione la hora inicial de la cita",0,99);
   $this->fv->isWithinRange("hor_horaFinal","Seleccione la hora final de la cita",0,99);
   $this->fv->isWithinRange("hor_diaSemana","Seleccione el dia de la cita",0,99);*/

  if ( $this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
      }
      else {
        /*  se pasa a la quota a bytes */
         $fields = array ("hxo_id", "usu_id", "hor_id", "hxo_cantidadHoras") ;

       // Arreglos para datos del query y el url para paginacion
      	$fields_array = array();
      	$values_array = array();
     	$query = 'INSERT INTO "horxodl" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
        if(${$v}!=""){
          array_push($fields_array,' "'.$v.'" ');
          array_push($values_array," '".${$v}."' ");
        }

      if(sizeof($fields_array)){
        $query .= implode(", ",$fields_array);
        $query .= ") VALUES (".implode(", ",$values_array) ;
        }
        $query .=")";

        $result=$table->sql_query($query);

        if (!$result){
        $mess = "No se pudo adicionar el horario por un fallo en el sistema, por Favor contacte al Administrador del Sistema.";
        $mess_cod = "alert";
        }else{
            logs::crear("horarios", "agregarHor", $query);
            $mess = "El horario fue adicionado con &eacute;xito ".$query;
            $mess_cod = "info";
         }

      }
    $mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }


  /************** Modificar horarios ****************/
  function modificar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

     /********************/

    $fields = array ("hor_id", "tcl_id", "usu_id", "hor_horaInicial", "hor_horaFinal", "hor_diaSemana", "hor_ubiClinica", "hor_estado") ;
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "horarios" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE hor_id = '$hor_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "<b>No se pudo modificar el horario por un fallo en el sistema</b>".$query;
		$mess_cod = "alert";		
	}else{
	       logs::crear("horarios", "modificar", $query);
	 	$mess = "<center><b>El Horario fu&eacute; Modificado con &eacute;xito</b></center>
	 	Haga click <a href='$PHP_SELF?opc=horarios&s_opc=listar2&usu_id=$usu_id'>Aqu&iacute;</a> para editar otro horario "; 
	 	
	 	$mess_cod="info";
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }

 /************** Borrar el horario de un paciente***************/
 
  function borrar($usu_id){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    
    $fields = array ("hor_id", "tcl_id", "hor_horaInicial", "hor_horaFinal", "hor_diaSemana", "hor_ubiClinica");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'DELETE FROM "horarios" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	
	
      $query .=  " WHERE hor_id =$hor_id";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo cancelar el horario por un fallo en el sistema, Comun&iacute;quese con el administrador.".$query;
		$mess_cod = "alert";		
	}else{
	       logs::crear("horarios", "borrar", $query);
	 	$mess = "<center><b>El horario $login fu&eacute; borrado con &eacute;xito</center></b><br>
	 	Haga click <a href='$PHP_SELF?opc=horarios&s_opc=listar2&usu_id=$usu_id'>Aqu&iacute;</a> para regresar al listado"; 
	    	$mess_cod = "info";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }

}
?>
