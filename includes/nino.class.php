<?
/****************************************************************/
// CLASE: nino
// Proposito: Manejar los datos de la historia de un nino.
// Ultima modificacion: Octubre de 2003
// /*************************************************************/
class nino{
  var $fv;
  var $data;
	  
   function nino($pac_id=0){
    global $table;

    $this->fv = new FormValidator;
    if ($pac_id){
        $query = "SELECT *
              FROM datos_personales_nino
              WHERE pac_id='$pac_id' ";
    $table->sql_query($query);
    $this->data = $table->sql_fetch_object();
        }

  }

  /**********************************************************/
  // Proposito: Creacion de la historia del nino 
  // return: arreglos con resultados de la creacion.
  /**********************************************************/
  function crear(){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      	${$k} = base::dispelMagicQuotes($v);
      
    foreach($_GET as $k=>$v)
     	${$k} = base::dispelMagicQuotes($v);
       
    $this->fv->resetErrorList();	
    if(isset($dpn_estudiante)){
    $this->fv->isWithinRange("gra_id","Seleccione el grado que cursa",21,31);
    $this->fv->isEmpty("dpn_colegio", "Debe insertar un nombre de colegio v&aacute;lido");
    }
    if ( $this->fv->isError() ) {
      	$mess = $this->fv->getMessage();
      	$mess_cod = "alert";
    	}else {
    		$query = 'SELECT * FROM datos_personales_nino  WHERE "pac_id"='.$pac_id;
      		$table->search($query);
    		if($table->nfound) {
			$mess = "<b>Los datos ya fueron ingresados.</b>";
			$mess_cod = "error";
			$s_opc = "adicionar_usuario";
      			}
             	else{
            
	/*  se pasa a la quota a bytes */
         $fields = array ("dpn_id", "pac_id", "cdn_id", "gra_id", "dpn_estudiante", "dpn_colegio", "dpn_direccionColegio", "dpn_nombrePadre", "dpn_ocupacionPadre", 
	                          "dpn_institucionPadre", "dpn_telefonoInstitucionPadre", "dpn_nombreMadre", "dpn_ocupacionMadre", "dpn_institucionMadre",  "dpn_estado",
	                           "dpn_telefonoInstitucionMadre", "dpn_fechaElaboracion");
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "datos_personales_nino" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "<b>No se pudo adicionar el paciente por un fallo en el sistema. Por favor comuniquese con el Administrador.</b>";
	$mess_cod = "alert";
					
	}else{
	       logs::crear("nino", "crear", $query);
		$mess = "<center><b>Los datos personales fueron adicionados con &eacute;xito<br>
		Por favor continue ingresando los Antecedentes Generales</center></b>";
		$mess_cod = "info";
	   	} 
	  }
    }

    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }


/********************* Buscar nino******************/

function buscar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);



             $fields = array ( "dpn_id", "pac_id", "cdn_id", "gra_id", "dpn_estudiante", "dpn_colegio", "dpn_direccionColegio", "dpn_nombrePadre", "dpn_ocupacionPadre", 
	                              "dpn_institucionPadre", "dpn_telefonoInstitucionPadre", "dpn_nombreMadre", "dpn_ocupacionMadre", "dpn_institucionMadre", 
	                              "dpn_telefonoInstitucionMadre", "par_id", "dpn_antMedPrenatalEnferEmbarazo", "dpn_medicamentosTomadosEmb", "dpn_antMedNatalEmplearonForceps", 
	                              "dpn_ninoCianotico", "dpn_antMedPostnatalFechaUltEx", "dpn_motivoUltimoExamen", "dpn_tratamientoMedicoActual", "dpn_medicTomActual", 
	                              "dpn_sufreAlergiaAnestesicolocal", "dpn_alergiaPenicilina", "dpn_alergiaDrogasComidas", 
	                              "dpn_porqueHaSidoHospitalizado", "dpn_sangraExageradamente", "dpn_impedimentosFisicos", 
	                              "dpn_haTenidoFiebreEscarlatina", "dpn_fiebreReumatica", "dpn_polio", "dpn_rubeola", "dpn_tetano", 
	                              "dpn_difteria", "dpn_tosFerina", "dpn_asma", "dpn_sarampion", "dpn_convulsiones", "dpn_paperas", 
	                              "dpn_hepatitis", "dpn_retardoMental", "dpn_autismo", "dpn_paralisisCerebral", "dpn_diabetes", "dpn_anemia",
	                              "dpn_observacionesEnfermedades", "dpn_antPsicVivePadres", "dpn_comoTratanNino", "dpn_cuantosHermanosTiene", "dpn_numeroOcupa", 
	                              "dpn_comoTratanHermanos", "dpn_observacionesPsicologicas", "dpn_antOdonFechaVisitaOdont", "dpn_motivo", "dpn_reaccionDesfavorableTratam", 
	                              "dpn_condicionesDentalesRaras", "dpn_tiposTraumaOralDental", "dpn_cambiosColorMovilidad", 
	                              "dpn_ninoCooperadorOdontologo", "dpn_tratamientosCaseros", "dpn_observacionesOdontologicas", 
	                              "dpn_habitosD1controlDesayuno", "dpn_1controlAlmuerzo", "dpn_1controlComida", "dpn_2controlDesayuno", 
	                              "dpn_2controlAlmuerzo", "dpn_2controlComida", "dpn_3controlDesayuno", "dpn_3controlAlmuerzo", 
	                              "dpn_3controlComida", "dpn_rechinaDientes", "dpn_chupaDedo", "dpn_comeUnas", "dpn_respiraBoca", 
	                              "dpn_tomaTetero", "dpn_otrosHabitos");
							      
       // Arreglos para datos del query y el url para paginacion

     $where_array = array();
     $query = 'SELECT  * FROM  "datos_personales_nino" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($where_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($where_array))
	$query .= " WHERE ".implode(", ",$where_array);
	      
	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "<b>No  hay registros que concuerden con su b&uacute;squeda.</b>";
		$mess_cod = "alert";		
	}else{
	       $mess = "El usuario $login fue adicionado con &eacute;xito "; 
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }


  /************** Modificar nino ****************/
  
  function modificar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);


    $fields = array ( "pac_id", "cdn_id", "gra_id", "dpn_estudiante", "dpn_colegio", "dpn_direccionColegio", "dpn_nombrePadre", "dpn_ocupacionPadre", 
	                      "dpn_institucionPadre", "dpn_telefonoInstitucionPadre", "dpn_nombreMadre", "dpn_ocupacionMadre", "dpn_institucionMadre", 
	                      "dpn_telefonoInstitucionMadre", "par_id", "dpn_antMedPrenatalEnferEmbarazo", "dpn_medicamentosTomadosEmb", "dpn_antMedNatalEmplearonForceps", 
	                      "dpn_ninoCianotico", "dpn_antMedPostnatalFechaUltEx", "dpn_motivoUltimoExamen", "dpn_tratamientoMedicoActual", "dpn_medicTomActual", 
	                      "dpn_sufreAlergiaAnestesicolocal", "dpn_alergiaPenicilina", "dpn_alergiaDrogasComidas", 
	                      "dpn_porqueHaSidoHospitalizado", "dpn_sangraExageradamente", "dpn_impedimentosFisicos", 
	                      "dpn_haTenidoFiebreEscarlatina", "dpn_fiebreReumatica", "dpn_polio", "dpn_rubeola", "dpn_tetano", 
	                      "dpn_difteria", "dpn_tosFerina", "dpn_asma", "dpn_sarampion", "dpn_convulsiones", "dpn_paperas", 
	                      "dpn_hepatitis", "dpn_retardoMental", "dpn_autismo", "dpn_paralisisCerebral", "dpn_diabetes", "dpn_anemia",
	                      "dpn_observacionesEnfermedades", "dpn_antPsicVivePadres", "dpn_comoTratanNino", "dpn_cuantosHermanosTiene", "dpn_numeroOcupa", 
	                      "dpn_comoTratanHermanos", "dpn_observacionesPsicologicas", "dpn_antOdonFechaVisitaOdont", "dpn_motivo", "dpn_reaccionDesfavorableTratam", 
	                      "dpn_condicionesDentalesRaras", "dpn_tiposTraumaOralDental", "dpn_cambiosColorMovilidad", 
	                      "dpn_ninoCooperadorOdontologo", "dpn_tratamientosCaseros", "dpn_observacionesOdontologicas", 
	                      "dpn_habitosD1controlDesayuno", "dpn_1controlAlmuerzo", "dpn_1controlComida", "dpn_2controlDesayuno", 
	                      "dpn_2controlAlmuerzo", "dpn_2controlComida", "dpn_3controlDesayuno", "dpn_3controlAlmuerzo", 
	                      "dpn_3controlComida", "dpn_rechinaDientes", "dpn_chupaDedo", "dpn_comeUnas", "dpn_respiraBoca", 
	                      "dpn_tomaTetero", "dpn_otrosHabitos", "dpn_estado");
							      
       // Arreglos para datos del query y el url para paginacion
       if ($fex_dia)
	    $dpn_antMedPostnatalFechaUltEx=$fex_dia.'-'.$fex_mes.'-'.$fex_anio;
	 if ($fvo_dia)
	    $dpn_antOdonFechaVisitaOdont =$fvo_dia.'-'.$fvo_mes.'-'.$fvo_anio;
      $set_array = array();
     $query = 'UPDATE  "datos_personales_nino" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE dpn_id = '$dpn_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "<b>No se pudo adicionar el paciente por un fallo en el sistema, Por favor comuniquese con el Administrador.</b>";
		$mess_cod = "alert";		
	}else{
	       logs::crear("nino", "modificar", $query);
	 	$mess = "<center><b>Los cambios $login fueron  adicionados con &eacute;xito</b><br></center>";
	 	$mess_cod = "info";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }


/************** creacion antMedNino y antPsicNino****************/

  function actualizar(){
   global $table, $_POST, $_GET;

   foreach($_POST as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;

    foreach($_GET as $k=>$v)
     ${$k} = base::dispelMagicQuotes($v);
     //${$k} = $v;
      
    $this->fv->resetErrorList();	
    if($dpn_estado==2){
    	    $this->fv->isEmpty("dpn_observacionesEnfermedades", "Debe ingresar las observaciones correspondientes");
	    }
       if($dpn_estado==3){
           $this->fv->isEmpty("dpn_observacionesPsicologicas", "Debe ingresar las observaciones correspondientes a la parte psicol&oacute;gica");
           $this->fv->isEmpty("dpn_observacionesOdontologicas", "Debe ingresar las observaciones correspondientes a la parte odontol&oacute;gica");
        }
    if ( $this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    }else{
        
    $fields = array ( "par_id", "cdn_id", "dpn_antMedPrenatalEnferEmbarazo", "dpn_medicamentosTomadosEmb", "dpn_antMedNatalEmplearonForceps", 
	                     "dpn_ninoCianotico", "dpn_antMedPostnatalFechaUltEx", "dpn_motivoUltimoExamen", "dpn_tratamientoMedicoActual", "dpn_medicTomActual", 
	                     "dpn_sufreAlergiaAnestesicolocal", "dpn_alergiaPenicilina", "dpn_alergiaDrogasComidas", "dpn_comentarioAntAler", 
	                     "dpn_porqueHaSidoHospitalizado", "dpn_sangraExageradamente", "dpn_impedimentosFisicos", 
	                     "dpn_haTenidoFiebreEscarlatina", "dpn_fiebreReumatica", "dpn_polio", "dpn_rubeola", "dpn_tetano", 
	                     "dpn_difteria", "dpn_tosFerina", "dpn_asma", "dpn_sarampion", "dpn_convulsiones", "dpn_paperas", 
	                     "dpn_hepatitis", "dpn_retardoMental", "dpn_autismo", "dpn_paralisisCerebral", "dpn_diabetes", "dpn_anemia",
	                     "dpn_observacionesEnfermedades", "dpn_antPsicVivePadres", "dpn_comoTratanNino", "dpn_cuantosHermanosTiene", "dpn_numeroOcupa", 
	                     "dpn_comoTratanHermanos", "dpn_observacionesPsicologicas", "dpn_antOdonFechaVisitaOdont", "dpn_motivo", "dpn_reaccionDesfavorableTratam", 
	                     "dpn_condicionesDentalesRaras", "dpn_tiposTraumaOralDental", "dpn_cambiosColorMovilidad", 
	                     "dpn_ninoCooperadorOdontologo", "dpn_tratamientosCaseros", "dpn_observacionesOdontologicas", 
	                     "dpn_habitosD1controlDesayuno", "dpn_1controlAlmuerzo", "dpn_1controlComida", "dpn_2controlDesayuno", 
	                     "dpn_2controlAlmuerzo", "dpn_2controlComida", "dpn_3controlDesayuno", "dpn_3controlAlmuerzo", 
	                     "dpn_3controlComida", "dpn_rechinaDientes", "dpn_chupaDedo", "dpn_comeUnas", "dpn_respiraBoca", 
	                     "dpn_tomaTetero", "dpn_otrosHabitos", "dpn_estado", "dpn_antGenerales");
							      
       // Arreglos para datos del query y el url para paginacion
	if ($fex_dia)
	    $dpn_antMedPostnatalFechaUltEx=$fex_dia.'-'.$fex_mes.'-'.$fex_anio;
	if ($fvo_dia)
	    $dpn_antOdonFechaVisitaOdont =$fvo_dia.'-'.$fvo_mes.'-'.$fvo_anio;
      $set_array = array();
      $query = 'UPDATE  "datos_personales_nino" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
       $query .=  " WHERE dpn_id = '$dpn_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "<b>No se pudo adicionar el paciente por un fallo en el sistema. Por favor comun�quese con el Administrador.</b>";
		$mess_cod = "alert";		
	}else{
	       logs::crear("nino", "actualizar", $query); 
	 	$mess = "<b>Los datos fueron adicionados con &eacute;xito</b>"; 
	    	$mess_cod = "info";
	    	
	} 
    //$mess .= $query;  
   
     }
     return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  
   }
 
 }
?>
