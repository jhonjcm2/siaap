<?
/**********************************************************/
// CLASE: nino
// Proposito: Realizar la historia del nino.
// Ultima modificacion: 22 de mayo de 2003
// /**********************************************************/
class nino{
  var $fv;
  var $data;
	
  function nino(){
//    $this->fv = new FormValidator;
  }

  function nino($id){
    global $table;

    $this->fv = new FormValidator;

    $query = "SELECT *
              FROM datos_personales_nino
              WHERE pac_id='$id' ";
    $table->sql_query($query);
    $this->data = $table->sql_fetch_object();


  }

  /**********************************************************/
  // Proposito: Creacion de la historia del nino (Viene del formulario)
  // return: arreglos con resultados de la creacion.
  /**********************************************************/
  function crear(){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;
      
 /*   $this->fv->resetErrorList();	 
 
    $this->fv->isEmpty("login","Falta el Campo Login.");
    $this->fv->isRegExp("login","^[[:alpha:]][-._[:alnum:]]+$","El Login s&oacute;lo puede contener punto( . ) , underscore( _ ) &oacute;<br>guion( - ), comenzar por una letra y sin espacios en blanco.");
    $this->fv->isEmpty("nombre","Falta el Campo Nombre.");
    $this->fv->issizeString("password",4,"La clave debe ser de 4 caracteres o m&aacute;s.");
    $this->fv->isEqualsString("password","cpassword","La clave y la confirmaci&oacute;n no coinciden.");
*/

 
//   if ( 0 /*$this->fv->isError()*/ ) {
if ( 0 ){
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    }else {
      $query = 'SELECT pac_nombres, pac_apellidos 
                FROM paciente
                WHERE "pac_numeroIdentificacion"='.$pac_numeroIdentificacion;
      $table->search($query);
    
      if($table->nfound) {
	$mess = "Un paciente con ese documento ya se encuentra registrado en el sistema, por favor escoja otro.";
	$mess_cod = "alert";
	$s_opc = "adicionar_usuario";
      }
      else {
	/*  se pasa a la quota a bytes */
         $fields = array ("dpn_id", "pac_id", "cdn_id", "gra_id", "dpn_estudiante", "dpn_colegio", "dpn_direccionColegio", "dpn_nombrePadre", "dpn_ocupacionPadre", 
	                          "dpn_institucionPadre", "dpn_telefonoInstitucionPadre", "dpn_nombreMadre", "dpn_ocupacionMadre", "dpn_institucionMadre", 
	                           "dpn_telefonoInstitucionMadre");
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "datos_personales_nino" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "No se pudo adicionar el paciente por un fallo en el sistema, por Favor llame a Holmes y nathalia que hicieron la aplicacion.";
	$mess_cod = "alert";		
	}else{
	    $mess = "El usuario $login fue adicionado con &eacute;xito ".$query; 
	    $s_opc = "info_usuario";
	 } 
	  
      }
    }

    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }


/********************* Buscar nino******************/

function buscar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);



             $fields = array ( "pac_id", "cdn_id", "gra_id", "dpn_estudiante", "dpn_colegio", "dpn_direccionColegio", "dpn_nombrePadre", "dpn_ocupacionPadre", 
	                               "dpn_institucionPadre", "dpn_telefonoInstitucionPadre", "dpn_nombreMadre", "dpn_ocupacionMadre", "dpn_institucionMadre", 
	                                "dpn_telefonoInstitucionMadre");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "datos_personales_nino" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE dpn_id = '$dpn_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo adicionar el paciente por un fallo en el sistema, por Favor llame a Holmes y nathalia que hicieron la aplicacion.".$query;
		$mess_cod = "alert";		
	}else{
	 	$mess = "El usuario $login fue adicionado con &eacute;xito ".$query; 
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }


  /************** Modificar nino ****************/
  function modificar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
/********************/

    $fields = array ( "pac_id", "cdn_id", "gra_id", "dpn_estudiante", "dpn_colegio", "dpn_direccionColegio", "dpn_nombrePadre", "dpn_ocupacionPadre", 
	                      "dpn_institucionPadre", "dpn_telefonoInstitucionPadre", "dpn_nombreMadre", "dpn_ocupacionMadre", "dpn_institucionMadre", 
	                       "dpn_telefonoInstitucionMadre");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "datos_personales_nino" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  "  WHERE dpn_id = '$dpn_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo adicionar el paciente por un fallo en el sistema, por Favor llame a Holmes y nathalia que hicieron la aplicacion.".$query;
		$mess_cod = "alert";		
	}else{
	 	$mess = "El usuario $login fue adicionado con &eacute;xito ".$query; 
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }
/**********************/    

/************** creacion antMedNino ****************/
  function actualizar(){
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;
/********************/
    $fields = array ( "par_id", "dpn_antMedPrenatalEnferEmbarazo", "dpn_medicamentosTomadosEmb", "dpn_antMedNatalEmplearonForceps", 
	                      "dpn_ninoCianotico", "dpn_antMedPostnatalFechaUltEx", "dpn_motivoUltimoExamen", "dpn_tratamientoMedicoActual", "dpn_medicTomActual", 
	                        "dpn_sufreAlergiaAnestesicolocal", "dpn_alergiaPenicilina", "dpn_alergiaDrogasComidas", 
	                         "dpn_porqueHaSidoHospitalizado", "dpn_sangraExageradamente", "dpn_impedimentosFisicos", 
	                          "dpn_haTenidoFiebreEscarlatina", "dpn_fiebreReumatica", "dpn_polio", "dpn_rubeola", "dpn_tetano", 
	                           "dpn_difteria", "dpn_tosFerina", "dpn_asma", "dpn_sarampion", "dpn_convulsiones", "dpn_paperas", 
	                            "dpn_hepatitis", "dpn_retardoMental", "dpn_autismo", "dpn_paralisisCerebral", "dpn_diabetes", "dpn_anemia",
	                             "dpn_observacionesEnfermedades");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "datos_personales_nino" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE dpn_id = '$dpn_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo adicionar el paciente por un fallo en el sistema, por Favor llame a Holmes y nathalia que hicieron la aplicacion.".$query;
		$mess_cod = "alert";		
	}else{
	 	$mess = "El usuario $login fue adicionado con &eacute;xito ".$query; 
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
   
  }
/**********************/    


/********************* Buscar AntMedNino ******************/

function buscarMed(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);



             $fields = array ( "par_id", "dpn_antMedPrenatalEnferEmbarazo", "dpn_medicamentosTomadosEmb", "dpn_antMedNatalEmplearonForceps", 
	                      "dpn_ninoCianotico", "dpn_antMedPostnatalFechaUltEx", "dpn_motivoUltimoExamen", "dpn_tratamientoMedicoActual", "dpn_medicTomActual", 
	                        "dpn_sufreAlergiaAnestesicolocal", "dpn_alergiaPenicilina", "dpn_alergiaDrogasComidas", 
	                         "dpn_porqueHaSidoHospitalizado", "dpn_sangraExageradamente", "dpn_impedimentosFisicos", 
	                          "dpn_haTenidoFiebreEscarlatina", "dpn_fiebreReumatica", "dpn_polio", "dpn_rubeola", "dpn_tetano", 
	                           "dpn_difteria", "dpn_tosFerina", "dpn_asma", "dpn_sarampion", "dpn_convulsiones", "dpn_paperas", 
	                            "dpn_hepatitis", "dpn_retardoMental", "dpn_autismo", "dpn_paralisisCerebral", "dpn_diabetes", "dpn_anemia",
	                             "dpn_observacionesEnfermedades");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "datos_personales_nino" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE dpn_id = '$dpn_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo adicionar el paciente por un fallo en el sistema, por Favor llame a Holmes y nathalia que hicieron la aplicacion.".$query;
		$mess_cod = "alert";		
	}else{
	 	$mess = "El usuario $login fue adicionado con &eacute;xito ".$query; 
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }


  /************** Modificar AntMedNino ****************/
  function modificarMed(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
/********************/

    $fields = array ( "par_id", "dpn_antMedPrenatalEnferEmbarazo", "dpn_medicamentosTomadosEmb", "dpn_antMedNatalEmplearonForceps", 
	                      "dpn_ninoCianotico", "dpn_antMedPostnatalFechaUltEx", "dpn_motivoUltimoExamen", "dpn_tratamientoMedicoActual", "dpn_medicTomActual", 
	                        "dpn_sufreAlergiaAnestesicolocal", "dpn_alergiaPenicilina", "dpn_alergiaDrogasComidas", 
	                         "dpn_porqueHaSidoHospitalizado", "dpn_sangraExageradamente", "dpn_impedimentosFisicos", 
	                          "dpn_haTenidoFiebreEscarlatina", "dpn_fiebreReumatica", "dpn_polio", "dpn_rubeola", "dpn_tetano", 
	                           "dpn_difteria", "dpn_tosFerina", "dpn_asma", "dpn_sarampion", "dpn_convulsiones", "dpn_paperas", 
	                            "dpn_hepatitis", "dpn_retardoMental", "dpn_autismo", "dpn_paralisisCerebral", "dpn_diabetes", "dpn_anemia",
	                             "dpn_observacionesEnfermedades");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "datos_personales_nino" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE dpn_id = '$dpn_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo adicionar el paciente por un fallo en el sistema, por Favor llame a Holmes y nathalia que hicieron la aplicacion.".$query;
		$mess_cod = "alert";		
	}else{
	 	$mess = "El usuario $login fue adicionado con &eacute;xito ".$query; 
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }
/**********************/    


/************** crear AntPsicNino ****************/

  function actualizar1(){
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;
/********************/

    $fields = array ( "dpn_antPsicVivePadres", "dpn_comoTratanNino", "dpn_cuantosHermanosTiene", "dpn_numeroOcupa", 
	                      "dpn_comoTratanHermanos", "dpn_observacionesPsicologicas", "dpn_antOdonFechaVisitaOdont", "dpn_motivo", "dpn_reaccionDesfavorableTratam", 
	                       "dpn_condicionesDentalesRaras", "dpn_tiposTraumaOralDental", "dpn_cambiosColorMovilidad", 
	                        "dpn_ninoCooperadorOdontologo", "dpn_tratamientosCaseros", "dpn_observacionesOdontologicas", 
	                         "dpn_habitosD1controlDesayuno", "dpn_1controlAlmuerzo", "dpn_1controlComida", "dpn_2controlDesayuno", 
	                          "dpn_2controlAlmuerzo", "dpn_2controlComida", "dpn_3controlDesayuno", "dpn_3controlAlmuerzo", 
	                           "dpn_3controlComida", "dpn_rechinaDientes", "dpn_chupaDedo", "dpn_comeUnas", "dpn_respiraBoca", 
	                            "dpn_tomaTetero", "dpn_otrosHabitos");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "datos_personales_nino" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE dpn_id = '$dpn_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo adicionar el paciente por un fallo en el sistema, por Favor llame a Holmes y nathalia que hicieron la aplicacion.".$query;
		$mess_cod = "alert";		
	}else{
	 	$mess = "El usuario $login fue adicionado con &eacute;xito ".$query; 
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }
/**********************/    


/********************* Buscar AntPsicNino ******************/

function buscarPsic(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);



             $fields = array ( "dpn_antPsicVivePadres", "dpn_comoTratanNino", "dpn_cuantosHermanosTiene", "dpn_numeroOcupa", 
	                      "dpn_comoTratanHermanos", "dpn_observacionesPsicologicas", "dpn_antOdonFechaVisitaOdont", "dpn_motivo", "dpn_reaccionDesfavorableTratam", 
	                       "dpn_condicionesDentalesRaras", "dpn_tiposTraumaOralDental", "dpn_cambiosColorMovilidad", 
	                        "dpn_ninoCooperadorOdontologo", "dpn_tratamientosCaseros", "dpn_observacionesOdontologicas", 
	                         "dpn_habitosD1controlDesayuno", "dpn_1controlAlmuerzo", "dpn_1controlComida", "dpn_2controlDesayuno", 
	                          "dpn_2controlAlmuerzo", "dpn_2controlComida", "dpn_3controlDesayuno", "dpn_3controlAlmuerzo", 
	                           "dpn_3controlComida", "dpn_rechinaDientes", "dpn_chupaDedo", "dpn_comeUnas", "dpn_respiraBoca", 
	                            "dpn_tomaTetero", "dpn_otrosHabitos");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "datos_personales_nino" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE dpn_id = '$dpn_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo adicionar el paciente por un fallo en el sistema, por Favor llame a Holmes y nathalia que hicieron la aplicacion.".$query;
		$mess_cod = "alert";		
	}else{
	 	$mess = "El usuario $login fue adicionado con &eacute;xito ".$query; 
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }


  /************** Modificar AntPsicNino ****************/
  function modificarPsic(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
/********************/

    $fields = array ( "dpn_antPsicVivePadres", "dpn_comoTratanNino", "dpn_cuantosHermanosTiene", "dpn_numeroOcupa", 
	                      "dpn_comoTratanHermanos", "dpn_observacionesPsicologicas", "dpn_antOdonFechaVisitaOdont", "dpn_motivo", "dpn_reaccionDesfavorableTratam", 
	                       "dpn_condicionesDentalesRaras", "dpn_tiposTraumaOralDental", "dpn_cambiosColorMovilidad", 
	                        "dpn_ninoCooperadorOdontologo", "dpn_tratamientosCaseros", "dpn_observacionesOdontologicas", 
	                         "dpn_habitosD1controlDesayuno", "dpn_1controlAlmuerzo", "dpn_1controlComida", "dpn_2controlDesayuno", 
	                          "dpn_2controlAlmuerzo", "dpn_2controlComida", "dpn_3controlDesayuno", "dpn_3controlAlmuerzo", 
	                           "dpn_3controlComida", "dpn_rechinaDientes", "dpn_chupaDedo", "dpn_comeUnas", "dpn_respiraBoca", 
	                            "dpn_tomaTetero", "dpn_otrosHabitos");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "datos_personales_nino" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE dpn_id = '$dpn_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo adicionar el paciente por un fallo en el sistema, por Favor llame a Holmes y nathalia que hicieron la aplicacion.".$query;
		$mess_cod = "alert";		
	}else{
	 	$mess = "El usuario $login fue adicionado con &eacute;xito ".$query; 
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }
/**********************/    





  /** Guardar los datos personales del usuario (Contenido Directorio) ****/
  function Guardar_Datos(){
    global $DB;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    $query = "UPDATE personal 
              SET nombre='$nombre',
              documento='$documento',
              direccion='$direccion',
              telefono='$telefono',
              fecha_nac='$ano-$mes-$dia',
              email='$email',
              celular='$celular',
              homepage='$homepage',
              sexo='$sexo' 
              WHERE id='$id_usuario'";
    $DB->sql_query($query);// echo $query;
    
    $query = "SELECT username,dominio 
              FROM usuario 
              WHERE id_usuario='$id_usuario' ";
    $DB->sql_query($query);
    $row = $DB->sql_fetch_object();
    $query = "UPDATE vpopmail 
              SET pw_gecos='$nombre' 
              WHERE pw_name='".$row->username."' 
              AND pw_domain='".$row->dominio."' ";
    $DB->sql_query($query);
    
    $result = $this->Datos();
    $result["mess"] = "Los cambios fueron realizados con exito!!" . $result["mess"] ;
    $result["mess_cod"] = "info";
    return $result;
  }

  /*** Buscar los datos personales para mostrarlos ****/
  function Datos(){
    global $DB;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    $query = "SELECT u.*,p.* 
              FROM personal p,usuario u 
              WHERE u.id_usuario='$id_usuario' 
              AND u.id_usuario = p.id";
   $DB->sql_query($query);//echo $query;
    if(!($dato = $DB->sql_fetch_object()))
      $dato = $id_usuario;
    $s_opc = "datos_personales";

    return array("s_opc"=>$s_opc,"dato"=>$dato);
  }

}
?>
