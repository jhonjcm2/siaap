<?
/*************************************************************************/
// CLASE: docente
// Proposito: Realizar tareas de docentes a cargo de la historia.
// Ultima modificacion: octubre 2003
// /**********************************************************************/
class docente {
  var $fv;
  var $data;
	
function docente(){
    global $table;
    
    $this->fv = new FormValidator;
   
  }

  /**********************************************************************************************/
  // Proposito: Creaci�n de un docente y odont�logo a cargo de una historia cl�nica
  // return: arreglos con resultados de la creacion.
  /**********************************************************************************************/
  function creardochic($doc_id){
    global $table, $_POST, $_GET, $_SESSION;

    foreach($_POST as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($_GET as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      

   if ( 0 ){
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    } else {
    /* $query="select last_value from paciente_pac_id_seq ";
	       $table->search($query);
	      	$obj = $table->sql_fetch_object();
	     	$hic_id=$obj->last_value;*/
	     	
	          	
	$fields = array ("pac_id", "usu_id", "deh_fechaEncargado" );
								     
       $usu_id=$doc_id;
       
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "docentes_encargados_historia" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "No se pudo adicionar el docente por un fallo en el sistema, por favor contacte a el administrador.".$query;
	$mess_cod = "alert";		
	}else{
	    logs::crear("docente", "creardochic", $query);
	    $mess = "los datos $login fueron adicionados con &eacute;xito $doc_id $usu_id ".$query; 
	    $s_opc = "busqueda";
	 } 
	  
    }

    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }

  /**********************************************************/
  function crearodohic($odl_id){
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($_GET as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      

   if ( 0 ){
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    } else {
   
	     	
	$fields = array ("pac_id", "usu_id", "oeh_fechaEncargado" );
					     
       $usu_id = $odl_id;

       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "odontologos_encargados_historia" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "No se pudo adicionar los datos por un fallo en el sistema, por Favor llame al administrador del sistema.";
	$mess_cod = "alert";		
	}else{
	    logs::crear("docente", "crearodohic", $query);
	    $mess = "los datos $login fueron adicionado con &eacute;xito  $odl_id $usu_id ".$query; 
	    $s_opc = "info_usuario";
	 } 
	  
    }

    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }





 
}
?>