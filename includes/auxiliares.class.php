<?
/**********************************************************/
// CLASE: auxiliares
// Proposito: Manejo de los procesos del �rea de auxiliares.
// Ultima modificacion: noviembre de 2006
// /**********************************************************/
class auxiliares{
  var $fv;
  var $data;
	
  function auxiliares($id=-1){
    global $table;
    
    $this->fv = new FormValidator;
    if ($id >= 0){
    	$query = "SELECT * 
        	     FROM auxiliares
        	     WHERE aux_id='$id' ";
	$table->sql_query($query);
    	$this->data = $table->sql_fetch_object();
    }
    
  }

  /**********************************************************/
  // Proposito: Almacenar cada asignatura de auxiliares
  // return: arreglos con resultados de la creacion.
  /**********************************************************/
  function crearAsg(){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;
 
    $this->fv->resetErrorList();
    $this->fv->isEmpty("usu_id","Seleccione un Docente para agregar");
    $this->fv->isEmpty("asg_codigo","Ingrese el c&oacute;digo de la asignatura");
    $this->fv->isEmpty("asg_nombre","Ingrese el nombre de la asignatura");
    
   if ( $this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    }else {
      $query = "SELECT * 
                FROM asignatura
                WHERE asg_semestre='$asg_semestre' and usu_id='$usu_id' and asg_codigo='$asg_codigo' and asg_grupo='$asg_grupo'";
      $table->search($query);
    
      if($table->nfound) {
	$mess = "<center>Una asignatura con ese c&oacute;digo y en ese grupo YA se encuentra registrada.<br>Haga click <a href='$PHP_SELF?opc=auxiliares&s_opc=default'>Aqu&iacute;</a> para ingresar otra asignatura</center>";
	$mess_cod = "alert";
	}
      else {
	/*  se pasa a la quota a bytes */
	//$cit_fecha="$fn_anio-$fn_mes-$fn_dia";
         $fields = array ("asg_id", "usu_id", "asg_nombre", "asg_codigo", "asg_semestre", "asg_unidad", "asg_grupo") ;
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "asignatura" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "<center><b>No se pudo Ingresar la asignatura por un fallo en el sistema</b><br> Por favor comuniquese con el administrador.</b></center>".$query;
	$mess_cod = "alert";		
	}else{
	 //   logs::crear("auxiliares", "crear", $query);	
	    $mess = "<center><b>La asignatura fue asignada con &eacute;xito</b><br>
	    Haga click <a href='$PHP_SELF?opc=auxiliares&s_opc=default'>Aqu&iacute;</a> para ingresar otra asignatura</center>";
	    $mess_cod="info";
	    $s_opc = "info_usuario";
	 } 
	  
      }
    }
     return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }

/*********************Crear una evaluacion **************/

 function creareva(){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;
 
    $this->fv->resetErrorList();
    $this->fv->isWithinRange("eva_periodo","Seleccione el periodo para el cual desea realizar la evaluaci&oacute;n", 1,99);
    $this->fv->isWithinRange("eva_ano","Seleccione el a&ntilde;o del periodo para el cual desea realizar la evaluaci&oacute;n", 1,99);
    $this->fv->isEmpty("eva_obs","Ingrese alguna observaci&oacute;n sobre la evaluaci&oacute;n o la asignatura");
   if ( $this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
  
    }else {
      $query = "SELECT * 
                FROM evaluacion_aux
                WHERE asg_id='$asg_id' and usu_id='$usu_id'  and eva_periodo='$eva_periodo' and eva_ano='$eva_ano' ";
      $table->search($query);
    
      if($table->nfound) {
	$mess = "<center>Usted ya realiz&oacute; la evaluci&oacute;n de esta asignatura<br>Haga click <a href='$PHP_SELF?opc=auxiliares&s_opc=busqueda'>Aqu&iacute;</a> para seleccionar otra asignatura</center>";
	$mess_cod = "alert";
	}
      else {
	/*  se pasa a la quota a bytes */
	//$cit_fecha="$fn_anio-$fn_mes-$fn_dia";
         $fields = array ( "usu_id", "asg_id", "eva_periodo", "eva_ano", "eva_fecha", "doc_id", "eva_infRelevante1", "eva_conCoherentes2", "eva_comprension3", "eva_trabIndependiente4",  
         "eva_ubicacion5",  "eva_practica6",  "eva_teoricos7",  "eva_conocer8", "eva_programa9",  "eva_planificacion10",  "eva_horario11",  "eva_metodologia12",  
         "eva_explicacion13", "eva_objetivos14",  "eva_consulta15", "eva_evaluacion16", "eva_resultados17", "eva_formacion18", "eva_acompanamiento19", 
         "eva_respeto20", "eva_responsabilidad21", "eva_construccion22", "eva_consulta23", "eva_horarioConsulta24", "eva_asistencia25", "eva_obs" ) ;
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "evaluacion_aux" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "<center><b>No se pudo Ingresar la evaluaci&oacute;n por un fallo en el sistema</b><br> Por favor comuniquese con el administrador.</b></center>".$query;
	$mess_cod = "alert";		
	}else{
	 //   logs::crear("auxiliares", "crear", $query);	
	    $mess = "<center><b>La evaluaci&oacute;n fue asignada con &eacute;xito</b><br>
	    Haga click <a href='$PHP_SELF?opc=auxiliares&s_opc=busqueda'>Aqu&iacute;</a> para evaluar otra asignatura</center>";
	    $mess_cod="info";
	    $s_opc = "info_usuario";
	 } 
	  
      }
    }
     return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }


/********************* Buscar un horario******************/

function buscar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);



             $fields = array ("cit_id", "pac_id", "hor_id", "cit_fecha", "cit_hora", "cit_tipoCita", "cit_estadoCita");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'SELECT * FROM  "auxiliares" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($where_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($where_array))
	$query .= " WHERE ".implode(", ",$set_array);
	
      	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No hay registros que concuerden con su busqueda".$query;
		$mess_cod = "alert";		
	}else{
	       $mess = "El horario fue adicionado con &eacute;xito ".$query; 
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess.$query,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }



  /************** Modificar auxiliares ****************/
  
  function modificar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    
    $fields = array ("asg_id", "usu_id", "asg_nombre", "asg_codigo", "asg_semestre", "asg_unidad", "asg_grupo") ;
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "asignatura" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE asg_id = '$asg_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "<b>No se pudo modificar la asignatura por un fallo en el sistema. Comuniquese con el administrador</b>".$query;
		$mess_cod = "alert";		
	}else{
	       logs::crear("auxiliares", "modificar", $query);
	 	$mess = "<b>La asignatura fu&eacute; cancelada con &eacute;xito</b>"; 
	 	$mess_cod="info";
	} 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }
 

}
?>
