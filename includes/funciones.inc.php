<?php
/******************************************************************************************
  
Funcion que devuelve el titulo de una pagina
 
 ******************************************************************************************/
function getTitle($url) {
	$str = file_get_contents($url);
	if ($str) {
		preg_match('/<title>([^<]+)</', $str, $title);
		return isset($title[1]) ? $title[1] : false;
	}
	return false;
}
/******************************************************************************************
  
Funcion que permite restar a una fecha, para calcular cual es la fecha hace tantos dias antes de la actual.
 
 ******************************************************************************************/
function calcularFecha($dias){
 		return date("Y-m-d", strtotime("$dias days"));
	}
/* funciones.inc */


/*******************************************************************************************
	fecha:		Funcion que formatea la fecha en espaniol, se podria usar date pero el set localle molesta un poco.
	Parametros:	$FECHA: fecha en formato yyyy-mm-dd
	Retorna:	$LONGFECHA: formato dia, diames de mes anio anio
*******************************************************************************************/
function fecha($FECHA){
	$LONGFECHA='';
	$date=split('-',$FECHA);
	$dia=date("w",mktime(0,0,0,$date[1],$date[2],$date[0]));
	switch($dia){
		case 0:
			$LONGFECHA.="Domingo,";
			break;
		case 1:
			$LONGFECHA.="Lunes,";
			break;
		case 2:
			$LONGFECHA.="Martes,";
			break;
		case 3:
			$LONGFECHA.="Miercoles,";
			break;
		case 4:
			$LONGFECHA.="Jueves,";
			break;
		case 5:
			$LONGFECHA.="Viernes,";
			break;
		case 6:
			$LONGFECHA.="Sabado,";
			break;
	}
	switch($date[1]){
		case 1:
			$LONGFECHA.=" $date[2] de enero a&ntilde;o $date[0]";
			break;
		case 2:
			$LONGFECHA.=" $date[2] de febrero a&ntilde;o $date[0]";
			break;
		case 3:
			$LONGFECHA.=" $date[2] de marzo a&ntilde;o $date[0]";
			break;
		case 4:
			$LONGFECHA.=" $date[2] de abril a&ntilde;o $date[0]";
			break;
		case 5:
			$LONGFECHA.=" $date[2] de mayo a&ntilde;o $date[0]";
			break;
		case 6:
			$LONGFECHA.=" $date[2] de junio a&ntilde;o $date[0]";
			break;
		case 7:
			$LONGFECHA.=" $date[2] de julio a&ntilde;o $date[0]";
			break;
		case 8:
			$LONGFECHA.=" $date[2] de agosto a&ntilde;o $date[0]";
			break;
		case 9:
			$LONGFECHA.=" $date[2] de septiembre a&ntilde;o $date[0]";
			break;
		case 10:
			$LONGFECHA.=" $date[2] de octubre a&ntilde;o $date[0]";
			break;
		case 11:
			$LONGFECHA.=" $date[2] de noviembre a&ntilde;o $date[0]";
			break;
		case 12:
			$LONGFECHA.=" $date[2] de diciembre a&ntilde;o $date[0]";
			break;
	}//switch ($date[1])
	return $LONGFECHA;
}

function validate() {
       
       global $SESSION,$SESS_LIFE;

//	 header("Location: http://siaap.univalle.edu.co/login.php");
       
        if (!is_logged_in()) { 
           //if (!isset($SESSION)){
                // no hay cookie. Ir a login
			// se borran los datos de inicio de sesion en la bd
		   //$maxlife = get_cfg_var("session.gc_maxlifetime");
		   //sess_gc($maxlife);$SESS_LIFE
		   sess_gc($SESS_LIFE);
		   session_destroy();
                   header("Location: login.php");
        } else { 
                // Al parecer hay una sesion activa. se debe validar
                if (session_id()){
                    return $SESSION["user"];
                } else { 
                        // la session expiro o no esta activa
  
			// se borran los datos de inicio de sesion en la bd
			   $maxlife = get_cfg_var("session.gc_maxlifetime");
			   //pgsql_session_gc($maxlife);
			   sess_gc($maxlife);
                        session_destroy();
                        // regresar a login screen
			   //echo "Mama 2"; 

                        header("Location: login.php");
                        exit;
                } // end if sql_error
        } // end if not $ses_id
}


function is_logged_in() {
	global $SESSION;
        return isset($SESSION) && isset($SESSION["user"]) ;
}


function standar_header() {
        error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);
        header("Content-type: text/html");
        header("Expires: Mon, 26 Jul 1992 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-cache, must-revalidate");
        header("Pragma: no-cache");
}


function show_mess($mess,$mess_cod)
{?>
<br />
<div align="center">
	<table border="0" cellpadding="3" cellspacing="1" bgcolor="#000099">
		<tr>
			<td bgcolor="#eeeeee" valign="middle">
				<center>
					<span class="navy"> 
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td width="40"><img src="images/<?=$mess_cod?>.gif" width="40" height="42" border="0" align="middle"></td>
								<td><span class="navy"><?=$mess?></span></td>
							</tr>
						</table>
					</span>
				</center>
			</td>
		</tr>
	</table>
</div>
<br />
<? } 

function email_valid($email) {

  list($local, $domain) = explode("@", $email);

  $pattern_local = '^([0-9a-z]*([-|_]?[0-9a-z]+)*)(([-|_]?)\.([-|_]?)[0-9a-z]*([-|_]?[0-9a-z]+)+)*([-|_]?)$';
  $pattern_domain = '^([0-9a-z]+([-]?[0-9a-z]+)*)(([-]?)\.([-]?)[0-9a-z]*([-]?[0-9a-z]+)+)*\.[a-z]{2,4}$';

  $match_local = eregi($pattern_local, $local);
  $match_domain = eregi($pattern_domain, $domain);
	
  if ($match_local && $match_domain) {
    return 1;
  } else {
    return 0;
  }
}

function getmicrotime(){ 
    list($usec, $sec) = explode(" ",microtime()); 
    return ((float)$usec + (float)$sec); 
    } 

function repetition(){
    static $color=0;
    $color = !$color;
    return ($color)?"#f1f1f1":"#cccccc" ;
}

//calcular la edad de una persona 
//recibe la fecha como un string en formato espa�ol 
//devuelve un entero con la edad. Devuelve false en caso de que la fecha sea incorrecta o mayor que el dia actual 
function calcular_edad($fecha){ 

    //calculo la fecha de hoy 
    //$hoy=new date("j,n,Y");
    //alert(hoy) 

    //calculo la fecha que recibo 
    //La descompongo en un array 
    $array_fecha = explode("-",$fecha);

    //compruebo que los ano, mes, dia son correctos 
    $ano = $array_fecha[0]; 
    if (!$ano)
       return false;

    $mes = $array_fecha[1]; 
    if (!$mes)
       return false;

    $dia = $array_fecha[2]; 
    if (!$dia)
       return false;

    //si el a�o de la fecha que recibo solo tiene 2 cifras hay que cambiarlo a 4 
    if ($ano<=99) 
       $ano +=1900; 

    //resto los a�os de las dos fechas 
    $edad=date("Y")- $ano - 1; //-1 porque no se si ha cumplido a�os ya este a�o 

    //si resto los meses y me da menor que 0 entonces no ha cumplido a�os. Si da mayor si ha cumplido 
    if (date("n") + 1 - $mes < 0) //+ 1 porque los meses empiezan en 0 
       return $edad ;
    if (date("n") + 1 - $mes > 0) 
       return $edad+1; 

    //entonces es que eran iguales. miro los dias 
    //si resto los dias y me da menor que 0 entonces no ha cumplido a�os. Si da mayor o igual si ha cumplido 
    if (date("j") - $dia >= 0) 
       return $edad + 1; 

    return $edad;
}

?>
