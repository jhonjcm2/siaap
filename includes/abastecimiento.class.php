<?
/**********************************************************/
// CLASE: abastecimiento
// Proposito: Realizar un abastecimiento de materiales.
// Ultima modificacion: octubre de 2003
// /**********************************************************/
class abastecimiento{
  var $fv;
  var $data;
	
  function abastecimiento($id=-1){
    global $table;
    
    $this->fv = new FormValidator;
    if ($id >= 0){
    	$query = "SELECT * 
        	     FROM abastecimiento
        	     WHERE aba_id='$id' ";
	$table->sql_query($query);
    	$this->data = $table->sql_fetch_object();
    }
    
  }

  /*********************************************************/
  // Proposito: abastecer el almacén de suministros
  // return: arreglos con resultados de la creación.
  /*********************************************************/
  function crear(){
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;

    foreach($_GET as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;
      
  
   if ($this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    }else {
      $query = 'SELECT * 
                FROM abastecimiento
                WHERE "aba_id"='.$aba_id;
      $table->search($query);
    
      if($table->nfound) {
	$mess = "Un pedido con ese c&oacute;digo ya se encuentra registrado en el sistema, por favor ingrese otro.";
	$mess_cod = "alert";
	$s_opc = "adicionar_usuario";
      }
      else {
	/*  se pasa a la quota a bytes */
         $fields = array ("aba_id", "bod_id", "aba_fechaAbastecimiento", "usu_id");
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "abastecimiento" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "<b>No se pudo realizar el abastecimiento por un fallo en el sistema. Comun&iacute;quese con el administrador</b>";
	$mess_cod = "alert";		
	}else{
	    logs::crear("logs", "crear", $query);
	    $mess = "<b>El abastecimiento fu&eacute; realizado con &eacute;xito</b>"; 
	    $mess_cod = "info";
	    $s_opc = "info_usuario";
	 }
      }
    }
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }


/********************* Buscar un abastecimiento******************/

function buscar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);



             $fields = array ("aba_id", "bod_id", "aba_fechaAbastecimiento", "usu_id");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'SELECT * FROM  "abastecimiento" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($where_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($where_array))
	$query .= " WHERE ".implode(", ",$where_array);
	
      	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No hay registros que concuerden con su busqueda.".$query;
		$mess_cod = "alert";		
	}else{
	       $mess = "El abastecimiento fue adicionado con &eacute;xito ".$query; 
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }
  
/*******Agregar materiales al abastecimiento(Bodega 1: almacen general)**************/

function agregarMat($aba_id=0){
	global $table, $_POST, $_GET;

	foreach($_POST as $k=>$v)
		${$k} = $v;

	foreach($_GET as $k=>$v)
		${$k} = $v;


	$this->fv->resetErrorList();
	$this->fv->isEmpty("mat_id","Seleccione un material para agregar");
	$this->fv->isWithinRange("mxa_cantidadMaterial","Ingrese una cantidad de material v&aacute;lida",1,1000000);
	$this->fv->isNumber("mxa_cantidadMaterial", "Debe insertar solo n&uacute;meros en la cantidad del material");
		
	$this->fv->isEmpty("lote","Debe colocar el lote del material");
	$this->fv->isDate("mxa_vence","No es una fecha correcta debe ser a&ntilde;o-mes-d&iacute;a");


	if ($this->fv->isError()) {
		$mess = $this->fv->getMessage();
		$mess_cod = "alert";
	}
	else {
		////////////////////////REEMPLAZAR///////////////////////////////////////////////////
		/*// se pasa a la quota a bytes
		$fields = array ("mxa_id", "mat_id", "aba_id", "mxa_cantidadMaterial", "mxa_valor", "mxa_vence");
	
		// Arreglos para datos del query y el url para paginacion
		$fields_array = array();
		$values_array = array();
		$query = 'INSERT INTO "matxaba" (';
      		// Adicionando los campos para el query
		foreach($fields as $v)
			if(${$v}!=""){
				array_push($fields_array,' "'.$v.'" ');
				array_push($values_array," '".${$v}."' ");
			}


		if(sizeof($fields_array)){
			$query .= implode(", ",$fields_array);
			$query .= ") VALUES (".implode(", ",$values_array) ;
		}
		$query .=")";

        	$result=$table->sql_query($query);
		if (!$result){
			$mess = "No se pudo realizar el abastecimiento por un fallo en el sistema, por Favor contacte al Administrador del Sistema.";
			$mess_cod = "alert";
		}else{
			$query = "SELECT * FROM matxbod WHERE mat_id=$mat_id AND bod_id=1";
			$table->sql_query($query);
 		      	//*** Subo los materiales a la bodega de destino 1****
			if ($row = $table->sql_fetch_object()){
				
				$cantMat=$row->mxb_cantidadMaterial + $mxa_cantidadMaterial;
				$query="UPDATE matxbod SET \"mxb_cantidadMaterial\"=$cantMat, \"mxb_saldoInicial\"= $cantMat, \"mxb_fechaAbastecimiento\"='". date('Y-m-d', time())."' WHERE  mat_id=$mat_id AND bod_id=1";
				$result = $table->sql_query($query);
			}else{
				$query="INSERT INTO matxbod (\"mxb_cantidadMaterial\", \"mxb_saldoInicial\", \"mxb_fechaAbastecimiento\", \"mxb_fechaActual\", mat_id, bod_id) VALUES 
				($mxa_cantidadMaterial, $mxa_cantidadMaterial, '". date('Y-m-d', time())."', '" . date('Y-m-d', time())."', $mat_id, 1)";
				$result = $table->sql_query($query);
			}
			//********************AGREGAMOS UNA FECHA DE VENCIMIENTO*****************************
			$vence= new Vencimiento();
			$vence->agregarFecha(1,$mat_id,$mxa_vence,$mxa_cantidadMaterial,$lote);
			$bodega = new Bodega();
			$bodega->actualizarCantidadEnBodega(1,$mat_id);
			//***********************************************************************************
			logs::crear("logs", "agregarMat", $query);
			$mess = "<b>El abastecimiento fu&eacute; realizado con &eacute;xito</b> ";
			$mess_cod = "info";
		}//*/
		/**********************CON*********************************************************/
		$fields = array ("mxa_id", "mat_id", "aba_id", "mxa_cantidadMaterial", "mxa_valor", "mxa_vence");
		// Arreglos para datos del query y el url para paginacion
		$fields_array = array();
		$values_array = array();
		$query = 'INSERT INTO "matxaba" (';
      		// Adicionando los campos para el query
		foreach($fields as $v)
			if(${$v}!=""){
				array_push($fields_array,' "'.$v.'" ');
				array_push($values_array," '".${$v}."' ");
			}


		if(sizeof($fields_array)){
			$query .= implode(", ",$fields_array);
			$query .= ") VALUES (".implode(", ",$values_array) ;
		}
		$query .=")";
        	$result=$table->sql_query($query);
		if ($result){
			//echo ":)";
			$bodega = new Bodega();
			$res=$bodega->agregarMaterial(1,$mat_id,$mxa_cantidadMaterial,$mxa_vence,$lote);
			if(strcasecmp($res["mess_cod"],"info")==0){
				logs::crear("logs", "agregarMat", $query);
				$mess = "<b>El abastecimiento fu&eacute; realizado con &eacute;xito</b> ";
				$mess_cod = "info";
			}else{
				$mess="No se puedo realizar el abastecimiento";
				$mess_cod ="alert";
			}
		}else{
			$mess = "No se pudo realizar el abastecimiento por un fallo en el sistema, por Favor contacte al Administrador del Sistema.";
			$mess_cod = "alert";
		}
		/**********************************************************************************/

      }
    //$mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }

  
/************** Borrar un abastecimiento antes de acentar su creaci&oacute;n****************/
 
  function borrar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

     /********************/

    $fields = array ("aba_id", "bod_id", "aba_fechaAbastecimiento", "usu_id");
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'DELETE FROM "abastecimiento" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
        $query .=  " WHERE aba_id = '$aba_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo cancelar el abastecimiento por un fallo en el sistema, Comun&iacute;quese con el administrador.";
		$mess_cod = "alert";		
	}else{
	       logs::crear("logs", "borrar", $query);
	 	$mess = "El abastecimiento $login fu&eacute; cancelado con &eacute;xito "; 
	    	$mess_cod = "info";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }

/***********borrar de la tabla materiales por abastecimiento un abastecimiento de materiales****************/

function borrarMat($aba_id=0){


global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;


//   if ( 0 /*$this->fv->isError()*/ ) {
if ( 0 ){
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
      }
      else {
        /*  se pasa a la quota a bytes */
         $fields = array ("mxa_id", "mat_id", "aba_id", "mxa_cantidadMaterial");

       // Arreglos para datos del query y el url para paginacion
      	$fields_array = array();
      	$values_array = array();
     	$query = 'DELETE FROM "matxaba" ';
     	// Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
        $query .=  " WHERE aba_id = $aba_id";

	$result=$table->sql_query($query);
    
        if (!$result){
        $mess = "No se pudo cancelar el abastecimiento por un fallo en el sistema, Contacte al Administrador del Sistema.";
        $mess_cod = "alert";
        }else{
              logs::crear("logs", "borrarMat", $query);
            	$mess = "<center><b>El abastecimiento $login fu&eacute;  cancelado</b><br>
            	Vuelva a ingresarlo</center>";
	 	$mess_cod = "info";
         }

      }
    //$mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }
}
?>
