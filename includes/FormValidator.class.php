<?php

// FormValidator.class.inc
// class to perform form validation

class FormValidator
{
	//
	// private variables
	// 
	
	var $_errorList;

	//
	// methods (private)
	// 
	
	// function to get the value of a variable (field)
	function _getValue($field)
	{
		global ${$field};
		return ${$field};
	}

	//
	// methods (public)
	// 

	// constructor
	// reset error list
	function FormValidator()
	{
		$this->resetErrorList();
	}
	
	// check whether input is empty
	function isEmpty($field, $msg)
	{
		$value = $this->_getValue($field);
		if (trim($value) == "")
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
		else
		{
			return true;
		}
	}

	// check whether input is a string
	function isString($field, $msg)
	{
		$value = $this->_getValue($field);
		if(!is_string($value))
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
		else
		{
			return true;
		}
	}

	// check whether input is equals to second input
	function isEqualsString($field1, $field2, $msg)
	{
		$value1 = $this->_getValue($field1);
		$value2 = $this->_getValue($field2);

		if(strcmp($value1,$value2))
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
		else
		{
			return true;
		}
	}
	
	// check whether input is a string and size of the string
	function isSizeString($field, $size, $msg)
	{
		$value = $this->_getValue($field);
		if(!is_string($value) || strlen($value) > $size)
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
		else
		{
			return true;
		}
	}
	
	// check whether input is a string and size of the string
	function isMoreThanSize($field, $size, $msg)
	{
		$value = $this->_getValue($field);
		if(!is_string($value) || strlen($value) < $size)
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
		else
		{
			return true;
		}
	}	
	// check whether input is a number
	function isNumber($field, $msg)
	{
		$value = $this->_getValue($field);
		if(!is_numeric($value))
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
		else
		{
			return true;
		}
	}

	// check whether input is an integer
	function isInteger($field, $msg)
	{
		$value = $this->_getValue($field);
		if(!is_integer($value))
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
		else
		{
			return true;
		}
	}

	// check whether input is equals to regular expression
	function isRegExp($field,$regexp, $msg)
	{
		$value = $this->_getValue($field);
		if(!ereg($regexp,$value))
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
		else
		{
			return true;
		}
	}
	
	// check whether input is a float
	function isFloat($field, $msg)
	{
		$value = $this->_getValue($field);
		if(!is_float($value))
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
		else
		{
			return true;
		}
	}
	
	// check whether input is alphabetic
	function isAlpha($field, $msg)
	{
		$value = $this->_getValue($field);
		$pattern = "/^[a-zA-Z]+$/";
		if(preg_match($pattern, $value))
		{
			return true;
		}
		else
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
	}

	// check whether input is within a valid numeric range
	function isWithinRange($field, $msg, $min, $max)
	{
		$value = $this->_getValue($field);
		if(!is_numeric($value) || $value < $min || $value > $max)
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
		else
		{
			return true;
		}
	}
	
	// check whether input is a valid email address
	function isEmailAddress($field, $msg)
	{
		$value = $this->_getValue($field);
		$pattern = "/^([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-]+)+$/";
		if(preg_match($pattern, $value))
		{
			return true;
		}
		else
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
	}
	
	// return the current list of errors
	function getErrorList()
	{
		return $this->_errorList;
	}
	
	// check whether any errors have occurred in validation
	// returns Boolean
	function isError()
	{
		if (sizeof($this->_errorList) > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function isDate($field, $msg){
			$value=$this->_getValue($field);
			$dia=substr($value,8,2);
			$mes=substr($value,5,2);
			$anio=substr($value,0,4);
			if(checkDate($mes,$dia,$anio)){
				return true;
			}else{
				$this->_errorList[] = array("field"=>$field, "value" =>$value, "msg" => $msg);
				return false;
			}
	}

	// reset the error list
	function resetErrorList()
	{
		$this->_errorList = array();
	}
	
	// return the error message
	function getMessage()
	{
	  $errors = $this->getErrorList();
	  $mess = "<b>ATENCION!</b><br>\nSe presentaron los siguientes errores:\n<ul>";
	  foreach($errors as $item) 
	    $mess .= "<li>".$item['msg']."\n";
	  
	  $mess .= "</ul>\nPor favor corr&iacute;jalo(s) e intente de nuevo\n";
	  return $mess;
	}
// end 
}

?>
