<?
/**********************************************************/
// CLASE: pagos
// Proposito: consignar los pagos del paciente.
// Ultima modificacion: Febrero de 2010
// /**********************************************************/
class pagos{
	var $fv;
  	var $data;
	
  	function pagos($id=-1){
    	global $table;
    	$this->fv = new FormValidator;
    	if ($id >= 0){
	    	$query = "SELECT * FROM pagos WHERE pag_id='$id' ";
	 		$table->sql_query($query);
			$this->data = $table->sql_fetch_object();
  		}
	}

  /**********************************************************/
  // Proposito: Consignar los pagos del paciente 
  // return: arreglos con resultados de la creacion.
  /**********************************************************/
	function crear(){
    	global $table, $_POST, $_GET;

    	foreach($_POST as $k=>$v)
	    	//${$k} = base::dispelMagicQuotes($v);
      		${$k} = $v;

    	foreach($_GET as $k=>$v)
     		// ${$k} = base::dispelMagicQuotes($v);
      		${$k} = $v;
      
      	$this->fv->resetErrorList();
     	$this->fv->isWithinRange("pag_centroInfo","Seleccione un centro de informaci&oacute;n",0,9999);
      	$this->fv->isWithinRange("pag_codigoIng","Seleccione un codigo de ingreso",0,9999);
      	$this->fv->isEmpty("pag_conceptoPago", "Debe ingresar el concepto del pago");
      	$this->fv->isWithinRange("ara_id","Seleccione una &aacute;rea de atencion",0,9999);
      	  
   		if ( $this->fv->isError() ) {
      		$mess = $this->fv->getMessage();
      		$mess_cod = "alert";
    	}else {
			$fields = array ("pag_id", "pag_fecha", "pac_id", "usu_id", "pag_numeroCuenta", "pag_tipoPagoEfectivo", "pag_valorEfectivo", 
				"pag_valorCopago", "pag_conceptoPago", "pag_tipoPagoCheque", "ara_id", "pag_centroInfo","pag_codigoIng", 
				"pag_tipoPagoCredito","pag_tipoPagoDebito");
			//Arreglos para datos del query y el url para paginacion
      		$fields_array = array();
      		$values_array = array();
     		$query = 'INSERT INTO "pagos" (';
	      	//Adicionando los campos para el query
      		foreach($fields as $v)
				if(${$v}!=""){
	  				array_push($fields_array,' "'.$v.'" ');
	  				array_push($values_array," '".${$v}."' ");
				}

      			if(sizeof($fields_array)){
					$query .= implode(", ",$fields_array);
					$query .= ") VALUES (".implode(", ",$values_array) ;
				}
				$query .=")";	
				$result=$table->sql_query($query);
				if (!$result){
					$mess = "<b>No se pudo registrar el pago por un fallo en el sistema. Comuniquese con el administrador.</b>";
					$mess_cod = "alert";		
				}else{
					$last_pag_id = array();
					$table->sql_query('SELECT max(pag_id) FROM pagos');
					array_push($last_pag_id, $table->sql_fetch_object());
					if($_POST['pag_tipoPagoCredito'] == 1){
						$this->agregarCredito($last_pag_id[0]->max);
					}
	    				if($_POST['pag_tipoPagoDebito'] == 1){
						$this->agregarDebito($last_pag_id[0]->max);
					}
					if($_POST['pag_tipoPagoCheque'] == 1){
						$this->agregarCheque($last_pag_id[0]->max);
					}
					logs::crear("pagos", "crear", $query);
					$mess = "<b>El pago fue registrado con &eacute;xito</b>".$query; 
					$mess_cod = "info";
				} 
	  	}	
		$mess .= $query;
    	return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  	}
/********************* Buscar pagos******************/

	function buscar(){
    	global $table, $_POST ,$_GET;

     	foreach($_POST as $k=>$v)
      		${$k} = base::dispelMagicQuotes($v);

    	foreach($_GET as $k=>$v)
      		${$k} = base::dispelMagicQuotes($v);

		$fields = array ( "pag_id", "pag_fecha", "pac_id", "usu_id", "pag_numeroCuenta", "pag_tipoPagoEfectivo", "pag_valorConsulta", 
			"pag_valorCopago", "pag_valorEnLetras", "pag_conceptoPago", "pag_eps", "pag_centroInfo", "pag_codigoIng", 
			"pag_tipoPagoDebito", "pag_tipoPagoCredito");
							      
       	// Arreglos para datos del query y el url para paginacion

     	$where_array = array();
     	$query = 'SELECT * FROM  "pagos" ';
      	// Adicionando los campos para el query
       
     	foreach($fields as $v)
        	if(${$v}!=""){
	  			array_push($where_array,' "'.$v.'" '." = '".${$v}."' ");
			}

       	if(sizeof($where_array))
			$query .= " WHERE ".implode(", ",$where_array);
			
		$result=$table->search($query);
		
		if (!$result){
			$mess = "No hay registros que concuerden con su busqueda.".$query;
			$mess_cod = "alert";		
		}else{
	    	$mess = "El pago fue adicionado con &eacute;xito ".$query; 
	    	$s_opc = "info_usuario";
	 	} 
		return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
	}

/**************************
almacena la informacion de cheques en la tabla pago_cheque
*************************/
  
	function agregarCheque($pag_id){

		global $table, $_POST, $_GET;
		$query = sprintf('INSERT INTO "pago_cheque" ("pch_chequeNo", "pch_nombreBanco", "pch_cuentaNo", "pch_valorCheque", "pag_id") 
			VALUES (\'%s\', \'%s\', \'%s\', \'%s\', \'%s\')', $_POST['pch_chequeNo'], $_POST['pch_nombreBanco'],$_POST['pch_cuentaNo'],
			$_POST['pch_valorCheque'], $pag_id);
  		$result = $table->sql_query($query);
  		if (!$result){
	   		$mess = "No se pudo adicionar los datos por un fallo en el sistema, Contacte al Administrador del Sistema.";
	   		$mess_cod = "alert";
   		}else{
       		logs::crear("pagos", "agregarCheque", $query);
       		$mess = "Los datos fueron adicionados con &eacute;xito ".$query;
       		$mess_cod = "info";
    	}
    	$mess .= $query;
    	return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
	}

/********************Agregar pago con tarjeta debito********************************/
  
	function agregarDebito($pag_id){
		global $table, $_POST, $_GET;
		$query = sprintf('INSERT INTO "pago_debito" ("pde_tarjetaNo", "pde_valor", "pag_id") 
			VALUES (\'%s\', \'%s\', \'%s\')',$_POST['pde_tarjetaNo'], $_POST['pde_valor'], $pag_id);
   		$result = $table->sql_query($query);
   		if (!$result){
	   		$mess = "No se pudo adicionar los datos por un fallo en el sistema, Contacte al Administrador del Sistema.";
	   		$mess_cod = "alert";
   		}else{
       		logs::crear("pagos", "agregarDebito", $query);
       		$mess = "Los datos fueron adicionados con &eacute;xito ".$query;
       		$mess_cod = "info";
    	}
    	$mess .= $query;
    	return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  	}
  
/********************Agregar pago con tarjeta debito********************************/
  
	function agregarCredito($pag_id){
		global $table, $_POST, $_GET;
		$query = sprintf('INSERT INTO "pago_credito" ("pcr_tarjetaNo", "pcr_valor", "pag_id") 
			VALUES (\'%s\', \'%s\', \'%s\')',$_POST['pcr_tarjetaNo'], $_POST['pcr_valor'], $pag_id);
   		$result = $table->sql_query($query);
   		if (!$result){
		   $mess = "No se pudo adicionar los datos por un fallo en el sistema, Contacte al Administrador del Sistema.";
		   $mess_cod = "alert";
   		}else{
       		logs::crear("pagos", "agregarCredito", $query);
       		$mess = "Los datos fueron adicionados con &eacute;xito ".$query;
       		$mess_cod = "info";
    	}
    	$mess .= $query;
    	return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  	}
  
/******************** Borrar un pago que no ha sido impreso***********************/
 
	function borrar(){
    	global $table;

    	foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      		${$k} = base::dispelMagicQuotes($v);

    	foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      		${$k} = base::dispelMagicQuotes($v);

   		$fields = array ("pag_id", "pag_fecha", "pac_id", "usu_id", "pag_numeroCuenta", "pag_tipoPagoEfectivo", "pag_valorEfectivo", 
   			"pag_valorCopago", "pag_conceptoPago", "pag_valorEnLetras", "pag_tipoPagoCheque", "pag_eps", "pag_centroInfo", 
   			"pag_codigoIng", "pag_tipoPagoCredito", "pag_tipoPagoDebito");
							      
       // Arreglos para datos del query y el url para paginacion

      	$set_array = array();
     	$query = 'DELETE FROM "pagos" ';
      	// Adicionando los campos para el query
      	foreach($fields as $v)
			if(${$v}!=""){
	  			array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
			}
		
       	if(sizeof($set_array))
			$query .= " SET ".implode(", ",$set_array);
	
      	$query .=  " WHERE pag_id =$pag_id";

		$result=$table->sql_query($query);
	
		if (!$result){
			$mess = "No se pudo cancelar el pago por un fallo en el sistema, Comun&iacute;quese con el administrador.".$query;
			$mess_cod = "alert";		
		}else{
	       	logs::crear("pedido", "borrar", $query);
	 		$mess = "El pago $login fu&eacute; cancelado con &eacute;xito ".$query; 
	    	$mess_cod = "info";
	 	} 
	  	return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  	}

/***********borrar de la tabla estado_cuenta un pago no impreso****************/

  	function borrarEst($pag_id=0){

		global $table, $_POST, $_GET;

    	foreach($_POST as $k=>$v)
	    	//${$k} = base::dispelMagicQuotes($v);
      		${$k} = $v;

    	foreach($_GET as $k=>$v)
	    	// ${$k} = base::dispelMagicQuotes($v);
      		${$k} = $v;

		//if ( 0 /*$this->fv->isError()*/ ) {
		if ( 0 ){
	      $mess = $this->fv->getMessage();
	      $mess_cod = "alert";
	    }
		else {
	    	/*  se pasa a la quota a bytes */
	        $fields = array  ("esc_id", "pac_id", "evo_id", "pag_id", "esc_fecha", "esc_saldo", "esc_debe", "esc_pago", "pag_codigoIng");

       // Arreglos para datos del query y el url para paginacion
	      	$fields_array = array();
	      	$values_array = array();
	     	$query = 'DELETE FROM "estado_cuenta" ';
	     	// Adicionando los campos para el query
	      	foreach($fields as $v)
				if(${$v}!=""){
					array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
				}
			
       		if(sizeof($set_array))
				$query .= " SET ".implode(", ",$set_array);
			
			$query .=  " WHERE pag_id = '$pag_id'";

			$result=$table->sql_query($query);
    
        	if (!$result){
        		$mess = "No se pudo cancelar el pago por un fallo en el sistema, Contacte al Administrador del Sistema.";
        		$mess_cod = "alert";
        	}else{
            	logs::crear("pedido", "borrarMat", $query);
            	$mess = "<center><b>El pago $login fu&eacute;  cancelado</b><br>
            	Vuelva a ingresarlo</center>".$query;
	 			$mess_cod = "info";
         	}
      	}
    //$mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }

/***********borrar de la tabla pago_cheque un pago no impreso****************/

	function borrarCheque($pag_id=0){

		global $table, $_POST, $_GET;

    	foreach($_POST as $k=>$v)
	    	//${$k} = base::dispelMagicQuotes($v);
      		${$k} = $v;
	
    	foreach($_GET as $k=>$v)
	 		// ${$k} = base::dispelMagicQuotes($v);
      		${$k} = $v;

		
//   if ( 0 /*$this->fv->isError()*/ ) {
		if ( 0 ){
      		$mess = $this->fv->getMessage();
      		$mess_cod = "alert";
      	}
      	else {
        	/*  se pasa a la quota a bytes */
         	$fields = array ("pch_id", "pch_chequeNo", "pch_nombreBanco", "pch_cuentaNo", "pch_valorCheque", "pag_id");

       		// Arreglos para datos del query y el url para paginacion
      		$fields_array = array();
      		$values_array = array();
     		$query = 'DELETE FROM "pago_cheque" ';
     		//Adicionando los campos para el query
      		foreach($fields as $v)
				if(${$v}!=""){
	  				array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
				}

       		if(sizeof($set_array))
				$query .= " SET ".implode(", ",$set_array);
	
      		$query .=  " WHERE pag_id = '$pag_id'";

			$result=$table->sql_query($query);
    
        	if (!$result){
        		$mess = "No se pudo cancelar el pago por un fallo en el sistema, Contacte al Administrador del Sistema.";
        		$mess_cod = "alert";
        	}else{
            	logs::crear("pedido", "borrarMat", $query);
            	$mess = "<center><b>El pago $login fu&eacute;  cancelado</b><br> Vuelva a ingresarlo</center>";
	 			$mess_cod = "info";
         	}
		}
    //$mess .= $query;
    	return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  	}
  	
/************** Anular un pago ****************/
  
  	function anular(){
  		global $table;

    	foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      		${$k} = base::dispelMagicQuotes($v);

    	foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      		${$k} = base::dispelMagicQuotes($v);

    	$fields = array ("pag_id", "pag_fecha", "pac_id", "usu_id", "pag_estado", "pag_tipoPagoEfectivo", "pag_valorEfectivo", 
    		"pag_valorCopago", "pag_conceptoPago", "pag_valorEnLetras", "pag_tipoPagoCheque", "pag_eps", "pag_centroInfo", 
    		"pag_codigoIng");
			// Arreglos para datos del query y el url para paginacion

	   	$set_array = array();
     	$query = 'UPDATE  "pagos" ';
      	// Adicionando los campos para el query
      	foreach($fields as $v)
			if(${$v}!=""){
	  			array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
			}

       	if(sizeof($set_array))
			$query .= " SET ".implode(", ",$set_array);
	
      	$query .=  " WHERE pag_id = '$pag_id'";

		$result=$table->sql_query($query);
	
		if (!$result){
			$mess = "<b>No se pudo anular el pago por un fallo en el sistema. Comuniquese con el administrador</b>";
			$mess_cod = "alert";		
		}else{
	    	logs::crear("cita", "modificar", $query);
	 		$mess = "<b>El pago fu&eacute; anulado con &eacute;xito</b>"; 
	 		$mess_cod="info";
		} 
	  
    	return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  	}
}
?>
