<?
/**********************************************************/
// CLASE: Cita
// Proposito: registrar cada asignacion de citas.
// Ultima modificacion: Octubre de 2003
// /**********************************************************/
class cita{
  var $fv;
  var $data;
	
  function cita($id=-1){
    global $table;
    
    $this->fv = new FormValidator;
    if ($id >= 0){
    	$query = "SELECT * 
        	     FROM cita
        	     WHERE cit_id='$id' ";
	$table->sql_query($query);
    	$this->data = $table->sql_fetch_object();
    }
    
  }

  /**********************************************************/
  // Proposito: Almacenar cada asignacion de cita
  // return: arreglos con resultados de la creacion.
  /**********************************************************/
  function crear(){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;
 
    $this->fv->resetErrorList();
    $this->fv->isEmpty("pac_id","Seleccione un paciente para agregar");
    $this->fv->isWithinRange("cit_tipoCita","Seleccione el tipo de cita",0,99);
    
   if ( $this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    }else {
      $query = "SELECT * 
                FROM cita
                WHERE hor_id ='$hor_id' and pac_id = '$pac_id' and cit_fecha='$cit_fecha' and \"cit_estadoCita\"='$cit_estadoCita'";
      $table->search($query);
    
      if($table->nfound) {
	$mess = "El paciente ya esta citado para esta cl&iacutenica, por favor verifique";
	$mess_cod = "alert";
	}
      else {
	/*  se pasa a la quota a bytes */
	//$cit_fecha="$fn_anio-$fn_mes-$fn_dia";
         $fields = array ("cit_id", "pac_id", "hor_id", "cit_fecha", "cit_hora", "cit_tipoCita", "cit_estadoCita","cit_horaCita", "cit_fechaIngreso") ;
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "cita" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "<center><b>No se pudo asignar la cita por un fallo en el sistema</b><br> Por favor comuniquese con el administrador.</b></center>".$query;
	$mess_cod = "alert";		
	}else{
	 //   logs::crear("cita", "crear", $query);	
	    $mess = "<b>La cita fue asignada con &eacute;xito</b><br>
	    Haga click <a href='$PHP_SELF?opc=horarios&s_opc=listar2&usu_id=$usu_id'>Aqu&iacute;</a> para Asignar otra Cita ";
	    $mess_cod="info";
	    $s_opc = "info_usuario";
	 } 
	  
      }
    }
     return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }


/********************* Buscar un horario******************/

function buscar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);



             $fields = array ("cit_id", "pac_id", "hor_id", "cit_fecha", "cit_hora", "cit_tipoCita", "cit_estadoCita");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'SELECT * FROM  "cita" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($where_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($where_array))
	$query .= " WHERE ".implode(", ",$set_array);
	
      	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No hay registros que concuerden con su busqueda".$query;
		$mess_cod = "alert";		
	}else{
	       $mess = "El horario fue adicionado con &eacute;xito ".$query; 
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess.$query,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }



  /************** Modificar cita ****************/
  
  function modificar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    
    $fields = array ("cit_id","pac_id", "hor_id", "cit_fecha", "cit_hora", "cit_tipoCita", "cit_estadoCita") ;
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "cita" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE cit_id = '$cit_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "<b>No se pudo modificar la cita por un fallo en el sistema. Comuniquese con el administrador</b>";
		$mess_cod = "alert";		
	}else{
	       logs::crear("cita", "modificar", $query);
	 	$mess = "<b>La cita fu&eacute; cancelada con &eacute;xito</b>"; 
	 	$mess_cod="info";
	} 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }
 

}
?>
