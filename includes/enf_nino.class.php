<?
/**********************************************************/
// CLASE: enf_nino
// Proposito: Realizar el manejo de datos de las enfermedades posibles de un  paciente en la ni�ez.
// Ultima modificacion: Mayo 13 de 2005
// /**********************************************************/
class enf_nino{
  var $fv;
  var $data;
	
  function enf_nino($id=-1){
    global $table;
    
    $this->fv = new FormValidator;
    if ($id >= 0){
    	$query = "SELECT * 
        	     FROM enf_nino
        	     WHERE enf_id='$id' ";
	$table->sql_query($query);
    	$this->data = $table->sql_fetch_object();
    }
    
  }

  /**********************************************************/
  // Proposito: Creacion de los datos de las enfermedades propies de un paciente en la ni�ez
  // return: arreglos con resultados de la creacion.
  /**********************************************************/
  function crear(){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;
      
      
      $this->fv->resetErrorList();
     
   if ( $this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    }else {
      $query = 'SELECT * 
                FROM enf_nino
                WHERE "enf_id"='.$enf_id;
      $table->search($query);
    
      if($table->nfound) {
	$mess = "Un registro  con esa identificacion ya se encuentra registrado en el sistema.";
	$mess_cod = "alert";
	$s_opc = "adicionar_usuario";
      }
      else {
	/*  se pasa a la quota a bytes */
	// $enf_fechaElaboracion="$fn_anio-$fn_mes-$fn_dia";
         $fields = array ("enf_id", "pac_id", "usu_id", "enf_fechaIngreso", "enf_comentario",  "enf_fechaElaboracion");
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "enf_nino" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "<b>No se pudo adicionar el registro por un fallo en el sistema, Comuniquese con el administrador del sistema.". $query;
	$mess_cod = "alert";		
	}else{
	   
	    $mess = "el registro  $login fu&eacute; adicionada con &eacute;xito "; 
	    $mess_cod = "info";
	 } 
	  
      }
    }
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }


/********************* Buscar una Evolucion *****************/

function buscar(){
    global $table, $_POST ,$_GET;

    foreach($_POST as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($_GET as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);



             $fields = array ("evo_id", "pac_id", "tco_id", "cac_id", "amp_id", "fip_id", "faq_id", "evo_fecha", "evo_comentario", "evo_autorizacion", "evo_numeroAutorizacion", "usu_id");
							      
       // Arreglos para datos del query y el url para paginacion

      $where_array = array();
     $query = 'SELECT * FROM  "evolucion" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($where_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($where_array))
	$query .= " WHERE ".implode(", ",$where_array);
	
      	$result=$table->search($query);
	
	if (!$result){
		$mess = "No hay registros que concuerden con su busqueda.".$query;
		$mess_cod = "alert";		
	}else{
	       $mess = "la evoluci&oacute;n $login fu&eacute; encontrada con &eacute;xito ".$query; 
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }

/***************************************************************/
function agregarCie($enf_id=0){


global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;
      
      $this->fv->resetErrorList();
      

   if ( $this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
      }
      else {
        /*  se pasa a la quota a bytes */
         $fields = array ("nxc_id", "cie_id", "enf_id");

       // Arreglos para datos del query y el url para paginacion
      	$fields_array = array();
      	$values_array = array();
     	$query = 'INSERT INTO "ninoxcie" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
        if(${$v}!=""){
          array_push($fields_array,' "'.$v.'" ');
          array_push($values_array," '".${$v}."' ");
        }

      if(sizeof($fields_array)){
        $query .= implode(", ",$fields_array);
        $query .= ") VALUES (".implode(", ",$values_array) ;
        }
        $query .=")";

        $result=$table->sql_query($query);

        if (!$result){
        $mess = "No se pudo adicionar el registro por un fallo en el sistema, por Favor contacte al Administrador del Sistema.";
        $mess_cod = "alert";
        }else{
           
            $mess = "El regsitro fu&eacute; adicionado con &eacute;xito ";
            $s_opc = "info_usuario";
         }

      }
    //$mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }

/************** Borrar una evoluci�n antes de ser autorizada ****************/
 
  function borrar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    
     	$$fields = array ("enf_id", "pac_id", "usu_id", "enf_fechaIngreso", 
     				   "enf_comentario",  "enf_fechaElaboracion", enf_estado);
							      
       // Arreglos para datos del query y el url para paginacion

      $fields_array = array();
      	$values_array = array();
     $query = 'DELETE FROM "enf_nino" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE enf_id =$enf_id";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo cancelar La informaci&oacute;n por un fallo en el sistema, Comun&iacute;quese con el administrador.".$query;
		$mess_cod = "alert";		
	}else{
	       logs::crear("evolucion", "borrar", $query);
	 	$mess = "La informaci&oacute;n $login fue cancelada con &eacute;xito "; 
	    	$mess_cod = "info";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }

/***********borrar de la tabla procxevo una evoluci&oacute;n****************/
function borrarProc($cie_id=0){


global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;


//   if ( 0 /*$this->fv->isError()*/ ) {
if ( 0 ){
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
      }
      else {
        /*  se pasa a la quota a bytes */
          $fields = array ("nxc_id", "cie_id", "enf_id");

       // Arreglos para datos del query y el url para paginacion
      	$fields_array = array();
      	$values_array = array();
     	$query = 'DELETE FROM "ninoxcie" ';
     	// Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHEREenf_id = '$enf_id'";

	$result=$table->sql_query($query);
    
        if (!$result){
        $mess = "No se pudo cancelar la informaci&oacute;n por un fallo en el sistema, Contacte al Administrador del Sistema.";
        $mess_cod = "alert";
        }else{
              logs::crear("evolucion", "borrarProc", $query);
            	$mess = "<center><b>La informaci&oacute;n $login fu&eacute; cancelada</b><br>
            	Vuelva a ingresarla</center>";
	 	$mess_cod = "info";
         }

      }
    //$mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }
/************** creacion comentarios enf_ni�ez****************/
  function actualizar(){
    global $table;

   foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      
      
       $this->fv->resetErrorList();	
       $this->fv->isEmpty("enf_comentario","Ingrese algun comentario");
      
    if ($this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    } else {
    
    
    $fields = array ("enf_comentario", "enf_estado");
							      
       // Arreglos para datos del query y el url para paginacion
     
     $set_array = array();
     $query = 'UPDATE  "enf_nino" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE enf_id = '$enf_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo adicionar el comentario por un fallo en el sistema, Por favor comun&iacute;quese con el administrador.".$query;
		$mess_cod = "alert";		
	}else{
	       logs::crear("diagnostico", "actualizar", $query);
	 	$mess = "<b>El Comentario  $login fue adicionado con &eacute;xito.</b>"; 
	    	$mess_cod = "info";
	    	//$s_opc = "busqueda";
	 } 
      
    }
   return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }


}
?>
