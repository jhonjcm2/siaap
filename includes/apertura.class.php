<?
/*****************************************************************************************/
// CLASE: Apertura de la Historia Cl�nica
// Proposito: Realizar la apertura de la historia cl�nica a determinado paciente
// Ultima modificacion: octubre de 2003
// /**************************************************************************************/
class apertura{
  var $fv;
  var $data;
	
  function apertura($pac_id=0){
  global $table;
    
    $this->fv = new FormValidator;
    if ($pac_id){
    	$query = "SELECT * 
        	     FROM apertura_historia_clinica
        	     WHERE ahc_id='$ahc_id' ";
	$table->sql_query($query);
    	$this->data = $table->sql_fetch_object();
    }
    
  }

  /*************************************************************************/
  // Proposito: Almacenar la apertura de la historia de un paciente
  // return: arreglos con resultados de la creacion.
  /*************************************************************************/
  function crear(){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;

    foreach($_GET as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;
      
//   if ( 0 /*$this->fv->isError()*/ ) {
if ( 0 ){
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    }else {
          
	/*  se pasa a la quota a bytes */
         $fields = array ("ahc_id", "pac_id", "ara_id", "ahc_fechaApertura");
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "apertura_historia_clinica" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "No se pudo adicionar la Apertura por un fallo en el sistema, por Favor contacte a el Administrador.";
	$mess_cod = "alert";		
	}else{
	  //logs::crear("apertura", "crear", $query);	
	    $mess = "<b>La apertura fu&eacute; realizada con &eacute;xito</b> "; 
	    $mess_cod = "info";
	    $s_opc = "busqueda";
	 } 
	  
  
     }
 
   // $mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }

  /********************* Buscar una apertura******************/

function buscar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);


             $fields = array ("ahc_id", "pac_id", "ara_id", "ahc_fechaApertura");
							      
       // Arreglos para datos del query y el url para paginacion

      $where_array = array();
     $query = 'SELECT * FROM  "apertura_historia_clinica" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($where_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($where_array))
	$query .= " WHERE ".implode(", ",$where_array);
	
      	$result=$table->search($query);
	
	if (!$result){
		$mess = "No hay registros que concuerden con su b&uacute;squeda.".$query;
		$mess_cod = "alert";		
	}else{
	      	$mess = "La Apertura fu&eacute; consultada con &eacute;xito ".$query.$table->nfound; 
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }


  /************** Modificar apertura ****************/
  function modificar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

     

    $fields = array ("ahc_id", "pac_id", "ara_id", "ahc_fechaApertura");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "apertura_historia_clinica" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE ahc_id = '$ahc_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo adicionar la Apertura por un fallo en el sistema, por Favor contacte a el administrador.".$query;
		$mess_cod = "alert";		
	}else{
	       logs::crear("apertura", "modificar", $query);
	 	$mess = "La Apertura fu&eacute; modificada con &eacute;xito ".$query; 
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }
  
  
/******************************************/    

function validar(){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;

    foreach($_GET as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;
      
	$query= 'SELECT * FROM pagos WHERE "pac_id"='.$pac_id;
   	$table->search($query);
   
   	if(!$table->nfound ){
   	$mess = "<center><b>El paciente no ha hecho el pago de la apertura de la historia cl&iacute;nica</b><br>
   	Por favor haga primero el pago.</center>";
   	$mess_cod = "alert";
   	$destino="2";
   	}else{
   	
   	/*if ( 0 ){
     		$mess = $this->fv->getMessage();
	      	$mess_cod = "alert";
      		$destino="0";
    	}else{
    
     	$query = 'SELECT *
                FROM apertura_historia_clinica 
                WHERE "pac_id"='.$pac_id;
      	$table->search($query);
      		if(!$table->nfound) {
			$mess ="<b>El paciente no tiene historia cl&iacute;nica, por favor haga la apertura de la historia.</b>";
			$mess_cod = "alert";
      			$destino="0";
      		}
      			else{
      				$query = "SELECT * FROM datos_per_pac  WHERE pac_id=$pac_id";
      				$table->search($query);
 				$obj = $table->sql_fetch_object();
  				if (!$table->nfound ){
   					$mess="<center><b>Ingrese los datos personales del paciente.</b> <br><br>";
   	 				$mess_cod="alert";
      					$destino="1";
   	 	   	 	}
   	 	   	 		else{*/
   	 	   	 		    //   logs::crear("apertura", "validar", $query);
   	 	  	 	 		$mess ="<center><b>Esta usted en la Historia Cl&iacute;nica de su Paciente.</center></b><br>
   						Haga click <a href='$PHP_SELF?opc=paciente&s_opc=submenu&pac_id=$pac_id'>&nbsp;&nbsp;<b>aqu&iacute;</b></a> para ingresar al men&uacute; Historia Cl&iacute;nica";
      						$mess_cod="info";
      						$destino="3";

      					}    
      				//}
      			//}
       return array("mess"=>$mess,"mess_cod"=>$mess_cod,"destino"=>$destino );
 	 	}
	}


?>
