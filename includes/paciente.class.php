<?
/**********************************************************/
// CLASE: paciente
// Proposito: Realizar tareas de manejo de paciente.
// Ultima modificacion: Octubre 2003
// /**********************************************************/
class paciente{
  var $fv;
  var $data;
	
  function paciente($id=-1){
    global $table;
    
    $this->fv = new FormValidator;
    if ($id >= 0){
    	$query = "SELECT * 
        	     FROM paciente 
        	     WHERE pac_id='$id' ";
	$table->sql_query($query);
    	$this->data = $table->sql_fetch_object();
    }
    
  }
 
  
  /**********************************************************/
  // Proposito: Creacion unica de un paciente 
  // return: arreglos con resultados de la creacion.
  /**********************************************************/
  function crear(){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;

    foreach($_GET as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;
      
    $this->fv->resetErrorList();
 
   $this->fv->isWithinRange("tip_id","Seleccione el tipo de Paciente",0,99);
   $this->fv->isWithinRange("ara_id","Seleccione el �rea de atenci�n",0,99);
   $this->fv->isEmpty("pac_nombres","Falta el Campo Nombre.");
   $this->fv->isEmpty("pac_apellidos","Falta el Campo Apellidos.");
   $this->fv->isWithinRange("tid_id","Seleccione el tipo de Identificaci�n",0,99);
   $this->fv->isEmpty("pac_numeroIdentificacion","Ingrese el N�mero de Identificaci�n.");
  
    if ( $this->fv->isError()) {
      $mess = $this->fv->getMessage();      
      $mess_cod = "alert";
      //include ("modules/paciente/cliente.php");
    } 
    else {
    
      $query = 'SELECT * 
                FROM paciente 
                WHERE "pac_numeroIdentificacion"='.$pac_numeroIdentificacion;
      $table->search($query);
    
      if($table->nfound) {
	$mess = "<b>Un paciente con ese documento ya se encuentra registrado en el sistema, por favor verifique la identificaci&oacute;n</b>";
	$mess_cod = "alert";
	$s_opc = "adicionar_usuario";
      }
      else {
      	/*  se pasa a la quota a bytes */
         $fields = array ("pac_id", "tid_id", "ciu_id", "ara_id", "tip_id", "pac_personaElabora", "pac_paciente","pac_fechaIngreso",
				   "pac_numeroIdentificacion", "pac_lugarExpedicion", "pac_nombres", "pac_apellidos", 
				   "pac_direccionResidencia", "pac_telefonos");
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "paciente" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "No se pudo adicionar el paciente por un fallo en el sistema, por favor comuniquese con el administrador.";
	$mess_cod = "alert";
	//$s_opc="default";		
	}else{
	    logs::crear("paciente", "crear", $query);
	    $mess = "<center><b>El paciente $login fu&eacute; adicionado con &eacute;xito <br><br>
	    Haga click <a href='$PHP_SELF?opc=paciente&s_opc=validacion'>Aqu&iacute;</a> para ingresar otro paciente</center></b> "; 
	    $mess_cod = "info";
	    $s_opc = "busqueda";
	 } 
	  
      }
    }

    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }

/***************Crear datos personales paciente*************/
 function creardpp(){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;

    foreach($_GET as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;
      
    /*$this->fv->resetErrorList();	 
 
   $this->fv->isWithinRange("tip_id","Seleccione el tipo de Paciente",0,99);
   $this->fv->isWithinRange("ara_id","Seleccione el �rea de atenci�n",0,99);
   $this->fv->isEmpty("pac_nombres","Falta el Campo Nombre.");
   $this->fv->isEmpty("pac_apellidos","Falta el Campo Apellidos.");
   $this->fv->isWithinRange("tid_id","Seleccione el tipo de Identificaci�n",0,99);
   $this->fv->isEmpty("pac_numeroIdentificacion","Ingrese el N�mero de Identificaci�n.");
   //$this->fv->isNumber("pac_numeroIdentificacion", "Debe insertar solo n&uacute;meros en la identificaci&oacute;n");

    if ( $this->fv->isError()) {
      $mess = $this->fv->getMessage();      
      $mess_cod = "alert";
      //include ("modules/paciente/default.php");
    } 
    else {
    
      $query = 'SELECT pac_nombres, pac_apellidos 
                FROM paciente 
                WHERE "pac_numeroIdentificacion"='.$pac_numeroIdentificacion;
      $table->search($query);
    
      if($table->nfound) {
	$mess = "<b>Un paciente con ese documento ya se encuentra registrado en el sistema, por favor verifique la identificaci&oacute;n</b>";
	$mess_cod = "alert";
	$s_opc = "adicionar_usuario";
      }
      else {*/
      	/*  se pasa a la quota a bytes */
      	  
         $fields = array  ("dpp_id", "pac_id", "eps_id", "ips_id", "pla_id", "zor_id", "tvu_id", "rea_id",  "dpp_sexo",
                     	     "dpp_email", "pdp_estrato", "coo_id", "dpp_estado");
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "datos_per_pac" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "No se pudo adicionar los datos del paciente por un fallo en el sistema, por favor comuniquese con el administrador.". $query;
	$mess_cod = "alert";
	//$s_opc="default";		
	}else{
	   // logs::crear("paciente", "crear", $query);
	    $mess = "<center><b>El paciente $login fu&eacute; adicionado con &eacute;xito <br><br>
	    Haga click <a href='$PHP_SELF?opc=paciente&s_opc=validacion'>Aqu&iacute;</a> para ingresar otro paciente</center></b> "; 
	    $mess_cod = "info";
	    $s_opc = "busqueda";
	 } 
	  
      
    

    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }

 /************** actualiza la tabla datos_per_pac con los datos de datospersonales1****************/
  function actualizardpp(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

 $dpp_fechaNacimiento="$fn_anio-$fn_mes-$fn_dia";
 $fields = array ("ume_id", "grs_id", "est_id", "dpp_fechaNacimiento", "dpp_lugarNacimiento", "dpp_edad", "dpp_personaMasAllegada", 
                       "dpp_parentescoPersonaAllegada", "dpp_direccionPersonaAllegada", 
                       "dpp_telefonoPersonaAllegada", "dpp_medicoTratante", "dpp_telefonoMedicoTratante", 
                       "dpp_remitidoPor", "dpp_personaEdita",  "dpp_ocupacionInstitucion", 
                       "dpp_nombreInstitucion", "dpp_direccionInstitucion", "dpp_telefonoInstitucion", "dpp_nombreConyuge", 
                       "dpp_dirConyuge", "dpp_ocupConyuge", "dpp_telConyuge", "dpp_nombrePadre", 
                       "dpp_ocupacionPadre", "dpp_dirPadre", "dpp_institucionPadre", "dpp_telefonoInstitucionPadre", 
                       "dpp_nombreMadre", "dpp_ocupacionMadre", "dpp_dirMadre", "dpp_institucionMadre", 
                       "dpp_telefonoInstitucionMadre","dpp_estado");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "datos_per_pac" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE pac_id = '$pac_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo adiccionar los datos paciente por un fallo en el sistema.<br> Contacte al administrador del sistema".$query;
		$mess_cod = "alert";		
	}else{
	       logs::crear("paciente", "modificar", $query);
	 	$mess = "<center><b>Los datos del paciente $login fueron adiccionados con &eacute;xito</b><br><br>
	 	Por favor continue ingresando los datos personales</center>"; 
	    	$mess_cod="info";
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }
 /************** actualiza la tabla datos_per_pac con los datos de datospersonales1****************/
 
  function actualizardpp1(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

  $fields = array ("est_id", "dpp_ocupacionInstitucion", "dpp_nombreInstitucion", "dpp_direccionInstitucion", 
  			  "dpp_telefonoInstitucion", "dpp_nombreConyuge", "dpp_dirConyuge", "dpp_ocupConyuge", 
  			  "dpp_telConyuge", "dpp_nombrePadre", "dpp_ocupacionPadre", "dpp_dirPadre", 
  			  "dpp_institucionPadre", "dpp_telefonoInstitucionPadre", "dpp_nombreMadre", 
  			  "dpp_ocupacionMadre", "dpp_dirMadre", "dpp_institucionMadre", 
                       "dpp_telefonoInstitucionMadre","dpp_estado");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "datos_per_pac" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE pac_id = '$pac_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo adiccionar los datos paciente por un fallo en el sistema.<br> Contacte al administrador del sistema".$query;
		$mess_cod = "alert";		
	}else{
	       logs::crear("paciente", "modificar", $query);
	 	$mess = "<center><b>Los datos del paciente $login fueron adiccionados con &eacute;xito</b><br><br>
	 	Por favor continue ingresando los datos de la Anamnesis</center>"; 
	    	$mess_cod="info";
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }
  
  

  

  /************** Modificar paciente****************/
  function modificarpac(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

/*$query = 'SELECT * 
                FROM paciente 
                WHERE "pac_numeroIdentificacion"='.$pac_numeroIdentificacion;
      $table->search($query);
    
      if($table->nfound) {
	$mess = "<b>Un paciente con ese documento ya se encuentra registrado en el sistema, por favor verifique la identificaci&oacute;n</b>";
	$mess_cod = "alert";
} else{*/
 $fields = array ( "pac_nombres", "pac_apellidos", "pac_numeroIdentificacion", "pac_lugarExpedicion", "tid_id", "ciu_id", "ara_id", "tip_id", "pac_direccionResidencia", "pac_telefonos", 
			   "pac_fechaEdicion", "pac_personaEdita", "pac_fechaApertura");
							   
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "paciente" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE pac_id = '$pac_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo modificar el paciente por un fallo en el sistema.<br> Contacte al administrador del sistema.";
		$mess_cod = "alert";		
	}else{
	       logs::crear("paciente", "modificar", $query);
	 	$mess = "<b>Los datos del paciente $login fueron modificados con &eacute;xito</b> "; 
	    	$mess_cod="info";
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }
//}
 /************** Modificar datos personales paciente****************/
  function modificardpp(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);


 $fields = array ( "eps_id", "ips_id", "pla_id", "zor_id", "tvu_id", "rea_id", "dpp_email", "pdp_estrato", "coo_id", 
 			  "ume_id", "grs_id", "est_id", "dpp_fechaNacimiento", "dpp_lugarNacimiento", "dpp_edad", "dpp_estado", "dpp_sexo",
 			  "dpp_personaMasAllegada", "dpp_parentescoPersonaAllegada", "dpp_direccionPersonaAllegada", 
                       "dpp_telefonoPersonaAllegada", "dpp_medicoTratante", "dpp_telefonoMedicoTratante", 
                       "dpp_remitidoPor", "dpp_personaEdita",  "dpp_ocupacionInstitucion", 
                       "dpp_nombreInstitucion", "dpp_direccionInstitucion", "dpp_telefonoInstitucion", "dpp_nombreConyuge", 
                       "dpp_dirConyuge", "dpp_ocupConyuge", "dpp_telConyuge", "dpp_nombrePadre", 
                       "dpp_ocupacionPadre", "dpp_dirPadre", "dpp_institucionPadre", "dpp_telefonoInstitucionPadre", 
                       "dpp_nombreMadre", "dpp_ocupacionMadre", "dpp_dirMadre", "dpp_institucionMadre", 
                       "dpp_telefonoInstitucionMadre");
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "datos_per_pac" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE pac_id = '$pac_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo modificar el paciente por un fallo en el sistema.<br> Contacte al administrador del sistema.";
		$mess_cod = "alert";		
	}else{
	       logs::crear("paciente", "modificar", $query);
	 	$mess = "<b>Los datos del paciente $login fueron modificados con &eacute;xito</b> "; 
	    	$mess_cod="info";
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }

   


/********************* Buscar un paciente******************/

function buscar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);



             $fields = array ( "tid_id", "ciu_id", "eps_id", "ips_id", "grs_id", "ara_id", "tip_id", "zor_id", "rea_id","pac_personaElabora","pac_fechaIngreso",
					"pac_numeroIdentificacion", "pac_lugarExpedicion", "pac_nombres", "pac_apellidos", "pac_direccionResidencia",
					"pac_telefonos", "pac_sexo", "pac_lugarNacimiento", "pac_edad","pac_fechaNacimiento", "pac_email", "pac_estrato", "pac_personaMasAllegada",
					"pac_parentescoPersonaAllegada", "pac_direccionPersonaAllegada", "pac_telefonoPersonaAllegada", "coo_id");
							      
       // Arreglos para datos del query y el url para paginacion

      $where_array = array();
     $query = 'SELECT  * FROM  "paciente" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($where_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($where_array))
	$query .= " WHERE ".implode(", ",$where_array);
	
	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No  hay registros que concuerden con su b�squeda".$query;
		$mess_cod = "alert";
	}else{
		$mess = "El paciente ya existe".$query;
		$s_opc = "info_usuario";
		$mess_cod="info";
	 } 
	 return $result;
//    return array("mess"=>$mess,"mess_cod"=>$mess_cod);
  }
  
/****************Valida el odontologo encargado del paciente************************/

function validarOdo($pac_id, $usu_id){
   global $table;

  $query="SELECT * FROM odontologos_encargados_historia WHERE hic_id=$pac_id ORDER BY oeh_id DESC";
  return false; //Leo   
  $table->sql_query($query);
  $row = $table->sql_fetch_object();
   if ($usu_id==$row->usu_id){
   	return true;
   }else{
   	      return false;
   	}
}

/***************************valida el docente encargado del paciente*****************/

function validarDoc($pac_id, $usu_id){
   global $table;

   $query="SELECT * FROM docentes_encargados_historia WHERE hic_id=$pac_id ORDER BY deh_id DESC";
                return true; //Leo
   $table->sql_query($query);
   $row = $table->sql_fetch_object();
   if ($usu_id==$row->usu_id){
   	return true;
   }else{
       return false;
   }
}

 /**********************************************************/
function crearDoc(){	
    global $table, $_POST, $_GET, $table2;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;
      
      
      $this->fv->resetErrorList();
    
  
   if ( $this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    }else {
    
	/*  se pasa a la quota a bytes */
	  //$cir_fecha="$fn_anio-$fn_mes-$fn_dia";
	  //$cir_hora="$hora:$min";
	  
         $fields = array ("usu_id", "pac_id", "deh_fechaEncargado");
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "docentes_encargados_historia" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "<b>No se pudo registrar el odontologo por un fallo en el sistema. Comuniquese con el administrador.</b>".$query;
	$mess_cod = "alert";		
	}else{
	
	    logs::crear("pagos", "crear", $query);
	    $mess = "<b>La cita fue asignada con &eacute;xito</b>"; 
	    $mess_cod = "alert";
	 } 
	  
      }
  
   // $mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }


}
?>
