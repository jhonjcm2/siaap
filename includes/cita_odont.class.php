<?
/**********************************************************/
// CLASE: cita_odont
// Proposito: registrar los resultados de la asignacion de cita odontologicas.
// Ultima modificacion: Febrero20 de 2008
// /**********************************************************/
class cita_odont{
  var $fv;
  var $data;
	
  function cita_odont($id=-1){
    global $table, $table2;

    $table2 = new my_DB2();
    $this->fv = new FormValidator;
	//$fv = new FormValidator;
    if ($id >= 0){
    $query = "SELECT *
              FROM cita_odont
              WHERE cio_id='$id' ";
 	$table->sql_query($query);
	$this->data = $table->sql_fetch_object();
  }
  
 }


  /**********************************************************/
  // Proposito: manejar lo procesos de citacion de pacientes 
  // return: arreglos con resultados de la creacion.
  /**********************************************************/
  function crear(){	
    global $table, $_POST, $_GET, $table2;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;
      
      
      $this->fv->resetErrorList();
      
    
  
   if ( $this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    }else {
     if ( $cio_fecha<$cio_fechaElaboracion) {
      	$mess ="<b>No se puede registrar la cita, VERIFIQUE LA FECHA, esta no puede ser inferior a la actual.</b>";
      	$mess_cod = "alert";
     }else {
      	$cio_hora="$hora:$min";
      	$query="SELECT * FROM cita_odont WHERE cio_fecha='$cio_fecha' and cio_hora='$cio_hora' and cio_estado !=2 and cio_estado !=5 and (odl_id='$odl_id' or pac_id='$pac_id') ";
       	$table->sql_query($query);
		$horcit = $table->sql_fetch_object();
	//echo $horcit;
	
	if($horcit >=1){
	$mess ="<center><b>No se puede registrar la cita,  VERIFIQUE FECHA Y HORA. <br>El Doctor(a)  o el Paciente ya tiene una cita en este horario.</b></center>";
      $mess_cod = "alert";
      }else {
	    
	/*  se pasa a la quota a bytes */
	  //$cir_fecha="$fn_anio-$fn_mes-$fn_dia";
	  
	 // echo $odl_id;
         $fields = array ("pac_id", "cio_fecha", "cio_jornada", "cio_hora", "cio_tipo",  "cio_duracion", "cio_fechaElaboracion", "usu_id", "cio_estado", "odl_id");
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
    
      
      
     $query = 'INSERT INTO "cita_odont" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
		
	if (!$result){
		$mess = "<b>No se pudo registrar la cita  por un fallo en el sistema. Comuniquese con el administrador.</b>";
		$mess_cod = "alert";	
	}else{
	    $cal_date = str_replace('-','',$cio_fecha);
	    $hora =  $hora; // Por el GMT -5
	    $horacal = ($cio_jornada)?$hora:$hora+12;// Por PM y AM
	    $cal_time = $horacal.$min.'00';
	    $cal_mod_date = date("Ymd");
	    $cal_mod_time = date("His");
	    $usu_id="$odl_id";
	    $query2="SELECT usu_login FROM usuario WHERE usu_id=$usu_id";
	    $table->sql_query($query2);
	    $usu= $table->sql_fetch_object();
	    $usu_login = $usu->usu_login;
	    //echo $usu_login;
	    $paciente = new paciente($pac_id);
	    $pac_nombre = $paciente->data->pac_nombres." ".$paciente->data->pac_apellidos;
	    //$descripcion = $paciente->data->pac_numeroIdentificacion."\n".$paciente->data->pac_nombres." ".$paciente->data->pac_apellidos;
	    //$descripcion = "$cir_estudioSol";			
	    
	    $query2="SELECT max(cio_id) as cal_id FROM cita_odont";// cio_id  = cal_id
	    $table->sql_query($query2);
	    $obj = $table->sql_fetch_object();
	    $cal_id = $obj->cal_id;
	    $query2 = "INSERT INTO webcal_entry SET cal_id='$cal_id', cal_create_by='$usu_login', cal_date='$cal_date', cal_time='$cal_time', 
	    			cal_mod_time='$cal_mod_time', cal_mod_date = '$cal_mod_date', cal_duration='$cio_duracion', 
	    			cal_name='$pac_nombre'";
	    //$table2->sql_query($query2);
	    $query2 = "INSERT INTO webcal_entry_user SET cal_login='$usu_login', cal_id='$cal_id'";
	    //$table2->sql_query($query2);
	    logs::crear("pagos", "crear", $query);
	    $mess = "<b>La cita fue asignada con &eacute;xito</b>"; 
	    $mess_cod = "info";
	    } 
	 } 
      }
  }
   // $mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }

/**********************************************************/
function crearPxc(){	
    global $table, $_POST, $_GET, $table2;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;
      
      
      $this->fv->resetErrorList();
    
  
   if ( $this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    }else {
    
	/*  se pasa a la quota a bytes */
	 	  
         $fields = array ("cio_id", "pto_id", "pxc_cantidad", "pxc_valor");
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "ptoxcio" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "<b>No se pudo registrar la cita radiologica por un fallo en el sistema. Comuniquese con el administrador.</b>";
	$mess_cod = "alert";		
	}else{
	
	    logs::crear("pagos", "crear", $query);
	    $mess = "<b>La cita fue asignada con &eacute;xito</b>"; 
	    $mess_cod = "alert";
	 } 
	  
      }
  
   // $mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }
  
  /**********************************************************/
function crearOdo(){	
    global $table, $_POST, $_GET, $table2;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;
      
      
      $this->fv->resetErrorList();
    
  
   if ( $this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    }else {
    
	/*  se pasa a la quota a bytes */
	  //$cir_fecha="$fn_anio-$fn_mes-$fn_dia";
	  //$cir_hora="$hora:$min";
	  
         $fields = array ("usu_id", "pac_id", "oeh_fechaEncargado");
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "odontologos_encargados_historia" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	echo $result;
	$mess = "<b>No se pudo registrar el odontologo por un fallo en el sistema. Comuniquese con el administrador.</b>".$query;
	$mess_cod = "alert";		
	}else{
	
	    logs::crear("pagos", "crear", $query);
	    $mess = "<b>La cita fue asignada con &eacute;xito</b>"; 
	    $mess_cod = "alert";
	 } 
	  
      }
  
   // $mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }
  
 
/************** Actualizar una cita odontologica en calendar ****************/
  
  function actCal(){
    global $table, $table2;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

	 $query2="SELECT * FROM ptoxcio x, procedimiento p WHERE x.pto_id=p.pto_id and cio_id=$cio_id";
	 $table->sql_query($query2);
	 $ptoStr="";
	 while($pto=$table->sql_fetch_object())
        $ptoStr.=" ".$pto->pto_nombreProcedimiento;
      //echo "asdaasdd $ptoStr.";
     // echo $query2;
        
                
    $query2 ="UPDATE  webcal_entry SET cal_description='$ptoStr' WHERE cal_id=$cio_id";
     
	$result=$table2->sql_query($query2);
	
	if (!$result){
		$mess = "<b>No se pudo modificar la cita por un fallo en el sistema. Comuniquese con el administrador</b>";
		$mess_cod = "alert";		
	}else{
		logs::crear("cita", "modificar", $query);
	 	$mess = "<b>La cita fu&eacute; Asignada con &Eacute;xito</b>"; 
	 	$mess_cod="info";
	} 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }
 

/************** Modificar cita "Cancelar una cita sin borrarla " ****************/
  
  function modificar(){
    global $table, $table2;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    
    $fields = array ("cio_estado") ;
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "cita_odont" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE cio_id = '$cio_id'";

	$result=$table->sql_query($query);
	if (!$result){
		$mess = "<b>No se pudo modificar la cita por un fallo en el sistema. </b>".$query;
		$mess_cod = "alert";		
	}else{
		//$table2->sql_query("DELETE FROM webcal_entry WHERE cal_id=$cio_id");
		//$table2->sql_query("DELETE FROM webcal_entry_user WHERE cal_id=$cio_id");
	       logs::crear("cita", "modificar", $query);
	 	$mess = "<b>La cita fu&eacute; cancelada con &eacute;xito</b>"; 
	 	$mess_cod="info";
	} 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }
 
/************** Modificar cita "Cambia el Estado a Cita Confirmada" ****************/
  
  function modificar1(){
    global $table, $table2;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    
    $fields = array ("cio_estado") ;
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "cita_odont" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE cio_id = '$cio_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "<b>No se pudo modificar la cita por un fallo en el sistema. </b>".$query;
		$mess_cod = "alert";		
	}else{
		
	 	$mess = "<b>La cita fu&eacute; confirmada con &eacute;xito</b>"; 
	 	$mess_cod="info";
	} 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }
 
/******************** Borrar un Procedimientos  de una cita***********************/
 
  function borrar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

   
    $fields = array ("pxc_id");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'DELETE FROM "ptoxcio" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	//$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE pxc_id =$pxc_id";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo borar el procedimiento por un fallo en el sistema, Comun&iacute;quese con el administrador.".$query;
		$mess_cod = "alert";		
	}else{
	       logs::crear("pedido", "borrar", $query);
	 	$mess = "El procedimiento fu&eacute; borrdo con &eacute;xito ".$query; 
	    	$mess_cod = "info";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }
 
}
?>
