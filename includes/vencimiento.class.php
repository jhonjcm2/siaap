<?php
/**********************************************************/
// CLASE: vencimiento
// Proposito: Crea una fecha de vencimiento para cada material en una bodega
// Ultima modificacion: Noviembre  del 2011
// /**********************************************************/
class Vencimiento{
	var $fv;
	var $data;
	var $saldoNegativoPaciente=50;
	var $pagoTotalPaciente=40;
	var $saldoNetoPaciente=30;
	function Vencimiento($id=-1){
		global $table;
		$this->fv = new FormValidator;
		if ($id >= 0){
			$query = "SELECT * FROM vencimiento_material WHERE vmat_id='$id' ";
			$table->sql_query($query);
			$this->data = $table->sql_fetch_object();
		}
	}
	
	/*Cambia o agrega la cantidad de material de una bodega con una fecha de vencimiento
	bod_id: Bodega a la que petenece
	mat_id: Material de la bodega
	vmat_fecha: fecha de vencimiento del materia
	vmat_cantidad: Cantidad de material que se agrega a la bodega con esa fecha de vencimiento
	*/
	function agregarFecha($bod_id,$mat_id,$vmat_fecha,$vmat_cantidad, $lote){
		/*******************************************
		
		$table2 = new my_DB();
		$query = "SELECT * FROM matxbod WHERE mat_id=$mat_id AND bod_id=$bod_id";
			$table2->sql_query($query);
 		      	// Subo los materiales a la bodega de destino
			if ($row = $table2->sql_fetch_object()){
				$cantMat=$row->mxb_cantidadMaterial + $vmat_cantidad;
				$query="UPDATE matxbod SET \"mxb_cantidadMaterial\"=$cantMat, \"mxb_saldoInicial\"= $cantMat, \"mxb_fechaAbastecimiento\"='". date('Y-m-d', time())."' WHERE  mat_id=$mat_id AND bod_id=$bod_id";
			  	$result = $table2->sql_query($query);
        		}else{
				echo "El material por bodega no existe por lo que se crea uno!!";
        			$mxp_cantidadMaterial =  $vmat_cantidad;
        			$query="INSERT INTO matxbod (\"mxb_cantidadMaterial\", \"mxb_saldoInicial\", \"mxb_fechaAbastecimiento\", \"mxb_fechaActual\", mat_id, bod_id) 
        				   VALUES ($vmat_cantidad, $vmat_cantidad, '". date('Y-m-d', time())."', '" . date('Y-m-d', time())."', $mat_id, $bod_id)";
	        		$result = $table2->sql_query($query);
			}
		/*******************************************/
		$tablex = new my_DB();
		$lote=strtoupper($lote);
		//Consultamos si el material por bodega existe
		$query = "SELECT * FROM matxbod WHERE mat_id=$mat_id AND bod_id=$bod_id";////
		$tablex->sql_query($query);
		//Si existe el material por bodega
		if($row = $tablex->sql_fetch_object()){
			
			//Consultamos si en esa bodega existe el material con esa fecha y ese lote
			$query="SELECT * FROM vencimiento_material WHERE vmat_fecha='$vmat_fecha' AND vmat_lote='$lote' AND mxb_id=$row->mxb_id";
			$tablex->search($query);
			echo "---->>>>".$query."<BR>";
			$registro = $tablex->sql_fetch_object();
			$nuevaCantidad=0;
			//si existe se actualiza
			if(!$registro){
				echo "Dentro del if<BR>";
				//Creamos una nueva fecha
				$query="INSERT INTO vencimiento_material (\"vmat_fecha\", \"vmat_cantidad\", \"mxb_id\", \"vmat_lote\") 
				VALUES ('$vmat_fecha',0,$row->mxb_id,'$lote')";
				echo "----Qif1>>>>".$query."<BR>";
				$tablex->sql_query($query);
				//Buscamos ese registro
				$query="SELECT * FROM vencimiento_material WHERE vmat_fecha='$vmat_fecha' AND vmat_lote='$lote' AND mxb_id=$row->mxb_id";
				echo "----Qif2>>>>".$query."<BR>";
				$tablex->search($query);
				$registro = $tablex->sql_fetch_object();
			}
			echo "Fuera del if<BR>";
			//Actualizamos la cantidad de materiales con esa fecha de vencimiento
			$nuevaCantidad=$registro->vmat_cantidad+$vmat_cantidad;//Calcula la cantidad
			$query="UPDATE vencimiento_material SET \"vmat_cantidad\"=$nuevaCantidad WHERE vmat_id=$registro->vmat_id";
			echo "----Q>>>>".$query."<BR>";
			$tablex->search($query);
			return true;
		}
		return false;
	}
	
	/** Actualiza la cantidad de un material en una bodega, dependiendo del total de material que haya en la bodega, por ejemplo si en la bodega la cantidad es de 20 y la suma de los materiales con fecha de vencimiento es 12 entonces restará 8 a los materiales de fecha de vencimiento, empezandodesde el mas viejo  
	function actulizarCantidadVencimiento($bod_id, $mat_id){
		$tablex = new my_DB();

		$query="SELECT * FROM matxbod mxb, vencimiento_material vm WHERE mxb.mxb_id=vm.mxb_id AND mxb.bod_id=$bod_id AND mxb.mat_id=$mat_id ORDER BY vm.vmat_fecha ASC";
		$tablex->search($query);
		if($tablex->nfound>0){
			while($registro=$tablex->sql_fetch_object()){
				$cantidadVence=$registro->vmat_cantidad;
				if($cantidad<$cantidadVence){
					
				}
			}
		}
	}*/
	/* Saca del almacen para que entre a otra bodega. Solo afecta a la tabla vencimiento_material
	bod_id: Bodega a la que se le va a añadir el material
	cantidad: Cantidad que se saca del almacén y se agrega a la otra bodega
	mat_id: Material que se va a sacar
	*/
	function sacarDeAlmacen($bod_id, $cantidad, $mat_id){
		$tablex = new my_DB();
		/*********************************
		//Actualizamos lo de la bodega primero
		$query = "SELECT * FROM matxbod WHERE mat_id=$mat_id AND bod_id=1";
		$tablex->sql_query($query);
		$row2 = $tablex->sql_fetch_object();
		$resta = $row2->mxb_cantidadMaterial - $cantidad;
		$query="UPDATE matxbod SET \"mxb_cantidadMaterial\"=$resta WHERE  mat_id=$mat_id AND bod_id=1";
		echo "Actualizamos el almacen:".$query;
		$result = $tablex->sql_query($query);
		/*********************************/
		//Actualizamos las fechas  de vencimiento: Ir descontando desde la fecha mas antigua
		//seleccionamos todos los materiales del almacen que haya, o sea que su cantidad sea mayor a cero
		$query = "SELECT * FROM matxbod mxb,vencimiento_material vm WHERE NOT vm.vmat_cantidad=0 AND mxb.mat_id=$mat_id AND mxb.bod_id=1 ORDER BY vm.vmat_fecha ASC";
		echo "1-->>".$query."<BR>";
		$tablex->search($query);
		echo "Registros encontrados:".$tablex->nfound."<BR>";
		$i=0;
		if($tablex->nfound>0)
		while($cantidad>0){//foreach($dato = $tablex->sql_fetch_object()){
			$i++;
			$dato = $tablex->sql_fetch_object();
			if(!$dato){
				echo "No hay dato";
				break;
			}
			//Si la cantidad es menor que lo que hay se puede suplir
			if($cantidad < $dato->vmat_cantidad){
				//Guardamos cuanto va a quedar en bodega
				$nuevaCantidad=$dato->vmat_cantidad-$cantidad;
				$query="UPDATE vencimiento_material SET \"vmat_cantidad\"='$nuevaCantidad' WHERE vmat_id=$dato->vmat_id";
				echo "2-->>".$query.".....".$dato->vmat_fecha."<BR>";
				$tablex->sql_query($query);
				$this->agregarFecha($bod_id,$mat_id,$dato->vmat_fecha,$cantidad);
				$cantidad=0;
				break;
			}else{
				//Como la cantidad no se puede suplir se disminuye la cantidad y se deja en cero la del almacen
				$cantidad=$cantidad-$dato->vmat_cantidad;
				//eLIMINAMOS EL REGISTRO DE ESE MATERIAL
				//$query='UPDATE vencimiento_material SET "mxb_cantidadMaterial"=0 WHERE vmat_id=$dato->vmat_id';
				$query="DELETE FROM vencimiento_material WHERE vmat_id=$dato->vmat_id";
				
				echo "3-->>".$query."<BR>";
				$tablex->sql_query($query);
				//se agrega este material a la otra bodega
				$this->agregarFecha($bod_id,$mat_id,$dato->vmat_fecha,$dato->vmat_cantidad,$dato->vmat_lote);
			}
		}
	}


}
?>
