<?
/**********************************************************/
// CLASE: radiologia
// Proposito: registrar los resultados de los procesos del �rea.
// Ultima modificacion: Dicembre de 2005
// /**********************************************************/
class radiologia{
  var $fv;
  var $data;
	
  function radiologia($id=-1){
    global $table, $table2;

    $table2 = new my_DB2();
    $this->fv = new FormValidator;
    if ($id >= 0){
    $query = "SELECT *
              FROM radiologia
              WHERErad_id='$id' ";
 	$table->sql_query($query);
	$this->data = $table->sql_fetch_object();
  }
  
 }


  /**********************************************************/
  // Proposito: manejar lo procesos del area de radiologia 
  // return: arreglos con resultados de la creacion.
  /**********************************************************/
  function crear(){	
    global $table, $_POST, $_GET, $table2;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;
      
      
      $this->fv->resetErrorList();
    
  
   if ( $this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    }else {
    
	/*  se pasa a la quota a bytes */
	  //$cir_fecha="$fn_anio-$fn_mes-$fn_dia";
	  $cir_hora="$hora:$min";
	  
         $fields = array ("pac_id", "cir_fecha", "cir_hora", "cir_tipoHora",  "cir_duracion", "cir_fechaElaboracion", 
                               "cir_edadPaciente", "cir_remite", "cir_estado", "cir_pacU");
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "cita_rad" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "<b>No se pudo registrar la cita radiologica por un fallo en el sistema. Comuniquese con el administrador.</b>";
	$mess_cod = "alert";		
	}else{
	
	    $cal_date = str_replace('-','',$cir_fecha);
	    $hora =  $hora; // Por el GMT -5
	    $horacal = ($cir_tipoHora)?$hora:$hora+12;// Por PM y AM
	    $cal_time = $horacal.$min.'00';
	    $cal_mod_date = date("Ymd");
	    $cal_mod_time = date("His");
	    $paciente = new paciente($pac_id);
	    $pac_nombre = $paciente->data->pac_nombres." ".$paciente->data->pac_apellidos;
	    //$descripcion = $paciente->data->pac_numeroIdentificacion."\n".$paciente->data->pac_nombres." ".$paciente->data->pac_apellidos;
	    //$descripcion = "$cir_estudioSol";			
	    
	    $query2="SELECT max(cir_id) as cal_id FROM cita_rad";// cir_id  = cal_id
	    $table->sql_query($query2);
	    $obj = $table->sql_fetch_object();
	    $cal_id = $obj->cal_id;
	    
	/*    $query2="SELECT * FROM estxcir x, estudio_rad e WHERE cir_id=$cla_id";
	    $table->sql_query($query2);
	    $est=$table->sql_fetch_object();
	    
	  */  
	    $query2 = "INSERT INTO webcal_entry SET cal_id=$cal_id, cal_create_by='dvargas', cal_date='$cal_date', cal_time=$cal_time, 
	    			cal_mod_time='$cal_mod_time', cal_mod_date = $cal_mod_date, cal_duration=$cir_duracion, 
	    			cal_name='$pac_nombre' ";
	    $table2->sql_query($query2);
	    
	    $query2 = "INSERT INTO webcal_entry_user SET cal_login='dvargas', cal_id=$cal_id";
	    $table2->sql_query($query2);
	    
	    logs::crear("pagos", "crear", $query);
	    $mess = "<b>La cita fue asignada con &eacute;xito</b>"; 
	    $mess_cod = "alert";
	 } 
	  
      }
  
   // $mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }


/**********************************************************/
function crearExc(){	
    global $table, $_POST, $_GET, $table2;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;
      
      
      $this->fv->resetErrorList();
    
  
   if ( $this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    }else {
    
	/*  se pasa a la quota a bytes */
	  //$cir_fecha="$fn_anio-$fn_mes-$fn_dia";
	  $cir_hora="$hora:$min";
	  
         $fields = array ("cir_id", "esr_id", "exc_cantidad", "exc_valor");
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "estxcir" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "<b>No se pudo registrar la cita radiologica por un fallo en el sistema. Comuniquese con el administrador.</b>";
	$mess_cod = "alert";		
	}else{
	
	    logs::crear("pagos", "crear", $query);
	    $mess = "<b>La cita fue asignada con &eacute;xito</b>"; 
	    $mess_cod = "alert";
	 } 
	  
      }
  
   // $mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }

 /************** Actualizar una cita en calendar ****************/
  
  function actCal(){
    global $table, $table2;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

	 $query2="SELECT * FROM estxcir x, estudio_rad e WHERE x.esr_id=e.esr_id and cir_id=$cir_id";
	 $table->sql_query($query2);
	 $estStr="";
	 while($est=$table->sql_fetch_object())
        $estStr.=" ".$est->esr_nombre;
      //  echo "asdaasdd $estStr.";
        
                
    $query2 ="UPDATE  webcal_entry SET cal_description='$estStr' WHERE cal_id=$cir_id";
     
	$result=$table2->sql_query($query2);
	
	if (!$result){
		$mess = "<b>No se pudo modificar la cita por un fallo en el sistema. Comuniquese con el administrador</b>";
		$mess_cod = "alert";		
	}else{
		logs::crear("cita", "modificar", $query);
	 	$mess = "<b>La cita fu&eacute; Asignada con &Eacute;xito</b>"; 
	 	$mess_cod="info";
	} 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }
 

/**********************************************************/
  // Proposito: manejar lo procesos del area de radiologia 
  // return: arreglos con resultados de la creacion.
  /**********************************************************/
  function crearest(){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;
      
      
      	$this->fv->resetErrorList();
    	$this->fv->isEmpty("esr_tipo", "Debe ingresar el tipo de estudio");
    	$this->fv->isEmpty("esr_nombre", "Debe ingresar el nombre del estudio");
      	//$this->fv->isWithinRange("ara_id","Seleccione una �rea de atenci&oacuten",0,9999);
      	$this->fv->isNumber("esr_valorP", "Debe insertar el valor del estudio a un particular, si no lo tiene ingrese '0' ");
  	$this->fv->isNumber("esr_valorU", "Debe insertar el valor del estudio a univalle, si no lo tiene ingrese '0' ");
   if ( $this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    }else {
    
	/*  se pasa a la quota a bytes */
	 $cir_hora="$hora:$min";
	  
         $fields = array ("esr_tipo", "esr_nombre", "esr_valorP", "esr_valorU", "esr_estado");
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "estudio_rad" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "<b>No se pudo registrar el estudio por un fallo en el sistema. Comuniquese con el administrador.</b>";
	$mess_cod = "alert";		
	}else{
	    logs::crear("pagos", "crear", $query);
	    $mess = "<b>El estudio fue creado con &eacute;xito</b>"; 
	    $mess_cod = "info";
	 } 
	  
      }
  
   // $mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }



/********************* Buscar pagos******************/

function buscar(){
    global $table, $_POST ,$_GET;

     foreach($_POST as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($_GET as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);




             $fields = array ( "pch_id", "pag_fecha", "pac_id", "usu_id", "pag_numeroCuenta", "pag_tipoPagoEfectivo", 
	                          "pag_valorConsulta", "pag_valorCopago", "pag_valorEnLetras", "pag_conceptoPago", "pag_eps", "pag_centroInfo");
							      
       // Arreglos para datos del query y el url para paginacion

     $where_array = array();
     $query = 'SELECT * FROM  "pagos" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($where_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($where_array))
	$query .= " WHERE ".implode(", ",$where_array);
	
      	$result=$table->search($query);
	
	if (!$result){
		$mess = "No hay registros que concuerden con su busqueda.".$query;
		$mess_cod = "alert";		
	}else{
	       $mess = "El pago fue adicionado con &eacute;xito ".$query; 
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }

/**************************almacena la informacion de los estudios a realizar al momento de darse una cita*************************/
function agregarEst(){


global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

        /*  se pasa a la quota a bytes */
         $fields = array ("cir_id", "esr_id", "exr_canIndicada", "exr_canTomada", "exr_guante", "exr_cd", "exr_valor", "exr_realizaEst");

       // Arreglos para datos del query y el url para paginacion
      	$fields_array = array();
      	$values_array = array();
     	$query = 'INSERT INTO "esrxrxr" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
        if(${$v}!=""){
          array_push($fields_array,' "'.$v.'" ');
          array_push($values_array," '".${$v}."' ");
        }

      if(sizeof($fields_array)){
        $query .= implode(", ",$fields_array);
        $query .= ") VALUES (".implode(", ",$values_array) ;
        }
        $query .=")";

        $result=$table->sql_query($query);

        if (!$result){
        $mess = "No se pudo adicionar los datos por un fallo en el sistema, Contacte al Administrador del Sistema.";
        $mess_cod = "alert";
        }else{
            logs::crear("pagos", "agregarCheque", $query);
            $mess = "Los datos fueron adicionados con &eacute;xito ".$query;
            $mess_cod = "alert";
           
         }

    
    $mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }
  
   /**********************************************************/
  // Proposito: manejar lo procesos del area de radiologia 
  // return: arreglos con resultados de la creacion.
  /**********************************************************/
  function crearRx(){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;
      
      
      $this->fv->resetErrorList();
    
  
   if ( $this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    }else {
  
	/*  se pasa a la quota a bytes */
	 // $cir_fecha="$fn_anio-$fn_mes-$fn_dia";
	  //$cir_hora="$hora:$min";
	  
         $fields = array ("cir_id", "rxr_fecha");
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "rx_realizado" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "<b>No se pudo registrar la cita radiologica por un fallo en el sistema. Comuniquese con el administrador.</b>";
	$mess_cod = "alert";		
	}else{
	    logs::crear("pagos", "crear", $query);
	    $mess = "<b>La cita fue asignada con &eacute;xito</b>".$query; 
	    $mess_cod = "alert";
	 } 
	  
      }
  
    $mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }

  
/******************** Borrar un pago que no ha sido impreso***********************/
 
  function borrar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

   
    $fields = array ("rxr_id", "cir_id", "rxr_fecha");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'DELETE FROM "rx_realizado" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	
	
      $query .=  " WHERE rxr_id =$rxr_id";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo cancelar el pago por un fallo en el sistema, Comun&iacute;quese con el administrador.".$query;
		$mess_cod = "alert";		
	}else{
	       logs::crear("pedido", "borrar", $query);
	 	$mess = "El Registro $login fu&eacute; cancelado con &eacute;xito ".$query; 
	    	$mess_cod = "info";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }

/***********borrar de la tabla estado_cuenta un pago no impreso****************/
function borrarRx($rxr_id){


global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;


//   if ( 0 /*$this->fv->isError()*/ ) {
if ( 0 ){
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
      }
      else {
        /*  se pasa a la quota a bytes */
         $fields = array  ("exr_id", "esr_id", "rxr_id", "exr_canTomada", "exr_canIndicada", "exr_guante");

       // Arreglos para datos del query y el url para paginacion
      	$fields_array = array();
      	$values_array = array();
     	$query = 'DELETE FROM "esrxrxr" ';
     	// Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE rxr_id = '$rxr_id'";

	$result=$table->sql_query($query);
    
        if (!$result){
        $mess = "No se pudo cancelar el pago por un fallo en el sistema, Contacte al Administrador del Sistema.";
        $mess_cod = "alert";
        }else{
              logs::crear("pedido", "borrarMat", $query);
            	$mess = "<center><b>El Registro $login fu&eacute;  cancelado</b><br>
            	Vuelva a ingresarlo</center>";
	 	$mess_cod = "info";
         }

      }
    //$mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }

/************** Modificar cita "Cancelar una cita " ****************/
  
  function modificar(){
    global $table, $table2;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    
    $fields = array ("cir_estado") ;
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "cita_rad" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE cir_id = '$cir_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "<b>No se pudo modificar la cita por un fallo en el sistema. Comuniquese con el administrador</b>";
		$mess_cod = "alert";		
	}else{
		$table2->sql_query("DELETE FROM webcal_entry WHERE cal_id=$cir_id");
		$table2->sql_query("DELETE FROM webcal_entry_user WHERE cal_id=$cir_id");
	       logs::crear("cita", "modificar", $query);
	 	$mess = "<b>La cita fu&eacute; cancelada con &eacute;xito</b>"; 
	 	$mess_cod="info";
	} 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }
 
 /************** Modificar un Estudio Radiologico****************/
  
  function modificar1(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
	
	$this->fv->resetErrorList();
    	$this->fv->isEmpty("esr_tipo", "Debe ingresar el tipo de estudio");
    	$this->fv->isEmpty("esr_nombre", "Debe ingresar el nombre del estudio");
      	//$this->fv->isWithinRange("ara_id","Seleccione una �rea de atenci&oacuten",0,9999);
      	$this->fv->isNumber("esr_valorP", "Debe insertar el valor del estudio a un particular, si no lo tiene ingrese '0' ");
  	$this->fv->isNumber("esr_valorU", "Debe insertar el valor del estudio a univalle, si no lo tiene ingrese '0' ");
   if ( $this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    }else {
    
    $fields = array ("esr_tipo", "esr_nombre", "esr_valorP", "esr_valorU", "esr_estado") ;
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "estudio_rad" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE esr_id = '$esr_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "<b>No se pudo modificar el estudio por un fallo en el sistema. Comuniquese con el administrador</b>";
		$mess_cod = "alert";		
	}else{
		$mess = "<center><b>El estudio fu&eacute; editado con &eacute;xito</b><br>
		 Haga click <a href='$PHP_SELF?opc=radiologia&s_opc=listar3'>Aqu&iacute;</a> para regresar al listado</center>"; 
	 	$mess_cod="info";
	} 
	}  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }
 
}
?>
