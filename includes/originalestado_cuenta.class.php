<?
/*****************************************************************************************/
// CLASE: Estado de Cuenta
// Proposito: Almacenar el estado de cuenta de un paciente
// Ultima modificacion: octubre de 2003
// /**************************************************************************************/
class estado_cuenta{
  var $fv;
  var $data;
	
  function estado_cuenta($pac_id=0){
  global $table;
    
  }

  /*************************************************************************/
  // Proposito: Almacenar el estado de cuenta de un paciente
  // return: arreglos con resultados de la creacion.
  /*************************************************************************/
  function crear($pag_id=0){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;

    foreach($_GET as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;

	$query=" SELECT esc_saldo FROM estado_cuenta WHERE esc_id  IN (SELECT MAX(esc_id) FROM estado_cuenta WHERE pac_id=$pac_id)";
	$table->sql_query($query);
	$esc = $table->sql_fetch_object();
	$esc_saldo=0;
	$esc_saldo += $esc->esc_saldo;
	 $mess = "<b>El estado de cuenta fue almacenado con &eacute;xito</b> ".$query; 
	    $mess_cod = "alert";
	     $mess = "<b>El estado de cuenta fue almacenado con &eacute;xito</b> ".$esc_saldo; 
	    $mess_cod = "alert";
	//echo $esc_saldo;
	
	$query=' SELECT  coo_id FROM datos_per_pac WHERE pac_id='.$pac_id.'';
	$table->sql_query($query);
	$paciente = $table->sql_fetch_object();
	
if (isset($evo_id) && $evo_id!=""){
	
	$query=' SELECT "odl_tipoOdontologo","pxe_cantidadProcedimientos", "pto_valorEstPre", "pto_valorEstPost", "pto_valorAsit", "pto_valorEsp"
	FROM odontologos o, evolucion e, procxevo p, procedimiento t WHERE e.usu_id=o.usu_id and p.evo_id=e.evo_id and p.pto_id=t.pto_id and p.evo_id='.$evo_id.'' ;
       $table->sql_query($query);
       $pxevo = $table->sql_fetch_object();
    	
      	$esc_debe=0;
     
	if($pxevo->odl_tipoOdontologo==1){
		do {
			$esc_debe+=$pxevo->pxe_cantidadProcedimientos*$pxevo->pto_valorEstPre;
		} while($pxevo=$table->sql_fetch_object());
		
   	}elseif($odl->odl_tipoOdontologo==2){
   		 do {
  			$esc_debe+=$pxevo->pxe_cantidadProcedimientos*$pxevo->pto_valorEstPost;
  		} while($pxevo=$table->sql_fetch_object());
  	}elseif($odl->odl_tipoOdontologo==3){
  		do{
	  		$esc_debe+=$pxevo->pxe_cantidadProcedimientos*$pxevo->pto_valorAsit;
	  	} while($pxevo=$table->sql_fetch_object());
	}elseif($odl->odl_tipoOdontologo==4){
  		do {
	  		$esc_debe+=$pxevo->pxe_cantidadProcedimientos*$pxevo->pto_valorEsp;	
	  	} while($pxevo=$table->sql_fetch_object());
	}
if($paciente->coo_id==2){
	$esc_debe=$esc_debe-($esc_debe*0.10);
	}
$esc_saldo+=$esc_debe;
	
	
}


if($pag_id>0){
	$esc_pago=0;
	
	if ($pch_valorCheque!="")
		$esc_pago+=$pch_valorCheque;
	if ($pag_valorEfectivo!="")
		$esc_pago+= $pag_valorEfectivo;
	
	
	/*$query=' SELECT  eps_id, pac_estrato FROM paciente WHERE pac_id='.$pac_id.'';
	$table->sql_query($query);
	$paciente = $table->sql_fetch_object();
	echo $query;	
		if($paciente->eps_id >1 && $paciente->pac_estrato == 2){
			$esc_saldo = $esc_saldo - ($esc_pago+$pag_eps);
		}else if($paciente->eps_id >1 && $paciente->pac_estrato == 1){
			$esc_saldo = $esc_saldo - $esc_pago * 20;
		}else if($paciente->pac_fetrabuv ==1){
			$esc_saldo = $esc_saldo - $esc_pago * 1.112;
		}else*/
			$esc_saldo = $esc_saldo - $esc_pago;
			
}
          
	/*  se pasa a la quota a bytes */
         $fields = array ("esc_id", "pac_id", "evo_id", "pag_id", "esc_fecha", "esc_saldo", "esc_debe", "esc_pago", "pag_centroInfo");
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "estado_cuenta" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "No se pudo adicionar el estado de cuenta por un fallo en el sistema, por Favor contacte a el Administrador.".$query;
	$mess_cod = "info";		
	}else{
	    logs::crear("estado_cuenta", "crear", $query);	
	    $mess = "<b>El estado de cuenta fue almacenado con &eacute;xito</b> ".$query; 
	    $mess_cod = "alert";
	    $s_opc = "busqueda";
	 } 
	  
   // $mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }

  /********************* Buscar un estado de cuenta******************/

function buscar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);


             $fields = array ("ahc_id", "pac_id", "ara_id", "ahc_fechaApertura");
							      
       // Arreglos para datos del query y el url para paginacion

      $where_array = array();
     $query = 'SELECT * FROM  "apertura_historia_clinica" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($where_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($where_array))
	$query .= " WHERE ".implode(", ",$where_array);
	
      	$result=$table->search($query);
	
	if (!$result){
		$mess = "No hay registros que concuerden con su b&uacute;squeda.".$query;
		$mess_cod = "alert";		
	}else{
	       $mess = "La Apertura fu&eacute; consultada con &eacute;xito ".$query.$table->nfound; 
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }


  /**************Actualiar el estado de cuenta de una paciente cuando hace un pago de radiologia*************/
  function estRad(){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;

    foreach($_GET as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;

	$query=" SELECT esc_saldo FROM estado_cuenta WHERE esc_id  IN (SELECT MAX(esc_id) FROM estado_cuenta WHERE pac_id=$pac_id)";
	$table->sql_query($query);
	$esc = $table->sql_fetch_object();
	$esc_saldo=0;
	$esc_saldo += $esc->esc_saldo;
		
  
$esc_saldo = $esc_saldo+$esc_debe;
$evo_id= '0';
$pag_id= '0';
$esc_pago= '0';
$esc_fecha=date("Y-m-d");
			

          
	/*  se pasa a la quota a bytes */
         $fields = array ("esc_id", "pac_id", "evo_id", "pag_id", "esc_fecha", "esc_saldo", "esc_debe", "esc_pago", "rxr_id", "old_id");
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "estado_cuenta" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "No se pudo adicionar el estado de cuenta por un fallo en el sistema, por Favor contacte a el Administrador.".$query;
	$mess_cod = "info";		
	}else{
	    logs::crear("estado_cuenta", "crear", $query);	
	    $mess = "<b>El estado de cuenta fue almacenado con &eacute;xito</b> ".$query; 
	    $mess_cod = "alert";
	    $s_opc = "busqueda";
	 } 
	  
   // $mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }

/**************Actualiar el estado de cuenta cuando un paciente Asiste a una cita Odontologica*************/
  function estCita(){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;

    foreach($_GET as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;

	$query=" SELECT esc_saldo FROM estado_cuenta WHERE esc_id  IN (SELECT MAX(esc_id) FROM estado_cuenta WHERE pac_id=$pac_id)";
	$table->sql_query($query);
	$esc = $table->sql_fetch_object();
	$esc_saldo=0;
	$esc_saldo += $esc->esc_saldo;
		
  
$esc_saldo = $esc_saldo+$esc_debe;
$evo_id= '0';
$rxr_id='0';
$pag_id= '0';
$esc_pago= '0';
$esc_fecha=date("Y-m-d");
			

          
	/*  se pasa a la quota a bytes */
         $fields = array ("esc_id", "pac_id", "evo_id", "pag_id", "esc_fecha", "esc_saldo", "esc_debe", "esc_pago", "rxr_id", "old_id");
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "estado_cuenta" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "No se pudo adicionar el estado de cuenta por un fallo en el sistema, por Favor contacte a el Administrador.".$query;
	$mess_cod = "info";		
	}else{
	    logs::crear("estado_cuenta", "crear", $query);	
	    $mess = "<b>El estado de cuenta fue almacenado con &eacute;xito</b> ".$query; 
	    $mess_cod = "alert";
	    $s_opc = "busqueda";
	 } 
	  
   // $mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }



	}


?>
