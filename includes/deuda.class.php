<?
/**********************************************************/
// CLASE: deuda
// Proposito: crear deuda a un paciente.
// Ultima modificacion: Septiembre 22 de 2011
// /**********************************************************/
class deuda{
	var $fv;
  	var $data;
	
  	function deuda($id=-1){
    		global $table;
    		$this->fv = new FormValidator;
		if ($id >= 0){
			$query = "SELECT * FROM deuda WHERE deu_id='$id' ";
	 		$table->sql_query($query);
			$this->data = $table->sql_fetch_object();
  		}
	}

  /**********************************************************/
  // Proposito: Crearle una deuda a un paciente especifico 
  // return: arreglos con resultados de la creacion.
  /**********************************************************/
	function crear(){
	//$table que contiene la consulta ademas de las variable get y post que contienen datos
    	global $table, $_POST, $_GET;
	//Creamos una variable por cada elemento en POST
    	foreach($_POST as $k=>$v)
      		${$k} = $v;

	//Creamos una variable por cada elemento en GET
    	foreach($_GET as $k=>$v)
      		${$k} = $v;
/*
Recibimos las variables 
deu_fecha
usu_id
pac_numeroIdentificacion
deu_valor
deu_dcto
deu_conceptodcto
pto_codigoProcedimiento
s_opc
opc
*/
      
	//deu_id, pac_id, deu_valor, deu_dcto,deu_conceptodcto,deu_fecha, usu_id,pto_id
	//recibidas: pac_numeroIdentificacion
      	$this->fv->resetErrorList();//Reseteamos la lista de errores

//Verificamos que no hayan datos vacios
	$this->fv->isEmpty("pac_numeroIdentificacion","Debe ingresar numero de identificacion del paciente");
	$this->fv->isEmpty("deu_valor","La deuda debe tener un valor");
	$this->fv->isEmpty("pto_codigoProcedimiento","Debe ingresar el codigo del procedimiento");
	//Verificar que ciertos valores sean 
	//$this->fv->isInteger($deu_valor,"El valor de la deuda debe ser un numero entero");
/***********************************************************************/
$msg_error="";
//Calculamos el pac_id a partir del pac_numeroIdentificacion
	//Creamos el objeto de base de datos
	$table2 = new my_DB();
	
	//Hacemos la consulta
	$query = "SELECT * FROM \"paciente\" WHERE \"pac_numeroIdentificacion\" = '".$pac_numeroIdentificacion."'";

	//Hacemos la consulta
	$table2->search($query);
	if ($table2->nfound == 1){
		$registro = $table2->sql_fetch_object();
		$pac_id=$registro->pac_id;
		
	}else{
		//hay un error en la identificacion
		if ($table2->nfound < 1){
			$msg_error.="<li>La identificacion no existe<br>";
		}else{
			$msg_error.="<li>Existen ".($table2->nfound)." pacientes con esa identificaci&oacute;n<br>";
		}		
	}

//Verificacion del campo de descuento y el comentario
	if(($deu_dcto && !($deu_conceptodcto)) || ($deu_conceptodcto && !($deu_dcto))){
		//Error, si existe un descuento entonces debe tener su concepto
		$msg_error.="<li>Si existe un descuento entonces debe tener su concepto<br>";
	}
	if($deu_dcto){
		if($deu_dcto>$deu_valor){
			$msg_error.="<li>El descuento no puede ser mayor que que el valor de la deuda<br>";
		}
	}
//Obtenemos el pto_id a partir del codigo de procedimiento
	$query = "SELECT * FROM \"procedimiento\" WHERE \"pto_codigoProcedimiento\"='".$pto_codigoProcedimiento."'";
	//Hacemos la consulta
	$table2->search($query);
	if ($table2->nfound == 1){
		$registro = $table2->sql_fetch_object();
		$pto_id=$registro->pto_id;
	}else{
		//hay un error en la identificacion
		$msg_error.="<li>El c&oacute;digo del procedimiento no es &uacute;nico o no existe!!<br>";
	}
/************************************************************/
	//Si hay errores lo mostramos
   	if ( $this->fv->isError() ||  $msg_error!="") {
		if($this->fv->isError())
      			$mess = $this->fv->getMessage();
		else
			$mess.=("<b>ATENCION!</b><br>Por favor tenga en cuenta que:<ul>".$msg_error."</ul>");

      		$mess_cod = "alert";
    	}else {
		//Campos de la tabla
		$fields = array ("deu_id","pac_id","deu_valor", "deu_dcto", "deu_conceptodcto", "deu_fecha", "usu_id", "pto_id");
		//Arreglos para datos del query y el url para paginacion
      		$fields_array = array();
      		$values_array = array();
     		$query = "INSERT INTO \"deuda\" (";
	      	//Adicionando los campos para el query
      		foreach($fields as $v)
			if(${$v}!=""){
	  			array_push($fields_array,' "'.$v.'" ');
	  			array_push($values_array," '".${$v}."' ");
			}

      			if(sizeof($fields_array)){
					$query .= implode(", ",$fields_array);
					$query .= ") VALUES (".implode(", ",$values_array) ;
			}
			$query .=")";	
			$result=$table->sql_query($query);
			if (!$result){
				$mess = "<b>No se pudo registrar la deuda por un fallo en el sistema. Comuniquese con el administrador.</b>";
				$mess_cod = "alert";		
			}else{
	    			logs::crear("deuda", "crear", $query);
	    			$mess = "<b>La deuda fue registrada con &eacute;xito</b>"; 
	    			$mess_cod = "info";
			}
	  }
	//$mess .= $query;
    	return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
}




/********************* Buscar pagos******************/

	function buscar(){
    	global $table, $_POST ,$_GET;

     	foreach($_POST as $k=>$v)
      		${$k} = base::dispelMagicQuotes($v);

    	foreach($_GET as $k=>$v)
      		${$k} = base::dispelMagicQuotes($v);

		$fields = array ( "pag_id", "pag_fecha", "pac_id", "usu_id", "pag_numeroCuenta", "pag_tipoPagoEfectivo", "pag_valorConsulta", 
			"pag_valorCopago", "pag_valorEnLetras", "pag_conceptoPago", "pag_eps", "pag_centroInfo", "pag_codigoIng", 
			"pag_tipoPagoDebito", "pag_tipoPagoCredito");
							      
       	// Arreglos para datos del query y el url para paginacion

     	$where_array = array();
     	$query = 'SELECT * FROM  "pagos" ';
      	// Adicionando los campos para el query
       
     	foreach($fields as $v)
        	if(${$v}!=""){
	  			array_push($where_array,' "'.$v.'" '." = '".${$v}."' ");
			}

       	if(sizeof($where_array))
			$query .= " WHERE ".implode(", ",$where_array);
			
		$result=$table->search($query);
		
		if (!$result){
			$mess = "No hay registros que concuerden con su busqueda.".$query;
			$mess_cod = "alert";		
		}else{
	    	$mess = "El pago fue adicionado con &eacute;xito ".$query; 
	    	$s_opc = "info_usuario";
	 	} 
		return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
	}

  	
/************** Anular un pago ****************/
  
  	function anular(){
  		global $table;

    	foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      		${$k} = base::dispelMagicQuotes($v);

    	foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      		${$k} = base::dispelMagicQuotes($v);

    	$fields = array ("pag_id", "pag_fecha", "pac_id", "usu_id", "pag_estado", "pag_tipoPagoEfectivo", "pag_valorEfectivo", 
    		"pag_valorCopago", "pag_conceptoPago", "pag_valorEnLetras", "pag_tipoPagoCheque", "pag_eps", "pag_centroInfo", 
    		"pag_codigoIng");
			// Arreglos para datos del query y el url para paginacion

	   	$set_array = array();
     	$query = 'UPDATE  "pagos" ';
      	// Adicionando los campos para el query
      	foreach($fields as $v)
			if(${$v}!=""){
	  			array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
			}

       	if(sizeof($set_array))
			$query .= " SET ".implode(", ",$set_array);
	
      	$query .=  " WHERE pag_id = '$pag_id'";

		$result=$table->sql_query($query);
	
		if (!$result){
			$mess = "<b>No se pudo anular el pago por un fallo en el sistema. Comuniquese con el administrador</b>";
			$mess_cod = "alert";		
		}else{
	    	logs::crear("cita", "modificar", $query);
	 		$mess = "<b>El pago fu&eacute; anulado con &eacute;xito</b>"; 
	 		$mess_cod="info";
		} 
	  
    	return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  	}
}
?>
