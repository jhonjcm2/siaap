<?
/***********************************************************************************************************/
// CLASE: codDiagnostico
// Proposito: Realizar el manejo de los diagnosticos odontologicos manejados por la escuela
// Ultima modificacion: Octubre de 2003
// /********************************************************************************************************/
class codDiagnostico{
  var $fv;
  var $data;
	
  function codDiagnostico($id=-1){
    global $table;
    
    $this->fv = new FormValidator;
    if ($id >= 0){
    	$query = "SELECT * 
        	     FROM codigos_diagnostico 
        	     WHERE cod_id='$id' ";
	$table->sql_query($query);
    	$this->data = $table->sql_fetch_object();
    }
    
  }

  /************************************************************/
  // Proposito: Creacion de los C�digos de Diagn�stico
  // return: arreglos con resultados de la creacion.
  /************************************************************/
  function crear(){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;
 
 
  // $this->fv->isNumber("cod_codigoDiagnostico", "Debe insertar solo n&uacute;meros");
   $this->fv->isEmpty("cod_descripcionDiagnostico", "Debe insertar el nombre del diagn&oacute;stico");
 
   if ( $this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    }else {
      $query = 'SELECT * 
                FROM codigos_diagnostico
                WHERE "cod_codigoDiagnostico"='.$cod_codigoDiagnostico;
      $table->search($query);
    
      if($table->nfound) {
	$mess = "<center><b>Un diagn�stico con ese c�digo ya se encuentra registrado en el sistema</b><br> por favor verifique el c&oacute;digo.</center>";
	$mess_cod = "alert";
	}
      else {
	/*  se pasa a la quota a bytes */
         $fields = array ("cod_id", "cod_codigoDiagnostico", "cod_descripcionDiagnostico");
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "codigos_diagnostico" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "<b>No se pudo adicionar el c�digo del diagn�stico por un fallo en el sistema.Comuniquese con el administrador</b>".$query;
	$mess_cod = "alert";		
	}else{
	    logs::crear("codDiagnostico", "crear", $query);
	    $mess = "<b>El diagn�stico fue ingresado con &eacute;xito.</b> "; 
	    $mess_cod = "info";
	  
	 } 
	  
      }
    }
     return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }


/********************* Buscar un c�digo de diagn�stico******************/

function buscar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);



             $fields = array ("cod_codigoDiagnostico", "cod_descripcionDiagnostico");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'SELECT * FROM  "codigos_diagnostico" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($where_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($where_array))
	$query .= " WHERE ".implode(", ",$set_array);
	
      	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No hay registros que concuerden con su b&uacute;squeda";
		$mess_cod = "alert";		
	}else{
	       $mess = "El c&oacute;digo del diagn&oacute;stico fu&eacute; adicionado con &eacute;xito ".$query; 
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess.$query,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }



  /************** Modificar codigos de diagn�stico ****************/
  
  function modificar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

     
    $fields = array ("cod_codigoDiagnostico", "cod_descripcionDiagnostico");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "codigos_diagnostico" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE cod_id = '$cod_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "<b>No se pudo modificar el diagnostico por un fallo en el sistema. Comuniquese con el administrador.</b> ";
		$mess_cod = "alert";		
	}else{
	       logs::crear("codDiagnostico", "modificar", $query);
	 	$mess = "<b>El c&oacute;digo del diagn&oacute;stico fu&eacute; modificado con &eacute;xito.</b> "; 
	    	$mess_cod="info";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }
 
}
?>
