<?php
/**********************************************************/
// CLASE: presupuesto
// Proposito: Realizar el presupuesto del paciente.
// Ultima modificacion: octubre de 2003
// /**********************************************************/
class presupuesto{
  var $fv;
  var $data;
	
  function presupuesto($id=-1){
    global $table;
    
    $this->fv = new FormValidator;
    if ($id >= 0){
    	$query = "SELECT * 
        	     FROM presupuesto 
        	     WHERE pre_id='$id' ";
	$table->sql_query($query);
    	$this->data = $table->sql_fetch_object();
    }
    
  }

  /**********************************************************/
  // Proposito: Creacion del presupuesto odontológico
  // return: arreglos con resultados de la creacion.
  /**********************************************************/
  function crear(){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;

    foreach($_GET as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;
      
  
//   if ( 0 /*$this->fv->isError()*/ ) {
if ( 0 ){
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    }else {
      $query = 'SELECT * 
                FROM presupuesto
                WHERE "pre_id"='.$pre_id;
      $table->search($query);
    
      if($table->nfound) {
	$mess = "Un presupuesto con ese documento ya se encuentra registrado en el sistema, por favor escoja otro.";
	$mess_cod = "alert";
	$s_opc = "adicionar_usuario";
      }
      else {
	/*  se pasa a la quota a bytes */
         $fields = array ("pre_id", "pac_id", "pre_fecha");
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "presupuesto" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "No se pudo adicionar el presupuesto por un fallo en el sistema, Por favor comuniquese con el administrador.".$query;
	$mess_cod = "alert";
			
	}else{
	    logs::crear("presupuesto", "crear", $query);
		
	    /*$esc = new estado_cuenta();
		$ret = $esc->crear($evo_id, $esc_pago);*/
		
		
		$mess = "<center><b>La actividad cl&iacute;nica $login fu&eacute; adicionada con &eacute;xito </b><br><br>
	    Siga ingresando las actividades !!!</center> "; 
	    $mess_cod = "info";
	    //$s_opc = "busqueda";
	 } 
	  
      }
    }
    //$mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }


/********************* Buscar un presupuesto******************/

function buscar(){
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($_GET as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);



             $fields = array ( "pac_id", "usu_id", "pre_fecha");
							      
       // Arreglos para datos del query y el url para paginacion

      $where_array = array();
     $query = 'SELECT * FROM  "presupuesto" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($where_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($where_array))
	$query .= " WHERE ".implode(", ",$where_array);
	
      	$result=$table->search($query);
	
	if (!$result){
		$mess = "No hay registros que concuerden con su busqueda.".$query;
		$mess_cod = "alert";		
	}else{
	 	$mess = "El presupuesto fue adicionado con &eacute;xito ".$query; 
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }

/*****************************************************/
function agregarProc($pre_id=0){


global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;
      
      $this->fv->resetErrorList();
      $this->fv->isEmpty("pto_id","Seleccione un procedimiento para agregar");
      $this->fv->isWithinRange("pxp_cantidadProcedimientos", "Ingrese una cantidad de procedimientos v&aacute;lida",1,1000000);
      $this->fv->isNumber("pxp_cantidadProcedimientos", "Debe insertar solo n&uacute;meros en la cantidad del procedimiento");


   if ($this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
      }
      else {
        /*  se pasa a la quota a bytes */
         $fields = array ("pre_id", "pto_id", "pxp_cantidadProcedimientos");

       // Arreglos para datos del query y el url para paginacion
      	$fields_array = array();
      	$values_array = array();
     	$query = 'INSERT INTO "procxpresu" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
        if(${$v}!=""){
          array_push($fields_array,' "'.$v.'" ');
          array_push($values_array," '".${$v}."' ");
        }

      if(sizeof($fields_array)){
        $query .= implode(", ",$fields_array);
        $query .= ") VALUES (".implode(", ",$values_array) ;
        }
        $query .=")";

        $result=$table->sql_query($query);

        if (!$result){
        $mess = "<b>No se pudo adicionar el procedimiento por un fallo en el sistema, por Favor contacte al Administrador del Sistema.</b>";
        $mess_cod = "alert";
        }else{
            logs::crear("presupuesto", "agregarProc", $query);
            $mess = "<center><b>La actividad cl&iacute;nica $login fu&eacute; adicionada con &eacute;xito </b><br><br>
	     Si lo desea, siga ingresando las actividades !!!</center> "; 
	     $mess_cod = "info";
         }

      }
    //$mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }

  /************** Modificar presupuesto ****************/
  
  function modificar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

   
    $fields = array ( "usu_id", "hic_id", "pre_fecha");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "presupuesto" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE pre_id = '$pre_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo adicionar el presupuesto por un fallo en el sistema, por favor llame al administrador del sistema".$query;
		$mess_cod = "alert";		
	}else{
	       logs::crear("presupuesto", "modificar", $query);
	 	$mess = "El presupuesto $login fue adicionado con &eacute;xito ".$query; 
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }


 /************** Borrar un presupuesto antes de acentar su creacion****************/
 
  function borrar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    

    $fields = array ("pre_fecha");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'DELETE FROM "presupuesto" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE pre_id =$pre_id";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo cancelar el presupuesto por un fallo en el sistema, Comun&iacute;quese con el administrador.";
		$mess_cod = "alert";		
	}else{
	       logs::crear("presupuesto", "borrar", $query);
	 	$mess = "El presupuesto $login fu&eacute; cancelado con &eacute;xito ".$query; 
	    	$mess_cod = "info";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }

/***********borrar de la tabla procxpresu  un presupuesto***********************************************/
function borrarProc($dia_id=0){


global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;


//   if ( 0 /*$this->fv->isError()*/ ) {
if ( 0 ){
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
      }
      else {
        /*  se pasa a la quota a bytes */
         $fields = array  ("pre_id", "pto_id", "pxp_cantidadProcedimientos");

       // Arreglos para datos del query y el url para paginacion
      	$fields_array = array();
      	$values_array = array();
     	$query = 'DELETE FROM "procxpresu" ';
     	// Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE pre_id = '$pre_id'";

	$result=$table->sql_query($query);
    
        if (!$result){
        $mess = "No se pudo cancelar el presupuesto por un fallo en el sistema, Contacte al Administrador del Sistema.";
        $mess_cod = "alert";
        }else{
              logs::crear("presupuesto", "borrarProc", $query);
            	$mess = "<center><b>El presupuesto $login fu&eacute; cancelado</b><br>
            	Vuelva a ingresarlo</center>";
	 	$mess_cod = "info";
         }

      }
    //$mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }


}
?>
