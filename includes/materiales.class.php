<?
/*********************************************************************/
// CLASE: materiales
// Proposito: Realizar el ingreso de los materiales al sistema.
// Ultima modificacion: octubre de 2003
// /******************************************************************/
class materiales{
  var $fv;
  var $data;
	
  function materiales($id=-1){
    global $table;
    
    $this->fv = new FormValidator;
    if ($id >= 0){
    	$query = "SELECT * 
        	     FROM materiales 
        	     WHERE mat_id='$id' ";
	$table->sql_query($query);
    	$this->data = $table->sql_fetch_object();
    }
    
  }

	/**/function eliminar($idMaterial){
		
		global $table, $_POST, $_GET;
		foreach($_POST as $k=>$v)
			${$k} = base::dispelMagicQuotes($v);
			//${$k} = $v;
			
		foreach($_GET as $k=>$v)
			${$k} = base::dispelMagicQuotes($v);
		
		//$query='DELETE FROM "materiales" WHERE "mat_codigoMaterial" LIKE '.$codigoMaterial;

		//Borramos el material
		$query='DELETE FROM "materiales" WHERE "mat_id"='.$idMaterial;
		$table->sql_query($query);

		$query='SELECT * FROM "materiales" WHERE "mat_id"='.$idMaterial;
		$table->search($query);
		//Eliminamos las existencias de las bodegas
		$query='DELETE FROM "matxbod" WHERE "mat_id"='.$idMaterial;
		$table->sql_query($query);
		

		if($table->nfound<=0){
			return $result=array("tipo"=>"ok","mess"=>"El material se elimin&oacute; correctamente","mess_cod"=>"info");
		}else{
			return $result=array("tipo"=>"error","mess"=>"El material no pudo ser eliminado, int&eacute;ntelo de nuevo","mess_cod"=>"error");
		}
	}/**/

	/*Devuelve un codigo nuevo para un material que se crea*/
	function crearCodigo(){
		
		global $table, $_POST, $_GET;
		foreach($_POST as $k=>$v)
			${$k} = base::dispelMagicQuotes($v);
			//${$k} = $v;
			
		foreach($_GET as $k=>$v)
			${$k} = base::dispelMagicQuotes($v);
			//${$k} = $v;
				/*if(isset($mat_descripcion))
					echo "mat_descripcion=".$mat_descripcion."<BR>";
				if(isset($mat_marca))
					echo "mat_marca=".$mat_marca."<BR>";
				if(isset($mat_area))
					echo "mat_area=".$mat_area."<BR>";*/

		$resultado=array("tipo"=>"","mess"=>"","code"=>"");
		//Obtengo las 3 primeras letras
		$nuevoCodigo=strtoupper($mat_descripcion{0}.$mat_marca{0}.$mat_area{0});
		//echo "new1=".$nuevoCodigo."<br>";
		//Busco los que comienzan por esos tres caracteres, si existen registros estos deben empezar por ABC000
		$queryCodigo = 'SELECT * FROM materiales WHERE "mat_codigoMaterial" LIKE '."'".$nuevoCodigo."___' ".' ORDER BY "mat_codigoMaterial" ASC';
		//echo $queryCodigo."<BR>";
		$table->search($queryCodigo);
		/*Colocamos el numero que sigue
		$ultimoRegistro =$table->sql_fetch_object();
		$i=substr($ultimoRegistro->mat_codigoMaterial,3,strlen($ultimoRegistro->mat_codigoMaterial)-1)+1;
		$nuevoCodigo=$nuevoCodigo.$i;
		*/
		/*Si hay registros*/
		if($table->nfound>0){
			$n=$table->nfound;
			//Empezamos a generar los códigos desde ABC000 ($nuevoCodigo."000")
			for($i=0; $i<$n; $i++){
				//echo "i es $i";
				$registro=$table->sql_fetch_object();
				$posibleCodigo=strtoupper($mat_descripcion{0}.$mat_marca{0}.$mat_area{0});
				if($i<10){
					$posibleCodigo=strtoupper($mat_descripcion{0}.$mat_marca{0}.$mat_area{0})."00".$i;
				}else if($i<100){
					$posibleCodigo=strtoupper($mat_descripcion{0}.$mat_marca{0}.$mat_area{0})."0".$i;
				}else if($i<1000){
					$posibleCodigo=strtoupper($mat_descripcion{0}.$mat_marca{0}.$mat_area{0}).$i;
				}
				//Lo comparo con el primero de la lista que si existe debe ser ABC000
				//Si es igual continuo con el siguiente
				//echo "Comparo ".$registro->mat_codigoMaterial." con ".$posibleCodigo;
				if(strcasecmp($registro->mat_codigoMaterial,$posibleCodigo)==0){
					//echo "-----Son iguales, continuo";
					continue;
				}
				//Si no es igual hago una búsqueda para asegurarme de que no existe el código generado
				else{
					$tablex=new my_DB();
					$query='SELECT * FROM materiales WHERE "mat_codigoMaterial" LIKE '."'".$posibleCodigo."' ".' ORDER BY "mat_codigoMaterial" ASC';
					//echo $query."*************<br>";
					$tablex->search($query);
					//Si no existen mas registros el code es unico
					if($tablex->nfound<=0){
						$nuevoCodigo=$posibleCodigo;
						return $result=array("tipo"=>"ok","mess"=>"C&oacute;digo &uacute;nico $posibleCodigo","code"=>"$posibleCodigo");
					}
					//Si existen registros quiere decir que no es único y hay algún error
					else{
						return $result=array("tipo"=>"error","mess"=>"posible redundancia de datos","code"=>"$posibleCodigo");
					}
				}
			}
			//Si se llego al final y no se encontró entonces va uno al final
			if($result["tipo"]=="" && $n<999){
				if($i<10){
					$posibleCodigo=strtoupper($mat_descripcion{0}.$mat_marca{0}.$mat_area{0})."00".$i;
				}else if($i<100){
					$posibleCodigo=strtoupper($mat_descripcion{0}.$mat_marca{0}.$mat_area{0})."0".$i;
				}else if($i<1000){
					$posibleCodigo=strtoupper($mat_descripcion{0}.$mat_marca{0}.$mat_area{0}).$i;
				}
				return $result=array("tipo"=>"ok","mess"=>"C&oacute;digo &uacute;nico $posibleCodigo","code"=>"$posibleCodigo");
			}else{
				return $result=array("tipo"=>"error","mess"=>"datos agotados","code"=>"$posibleCodigo");
			}
		}
		//Si no hay registros el primero es ABC000
		else{
			$nuevoCodigo=$nuevoCodigo."000";
			return $result=array("tipo"=>"ok","mess"=>"C&oacute;digo &uacute;nico $posibleCodigo","code"=>"$nuevoCodigo");
		}/**/
		//echo "this is.....".$nuevoCodigo;
	}
  /**************************************************************/
  // Proposito: Creacion de los materiales en el sistema
  // return: arreglos con resultados de la creacion.
  /**************************************************************/
  function crear(){
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;
      
      //$this->fv->isEmpty("mat_codigoMaterial", "Debe ingresar el c&oacute;digo del material");
      $this->fv->isEmpty("mat_descripcion", "Debe ingresarr la descripci&oacute;n del material");
      $this->fv->isNumber("mat_valorUnitario", "Debe  ingresar un valor mayor o igual a 0 (cero)");
      $this->fv->isEmpty("mat_presentacion", "Debe ingresar la presentaci&oacute;n del material");
      $this->fv->isEmpty("mat_marca", "Debe ingresar la marca del material");
      $this->fv->isEmpty("mat_area", "Debe ingresar el &Acute;rea a la que pertenece el material");
      $this->fv->isEmpty("mat_unidades", "Debe ingresar el numero de unidades por cada producto");
      $this->fv->isNumber("mat_unidades", "Debe  ingresar un valor mayor o igual a 0 (cero)");

   if ($this->fv->isError()){
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
	
    }else {
	/*******************Creamos el codigo****************/
	$resultadoCodigo=$this->crearCodigo();
	$mat_codigoMaterial=$resultadoCodigo["code"];
	/****************************************************/

      $query ="SELECT *
                FROM materiales
                WHERE \"mat_codigoMaterial\"='$mat_codigoMaterial'";
      $table->search($query);
	//echo "primer   ".$query."<br>";
	//Si el tipo de mensage generado es distinto de ok, mandamos error
	if(strcasecmp($resultadoCodigo["tipo"],"ok")!=0){
		$mess = $resultadoCodigo["mess"];
		$mess_cod = "alert";
	}
	else if($table->nfound) {
		$mess = "<center><b>Un material con ese c&oacute;digo ya se encuentra registrado en el sistema<br></b> por favor ingrese otro.</center>";
		$mess_cod = "alert";
	}
	else {
	/*  se pasa a la quota a bytes */
         $fields = array ("mat_id", "mat_codigoMaterial", "mat_descripcion", "mat_valorUnitario", "mat_presentacion","mat_fechaIngreso", "usu_id", "mat_marca", "mat_area", "mat_unidades");
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "materiales" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "<b>No se pudo adicionar el material por un fallo en el sistema. Comuniquese con el administrador</b>";
	$mess_cod = "alert";		
	}else{
	    logs::crear("materiales", "crear", $query);
	    $mess = "<center><b>El material $login fu&eacute; adicionado con &eacute;xito </b><br><br></center>";
/****************************/
$nombres=array("C&oacute;digo: ","Descripci&oacute;n: ","Valor unitario: ","Marca","Presentaci&oacute;n: ","UxP :","&Aacute;rea: ");
$campos=array ("mat_codigoMaterial", "mat_descripcion", "mat_valorUnitario","mat_marca", "mat_presentacion","mat_unidades","mat_area");
$i=0;
$mess="<center>".$mess;
foreach($campos as $k){
	if(${$k}){
		$mess=$mess."<BR>".$nombres[$i].${$k};
	}
	$i++;
}
$mess=$mess."</center>";
/****************************/

	    $mess_cod = "alert";
	    //$s_opc = "info_usuario";
	 }
	  
      }
    }
    //$mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }


/********************* Buscar materiales******************/

function buscar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);



             $fields = array ("mat_codigoMaterial", "mat_descripcion", "mat_valorUnitario", "mat_presentacion");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'SELECT * FROM  "materiales" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($where_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($where_array))
	$query .= " WHERE ".implode(", ",$where_array);
	
      	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No hay registros que concuerden con su busqueda".$query;
		$mess_cod = "alert";		
	}else{
	       $mess = "El material $login fue adicionado con &eacute;xito ".$query; 
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }

  /************** Modificar materiales ****************/
  function modificar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    
    $fields = array ("mat_id", "mat_codigoMaterial", "mat_descripcion", "mat_valorUnitario", "mat_presentacion","mat_fechaIngreso", "mat_area", "mat_unidades", "mat_marca", "usu_id");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "materiales" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE mat_id = '$mat_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo Modificar el material por un fallo en el sistema, por Favor llame a Holmes y nathalia que hicieron la aplicacion.".$query;
		$mess_cod = "alert";		
	}else{
	       logs::crear("materiales", "modificar", $query); 
	 	$mess = "<center><b>El material $login fu&eacute; editado con &eacute;xito </b></center><br>
	 	Haga click <a href='$PHP_SELF?opc=materiales&s_opc=listar'>AQU&Iacute;</a> para regresar al listado"; 
	    	$mess_cod = "alert";
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }
 
}
?>
