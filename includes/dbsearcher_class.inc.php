<?
/* db_searcher.inc : Abr 23 2000: Felipe Borrero */

/***************************************************************
realiza busquedas sobre una base de datos y retorna parametros para 
la navegacion entre paginas y registros.
****************************************************************/

class db_searcher extends sql_db {

// Propiedades

	var	$nfound=0;		// 	numero de registros encontrados
	var	$nshown=0;		//	numero de registros mostrados en la pagina
	var	$begnum=0;		//	sequencia del primer registro mostrado
	var	$endnum=0;		//	sequencia del ultimo registro mostrado
	var	$skip=0;			//	indice del primer registro a mostrar en la seleccion
	var	$max;					//	numero de registros a mostrar
	var $max_pages;		//  numero maximo de paginas a mostrar
	var $index = 0;		//	registro corriente
	var	$next = "";
	var	$prev = "";
	var	$search_url;
	var $target;

	function db_searcher($type="db_searcher") {
		// constructor 
		$this->sql_db($type);
		return;
	}

	function search($query,$skip=0,$target="",$url="") {
		
		global $PHP_SELF;
		// inicializar atributos de resultado
			$this->nfound=0;
			$this->begnum=0;
			$this->endnum=0;
		// se gaurda el url para hacer link a next, prev y pages
		$this->search_url = $url;
		//  si el target esta vacio se asume como el mismo script
		if ($target=="") {
			$this->target = $PHP_SELF;
		} else {
			$this->target = $target;
		}
		// se determina el primer registro a mostrar
		$this->skip = $skip;

		// se ejecuta la consulta para encontrar los registros
		$result = $this->sql_query($query); 
		// calcular atributos si la busqueda funciona
		if ($result) {
			// guardar el numero de registros encontrados
			$this->nfound = $this->sql_num_rows();
			// ejeutar todo si se encontraron registros
			if ($this->nfound) {
			
			// se deben calcular los limites minimo y maximo del set
			if (($this->nfound - $this->skip) < $this->max) {
				// si el numero de registros restantes es menor a max
				// se debe tomar la dferencia en nshown
				$this->endnum = $this->nfound;
				$this->nshown = $this->nfound - $this->skip;
			} else {
				$this->endnum = $this->skip + $this->max;
				$this->nshown = $this->max;
			}
			// el primer registro mostrado (sumar uno, SQL empieza en 0)
			$this->begnum = $this->skip+1;
			// ubicar el cursor en el registro correspondiente
			$this->index = $this->sql_data_seek($this->skip);
			} // end if nfound
		}
		return $result;
	}

	function pages($class="") {

		$num_pages = $this->nfound/$this->max;
		// se calcula el numero de paginas
		if ($this->nfound % $this->max) 
			$num_pages .+ 1;
			
		$more_recs = FALSE;
		
		// solo se muestran $this->max_pages paginas
		if ($num_pages > $this->max_pages) {
			$num_pages = $this->max_pages;
			$more_recs = TRUE;
		}
		if (!empty($class)) {
			$class = "class=\"".$class."\"";
		} else {
			$class="";
		}
		$pages = "| ";
		for ($i=0; $i < $num_pages; $i++) {
			$from = $i*$this->max;
			$target = $this->target."?skip=".$from."&max=".$this->max.$this->search_url;
			if (($from+1) == $this->begnum) {
				$link = "<b>".($i+1)."</b>";
			} else {
				$link = $i+1;
			}
			$pages .= "<a ".$class." href=\"".$target."\">".$link."</a> | ";
		}
		if ($more_recs) {
			$from = $i*$this->max;
			$target = $this->target."?skip=".$from."&max=".$this->max.$this->search_url;
			$pages .= "<a ".$class." href=\"".$target."\">M&aacute;s...</a> |";
		} // end if
		return $pages;

	}

	function next($link="",$class="") {
		if ($link=="")
			$link = "Siguientes";
		if ($class!="")
			$class = "class=\"".$class."\"";
		if ($this->endnum != $this->nfound) {
			$max = $this->max;
			$skip = $this->endnum;
			$next = "<a ".$class." href=\"".$this->target."?max=".$max."&skip=".$skip.$this->search_url."\">";
			$next .= $link."</a>";
			$this->next = $next;
		} else {
			$this->next = "";
		}
		return $this->next;
	}

	function prev($link="",$class="") {
		if ($link=="")
			$link = "Anteriores";
		if ($class!="")
			$class = "class=\"".$class."\"";
		if ($this->begnum != 1) {
			$max = $this->max;
			$skip = $this->begnum - $this->max - 1;
			$prev = "<a ".$class." href=\"".$this->target."?max=".$max."&skip=".$skip.$this->search_url."\">";
			$prev .= $link."</a>";
			$this->prev = $prev;
		} else {
			$this->prev = "";
		}
		return $this->prev;
	}
	
	
	function rewind() {
				$this->index = $this->sql_data_seek($this->skip);
	}

}	// end class definition

?>