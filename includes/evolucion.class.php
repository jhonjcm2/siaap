<?php
/**********************************************************/
// CLASE: evoluci�n
// Proposito: Realizar la evoluci�n del paciente.
// Ultima modificacion: octubre de 2003
// /**********************************************************/
class evolucion{
  var $fv;
  var $data;
	
  function evolucion($id=-1){
    global $table;
    
    $this->fv = new FormValidator;
    if ($id >= 0){
    	$query = "SELECT * 
        	     FROM evolucion
        	     WHERE evo_id='$id' ";
	$table->sql_query($query);
    	$this->data = $table->sql_fetch_object();
    }
    
  }

  /**********************************************************/
  // Proposito: Creacion de la evolucion odontologica
  // return: arreglos con resultados de la creacion.
  /**********************************************************/
  function crear(){
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;
      
      
      $this->fv->resetErrorList();
      $this->fv->isWithinRange("tco_id", "Seleccione el tipo de Consulta",890203,890704);
      $this->fv->isWithinRange("cac_id", "Seleccione la Causa de la Consulta",0,99);
      $this->fv->isWithinRange("amp_id", "Seleccione el &aacute;mbito del Procedimiento",0,99);
      $this->fv->isWithinRange("fip_id", "Seleccione la Finalidad del Procedimiento",0,99); 
      $this->fv->isWithinRange("faq_id", "Seleccione la forma del Acto Quir&uacute;rgico relacionada",0,99);
      $this->fv->isEmpty("pto_id", "Seleccione un procedimiento para agregar");
      $this->fv->isNumber("pxe_cantidadProcedimientos","Ingrese un n&uacute;mero de procedimientos v&aacute;lido",1,1000000);
             
   if ( $this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    }else {
      $query = 'SELECT * 
                FROM evolucion
                WHERE "evo_id"='.$evo_id; 
      $table->search($query);
    
      if($table->nfound) {
	$mess = "Una evolucion con ese documento ya se encuentra registrado en el sistema, por favor escoja otro.";
	$mess_cod = "alert";
	$s_opc = "adicionar_usuario";
      }
      else {
	/*  se pasa a la quota a bytes */
	 $evo_fechaElaboracion="$fn_anio-$fn_mes-$fn_dia";
         $fields = array ("evo_id", "pac_id", "tco_id", "cac_id", "amp_id", "fip_id", "faq_id", "evo_fecha", "evo_comentario", "evo_autorizacion", "evo_numeroAutorizacion", "usu_id", "evo_fechaElaboracion");
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "evolucion" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	
      }
	$query .=")";
	$result=$table->sql_query($query);

		if (!$result){
	$mess = "<b>No se pudo adicionar La evolucion por un fallo en el sistema, Comuniquese con el administrador del sistema.".$query;
	$mess_cod = "alert";		
	}else{
	    		
		logs::crear("evolucion", "crear", $query);
	    $mess = "La evolucion $login fu&eacute; adicionada con &eacute;xito "; 
	    $mess_cod = "info";
	       
	    
 	} 
	  
      }
    }
    //logs::crear("estado_cuenta", "crear", $query);	
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
    
  }


  /**********************************************************/
  // Proposito: Creacion de la evolucion odontologica arbitraria, con autorizacion 0000, comentario "Evolucion arbitraria"
  // return: arreglos con resultados de la creacion.
  /**********************************************************/
  function crear2($pac_id,$tco_id, $cac_id, $amp_id, $fip_id, $faq_id, $pto_id, $pxe_cantidadProcedimientos,$usu_id,$comentario=""){
        global $table, $_POST, $_GET;

      $query = 'SELECT * 
                FROM evolucion
                WHERE "evo_id"='.$evo_id;
      $table->search($query);
    
      if($table->nfound) {
	$mess = "Una evolucion con ese documento ya se encuentra registrado en el sistema, por favor escoja otro.";
	$mess_cod = "alert";
	$s_opc = "adicionar_usuario";
      }
      else {
	/*  se pasa a la quota a bytes */
	//$evo_fechaElaboracion=date("d-m-Y")."";
	$evo_fecha=date("d-m-Y")."";
	$evo_comentario="Evolucion arbitraria. ".$comentario;
	$evo_autorizacion="0000";
         $fields = array ("evo_id", "pac_id", "tco_id", "cac_id", "amp_id", "fip_id", "faq_id", "evo_fecha", "evo_comentario", "evo_autorizacion", "evo_numeroAutorizacion", "usu_id", "evo_fechaElaboracion");
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "evolucion" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	
      }
	$query .=")";
	$result=$table->sql_query($query);

		if (!$result){
	$mess = "<b>No se pudo adicionar La evolucion por un fallo en el sistema, Comuniquese con el administrador del sistema.".$query;
	$mess_cod = "alert";		
	}else{
	    		
		logs::crear("evolucion", "crear", $query);
	    $mess = "La evolucion $login fu&eacute; adicionada con &eacute;xito "; 
	    $mess_cod = "info";
	       
	    
 	} 
	  
      }
    /*}*/
    //logs::crear("estado_cuenta", "crear", $query);	
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
    
  }

/********************* Buscar una Evolucion *****************/

function buscar(){
    global $table, $_POST ,$_GET;

    foreach($_POST as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($_GET as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);



             $fields = array ("evo_id", "pac_id", "tco_id", "cac_id", "amp_id", "fip_id", "faq_id", "evo_fecha", "evo_comentario", "evo_autorizacion", "evo_numeroAutorizacion", "usu_id");
							      
       // Arreglos para datos del query y el url para paginacion

      $where_array = array();
     $query = 'SELECT * FROM  "evolucion" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($where_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($where_array))
	$query .= " WHERE ".implode(", ",$where_array);
	
      	$result=$table->search($query);
	
	if (!$result){
		$mess = "No hay registros que concuerden con su busqueda.".$query;
		$mess_cod = "alert";		
	}else{
	       $mess = "la evoluci&oacute;n $login fu&eacute; encontrada con &eacute;xito ".$query; 
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }

/***************************************************************
Proposito: Agrega un procedimiento a una evolucion, contiene el valor en ese momento del procedimiento
return: arreglo con el resultado de la creacion
/***************************************************************/
function agregarProc($evo_id=0){

	global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;
      
      $this->fv->resetErrorList();
      

   if ( $this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
      }
      else {
        /*  se pasa a la quota a bytes */
         $fields = array ("pxe_id", "pto_id", "cod_id", "evo_id", "pxe_cantidadProcedimientos", "pxe_diente", "pxe_incisal", "pxe_oclusal", "pxe_mesial", "pxe_vestibular", "pxe_distal", "pxe_palatino", "pxe_lingual","pxe_valorUnitario");

	/////////////////////////////////////////////////////////////////////////
		//Mirar que tipo de odontologo es
		//Hacer consulta de los odontologos
			$query = "SELECT * FROM odontologos WHERE usu_id=$usu_id";
			$table->search($query);
		//Guardamos el odontologo y el tipo
			$miodontologo = $table->sql_fetch_object();
			$tipoOdontologo = $miodontologo->odl_tipoOdontologo;
		//Dependiendo del tipo de odontologo le escojo un valor
			$query="SELECT * FROM procedimiento WHERE pto_id=$pto_id";
			$table->search($query);
			$registroprocedimiento=$table->sql_fetch_object();;
			if($tipoOdontologo==1){
				$pxe_valorUnitario=$registroprocedimiento->pto_valorEstPre;
			}else if($tipoOdontologo==2){
				$pxe_valorUnitario=$registroprocedimiento->pto_valorEstPost;
			}else if($tipoOdontologo==3){
				$pxe_valorUnitario=$registroprocedimiento->pto_valorAsit;
			}else if($tipoOdontologo==4){
				$pxe_valorUnitario=$registroprocedimiento->pto_valorEsp;
			}else if($tipoOdontologo==5){
				$pxe_valorUnitario=$registroprocedimiento->pto_valorHigOral;
			}/**/
	/////////////////////////////////////////////////////////////////////////
       // Arreglos para datos del query y el url para paginacion
      	$fields_array = array();
      	$values_array = array();
     	$query = 'INSERT INTO "procxevo" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
        if(${$v}!=""){
          array_push($fields_array,' "'.$v.'" ');
          array_push($values_array," '".${$v}."' ");
        }

      if(sizeof($fields_array)){
        $query .= implode(", ",$fields_array);
        $query .= ") VALUES (".implode(", ",$values_array) ;
        }
        $query .=")";

        $result=$table->sql_query($query);

        if (!$result){
        $mess = "No se pudo adicionar el procedimiento por un fallo en el sistema, por Favor contacte al Administrador del Sistema.";
        $mess_cod = "alert";
        }else{
            logs::crear("evolucion", "agregarProc", $query);
            $mess = "El procedimiento fu&eacute; adicionado con &eacute;xito ";
            $s_opc = "info_usuario";
         }

      }
    //$mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }

/************** 
Proposito: Borrar una evolucion antes de ser autorizada
****************/
 
  function borrar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    
     	$fields = array ("tco_id", "cac_id", "amp_id", "fip_id", "faq_id", "evo_fecha", 
     				"evo_comentario", "evo_autorizacion", "evo_numeroAutorizacion", "usu_id");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'DELETE FROM "evolucion" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE evo_id =$evo_id";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo cancelar La evoluci&oacute;n por un fallo en el sistema, Comun&iacute;quese con el administrador.".$query;
		$mess_cod = "alert";		
	}else{
	       logs::crear("evolucion", "borrar", $query);
	 	$mess = "La evoluci&oacute;n $login fue cancelada con &eacute;xito "; 
	    	$mess_cod = "info";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }

/***********
borrar de la tabla procxevo una evoluci&oacute;n
****************/
function borrarProc($com_id=0){


global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;


//   if ( 0 /*$this->fv->isError()*/ ) {
if ( 0 ){
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
      }
      else {
        /*  se pasa a la quota a bytes */
         $fields = array ("pxe_id", "pto_id", "evo_id", "pxe_cantidadProcedimientos", "pxe_diente", 
         				"pxe_incisal", "pxe_oclusal", "pxe_mesial", "pxe_vestibular", "pxe_distal", 
         				"pxe_palatino", "pxe_lingual");

       // Arreglos para datos del query y el url para paginacion
      	$fields_array = array();
      	$values_array = array();
     	$query = 'DELETE FROM "procxevo" ';
     	// Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE evo_id = '$evo_id'";

	$result=$table->sql_query($query);
    
        if (!$result){
        $mess = "No se pudo cancelar la evoluci&oacute;n por un fallo en el sistema, Contacte al Administrador del Sistema.";
        $mess_cod = "alert";
        }else{
              logs::crear("evolucion", "borrarProc", $query);
            	$mess = "<center><b>La evoluci&oacute;n $login fu&eacute; cancelada</b><br>
            	Vuelva a ingresarla</center>";
	 	$mess_cod = "info";
         }

      }
    //$mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }


}
?>
