<?
/**********************************************************/
// CLASE: usuario
// Proposito: Realizar tareas de manejo de usuarios.
// Ultima modificacion: Octubre 2003
// /**********************************************************/
class usuario{
  var $fv;
  var $data;	

  function usuario($id=-1){
    global $table;
    
    $this->fv = new FormValidator;
    if ($id >= 0){
    	$query = "SELECT * 
        	     FROM usuario 
        	     WHERE usu_id='$id' ";
	$table->sql_query($query);
    	$this->data = $table->sql_fetch_object();
    }
    
  }

  /**********************************************************/
  // Proposito: Creacion unica de un usuarios
  // return: arreglos con resultados de la creacion.
  /**********************************************************/
  function crear(){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;

    foreach($_GET as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;
      
       $this->fv->resetErrorList();	 
       $this->fv->isEqualsString("usu_password","usu_cPassword","La clave y la confirmaci&oacute;n no coinciden.");
       $this->fv->isWithinRange("tiu_id","Seleccione el tipo de Usuario",0,99);
       $this->fv->isEmpty("usu_nombres", "Debe insertar los nombres del usuario");
       $this->fv->isEmpty("usu_apellidos", "Debe insertar los apellidos del usuario");
       $this->fv->isWithinRange("tid_id","Seleccione el tipo de Documento",0,99);
       $this->fv->isEmpty("usu_telefono", "Debe insertar el numero telef&oacute;nico del usuario");
       $this->fv->isEmpty("usu_numeroIdentificacion", "Debe insertar el numero de Identificaci&oacute;n del usuario");
       //$this->fv->isWithinRange("odl_tipoOdontologo","Seleccione el tipo de Odont&oacute;logo",0,99);
       //$this->fv->isNumber("usu_numeroIdentificacion", "Debe insertar solo n&uacute;meros");
     
      if ($tiu_id==2 || ($tiu_id==3 && $odl_tipoOdontologo > 2 && $odl_tipoOdontologo < 5 )){// Docente u odontologo
      		$this->fv->isEmpty("pro_codigoProfesional", "Debe insertar el codigo del profesional.");      		
      		$this->fv->isEmpty("pro_nombreEspecialidad", "Debe insertar la especialidad");
      		$mess = "Docente";
        	$mess_cod = "alert";
        	
      	}elseif ($tiu_id==3 && $odl_tipoOdontologo<=2) {
      		$this->fv->isEmpty("ete_codigo", "Debe insertar el c&oacute;digo del estudiante");
      		$this->fv->isNumber("ete_semestre", "Debe insertar el n�mero que equivale al semestre del estudiante");
      		$this->fv->isEmpty("ete_periodoAcademico", "Debe agregar el per&iacute;odo acad&eacute;mico");
      		$mess = "Est";
        	$mess_cod = "alert";	
      	       }
      	
     if ($this->fv->isError() ) {
        $mess = $this->fv->getMessage();
        $mess_cod = "alert";
            //include ("modules/usuario/default.php");
        } 
    else {
      $query = 'SELECT * 
                FROM usuario 
                WHERE "usu_numeroIdentificacion"='.$usu_numeroIdentificacion;
      $table->search($query);
    
      if($table->nfound) {
	   $mess = "<center><b>Un usuario con ese documento ya se encuentra registrado en el sistema.</b><br> por favor revise la identificaci&oacute;n.</center>";
	   $mess_cod = "alert";
	   $s_opc = "adicionar_usuario";
	   }
	
	else {
      $query = 'SELECT * 
                FROM usuario 
                WHERE "usu_login"='."'$usu_login'";
      $table->search($query);
    
      if($table->nfound) {
	    $mess = "<center><b>Un usuario con ese login ya se encuentra registrado en el sistema, </b><br>por favor cambie el login.</center>";
	    $mess_cod = "alert";
	    $s_opc = "adicionar_usuario";
	    }
	
	
      else {
	/*  se pasa a la quota a bytes */
	
	$fields = array ("usu_id", "tid_id", "tiu_id", "bod_id", "usu_estadoUsuario", "usu_nombres", "usu_apellidos", "usu_numeroIdentificacion",
	                       "usu_lugarExpedicion", "usu_direccion", "usu_telefono", "usu_login", "usu_password", "usu_fechaIngreso", "usu_personaElabora");
	
     $usu_password = md5($usu_password);    
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array( );
      $values_array = array( );
     $query = 'INSERT INTO "usuario" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	   array_push($fields_array,' "'.$v.'" ');
	   array_push($values_array," '".${$v}."' ");
	   }

      if(sizeof($fields_array)){
	   $query .= implode(", ",$fields_array);
	   $query .= ") VALUES (".implode(", ",$values_array) ;
	   }
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	   $mess = "<center><b>No se pudo adicionar el usuario por un fallo en el sistema</b></center>".$query;
	   $mess_cod = "alert";
	   $s_opc="default";
	   }
	else{
	   if ($tiu_id==2){
	   	    $query="select last_value from usuario_usu_id_seq";
	   	    $table->sql_query($query);
	   	    $data = $table->sql_fetch_object();
	   	    $usu_id=$data->last_value;
	   	    $doc_codigoProfesional=$pro_codigoProfesional;
	   	    $doc_especialidad=$pro_nombreEspecialidad;
		    $fields = array ("usu_id", "doc_codigoProfesional", "doc_especialidad");
			
		       // Arreglos para datos del query y el url para paginacion
		      $fields_array = array( );
		      $values_array = array( );
		     $query = 'INSERT INTO "docente" (';
		      // Adicionando los campos para el query
		      foreach($fields as $v)
			if(${$v}!=""){
			   array_push($fields_array,' "'.$v.'" ');
			   array_push($values_array," '".${$v}."' ");
			   }
		
		      if(sizeof($fields_array)){
			   $query .= implode(", ",$fields_array);
			   $query .= ") VALUES (".implode(", ",$values_array) ;
			   }
			$query .=")";
			
			$result=$table->sql_query($query);
		
		}elseif($tiu_id==3 && $odl_tipoOdontologo==3){
		    $query="select last_value from usuario_usu_id_seq";
	   	    $table->sql_query($query);
	   	    $data = $table->sql_fetch_object();
	   	    $usu_id=$data->last_value;
	   	    
		   $query="INSERT INTO odontologos ( \"usu_id\", \"odl_tipoOdontologo\", ara_id) VALUES('$usu_id', 3, '$ara_id')";
		    $table->sql_query($query);
		     //echo $query;
		    $query="select last_value from odontologos_odl_id_seq";
	   	    $table->sql_query($query);
	   	    $data = $table->sql_fetch_object();
	   	    $odl_id=$data->last_value;
	   	    //echo $query;
		    $query="INSERT INTO profesional (odl_id, \"pro_codigoProfesional\", \"pro_nombreEspecialidad\" ) VALUES ($odl_id, '$pro_codigoProfesional', '$pro_nombreEspecialidad')";
		    $table->sql_query($query);
		    
		}elseif($tiu_id==3 && $odl_tipoOdontologo==4){
		    $query="select last_value from usuario_usu_id_seq";
	   	    $table->sql_query($query);
	   	    $data = $table->sql_fetch_object();
	   	    $usu_id=$data->last_value;
	   	    
		    $query="INSERT INTO odontologos ( \"usu_id\", \"odl_tipoOdontologo\", ara_id) VALUES('$usu_id', 4, '$ara_id')";
		    $table->sql_query($query);
		    
		    $query="select last_value from odontologos_odl_id_seq";
	   	    $table->sql_query($query);
	   	    $data = $table->sql_fetch_object();
	   	    $odl_id=$data->last_value;
		    $query="INSERT INTO profesional (odl_id, \"pro_codigoProfesional\", \"pro_nombreEspecialidad\" ) VALUES ($odl_id, '$pro_codigoProfesional', '$pro_nombreEspecialidad')";
		    $table->sql_query($query);
		    
		}elseif($tiu_id==3 && $odl_tipoOdontologo==1){
		
		    $query="select last_value from usuario_usu_id_seq";
	   	    $table->sql_query($query);
	   	    $data = $table->sql_fetch_object();
	   	    $usu_id=$data->last_value;
	   	    
		    $query="INSERT INTO odontologos ( \"usu_id\", \"odl_tipoOdontologo\") VALUES('$usu_id', 1)";
		    $table->sql_query($query);
		    $query="select last_value from odontologos_odl_id_seq";
	   	    $table->sql_query($query);
	   	    $data = $table->sql_fetch_object();
	   	    $odl_id=$data->last_value;
		    
		    $query="INSERT INTO estudiante (odl_id, \"ete_semestre\", \"ete_codigo\", \"ete_periodoAcademico\" ) VALUES  ($odl_id, '$ete_semestre', '$ete_codigo', '$ete_periodoAcademico') ";
		    $table->sql_query($query);
		  
		  }elseif($tiu_id==3 && $odl_tipoOdontologo==2){
		
		    $query="select last_value from usuario_usu_id_seq";
	   	    $table->sql_query($query);
	   	    $data = $table->sql_fetch_object();
	   	    $usu_id=$data->last_value;
	   	    
		    $query="INSERT INTO odontologos ( \"usu_id\", \"odl_tipoOdontologo\", ara_id) VALUES('$usu_id', 2, '$ara_id')";
		    $table->sql_query($query);
		    $query="select last_value from odontologos_odl_id_seq";
	   	    $table->sql_query($query);
	   	    $data = $table->sql_fetch_object();
	   	    $odl_id=$data->last_value;
		    
		    $query="INSERT INTO estudiante (odl_id, \"ete_semestre\", \"ete_codigo\", \"ete_periodoAcademico\" ) VALUES  ($odl_id, '$ete_semestre', '$ete_codigo', '$ete_periodoAcademico') ";
		    $table->sql_query($query); 
		    
		    }elseif($tiu_id==3 && $odl_tipoOdontologo==5){
		
		    $query="select last_value from usuario_usu_id_seq";
	   	    $table->sql_query($query);
	   	    $data = $table->sql_fetch_object();
	   	    $usu_id=$data->last_value;
	   	    
		    $query="INSERT INTO odontologos ( \"usu_id\", \"odl_tipoOdontologo\") VALUES('$usu_id', 5)";
		    $table->sql_query($query);
		    
		    $query="select last_value from odontologos_odl_id_seq";
	   	    $table->sql_query($query);
	   	    $data = $table->sql_fetch_object();
	   	    $odl_id=$data->last_value;
		     $mess_cod = "$old_id";
		    $query="INSERT INTO estudiante (odl_id, \"ete_semestre\", \"ete_codigo\", \"ete_periodoAcademico\" ) VALUES  ($odl_id, '$ete_semestre', '$ete_codigo', '$ete_periodoAcademico') ";
		    $table->sql_query($query); 
		}
	    logs::crear("usuario", "crear", $query);	
	    $mess = "<center><b>El usuario $login fu&eacute; adicionado con &eacute;xito </b><br></center>
	    Haga click <a href='$PHP_SELF?opc=usuario&s_opc=default'>&nbsp;&nbsp;<b>aqu&iacute;</b></a> para ingresar otro Usuario";
	    $mess_cod = "info";
	    $s_opc = "busqueda";
	    } 
	  
      }
    }  
   }
    //$mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login); 
 
}

/**** Validar usuarios ****/
function validar($usuario, $password){
	global $table;
	
	$encriptado = md5($password);
	
	$query="SELECT * FROM usuario WHERE usu_login='$usuario' AND  usu_password='$encriptado' AND \"usu_estadoUsuario\" = 't'";
       
       $table->search($query);	
       
       if ($table->nfound){
            $this->datos=$table->sql_fetch_object();
            return 1;
       } else{
              logs::crear("usuario", "validar", $query);
       	$mess="Usuario o Login incorrecto, por favor verifique que las mayusculas no esten activadas";
       	$mess_cod="alert";
       	return 0;
       }       
//    return array("mess"=>$mess.$query,"mess_cod"=>$mess_cod,"datos"=>$datos);
	
}


/**** Validar usuarios donde $tipo es el tipo de usuario****/
function validarUsuario($usuario, $password, $pac_id, $tipo=0, $tipo1=0){
	global $table;
	
	$encriptado = md5($password);
	
	$query="SELECT * FROM usuario WHERE usu_login='$usuario' AND  usu_password='$encriptado' AND tiu_id=$tipo";
       
       $table->search($query);	
       
       if ($table->nfound){
            $this->datos=$table->sql_fetch_object();
            $valido=1;
       } else{
              logs::crear("usuario", "validarUsuario", $query);
       	$mess="Usuario o Login incorrecto, por favor verifique que las may&uacute;sculas no est&eacute;n activadas";
       	$mess_cod="alert";
       	$valido=0;
       }       
   return array("mess"=>$mess,"mess_cod"=>$mess_cod,"valido"=>$valido, "usu_id"=>$this->datos->usu_id, "usu_nombres"=>$this->datos->usu_nombres,"usu_apellidos"=>$this->datos->usu_apellidos);
	
}


/********************* Buscar usuarios******************/

function buscar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);



             $fields = array ( "tid_id", "tiu_id", "bod_id", "usu_estadoUsuario", "usu_nombres", "usu_apellidos", "usu_numeroIdentificacion",
	                               "usu_lugarExpedicion", "usu_direccion", "usu_telefono", "usu_login");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'SELECT  * FROM  "usuario" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($where_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($where_array))
	$query .= " WHERE ".implode(", ",$where_array);
	
      	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No hay registros que concuerden con su busqueda".$query;
		$mess_cod = "alert";		
	}else{
	      	$mess = "El usuario $login fue adicionado con &eacute;xito ".$query; 
	    	$s_opc = "info_usuario";
	 } 
	return $result;
   // return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }


  /************** Modificar Usuarios ****************/
  function modificar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

     /********************/

    $fields = array ( "tid_id", "tiu_id", "bod_id", "usu_estadoUsuario", "usu_nombres", "usu_apellidos", "usu_numeroIdentificacion",
	                       "usu_lugarExpedicion", "usu_direccion", "usu_telefono", "usu_login", "usu_password");
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "usuario" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE usu_id = '$usu_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "<b>No se pudo adicionar el usuario por un fallo en el sistema</b>";
		$mess_cod = "alert";		
	}else{
	       logs::crear("usuario", "modificar", $query);
	 	$mess = "<b>Los cambios $login fueron adicionado con &eacute;xito</b> "; 
	    	$mess_cod ="info";
	    	$s_opc = "info";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }
  
  /*****************Cambio de password*********************/
  
  function cambiarclave($usu_login){
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;

    foreach($_GET as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;
      
      $this->fv->resetErrorList();	 
      $this->fv->isEqualsString("usu_password","usu_passwordC","La clave y la confirmaci&oacute;n no coinciden.");
      $this->fv->isMoreThanSize("usu_password",6,"La clave debe contener al menos 6 caracteres..");
      
      	
     if ($this->fv->isError() ) {
        $mess = $this->fv->getMessage();
        $mess_cod = "alert";
        } 
    else {
	      if(!$this->validar($usu_login,$password)) {
		    $mess = "<center><b>La Contrase&ntilde;a es inv&aacute;lida </b></center>";
		    $mess_cod = "alert";
		    }
	      else {
		
			$usu_password = md5($usu_password);    
			// Arreglos para datos del query y el url para paginacion
			$set_array = array();
			$query = "UPDATE  usuario SET usu_password='$usu_password' WHERE usu_login = '$usu_login'";
			
			$result=$table->sql_query($query);
			
			if (!$result){
			  $mess = "<b>No se pudo modificar la clave por un fallo en el sistema.<br> Por favor contacte al administrador del sistema</b>".$query;
			  $mess_cod = "alert";		
			}else{
			  logs::crear("usuario", "cambiarClave", $query);
			  $mess = "<b>La clave fu&eacute; modificada con &eacute;xito</b> "; 
			  $mess_cod ="info";
			  $s_opc = "info";
			} 
		  
			//$mess .= $query;
			//return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
	
	      }
    }
     return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }

	function estaAutorizadoFact($usu_id_autorizado){
		global $table;
		$query="SELECT * FROM autorizados_fact WHERE usu_id=".$usu_id_autorizado;
		$table->search($query);
		if($table->nfound>0)
			return true;
		else
			return false;
	}

/*Cambia la clave de otro usuario*/
  function cambiarclaveUsuario($usu_login){
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;

    foreach($_GET as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;
      
      $this->fv->resetErrorList();	 
      $this->fv->isEqualsString("usu_password","usu_passwordC","La clave y la confirmaci&oacute;n no coinciden.");
      $this->fv->isMoreThanSize("usu_password",6,"La clave debe contener al menos 6 caracteres..");
      
      	
     if ($this->fv->isError() ) {
        $mess = $this->fv->getMessage();
        $mess_cod = "alert";
        } 
    else {
		
			$usu_password = md5($usu_password);    
			// Arreglos para datos del query y el url para paginacion
			$set_array = array();
			$query = "UPDATE  usuario SET usu_password='$usu_password' WHERE usu_login = '$usu_login'";
			
			$result=$table->sql_query($query);
			
			if (!$result){
			  $mess = "<b>No se pudo modificar la clave por un fallo en el sistema.<br> Por favor contacte al administrador del sistema</b>".$query;
			  $mess_cod = "alert";		
			}else{
			  logs::crear("usuario", "cambiarClave", $query);
			  $mess = "<b>La clave fu&eacute; modificada con &eacute;xito</b> "; 
			  $mess_cod ="info";
			  $s_opc = "info";
			} 
		  
			//$mess .= $query;
			//return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
    }
     return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }
}
?>