<?
/*********************************************************************************/
// CLASE: adulto
// Proposito: Realizar tareas de manejo de datos de un paciente adulto.
// Ultima modificacion: Octubre de 2003
// /******************************************************************************/
class adulto{
  var $fv;
  var $data;

  function adulto($pac_id=0){
    global $table;
    
    $this->fv = new FormValidator;
    if ($pac_id){
	    $query = "SELECT * 
	              FROM datos_personales_adulto 
	              WHERE pac_id='$pac_id' ";
	    $table->sql_query($query);
	    $this->data = $table->sql_fetch_object();
	  }
    
  }
  
  /**********************************************************/
  // Proposito: Creacion unica de un paciente adulto
  // return: arreglos con resultados de la creacion.
  /**********************************************************/
function crear(){	
global $table, $_POST, $_GET;

	foreach($_POST as $k=>$v)
      	${$k} = base::dispelMagicQuotes($v);
      
    	foreach($_GET as $k=>$v)
	      ${$k} = base::dispelMagicQuotes($v);
    
    	$this->fv->resetErrorList();	 
    	if($est_id==2 || $est_id==5){
    		$this->fv->isEmpty("dpa_nombreConyuge","Ingrese el nombre del conyuge");
    	}
    	$this->fv->isEmpty("dpa_ocupacionInstitucion","Falta el campo ocupaci&oacute;n");
     
    	if ($this->fv->isError() ) {
      		$mess = $this->fv->getMessage();
      		$mess_cod = "alert";
    	}else {
    		$query = 'SELECT * FROM datos_personales_adulto WHERE "pac_id"='.$pac_id;
      		$table->search($query);
    		if($table->nfound) {
			$mess = "<b>Los datos ya fueron ingresados.</b>";
			$mess_cod = "error";
			$s_opc = "adicionar_usuario";
      		}
		else{
    
 			/*  se pasa a la quota a bytes */
			$fields = array ("dpa_id", "pac_id", "est_id", "dpa_ocupacionInstitucion", "dpa_nombreInstitucion", 
						 "dpa_direccionInstitucion", "dpa_telefonoInstitucion", "dpa_nombreConyuge", 
						 "dpa_fechaElaboracion", "dpa_dirConyuge", "dpa_ocupConyuge", "dpa_telConyuge");
	
       		 // Arreglos para datos del query y el url para paginacion
      			$fields_array = array();
      			$values_array = array();
     			$query = 'INSERT INTO "datos_personales_adulto" (';
      			// Adicionando los campos para el query
      			foreach($fields as $v)
			if(${$v}!=""){
	  			array_push($fields_array,' "'.$v.'" ');
	  			array_push($values_array," '".${$v}."' ");
			}

      			if(sizeof($fields_array)){
				$query .= implode(", ",$fields_array);
				$query .= ") VALUES (".implode(", ",$values_array) ;
			}
			$query .=")";
			$result=$table->sql_query($query);
	
			if (!$result){
				$mess = "<b>No se pudo adicionar el paciente por un fallo en el sistema, por favor llame al administrador.</b>";
				$mess_cod = "alert";		
			}else{
			    //   logs::crear("adulto", "crear", $query);
	    			$mess = "<center><b>Los datos Personales del paciente$login fueron adicionados con &eacute;xito. <br><br>
	    			Por favor continue ingresando los datos de la Anamnesis </center></b>"; 
	    			$mess_cod = "info";
	    		} 
		} 
	}
	return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
}

/********************* Buscar un adulto******************/

function buscar(){
global $table;

    	foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      	${$k} = base::dispelMagicQuotes($v);

    	foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      	${$k} = base::dispelMagicQuotes($v);

	$fields = array ( "dpa_id", "pac_id", "est_id", "dpa_ocupacionInstitucion", "dpa_nombreInstitucion", "dpa_direccionInstitucion", 
				"dpa_telefonoInstitucion", "dpa_nombreConyuge", "dpa_estado");
      
       // Arreglos para datos del query y el url para paginacion

      	$set_array = array();
     	$query = 'UPDATE  "datos_personales_adulto" ';
      	// Adicionando los campos para el query
      	foreach($fields as $v)
	if(${$v}!=""){
	  	array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
		$query .= " SET ".implode(", ",$set_array);
	 	$query .=  " WHERE dpa_id = '$dpa_id'";
		$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "<b>No se pudo buscar el paciente por un fallo en el sistema, por favor llame al administrador.</b>".$query;
		$mess_cod = "alert";		
	}else{
		$mess = "<b>El usuario fu&eacute; buscado con &eacute;xito </b>".$query; 
	    	$s_opc = "info_usuario";
	 } 
 	return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
}

  /************** Modificar adulto****************/
  function modificar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);


           $fields = array ( "est_id", "dpa_ocupacionInstitucion", "dpa_nombreInstitucion", 
           				"dpa_direccionInstitucion", "dpa_telefonoInstitucion", "dpa_nombreConyuge",  "dpa_estado");

       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "datos_personales_adulto" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE dpa_id = '$dpa_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo modificar el paciente por un fallo en el sistema, por favor llame al administrador del sistema.".$query;
		$mess_cod = "alert";		
	}else{
		logs::crear("adulto", "modificar", $query);
	 	$mess = "<center><b>Los cambios $login fueron  adicionados con &eacute;xito</b><br></center>";
	 	$mess_cod = "info";
	    	
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }
  
/************Seguir Creando el Adulto**********/    

 function actualizar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      
      
    $this->fv->resetErrorList();	
    if($dpa_estado==2){
       $this->fv->isEmpty("dpa_comentariosTrauma","Ingrese las observaciones correspondientes al trauma");
       $this->fv->isEmpty("dpa_comentariosRadiacion","Ingrese las observaciones correspondientes a la radiaci&oacute;n");
       $this->fv->isEmpty("dpa_comentariosPatologicos","Ingrese las observaciones correspondientes a los antecedentes patol&oacute;gicos");
       $this->fv->isEmpty("dpa_comentariosProblemas","Ingrese las observaciones correspondientes a los problemas articulares");
    }
    
     
    if ($this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    } else {     
      
      
		$fields = array ("dpa_antSocMotivacionInterna", "dpa_espectativasTratamiento", "dpa_entornoSocioeconomico",
  			     "dpa_hobbies", "dpa_antMedPatologicosEruptivas", "dpa_gastrointestinales", "dpa_cardiovasculares",
  			     "dpa_pulmonares", "dpa_genitourinarios", "dpa_inmunologicos", "dpa_endocrinos", "dpa_neurologicos", 
  			     "dpa_siquiatricos", "dpa_neoplasicos", "dpa_venereos", "dpa_oseos", "dpa_comentariosPatologicos", "dpa_hospitalarios",
  			     "dpa_quirurgicos", "dpa_traumaticos", "dpa_toxicoalergicos", "dpa_farmacos", 
  			     "dpa_radiacion",  "dpa_hemorragicos", "dpa_ginecobstetricos", "dpa_problemasArticulares", "dpa_comentariosOtrasEnfAdu",
  			     "dpa_antOdonPrevencion", "dpa_operatoria", "dpa_endodoncia", "dpa_periodoncia", "dpa_protesis", 
  			     "dpa_ortodoncia", "dpa_cirugiaOral", "dpa_ortopedia",  "dpa_comentariosAntOdo", "dpa_estado");
     

       
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "datos_personales_adulto" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE dpa_id = '$dpa_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "<b>No se pudo adicionar el paciente por un fallo en el sistema, por Favor comuniquese con el administrador</b>";
		$mess_cod = "alert";		
	}else{
		logs::crear("adulto", "actualizar", $query);
	 	$mess = "<center><b>Los Antecedentes Generales del Paciente $login fueron adicionados con &eacute;xito</b><br><br>"; 
	    	$mess_cod = "info";
	    	//$s_opc = "busqueda";
	 } 
    }	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }
 } 
 
  /*********************Seguir Buscar un adulto******************/

function buscar1(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

	$fields = array ( "dpa_id", "pac_id", "est_id", "dpa_ocupacionInstitucion", "dpa_nombreInstitucion", "dpa_direccionInstitucion", 
											"dpa_telefonoInstitucion", "dpa_nombreConyuge");
      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "datos_personales_adulto" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE dpa_id = '$dpa_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo buscar el paciente por un fallo en el sistema, por favor comun&iacute;quese con el administrador".$query;
		$mess_cod = "alert";		
	}else{
		$mess = "El usuario $login fue adicionado con &eacute;xito ".$query; 
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }

  
  /************** Seguir Modificar Adulto****************/
  
  function modificar1(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);


          $fields = array ("dpa_antSocMotivacionInterna", "dpa_espectativasTratamiento", "dpa_entornoSocioeconomico",
  			     "dpa_hobbies", "dpa_antMedPatologicosEruptivas", "dpa_gatrointestinales", "dpa_cardiovasculares",
  			     "dpa_pulmonares", "dpa_genitourinarios", "dpa_inmunologicos", "dpa_endocrinos", "dpa_neurologicos", 
  			     "dpa_siquiatricos", "dpa_neoplasicos", "dpa_venereos", "dpa_comentariosPatologicos", "dpa_hospitalarios",
  			     "dpa_quirurgicos", "dpa_traumaticos", "dpa_comentariosTrauma", "dpa_toxicoalergicos", "dpa_farmacos", 
  			     "dpa_radiacion", "dpa_comentariosRadiacion", "dpa_hemorragicos", "dpa_ginecobstetricos", 
  			     "dpa_antOdonPrevencion", "dpa_operatoria", "dpa_endodoncia", "dpa_periodoncia", "dpa_protesis", 
  			     "dpa_ortodoncia", "dpa_cirugiaOral", "dpa_ortopedia", "dpa_problemasArticulares", "dpa_comentariosProblemas");

       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "datos_personales_adulto" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE dpa_id = '$dpa_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo modificar el paciente por un fallo en el sistema, por favor contacte a el administrador.";
		$mess_cod = "alert";		
	}else{
		logs::crear("adulto", "modificar1", $query);
	 	$mess = "El usuario $login fu&eacute; modificado con &eacute;xito ".$query; 
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }

?>