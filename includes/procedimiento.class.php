<?
/*********************************************************************************************************************/
// CLASE: procedimiento
// Proposito: Realizar el mantenimiento de la informacion referente a los procedimientos odontologicos
			//que maneja la escuela.
// Ultima modificacion: Octubre de 2003
// /******************************************************************************************************************/
class procedimiento{
  var $fv;
  var $data;
	
  function procedimiento($id=-1){
    global $table;
    
    $this->fv = new FormValidator;
    if ($id >= 0){
    	$query = "SELECT * 
        	     FROM procedimiento 
        	     WHERE pto_id='$id' ";
	$table->sql_query($query);
    	$this->data = $table->sql_fetch_object();
    }
    
  }

  /**********************************************************/
  // Proposito: ingresar un procedimiento a la historia
  // return: arreglos con resultados de la creacion.
  /**********************************************************/
  function crear(){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;
 
 
   $this->fv->resetErrorList();
   $this->fv->isEmpty("pto_codigoProcedimiento", "Debe insertar el c&oacute;digo del procedimiento");
   $this->fv->isEmpty("pto_nombreProcedimiento", "Debe insertar el nombre del procedimiento");
  // $this->fv->isEmpty("pto_valorProcedimiento", "Debe insertar el valor del procedimiento");
 //  $this->fv->isNumber("pto_codigoProcedimiento", "Debe insertar solo n&uacute;meros en el c&oacute;digo del procedimiento");
   //$this->fv->isNumber("pto_valorProcedimiento", "Debe insertar un valor mayor o igual a CERO");
 
   if ($this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    }else {
      $query = 'SELECT * 
                FROM procedimiento
                WHERE "pto_codigoProcedimiento"='.$pto_codigoProcedimiento;
      $table->search($query);
    
      if($table->nfound) {
	$mess = "<center><b>Un procedimiento con ese c&oacute;digo ya se encuentra registrado en el sistema</b><br> por favor revise c&oacute;digo.</center>";
	$mess_cod = "alert";
	}
      else {
	/*  se pasa a la quota a bytes */
         $fields = array ("pto_id", "pto_codigoProcedimiento", "pto_areaProcedimiento", "pto_nombreProcedimiento", "pto_valorEstPre", "pto_valorEstPost", "pto_valorAsit", "pto_valorEsp");
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "procedimiento" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "<b>No se pudo adicionar el procedimiento por un fallo en el sistema. Comuniquese con el administrador</b>";
	$mess_cod = "alert";		
	}else{
	    logs::crear("procedimiento", "crear", $query);
	    $mess = "<b>El procedimiento $login fu&eacute; adicionado con &eacute;xito.</b> "; 
	    $mess_cod = "info";
	    $s_opc = "info_usuario";
	 } 
	  
      }
    }
   // $mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }


/********************* Buscar un procedimiento******************/

function buscar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);



             $fields = array ("pto_codigoProcedimiento", "pto_areaProcedimiento", "pto_nombreProcedimiento", "pto_valorEstPre", "pto_valorEstPost", "pto_valorAsit", "pto_valorEsp");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'SELECT * FROM  "procedimiento" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($where_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($where_array))
	$query .= " WHERE ".implode(", ",$set_array);
	
      	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "<b>No hay registros que concuerden con su busqueda</b>";
		$mess_cod = "alert";		
	}else{
	       $mess = "El procedimiento $login fue adicionado con &eacute;xito ".$query; 
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess.$query,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }



  /************** Modificar procedimiento ****************/
  function modificar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

   
    $fields = array ("pto_codigoProcedimiento", "pto_areaProcedimiento", "pto_nombreProcedimiento", "pto_valorEstPre", "pto_valorEstPost", "pto_valorAsit", "pto_valorEsp");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "procedimiento" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE pto_id = '$pto_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "<b>No se pudo modificar el procedimiento por un fallo en el sistema. Comuniquese con el administrador";
		$mess_cod = "alert";		
	}else{
	       logs::crear("procedimiento", "modificar", $query); 
	 	$mess = "<b>El procedimiento fue modificado con &eacute;xito.</b> "; 
		$mess_cod="info";
		
	?>	<p align="center"><a href="index.php?opc=procedimiento&s_opc=busqueda"> <b> Consultar otro Procedimiento </b> </a></p> 
	<?php
     } 
	 
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
	
  }

}
?>
