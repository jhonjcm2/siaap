<?

/* Base class librarie */

class base {
// properties
        var $object_type;
        var $debug = "";

// method definitions

// class constructor
        function base($type="base") {
                $this->object_type = $type;
                return;
        }

        function debug($mode = FALSE) {
                $this->debug = $mode;
        }

    /**
     * If magic_quotes_gpc is in use, run stripslashes() on $var.
     *
     * @access public
     *
     * @param  string $var The string to un-quote, if necessary.
     * @return string      $var, minus any magic quotes.
     */
    function dispelMagicQuotes(&$var)
    {
    	static $magic_quotes;
        
        if (!isset($magic_quotes)) {
            $magic_quotes = get_magic_quotes_gpc();
        }

        if ($magic_quotes) {
            if (!is_array($var)) {
                $var = stripslashes($var);
            } /*else {
            	
                array_walk($var, array('Horde', 'dispelMagicQuotes'));
            }*/
        }

        return $var;
    }
}

?>