<?
/*****************************************************************************************/
// CLASE: antecedente
// Proposito: Realizar el manejo de los datos de los antecedentes  medicos 
// de la historia cl�nica de un  paciente
// Ultima modificacion: octubre de 2003
// /**************************************************************************************/
class antecedente{
  var $fv;
  var $data;
	
  function antecedente($pac_id=0){
  global $table;
    
    $this->fv = new FormValidator;
    if ($pac_id){
    	$query = "SELECT * 
        	     FROM antecedente
        	     WHERE ant_id='$ant_id' ";
	$table->sql_query($query);
    	$this->data = $table->sql_fetch_object();
    }
    
  }

  /*************************************************************************/
  // Proposito: Almacenar los antecedentes medicos de la historia de un paciente
  // return: arreglos con resultados de la creacion.
  /*************************************************************************/
  function crear(){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;

    foreach($_GET as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;
      
//   if ( 0 /*$this->fv->isError()*/ ) {
if ( 0 ){
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    }else {
          
	/*  se pasa a la quota a bytes */
	 
         $fields = array ("ant_id", "pac_id", "ant_fechaIngresoAnt", "ant_psicologicos", 
         			   "ant_expectativasTratamiento", "ant_entornoSocioeconomico",
         			   "ant_hobbies");
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "antecedente" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "No se pudo adicionar los antecedentes m&eacute;dicos, por Favor contacte a el Administrador.".$query;
	$mess_cod = "alert";		
	}else{
	  //logs::crear("apertura", "crear", $query);	
	    $mess = "<center><b>Los datos de los Ant. Sociales fueron adiccionados &eacute;xito</b><br><br>
	    Por favor continue ingresando los Ant M&eacute;dicos Personales "; 
	    $mess_cod = "info";
	    $s_opc = "busqueda";
	 } 
	  
  
     }
 
   // $mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }

  /********************* Buscar una apertura******************/

function buscar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);


             $fields = array ();
							      
       // Arreglos para datos del query y el url para paginacion

      $where_array = array();
     $query = 'SELECT * FROM  "antecedente" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($where_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($where_array))
	$query .= " WHERE ".implode(", ",$where_array);
	
      	$result=$table->search($query);
	
	if (!$result){
		$mess = "No hay registros que concuerden con su b&uacute;squeda.".$query;
		$mess_cod = "alert";		
	}else{
	      	$mess = "Consulta  &eacute;xitosa ".$query.$table->nfound; 
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }


  /************** Actualizar antecedentes ****************/
  function actualizar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

      	$ant_antMedPostnatalFechaUltEx="$fex_anio-$fex_mes-$fex_dia";
	$ant_antOdonFechaVisitaOdont="$fvo_anio-$fvo_mes-$fvo_dia";
    $fields = array ("par_id", "ant_fechaIngresoAnt1","ant_fechaIngresoAnt4", "ant_fechaIngresoAnt5",
    			      "ant_antMedPrenatalEnferEmbarazo", "ant_medicamentosTomadosEmb", "ant_antMedNatalEmplearonForceps", 
	                   "ant_ninoCianotico", "ant_antMedPostnatalFechaUltEx", "ant_motivoUltimoExamen", "ant_tratamientoMedicoActual", "ant_medicTomActual", 
	                   "ant_antOdonPrevencion", "ant_operatoria", "ant_endodoncia", "ant_periodoncia", "ant_protesis", 
  			     "ant_ortodoncia", "ant_cirugiaOral", "ant_ortopedia",  "ant_comentariosAntOdo", 
  			     "ant_antOdonFechaVisitaOdont", "ant_motivo", "ant_reaccionDesfavorableTratam", 
	                   "ant_observacionesOdontologicas", 
	                   "ant_sufreAlergiaAnestesicolocal", "ant_alergiaPenicilina", "ant_alergiaDrogasComidas", "ant_comentarioAntAler", 
	                   "ant_rechinaDientes", "ant_chupaDedo", "ant_comeUnas", "ant_respiraBoca", 
	                   "ant_aprietaDiente", "ant_otrosHabitos", "ant_estado");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "antecedente" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE ant_id = '$ant_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo adicionar los datos por un fallo en el sistema, por Favor contacte a el administrador.".$query;
		$mess_cod = "alert";		
	}else{
	       logs::crear("apertura", "modificar", $query);
	 	$mess = "<center><b>Los datos fueron adicionados  con &eacute;xito </b><br><br>
	 	Por favor continue ingresando los datos de Revisi&oacute;n por Sistema "; 
	 	$mess_cod = "info";
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }
  
}
?>
