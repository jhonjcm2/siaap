<?
/***************************************************************************************/
// CLASE: historia
// Proposito: Realizar tareas de manejo de los datos historicos del paciente.
// Ultima modificacion: Octubre 2003: 
// /************************************************************************************/
class historia {
  var $fv;
  var $data;
	
function historia($pac_id=0){
    global $table;
    
    $this->fv = new FormValidator;
    if ($pac_id){
    $query = "SELECT * 
              FROM historia_clinica 
              WHERE pac_id='$pac_id' ";
    $table->sql_query($query);
    $this->data = $table->sql_fetch_object();
    }
    
  }

  /**********************************************************/
  // Proposito: Creacion de una historia clinica
  // return: arreglos con resultados de la creacion.
  /**********************************************************/
  function crear(){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;

    foreach($_GET as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;
      
   $this->fv->resetErrorList();	 
   $this->fv->isEmpty("hic_anamHistoriaEnfActual","ingrese el motivo de la enfermedad actual");
 
   if ( $this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    } else {
      $query = 'SELECT * 
                FROM historia_clinica 
                WHERE "pac_id"='.$pac_id;
      $table->search($query);
    
      if($table->nfound) {
	$mess = "Un paciente con ese documento ya se encuentra registrado en el sistema, por favor escoja otro.";
	$mess_cod = "error";
	$s_opc = "adicionar_usuario";
      }
      else {
	/*  se pasa a la quota a bytes */
	$fields = array ("hic_id", "pac_id", "usu_id", "hic_anamHistoriaEnfActual",   "hic_anamMotivoConsulta", "hic_fechaAnamnesis" );
								     
         					      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "historia_clinica" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "<b>No se pudo adicionar los datos por un fallo en el sistema</b>,".$query;
	$mess_cod = "alert";		
	}else{
	    logs::crear("historia", "crear", $query);
	    $mess = "<center><b>Los datos de la Anamnesis $login fueron adicionados con &eacute;xito</b><br><br>
	    Por favor continue ingresando los Antecedentes Generales"; 
	    $mess_cod = "info";
	    $s_opc = "info";
	 } 
	  
      }
    }

    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }

/********************* Buscar una HC ******************/

function buscar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);



             $fields = array ("pac_id", "usu_id", "raz_id", "hic_anamHistoriaEnfActual", "hic_antFamiliares", "hic_habitos", "hic_rxsNerviosoAnsiedad", "hic_irritabilidad", 
             "hic_sincopes", "hic_depresion", "hic_lipotimias", "hic_epilepsia", "hic_convulsiones", "hic_paresias", "hic_accidenteCerebroVascular", "hic_cefaleas", 
             "hic_neuralgia", "hic_paralisis", "hic_migrana", "hic_rxsRespiratorioTos", "hic_disnea", "hic_rinitis", "hic_pulmonares", "hic_micosis", "hic_hemoptisis", 
             "hic_asma", "hic_tbc", "hic_rxsCardiovascularArritmias", "hic_cardiopatiasCongenitas", "hic_anasarca", "hic_varices", "hic_cianosis", "hic_hta", 
             "hic_rxsGastrointestinalDiarrea", "hic_gastritis", "hic_hepatitis", "hic_parasitos", "hic_ulceras", "hic_ictericia", "hic_rxsGenitoUrinarioInfeccion", 
             "hic_insuficienciaRenal", "hic_cistitis", "hic_dismenorrea", "hic_embarazo", "hic_abortos", "hic_planificacion", "hic_ets", "hic_rxsInmAut", "hic_rxsInmDef",  
             "hic_rxsEndocrinoEnanismo", "hic_gigantismo", "hic_acromegalia", "hic_cretinismo", "hic_diabetes", "hic_rxsOsteoarticularArtritis", "hic_espasmos", 
             "hic_tics", "hic_artrosis", "hic_doloresArticulares", "hic_rxsPielErupcionesCut", "hic_micosis1", "hic_psoriasis", "hic_rxsObsInm", "hic_rxsInmCom", "hic_rxsInmAle","hic_exFiSignosVitalesPeso", 
             "hic_talla", "hic_temperatura", "hic_pulso", "hic_tensionArterial", "hic_frecuenciaRespiratoria", "hic_caracteristicasFisicasOrien", "hic_contextura", 
             "hic_aparienciaNutricionalNormal", "hic_picosCrecimiento", "hic_crecimientoComparado", "hic_exaFisObs", "hic_estomExtraOralTipoPerfil", 
             "hic_patronCrecimientoFacial", "hic_comentariosExtraOral", "hic_atm", "hic_regionSubmandibular", "hic_cuello",  
             "hic_regLabSuperiorNormal", "hic_regLabSuperiorCorto", "hic_regLabSuperiorFuncional", "hic_regLabSuperiorHipofuncional", "hic_regLabSuperiorHiperfunciona", 
             "hic_regLabInfHiperfuncional", "hic_regLabInfHipofuncional", "hic_regLabInfFuncional", "hic_tonicidadHipotonico", "hic_tonicidadHipertonico", 
             "hic_tonicidadNormotonico", "hic_selleLabialCompetencia", "hic_estomIntraOralVestibulo", "hic_paladarDuroColor", "hic_profundidad", "hic_ancho", "hic_torus", 
             "hic_paladarBlandoNormal", "hic_submucoso", "hic_uvulaBifida", "hic_lenguaTamano", "hic_movilidadNormal", "hic_posicionNormal", "hic_frenilloLingualNormal", 
             "hic_funcionGustativaNormal", "hic_pisoBoca", "hic_funcionGlandular", "hic_rebordesAlveolares", "hic_formaArcadaSuperior", "hic_Inferior", 
             "hic_tiposArcoSuperior", "hic_arcoInferior", "hic_relCanPermanentesDerecho", "hic_permanentesIzquierdo", "hic_deciduosDerecho", "hic_deciduosIzquierdo", 
             "hic_molaresDecidSupDerecho", "hic_molaresDecidSupIzquierdo", "hic_molaresDecidInfDerecho", "hic_molaresDecidInfIzquierdo", "hic_cavidadOralLocalizacion", 
             "hic_evolucion", "hic_aspecto", "hic_textura", "hic_tamano", "hic_consistencia", "hic_color", "hic_movilidad", "hic_base", "hic_dolor", "hic_exudado", 
             "hic_adenopatias", "hic_compromisoSistematico", "hic_estado", "hic_rxs", "hic_efg", "hic_extraO", "hic_intraO", "hic_fechaElaboracionAntMed", "hic_fechaElaboracionRxs", "hic_fechaElaboracionEfg", 
		"hic_fechaElaboracionExtraO", "hic_fechaElaboracionIntraO");
							      
       // Arreglos para datos del query y el url para paginacion

      $where_array = array();
     $query = 'SELECT  * FROM  "historia_clinica" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($where_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($where_array))
	$query .= " WHERE ".implode(", ",$where_array);
	
	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No  hay registros que concuerden con su b�squeda";
		$mess_cod = "alert";		
	}else{
	       $mess = "El usuario $login fue adicionado con &eacute;xito "; 
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }

  /************** Modificar HC****************/
  function modificar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
/********************/
		
		$fields = array ("pac_id", "usu_id", "raz_id", "hic_anamHistoriaEnfActual", "hic_antFamiliares", "hic_habitos", "hic_rxsNerviosoAnsiedad", 
		"hic_irritabilidad", "hic_sincopes", "hic_depresion", "hic_lipotimias", "hic_epilepsia", "hic_convulsiones", "hic_paresias", "hic_accidenteCerebroVascular", 
		"hic_cefaleas", "hic_neuralgia", "hic_paralisis", "hic_migrana", "hic_rxsRespiratorioTos", "hic_disnea", "hic_rinitis", "hic_pulmonares", "hic_micosis", 
		"hic_hemoptisis", "hic_asma", "hic_tbc", "hic_rxsCardiovascularArritmias", "hic_cardiopatiasCongenitas", "hic_anasarca", "hic_varices", "hic_cianosis", 
		"hic_hta", "hic_rxsGastrointestinalDiarrea", "hic_gastritis", "hic_hepatitis", "hic_parasitos", "hic_ulceras", "hic_ictericia", "hic_rxsGenitoUrinarioInfeccion", 
		"hic_insuficienciaRenal", "hic_cistitis", "hic_dismenorrea", "hic_embarazo", "hic_abortos", "hic_planificacion", "hic_ets", "hic_rxsInmAut", "hic_rxsInmDef", 
		"hic_rxsEndocrinoEnanismo", "hic_gigantismo", "hic_acromegalia", "hic_cretinismo", "hic_diabetes", "hic_rxsOsteoarticularArtritis", "hic_espasmos", 
		"hic_tics", "hic_artrosis", "hic_doloresArticulares", "hic_rxsPielErupcionesCut", "hic_micosis1", "hic_psoriasis", "hic_rxsObsInm", "hic_rxsInmCom", "hic_rxsInmAle","hic_exFiSignosVitalesPeso", 
		"hic_talla", "hic_temperatura", "hic_pulso", "hic_tensionArterial", "hic_frecuenciaRespiratoria", "hic_caracteristicasFisicasOrien", "hic_contextura", 
		"hic_aparienciaNutricionalNormal", "hic_picosCrecimiento", "hic_crecimientoComparado", "hic_exaFisObs", "hic_estomExtraOralTipoPerfil", 
		"hic_patronCrecimientoFacial", "hic_comentariosExtraOral", "hic_atm", "hic_regionSubmandibular", "hic_cuello", 
		"hic_regLabSuperiorNormal", "hic_regLabSuperiorCorto", "hic_regLabSuperiorFuncional", "hic_regLabSuperiorHipofuncional", "hic_regLabSuperiorHiperfunciona", 
		"hic_regLabInfHiperfuncional", "hic_regLabInfHipofuncional", "hic_regLabInfFuncional", "hic_tonicidadHipotonico", "hic_tonicidadHipertonico", 
		"hic_tonicidadNormotonico", "hic_selleLabialCompetencia", "hic_estomIntraOralVestibulo", "hic_paladarDuroColor", "hic_profundidad", "hic_ancho", "hic_torus", 
		"hic_paladarBlandoNormal", "hic_submucoso", "hic_uvlaBifida", "hic_lenguaTamano", "hic_movilidadNormal", "hic_posicionNormal", "hic_frenilloLingualNormal", 
		"hic_funcionGustativaNormal", "hic_pisoBoca", "hic_funcionGlandular", "hic_rebordesAlveolares", "hic_formaArcadaSuperior", "hic_Inferior", 
		"hic_tiposArcoSuperior", "hic_arcoInferior", "hic_relCanPermanentesDerecho", "hic_permanentesIzquierdo", "hic_deciduosDerecho", "hic_deciduosIzquierdo", 
		"hic_molaresDecidSupDerecho", "hic_molaresDecidSupIzquierdo", "hic_molaresDecidInfDerecho", "hic_molaresDecidInfIzquierdo", "hic_cavidadOralLocalizacion", 
		"hic_evolucion", "hic_aspecto", "hic_textura", "hic_tamano", "hic_consistencia", "hic_color", "hic_movilidad", "hic_base", "hic_dolor", "hic_exudado", 
		"hic_adenopatias", "hic_compromisoSistematico", "hic_estado", "hic_rxs", "hic_efg", "hic_extraO", "hic_intraO", "hic_fechaElaboracionAntMed", "hic_fechaElaboracionRxs", "hic_fechaElaboracionEfg", 
		"hic_fechaElaboracionExtraO", "hic_fechaElaboracionIntraO", "hic_anamMotivoConsulta");
             
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "historia_clinica" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
    $query .=  " WHERE pac_id = '$pac_id'";
	$result=$table->sql_query($query);
		
	if (!$result){
		$mess = "<center><b>No se pudo adicionar los datos por un fallo en el sistema, comuniquese con el administrador.</center></b>";
		$mess_cod = "alert";		
	}else{
	       logs::crear("historia", "modificar", $query);
	 	$mess = "<b>Los datos $login fueron adicionado con &eacute;xito.</b> "; 
	    	$mess_cod = "info";
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }

/******************Seguir Creando HC****/    

function actualizar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

		
		$this->fv->resetErrorList();
		/*if($hic_rxs==1){	 
   		$this->fv->isEmpty("hic_rxsAnexos","ingrese un comentario de la revisi&oacute;n por sistema");
		}	*/
		
		if($hic_efg==1){	
		$this->fv->isNumber("hic_exFiSignosVitalesPeso", "Debe insertar solo n&uacute;meros en el peso del paciente");
		$this->fv->isNumber("hic_talla", "Debe insertar solo n&uacute;meros en la talla del paciente");
		$this->fv->isNumber("hic_temperatura", "Debe insertar solo n&uacute;meros en la temperatura del paciente");
   		$this->fv->isWithinRange("hic_contextura","Seleccione el tipo de contextura",0,4);
   		$this->fv->isWithinRange("hic_picosCrecimiento","Seleccione un pico de crecimiento",0,4);
   		//$this->fv->isWithinRange("hic_crecimientoComparado","Seleccione un tipo de crecimiento",0,4);
		}	
		
		if($hic_extraO==1){	
		$this->fv->isEmpty("hic_comentariosExtraOral","ingrese un comentario del ex&aacute;men extra oral");
		}
		
		
		 if ( $this->fv->isError() ) {
		      $mess = $this->fv->getMessage();
		      $mess_cod = "alert";
		    } else {


		$fields = array ("raz_id","hic_rxsNerviosoAnsiedad", "hic_irritabilidad", "hic_sincopes", "hic_depresion", "hic_lipotimias", "hic_epilepsia", "hic_convulsiones", 
		"hic_paresias", "hic_accidenteCerebroVascular", "hic_cefaleas", "hic_neuralgia", "hic_paralisis", "hic_migrana", "hic_nerviosoOtro", "hic_rxsRespiratorioTos", "hic_disnea", 
		"hic_rinitis", "hic_pulmonares", "hic_micosis", "hic_hemoptisis", "hic_asma", "hic_tbc", "hic_resOtro",  "hic_rxsCardiovascularArritmias", "hic_cardiopatiasCongenitas", 
		"hic_anasarca", "hic_varices", "hic_cianosis", "hic_cadioOtro", "hic_hta", "hic_rxsGastrointestinalDiarrea", "hic_gastritis", "hic_hepatitis", "hic_parasitos", "hic_ulceras", 
		"hic_ictericia", "gastroOtro", "hic_rxsGenitoUrinarioInfeccion", "hic_insuficienciaRenal", "hic_cistitis", "hic_dismenorrea", "hic_embarazo", "hic_abortos", "hic_planificacion", 
		"hic_ets", "hic_genitoOtro", "hic_rxsInmAut", "hic_rxsInmDef", "hic_inmuOtro", "hic_rxsEndocrinoEnanismo", "hic_gigantismo", "hic_acromegalia", "hic_cretinismo", "hic_diabetes", 
		"hic_endocrinoOtro", "hic_rxsOsteoarticularArtritis", "hic_espasmos", "hic_tics", "hic_artrosis", "hic_doloresArticulares", "hic_osteoOtro", "hic_rxsPielErupcionesCut", "hic_micosis1", 
		"hic_psoriasis", "hic_pielOtro", "hic_rxsObsInm", "hic_rxsInmCom", "hic_rxsInmAle","hic_exFiSignosVitalesPeso", "hic_talla", "hic_temperatura", "hic_pulso", "hic_tensionArterial", "hic_frecuenciaRespiratoria", 
		"hic_caracteristicasFisicasOrien", "hic_contextura", "hic_aparienciaNutricionalNormal", "hic_picosCrecimiento", "hic_crecimientoComparado", 
		"hic_exaFisObs","hic_estomExtraOralTipoPerfil", "hic_comentariosExtraOral", "hic_atm", "hic_regionSubmandibular", 
		"hic_cuello", "hic_labioSup", "hic_labioInf", "hic_estomIntraOralVestibulo", "hic_bucas",
		"hic_paladarColor", "hic_paladarPro", "hic_paladarAncho", "hic_paladarTorus", "hic_paladarRugas", "hic_paladarSubmucoso", "hic_paladarUvula", "hic_paladarClasif", "hic_paladarComentario", "hic_lenguaTam", 
		"hic_lenguaMov", "hic_lenguaFor", "hic_diente", "hic_pisoBoca", "hic_funcionGlandular", "hic_rebordesAlveolares", 
		"hic_rxs", "hic_efg", "hic_extraO", "hic_intraO", "hic_fechaElaboracionAntMed", "hic_fechaElaboracionRxs", "hic_fechaElaboracionEfg", 
		"hic_fechaElaboracionExtraO", "hic_fechaElaboracionIntraO", "hic_obsNervioso", "hic_obsRespiratorio", "hic_obsCardiovascular", "hic_obsGastrointestinal", "hic_obsGenito", 
		"hic_obsEndocrino", "hic_obsOsteoArt", "hic_obsPiel");
             
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "historia_clinica" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE hic_id='$hic_id' ";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "<b>No se pudo adicionar los datos por un fallo en el sistema, por favor comuniquese con el administrador.$query</b>";
		$mess_cod = "alert";		
	}else{
	       logs::crear("historia", "actualizar", $query);
	 	$mess = "<b>Los datos $login fueron adicionados con &eacute;xito.</b> "; 
	    	$mess_cod = "info";
	    	$s_opc = "info_usuario";
	 } 
	  
    
  }
return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
}

}
?>