<?
/**********************************************************/
// CLASE: pedido
// Proposito: Realizar un pedido de materiales.
// Ultima modificacion: octubre de 2003
// /**********************************************************/
class pedido{
  var $fv;
  var $data;
	
  function pedido($id=-1){
    global $table;
    
    $this->fv = new FormValidator;
    if ($id >= 0){
    	$query = "SELECT * 
        	     FROM pedido
        	     WHERE ped_id='$id' ";
	$table->sql_query($query);
    	$this->data = $table->sql_fetch_object();
    }
    
  }

  /**********************************************************/
  // Proposito: Creacion de un pedido de materiales
  // return: arreglos con resultados de la creacion.
  /**********************************************************/
  function crear(){	
    global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;

    foreach($_GET as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);
      //${$k} = $v;
      
        
   if ($this->fv->isError() ) {
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
    }else {
      $query = 'SELECT * 
                FROM pedido
                WHERE "ped_id"='.$ped_id;
      $table->search($query);
    
      if($table->nfound) {
	$mess = "<center><b>Un pedido con ese c&oacute;digo ya se encuentra registrado en el sistema </b><br> por favor escoja otro.";
	$mess_cod = "alert";
	//$s_opc = "adicionar_usuario";
      }
      else {
	/*  se pasa a la quota a bytes */
         $fields = array ("ped_id", "usu_id", "bod_id", "ped_fechaSolicitud", "ped_fechaAprobacion", "ped_despachado");
							      
       // Arreglos para datos del query y el url para paginacion
      $fields_array = array();
      $values_array = array();
     $query = 'INSERT INTO "pedido" (';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($fields_array,' "'.$v.'" ');
	  array_push($values_array," '".${$v}."' ");
	}

      if(sizeof($fields_array)){
	$query .= implode(", ",$fields_array);
	$query .= ") VALUES (".implode(", ",$values_array) ;
	}
	$query .=")";
	
	$result=$table->sql_query($query);
	
	if (!$result){
	$mess = "<b>No se pudo adicionar el pedido por un fallo en el sistema. Comuniquese con el administrador</b>";
	$mess_cod = "alert";		
	}else{
	    logs::crear("pedido", "crear", $query);
	    $mess = "<b>Los datos del pedido fueron adicionados con &eacute;xito</b>"; 
	    $mess_cod = "alert";
	    $s_opc = "info_usuario";
	    //$s_opc = "busqueda";
	 } 
	  
      }
    }
    //$mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }

/************** acentar pedido ****************/
  
  function acentar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

     
    $fields = array ("ped_acentado");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "pedido" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE ped_id = '$ped_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo adicionar el pedido por un fallo en el sistema, Comuniquese con el administrador del sistema".$query;
		$mess_cod = "alert";		
	}else{
	       logs::crear("pedido", "modificar", $query);
	 	//$mess = "<center><b>El pedido fue realizado con &eacute;xito </center></b>".$query;
	 	$mess = "<center><b>El pedido fue realizado con &eacute;xito </center></b>";  
	    	$mess_cod = "info";
	    //	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }
  
  
/*******************************************/    

/********************* Buscar un pedido******************/

function buscar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);



             $fields = array ("usu_id", "bod_id", "ped_fechaSolicitud", "ped_fechaAprobacion", "ped_despachado");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'SELECT * FROM  "pedido" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($where_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($where_array))
	$query .= " WHERE ".implode(", ",$where_array);
	
      	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No hay registros que concuerden con su busqueda.".$query;
		$mess_cod = "alert";		
	}else{
	       $mess = "El pedido fue adicionado con &eacute;xito ".$query; 
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }
  
  
/***************************************************/

function agregarMat($ped_id=0){


	global $table, $_POST, $_GET;
	
	$table2 = new my_DB();

	foreach($_POST as $k=>$v)
		//${$k} = base::dispelMagicQuotes($v);
		${$k} = $v;

	foreach($_GET as $k=>$v)
		// ${$k} = base::dispelMagicQuotes($v);
		${$k} = $v;

	$this->fv->resetErrorList();
	$this->fv->isEmpty("mat_id","Seleccione un material para agregar");
	$this->fv->isWithinRange("mxp_cantidadMaterial","Ingrese una cantidad de material v&aacute;lida",1,1000000);
	$this->fv->isNumber("mxp_cantidadMaterial", "Debe insertar solo n&uacute;meros en la cantidad del material");


	if ( $this->fv->isError() ) {
		$mess = $this->fv->getMessage();
		$mess_cod = "alert";
	}else{
		$query= "SELECT * FROM matxbod WHERE mat_id=$mat_id AND bod_id=1";
		$table2->sql_query($query);
		$can1 = $table2->sql_fetch_object();
		$resta1 = $can1->mxb_cantidadMaterial - $mxp_cantidadMaterial;
		
		if ($resta1< 0){
			$query="SELECT * FROM materiales WHERE mat_id=$mat_id";
			$table2->sql_query($query);
			$can = $table2->sql_fetch_object();
			$result=$table->sql_query($query);
			$mess .= "<center><b>No hay suficientes existencias del material ".$can->mat_descripcion." <br>Por Favor verifique la existencia </b></center>";
			$mess_cod="alert";
		}else {
        		/*  se pasa a la quota a bytes */
			$fields = array ("ped_id", "mat_id", "mxp_cantidadMaterial");
		
			// Arreglos para datos del query y el url para paginacion
			$fields_array = array();
			$values_array = array();
			$query = 'INSERT INTO "matxped" (';
      			// Adicionando los campos para el query
			foreach($fields as $v)
				if(${$v}!=""){
				array_push($fields_array,' "'.$v.'" ');
				array_push($values_array," '".${$v}."' ");
				}

			if(sizeof($fields_array)){
				$query .= implode(", ",$fields_array);
				$query .= ") VALUES (".implode(", ",$values_array) ;
			}
        		$query .=")";

        		$result=$table->sql_query($query);
		}
	}
	//$mess .= $query;
	return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
}

  /************** Modificar pedido ****************/
  
  function modificar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

     
    $fields = array ("usu_id", "bod_id", "ped_fechaSolicitud", "ped_fechaAprobacion", "ped_despachado");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "pedido" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE ped_id = '$ped_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo adicionar el pedido por un fallo en el sistema, Comuniquese con el administrador del sistema";
		$mess_cod = "alert";		
	}else{
	       logs::crear("pedido", "modificar", $query);
	 	$mess = "El pedido fue adicionado con &eacute;xito "; 
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }
  
  
/*******************************************/    
 /************** Modificar cantidad de un material de un pedido ****************/
  
  function modificarCant($mat_id){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

     
    $fields = array ("mxp_cantidadMaterial");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'UPDATE  "matxped" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE mat_id=$mat_id and ped_id = '$ped_id'";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo modificar el pedido por un fallo en el sistema, Comuniquese con el administrador del sistema".$query;
		$mess_cod = "alert";		
	}else{
	       logs::crear("pedido", "modificar", $query);
	 	$mess ="<center><b>El pedido fue modificado con &eacute;xito<br><br>
	 	Haga click <a href='$PHP_SELF?opc=pedido&s_opc=visualizar&ped_id=$ped_id'>Aqu&iacute;</a> para despachar el pedido</center></b>"; 
	 	$mess_cod = "info";
	    	$s_opc = "info_usuario";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }
  
  
/******************************************
Despacha el material:Sube la cantidad de material a la bodega destino y le quita el material
a la bodega principal
*******************************************/

    function despachar()
    {
       global $table, $_GET, $_POST;
       
       $table2 = new my_DB();
        
	foreach($_POST as $k=>$v)
		${$k} = base::dispelMagicQuotes($v);

	foreach($_GET as $k=>$v)
		${$k} = base::dispelMagicQuotes($v);

	/*$query = 'SELECT * FROM 
		(
		
		SELECT 
		SUM(mxb."mxb_cantidadMaterial") AS suma, mxp.mat_id AS idmat, 
		"mxp_cantidadMaterial" AS cant_pedida,
		"mat_descripcion" AS nombre
		FROM matxped mxp,matxbod mxb, materiales mat
		WHERE ped_id='.$ped_id.' AND  mxp.mat_id=mxb.mat_id AND mat.mat_id=mxp.mat_id
		GROUP BY mxp.mat_id, "mxp_cantidadMaterial", "mat_descripcion"
		ORDER BY mxp.mat_id
		
		) AS tbtotal
		WHERE cant_pedida>suma';
	echo $query;
	$table->sql_query($query);
	if($table->nfound > 0){
		$mess="";
		while($obj=$table->sql_fetch_object()){
			echo $mess;
			$mess.="<br>Cantidad excedida en ".$obj->nombre.", hay ".$obj->suma." y se requieren ".$obj->cant_pedida;
		}
		return Array("mess"=>$mess, "mess_cod" => 'alert');
	}*/
		
	$query = "SELECT * FROM matxped WHERE  ped_id=$ped_id";
	$table->sql_query($query);
	$bod_id=$this->data->bod_id;
	$mess_cod="info";
	$bodega = new Bodega();
	/////////
	while($obj=$table->sql_fetch_object()){
		$mat_id=$obj->mat_id;
		$res=$bodega->puedePasarse($mat_id,$obj->mxp_cantidadMaterial,$bod_id,1);
		if(strcasecmp($res["mess_cod"],"alert")==0){
			return Array("mess"=>"No puede realizarse", "mess_cod" => "alert"); 
		}
	}

		
	$query = "SELECT * FROM matxped WHERE  ped_id=$ped_id";
	$table->sql_query($query);
	$bod_id=$this->data->bod_id;
	$mess_cod="info";

	while($obj=$table->sql_fetch_object()){
		$mat_id=$obj->mat_id;
		
//////////////////////REEMPLAZAR////////////////////////////////////////////////////////////////
		/***** Valido que el material exista en el almacen ****
		$query = "SELECT * FROM matxbod WHERE mat_id=$mat_id AND bod_id=1";
		$table2->sql_query($query);
		$row2 = $table2->sql_fetch_object();
		$resta = $row2->mxb_cantidadMaterial - $obj->mxp_cantidadMaterial;
		
		if ($resta>=0){
	        	$query = "SELECT * FROM matxbod WHERE mat_id=$mat_id AND bod_id=$bod_id";
			$table2->sql_query($query);
 		      	//*** Subo los materiales a la bodega de destino ****
			if ($row = $table2->sql_fetch_object()){
				$cantMat=$row->mxb_cantidadMaterial + $obj->mxp_cantidadMaterial;
				$query="UPDATE matxbod SET \"mxb_cantidadMaterial\"=$cantMat, \"mxb_saldoInicial\"= $cantMat, \"mxb_fechaAbastecimiento\"='". date('Y-m-d', time())."' WHERE  mat_id=$mat_id AND bod_id=$bod_id";
			  	$result = $table2->sql_query($query);
        		}else{
        			$mxp_cantidadMaterial =  $obj->mxp_cantidadMaterial;
        			$query="INSERT INTO matxbod (\"mxb_cantidadMaterial\", \"mxb_saldoInicial\", \"mxb_fechaAbastecimiento\", \"mxb_fechaActual\", mat_id, bod_id) 
        				   VALUES ($mxp_cantidadMaterial, $mxp_cantidadMaterial, '". date('Y-m-d', time())."', '" . date('Y-m-d', time())."', $mat_id, $bod_id)";
	        		$result = $table2->sql_query($query);
			}//**********AGREGAMOS UNA FECHA DE VENCIMIENTO***********
			$vence= new Vencimiento();
			$vence->sacarDeAlmacen($bod_id, $obj->mxp_cantidadMaterial, $mat_id);
			
  		      	
			//$cantMat=$row2->mxb_cantidadMaterial - $obj->mxp_cantidadMaterial;
			$query="UPDATE matxbod SET \"mxb_cantidadMaterial\"=$resta WHERE  mat_id=$mat_id AND bod_id=1";
			$result = $table2->sql_query($query);
		}else{
		       logs::crear("pedido", "despachar", $query);
			$query="SELECT * FROM materiales WHERE mat_id=$mat_id";
			$table2->sql_query($query);
			$row = $table2->sql_fetch_object();
			$mess .= "No hay suficientes existencias del material ".$row->mat_descripcion." <br>";
			$mess_cod="alert";
		}//*/
/************CON************************************************************************/
		$bodega = new Bodega();
		//se pasa de la bodega principal a la otra bodega
		$res=$bodega->pasar($mat_id,$obj->mxp_cantidadMaterial,$bod_id,1);
		//SE COMENTA PARA 
		/*if(strcasecmp($res["mess_cod"],"info")==0){
			$mess="El pedido fu&eacute; despachado de manera exitosa";
			$mess_cod=$res["mess_cod"];
		}
		else{
			show_mess("El pedido no pudo realizarse",$res["mess_cod"]);
			logs::crear("pedido", "despachar", $query);
			$query="SELECT * FROM materiales WHERE mat_id=$mat_id";
			$table2->sql_query($query);
			$row = $table2->sql_fetch_object();
			$mess .= "No hay suficientes existencias del material ".$row->mat_descripcion." <br>";
			$mess_cod="alert";
		}*/
/************************************************************************************/
	}
	return Array("mess"=>$mess, "mess_cod" => $mess_cod);
}

 /************** Borrar un pedido antes de acentar su creacion****************/
 
  function borrar(){
    global $table;

    foreach($GLOBALS['HTTP_POST_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

    foreach($GLOBALS['HTTP_GET_VARS'] as $k=>$v)
      ${$k} = base::dispelMagicQuotes($v);

   
    $fields = array ("bod_id", "ped_fechaSolicitud", "ped_fechaAprobacion", "ped_despachado");
							      
       // Arreglos para datos del query y el url para paginacion

      $set_array = array();
     $query = 'DELETE FROM "pedido" ';
      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE ped_id =$ped_id";

	$result=$table->sql_query($query);
	
	if (!$result){
		$mess = "No se pudo cancelar el pedido por un fallo en el sistema, Comun&iacute;quese con el administrador.";
		$mess_cod = "alert";		
	}else{
	       logs::crear("pedido", "borrar", $query);
	 	$mess = "El pedido $login fu&eacute; cancelado con &eacute;xito ".$query; 
	    	$mess_cod = "info";
	 } 
	  
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc);
  }

/***********borrar de la tabla matxped un pedido****************/
function borrarMat($ped_id=0){


global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;


//   if ( 0 /*$this->fv->isError()*/ ) {
if ( 0 ){
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
      }
      else {
        /*  se pasa a la quota a bytes */
         $fields = array  ("ped_id", "mat_id", "mxp_cantidadMaterial");

       // Arreglos para datos del query y el url para paginacion
      	$fields_array = array();
      	$values_array = array();
     	$query = 'DELETE FROM "matxped" ';
     	// Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE ped_id = '$ped_id'";

	$result=$table->sql_query($query);
    
        if (!$result){
        $mess = "No se pudo cancelar el pedido por un fallo en el sistema, Contacte al Administrador del Sistema.";
        $mess_cod = "alert";
        }else{
              logs::crear("pedido", "borrarMat", $query);
            	$mess = "<center><b>El pedido $login fu&eacute;  cancelado</b><br>
            	Vuelva a ingresarlo</center>";
	 	$mess_cod = "info";
         }

      }
    //$mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }

/***********borrar de la tabla matxped un solo material de un pedido****************/
function borrarSolo($ped_id=0){


global $table, $_POST, $_GET;

    foreach($_POST as $k=>$v)
      //${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;

    foreach($_GET as $k=>$v)
     // ${$k} = base::dispelMagicQuotes($v);
      ${$k} = $v;


//   if ( 0 /*$this->fv->isError()*/ ) {
if ( 0 ){
      $mess = $this->fv->getMessage();
      $mess_cod = "alert";
      }
      else {
        /*  se pasa a la quota a bytes */
         $fields = array  ("ped_id", "mat_id", "mxp_cantidadMaterial");

       // Arreglos para datos del query y el url para paginacion
      	$fields_array = array();
      	$values_array = array();
     	$query = 'DELETE FROM "matxped" ';
     	// Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($set_array,' "'.$v.'" '." = '".${$v}."' ");
	}

       if(sizeof($set_array))
	$query .= " SET ".implode(", ",$set_array);
	
      $query .=  " WHERE ped_id = '$ped_id' and mat_id= '$mat_id' ";

	$result=$table->sql_query($query);
    
        if (!$result){
        $mess = "No se pudo cancelar el pedido por un fallo en el sistema, Contacte al Administrador del Sistema.";
        $mess_cod = "alert";
        }else{
              logs::crear("pedido", "borrarMat", $query);
            	$mess = "<center><b>El material solicitado fu&eacute;  cancelado</b><br>
            	Ingreselo Correctamente</center>";
	 	$mess_cod = "info";
         }

      }
  // $mess .= $query;
    return array("mess"=>$mess,"mess_cod"=>$mess_cod,"s_opc"=>$s_opc,"username"=>$login);
  }

}
?>
