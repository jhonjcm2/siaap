<?

/*Coloca en ceros la bodega indicada*/
function vaciar($id_bodega){
	//'UPDATE matxbod SET "mxb_cantidadMaterial"=0'
	//'UPDATE matxbod SET "mxb_unidadesDespachadas"=0'
	$table= new my_DB();
	$table2= new my_DB();
	$query = "SELECT * FROM matxbod x WHERE x.bod_id=$id_bodega";
	$table->search($query);
	//echo "$id_bodega:".$query."<br>".($table->nfound)."------<---<br>";
	while($result=$table->sql_fetch_object($query)){
		$query="UPDATE vencimiento_material SET vmat_cantidad=0 WHERE mxb_id=$result->mxb_id";
		$table2->sql_query($query);
		//echo $query."<br>";
	}
	$query = 'UPDATE matxbod x SET mxb_cantidadMaterial=0, mxb_unidadesDespachadas=0 WHERE x.bod_id='.$id_bodega;
	$result=$table->search($query);
	
	//$result=array("mess"=>"vac&iacute;o","code"=>"info");
	if(!$result){
		return array("mess"=>"Error en la base de datos","code"=>"alert");
	}else{
		return array("mess"=>"vac&iacute;o","code"=>"info");
	}
}

/*Borra los registros de materiales q estan con cantidad cero*/
function eliminarMaterialesCeros($id_bodega){
	//$query='DELETE FROM matxbod WHERE bod_id='.$bod_id.' AND "mxb_cantidadMaterial"=0';
	//'UPDATE matxbod SET "mxb_cantidadMaterial"=0'
	//'UPDATE matxbod SET "mxb_unidadesDespachadas"=0'
	$table= new my_DB();
	$table2= new my_DB();
	$query = 'SELECT * FROM matxbod x WHERE x.bod_id='.$id_bodega;
	$table->search($query);
	//echo "$id_bodega:".$query."<br>".($table->nfound)."------<---<br>";
	//Eliminamos los de esta bodega que tengan Vencimiento y esten en cero
	while($result=$table->sql_fetch_object()){
		$query="DELETE FROM vencimiento_material WHERE mxb_id=$result->mxb_id AND vmat_cantidad=0";
		$table2->search($query);
		//echo $query."<br>";
	}
	//Borramos los q sean en total cero
	$query = 'DELETE FROM matxbod x WHERE x."mxb_cantidadMaterial"=0 AND x.bod_id='.$id_bodega;
	$result=$table->search($query);
	
	//$result=array("mess"=>"vac&iacute;o","code"=>"info");
	if(!$result){
		return array("mess"=>"Error en la base de datos","code"=>"alert");
	}else{
		return array("mess"=>"vac&iacute;o","code"=>"info");
	}
}
/*Coloca en ceros toda una bodega y registra el vaciado*/
function vaciarBodega($id_bodega){
	$guardado=registrarVaciado($id_bodega);
	if($guardado["code"]=="info"){
		return vaciar($id_bodega);
	}else{
		return $guardado;
	}
}

/*Coloca en ceros todas las bodegas y registra el vaciado*/
function vaciarTodaBodega(){
	$guardado=registrarVaciado("Todas");
	if($guardado["code"]=="info"){$table= new my_DB();
		$query = "select * from bodega";
		$table->search($query);
		while($bodega = $table->sql_fetch_object()){
			$x=vaciar($bodega->bod_id);
			if($x["code"]!="info")
				return $x;
		}
	}else{
		return $guardado;
	}
	return array("mess"=>"vac&iacute;o","code"=>"info");
}

/*Registra en un documento el vaciado de la bodega*/
function registrarVaciado($id_bodega){
	global $usuario;
	//echo $usuario->usu_id."---------------------";

	$table= new my_DB();
	$query="SELECT * FROM bodega WHERE bod_id=".$id_bodega;
	$table->search($query);
	$bodega=$table->sql_fetch_object();

	$control=fopen("./vaciadoDeBodegas/registro.html",'a+');
	if($control == false){
		return array("mess"=>"No se puede escribir el fichero","code"=>"alert");
	}
	$contenido.="\n<tr>\n\t<td>".$usuario->usu_id;
	$contenido.="\n\t<td>".$usuario->usu_nombres." ".$usuario->usu_apellidos."</td>";
	$contenido.="\n\t<td>".$usuario->usu_numeroIdentificacion;
	$contenido.="\n\t<td>".date("Y-M-D");
	$contenido.="\n\t<td>".$bodega->bod_nombreBodega;
	$contenido.="\n\t<td>".$usuario->usu_login;
	$contenido.="\n</tr>";
	fwrite($control,$contenido);
	fclose($control);
	return array("mess"=>"El fichero se escribi&oacute; correctamente","code"=>"info");;
}


/*Crea el archivo y lo guarda en la ruta q se le indique*/
function guardarHC($pac_id,$ruta){
	$_GET["pac_id"]=$pac_id;
	$ntable= new my_DB();
	$ntable->search("SELECT * FROM paciente WHERE pac_id=$pac_id");
	$paciente=$ntable->sql_fetch_object();
	$nameFile=$paciente->pac_numeroIdentificacion."_".$pac_id;
	ob_start();
	include("hojaHCsaved.php");
	$html=ob_get_contents();
	ob_end_clean();	
	
	//Creando archivos
	$control = fopen("$ruta/$nameFile.html","w+");
	if($control == false){
		return false;
	}
	
	//Iniciando el archivo html
	fwrite($control,$html);
	
	//Terminando el archivo
	fclose($control);
	return true;
}

/*Planifica un borrado creando un registros_borrados
retorna: 
1 si no se encuentra el usu2
2 si se encuantra mas de 1 login2 igual
3 si no se encuentra el usu1
4  si se encuentra mas de 1 login1 igual
5 si la clave no concuerda con el usu1
-1 si alguno no es administrador
0 si se procedió correctamente
-2 si login1 y login2 son el mismo*/
function planificarBorrado($login1,$pass,$login2,$anios){
	if($login1==$login2){
		return -2;//Retornamos -2 si login1 y login2 son el mismo
	}
	$table=new my_DB();
	//Verificamos q el $login_usu2 exista y sea administrador
	$query="SELECT * FROM usuario WHERE usu_login='".$login2."'";
	//echo("<br>11111<br>".$query);
	$table->search($query);
	//retornamos 1 si no se encuentra el usu2
	if($table->nfound==0){
		return 1;
	}else if($table->nfound>1){//Retornamos 2 si se encuantra mas de 1 login2 igual
		return 2;
	}if($table->nfound==1){
		$usuAdmin2=$table->sql_fetch_object();
		$query="SELECT * FROM usuario WHERE usu_login='".$login1."'";
		//echo("<br>2222<br>".$query);
		$table->search($query);
		//Retornamos 3 si no se encuentra el usu1
		if($table->nfound==0)
			return 3;
		//Retornamos 4  si se encuentra mas de 1 login1 igual
		if($table->nfound>1)
			return 4;

		$usuAdmin1=$table->sql_fetch_object();
		//Retornamos 5 si la clave no concuerda
		if($usuAdmin1->usu_password!=md5($pass))
			return 5;

		$fecha=date("Y-m-d");
		if($usuAdmin1->tiu_id==1 && $usuAdmin2->tiu_id==1){
			$query="INSERT INTO registros_borrados (usu_id1,usu_id2,bor_fecha_creacion,bor_anios) VALUES($usuAdmin1->usu_id,$usuAdmin2->usu_id,'$fecha',$anios)";
			//echo("<br>3333<br>".$query."<br>");
			$table->sql_query($query);
			//Retornamos 0 si se procedió correctamente
			return 0;
		}else{
			//Retornamos -1 si alguno no es administrador
			return -1;
		}
	}
}

/*Retorna la consulta completa (tabla) con los backups pendientes por autorizar el usu2*/
function tieneBackupPendiente($id_usuario){
	$tabladb = new my_DB();
	$query="SELECT * FROM registros_borrados, usuario WHERE usu_id2=".$id_usuario." AND bor_ejecutado='false' AND usu_id1=usu_id";
	$tabladb->search($query);
	//echo $query;
	return $tabladb;
}

/*Realiza un backup ya registrado, es lo mismo q el usu2 acepta realizar un backup propuesto por usu1
retorna -1 si el login o la contrasenia no concuerdan, o si hay error al escribir el archivo
0 si se hace correctamente*/
function realizarBackup($idBackup, $login2, $pass2){
	$tablex=new my_DB();
	$query="SELECT * FROM usuario WHERE usu_login='".$login2."' AND usu_password='".md5($pass2)."'";
	$tablex->search($query);
	if($tablex->nfound==1){
		//Registramos el borrado
		$fecha=date("Y-m-d");
		$query="UPDATE registros_borrados SET bor_ejecutado='true', bor_aceptado='true', bor_fecha_ejecucion='$fecha' WHERE bor_id=$idBackup";
		$tablex->sql_query($query);
		//Buscamos los anios de borrados
		$query="SELECT * FROM registros_borrados WHERE bor_id=$idBackup";
		$tablex->sql_query($query);
		$registro=$tablex->sql_fetch_object();
		$anios=$registro->bor_anios;
		//Gurdamos los datos de los pacientes en una carpeta  y los borramos de la BD
		//echo "Creamos el directorio. "."./backupspacientes/backup".$idBackup."<br>";
		$creadoDir=mkdir("./backupspacientes/backup".$idBackup,0777);
		if(!$creadoDir)
			return -2;
		//echo "Creado exitoso<br>";
		
		$t1=borrarPacientesInactivos1($anios,1,"backupspacientes/backup$idBackup/");
		$t2=borrarPacientesInactivos2($anios,1,"backupspacientes/backup$idBackup/");
		if($t1==-1 || $t2==-1){
			return -1;
		 }
		return 0;
	}else{
		return -1;
	}
}


function cancelarBackup($idBackup, $login2, $pass2){
	//echo "ejecutando......";
	$tablex=new my_DB();
	$query="SELECT * FROM usuario WHERE usu_login='".$login2."' AND usu_password='".md5($pass2)."'";
	$tablex->search($query);
	//echo "ejecutando......";
	if($tablex->nfound==1){
		//Cancelamos
		$fecha=date("Y-m-d");
		$query="UPDATE registros_borrados SET bor_ejecutado='true', bor_aceptado='false', bor_fecha_ejecucion='$fecha' WHERE bor_id=$idBackup";
		//echo $query;
		$tablex->sql_query($query);
		return 0;
	}else{
		return -1;
	}
}
/*Devuelve los pac_id de los pacientes quienes no tienen pagos desde hace $anios y además no tienen evoluciones desde hace $anios o no tinen evoluciones.*/
function damePacientesInactivos1($anios){
	$fecha=split('-',date("Y-m-d"));
	$queryInactivos1=
		'SELECT * FROM( SELECT t1.pac_id, MAX(e."evo_fechaElaboracion") AS ultimafecha
			FROM
			(
				SELECT pac_id,MAX("pag_fecha") AS ultimo 
				FROM pagos
				GROUP BY pac_id 
				HAVING MAX("pag_fecha")<'."'".($fecha[0]-$anios).'-'.$fecha[1].'-'.$fecha[2]."'".'
			)AS t1
			LEFT OUTER JOIN
				evolucion AS e
			ON
				t1.pac_id=e.pac_id
			GROUP BY t1.pac_id
		) as t2
		WHERE
		ultimafecha<'."'".($fecha[0]-$anios).'-'.$fecha[1].'-'.$fecha[2]."'".' OR ultimafecha IS NULL
		ORDER BY t2.pac_id';
	//echo $queryInactivos1."<br>";
	$tableInactivos1 = new my_DB();
	$tableInactivos1->search($queryInactivos1);
	return $tableInactivos1;
}

/*Devuelve los id_pac de los pacientes quienes no tienen evoluciones desde hace $anios y además no tienen pagos desde hace $anios o no tinen pagos.*/
function damePacientesInactivos2($anios){
	$fecha=split('-',date("Y-m-d"));
	$queryInactivos2=
		'SELECT *
		FROM(
			SELECT t1.pac_id, MAX(pg."pag_fecha") AS ultimafecha
			FROM
			(
				/*Seleccionamos todos los pacientes q pecan por evolucion*/
				SELECT pac_id,MAX("evo_fechaElaboracion") AS ultimo 
				FROM evolucion
				GROUP BY pac_id 
				HAVING MAX("evo_fechaElaboracion")<'."'".($fecha[0]-$anios).'-'.$fecha[1].'-'.$fecha[2]."'".'
			)AS t1
			LEFT OUTER JOIN
				pagos AS pg
			ON
				t1.pac_id=pg.pac_id
			GROUP BY t1.pac_id
		) as t2
		WHERE
		ultimafecha<'."'".($fecha[0]-$anios).'-'.$fecha[1].'-'.$fecha[2]."'".' OR ultimafecha IS NULL
		ORDER BY t2.pac_id';
	$tableInactivos2 = new my_DB();
	$tableInactivos2->search($queryInactivos2);
	return $tableInactivos2;
}

/*Borra los pacientes quienes no tienen pagos desde hace $anios y además no tienen evoluciones desde hace $anios o no tinen evoluciones.
Poner guardar en 1 para guardar un backup en $ruta
Retorna menos 1 cuando no pudo completar el borrado debido a error en archivo*/
function borrarPacientesInactivos1($anios,$guardar=0,$ruta){
	//echo "Borrando pacientes inactivos1<br>";
	$tableInactivos1=damePacientesInactivos1($anios);
	$totalBorrados=0;
	while ($registroInactivo1=$tableInactivos1->sql_fetch_object()){
		if($guardar==1){
			$guardado=guardarHC($registroInactivo1->pac_id,$ruta);
			if(!$guardado){
				//echo "Error creando archivo<br>";
				return -1;
			}
		}
		borrarPaciente($registroInactivo1->pac_id);
		$totalBorrados++;
	}
	//echo "Finalizando borrado 1<br>";
	return $totalBorrados;
}
/*Borra los pacientes quienes no tienen evoluciones desde hace $anios y además no tienen pagos desde hace $anios o no tinen pagos.
Poner guardar en 1 para guardar un backup en $ruta
Retorna menos 1 cuando no pudo completar el borrado debido a error en archivo*/
function borrarPacientesInactivos2($anios,$guardar=0,$ruta){
	$tableInactivos2=damePacientesInactivos2($anios);
	$totalBorrados=0;
	while ($registroInactivo2=$tableInactivos2->sql_fetch_object()){
		if($guardar==1){
			$guardado=guardarHC($registroInactivo2->pac_id,$ruta);
			if(!$guardado)
				return -1;
		}
		borrarPaciente($registroInactivo2->pac_id);
		$totalBorrados++;
	}
	return $totalBorrados;
}

function borrarPaciente($id_paciente){
	eliminarPrimerNivel("antecedente","pac_id",$id_paciente);
	eliminarPrimerNivel("cita","pac_id",$id_paciente);
	eliminarPrimerNivel("datos_per_pac","pac_id",$id_paciente);
	eliminarPrimerNivel("deuda","pac_id",$id_paciente);
	eliminarPrimerNivel("docentes_encargados_historia","pac_id",$id_paciente);
	eliminarPrimerNivel("estado_cuenta","pac_id",$id_paciente);
	eliminarPrimerNivel("historia_clinica","pac_id",$id_paciente);
	eliminarPrimerNivel("odontograma","pac_id",$id_paciente);
	eliminarPrimerNivel("odontologos_encargados_historia","pac_id",$id_paciente);
	eliminarPrimerNivel("urgencias","pac_id",$id_paciente);
	eliminarPrimerNivel("periodontograma","pac_id",$id_paciente);
	
	eliminarSegundoNivel("cita_odont","ptoxcio","pac_id","cio_id",$id_paciente);
	eliminarPrimerNivel("cita_odont","pac_id",$id_paciente);
	
	
	eliminarSegundoTercerNivel("cita_rad","rx_realizado","estado_cuenta","pac_id","cir_id","rxr_id",$id_paciente);
	eliminarSegundoNivel("cita_rad","esrxrxr","pac_id","cir_id",$id_paciente);
	eliminarSegundoNivel("cita_rad","estxcir","pac_id","cir_id",$id_paciente);
	eliminarPrimerNivel("cita_rad","pac_id",$id_paciente);
	
	
	eliminarSegundoNivel("control_materiales","matxcom","pac_id","com_id",$id_paciente);
	eliminarPrimerNivel("control_materiales","pac_id",$id_paciente);
	
	eliminarSegundoNivel("diagnostico","diaxcod","pac_id","dia_id",$id_paciente);
	eliminarPrimerNivel("diagnostico","pac_id",$id_paciente);
	
	eliminarSegundoNivel("enf_adulto","adultoxcie","pac_id","adu_id",$id_paciente);
	eliminarPrimerNivel("enf_adulto","pac_id",$id_paciente);
	
	eliminarSegundoNivel("enf_familia","familiaxcie","pac_id","fam_id",$id_paciente);
	eliminarPrimerNivel("enf_familia","pac_id",$id_paciente);
	
	eliminarSegundoNivel("enf_nino","ninoxcie","pac_id","enf_id",$id_paciente);
	eliminarPrimerNivel("enf_nino","pac_id",$id_paciente);
	
	eliminarSegundoNivel("evolucion","procxevo","pac_id","evo_id",$id_paciente);
	eliminarSegundoNivel("evolucion","saldo_negativo","pac_id","evo_id",$id_paciente);
	eliminarPrimerNivel("evolucion","pac_id",$id_paciente);
	
	eliminarSegundoNivel("orden_lab","evolucion_lab","pac_id","old_id",$id_paciente);
	eliminarSegundoNivel("orden_lab","oldxpld","pac_id","old_id",$id_paciente);
	eliminarPrimerNivel("orden_lab","pac_id",$id_paciente);
	
	eliminarSegundoNivel("pagos","pago_cheque","pac_id","pag_id",$id_paciente);
	eliminarSegundoNivel("pagos","pago_credito","pac_id","pag_id",$id_paciente);
	eliminarSegundoNivel("pagos","pago_debito","pac_id","pag_id",$id_paciente);
	eliminarPrimerNivel("pagos","pac_id",$id_paciente);
	
	eliminarSegundoNivel("presupuesto","procxpresu","pac_id","pre_id",$id_paciente);
	eliminarPrimerNivel("presupuesto","pac_id",$id_paciente);
	
	eliminarSegundoTercerNivel("control_mat_area","matxcma","matxaba","pac_id","cma_id","mxa_id",$id_paciente);
	eliminarPrimerNivel("control_mat_area","pac_id",$id_paciente);
	
	eliminarPrimerNivel("paciente","pac_id",$id_paciente);
}


//Elimina todo lo de la tabla $tablaNiv1 que su campo $campoNiv1 sea igual al valor $valorCampoNiv1
function eliminarPrimerNivel($tablaNiv1, $campoNiv1, $valorCampoNiv1){
	$tabla=new my_DB();
	$query="DELETE FROM $tablaNiv1 WHERE $campoNiv1=$valorCampoNiv1";
	echo("<br>&nbsp;&nbsp;".$query);
	$tabla->sql_query($query);
}

//eliminarSegundoNivel("presupuesto","procxpresu","pac_id","pre_id",1);
//Busca todos los presupuestos cuyo pac_id es 1, luego borra todos los procxpresu los cuales corresponden a los presupuestos encontrados
function eliminarSegundoNivel($tablaNiv1, $tablaNiv2, $campoNiv1, $campoNiv2, $valorCampoNiv1){
	$tabla=new my_DB();
	$query="SELECT * FROM $tablaNiv1 WHERE $campoNiv1=$valorCampoNiv1";
	$tabla->search($query);
	while ($registro=$tabla->sql_fetch_object()){
		$valor=$registro->$campoNiv2;
		eliminarPrimerNivel($tablaNiv2,$campoNiv2,$valor);
	}
}
//eliminarSegundoTercerNivel("control_mat_area","matxcma","matxaba",     "pac_id","cma_id","mxa_id",    1);
function eliminarSegundoTercerNivel($tablaNiv1, $tablaNiv2,$tablaNiv3, $campoNiv1, $campoNiv2,$campoNiv3, $valorCampoNiv1){
	$tabla=new my_DB();
	$query="SELECT * FROM $tablaNiv1 WHERE $campoNiv1=$valorCampoNiv1";
	$tabla->search($query);
	while ($registro=$tabla->sql_fetch_object()){
		$valor=$registro->$campoNiv2;
		eliminarSegundoNivel($tablaNiv2,$tablaNiv3,$campoNiv2,$campoNiv3,$valor);
		eliminarPrimerNivel($tablaNiv2,$campoNiv2,$valor);
	}
}
?>
