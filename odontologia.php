<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<!--****************************************************-->
<LINK rel="stylesheet" type="text/css" href="images/estilo.css">
<LINK rel="stylesheet" type="text/css" href="images/sdmenu.css">
<LINK rel="stylesheet" type="text/css" href="files_acordeon/estilo.css">
<link rel="stylesheet" type="text/css" href="estilos1.css">
<!--****************************************************-->
<head>

<title>Registro pacientes</title>
	<script type="text/javascript" src="ManResp.js" language="javascript"></script>
	<script src="files_acordeon/jquery.js" type="text/javascript" charset="utf-8">
    </script><!--script type="text/javascript" src="menusup/menu.js">
    </script-->	
	<script type="text/javascript" charset="utf-8">
	$(function(){
		$('#menu li a').click(function(event){
			var elem = $(this).next();
			if(elem.is('ul')){
				event.preventDefault();
				$('#menu ul:visible').not(elem).slideUp();
				elem.slideToggle();
			}
		});
	});
	</script>

<meta http-equiv="Content-Type" content="text/html;" charset="iso-8859-1" />
<!--meta name="SIAAP"  content="text/html;" http-equiv="content-type" charset="utf-8"/-->
</head>
<body>
<div id="todo">
<!--*********************************************************************************-->

	<!--********************************************************************
	Imagenes para la GUI-->
    <div id="imagen1">
	<?$mes=date("m");
	if($mes!=12){?>
        	<img id="Odontologia_02" src="images/Odontologia_02.png" width="895" height="214" alt="" />
	<?}else{?>
        	<img id="Odontologia_02" src="images/Odontologia_02_Navidad.png" width="895" height="214" alt="" />
	<?}?>
    </div>
    <div id="imagen2">
        <img id="Odontologia_04" src="images/Odontologia_04.png" width="208" height="34" alt="" />
    </div>
    <div id="imagen3">
        <img id="Odontologia_10" src="images/Odontologia_10.png" width="189" height="52" alt="" />
    </div>
    <div id="imagen4">
        <img id="Odontologia_08" src="images/Odontologia_08.png" width="111" height="34" alt="" />
    </div>
	<!--FIN Imagenes para la GUI
	********************************************************************-->
	
	<!--********************************************************************
	Menu izquierdo de opciones, depende si es de HC o general-->
	<?php
	if ( $opc=="antecedente" || $opc=="historia" || $opc=="odontograma" || $opc=="enf_nino" || ($opc=="enf_adulto") || ($opc=="enf_familia") || 
	($opc==paciente && ($s_opc==submenu || $s_opc==datospersonales ||$s_opc==datospersonales1 ||$s_opc==crear1||$s_opc==crear2 || $s_opc==visualizardp ||$s_opc==visualizardp1 || $s_opc==editar1 )) ||
	$opc=="presupuesto" || $opc=="diagnostico" || $opc=="periodontograma" || $opc=="control_materiales" || $opc=="evolucion" || ($opc==laboratorio && ($s_opc==autorizar  || $s_opc==crear || $s_opc=="crear1"  || $s_opc=="crear2" || $s_opc=="crear4" || $s_opc==delete || $s_opc==delete1 || $s_opc==delete2  || $s_opc==docautorizar  || 
	$s_opc==evolucion || $s_opc==labautorizar  || $s_opc==listar  || $s_opc==observacionold ||  $s_opc==pld || $s_opc==visualizar ||  $s_opc==visualizar1 || $s_opc==modificar  || $s_opc==modificar2 || $s_opc==modificar3|| 
	$s_opc==visualizarevo || $s_opc==visualizarevo1 || $s_opc==editar2 || $s_opc==editar3 || $s_opc==ajustarorden || $s_opc==ajustarorden1 || $s_opc=="default"))
	|| ($opc=="urgencias" && ($s_opc!="insertarsnurgencia" && $s_opc!="crearsnurgencia"))) {
		include('menuhistorianew.php');
	}else {
		include("menunew.php");
	}?>
	<!--FIN Menu izquierdo de opciones
	********************************************************************-->
    
    

	<!--********************************************************************
	Contenido mensaje a usuario-->
    <div class="mensaje">
	<?
	$usuario=$user->datos;
	$query ="select * from usuario where usu_id=$usuario->usu_id";
	$table->search($query);
	$usu=$table->sql_fetch_object();
//Mensajes para los usuarios
	/*if($usuario->tiu_id==1 && $usu->usu_id==854){
		include("funcionesAdmin.php");
		$tabla2=tieneBackupPendiente($usuario->usu_id);
		if($tabla2->nfound>0){
			?>
			<script>
				alert("Tiene backup pendiente, por favor aceptarlo o cancelarlo");
			</script>
			<?
		}
	}*/
//
	if ($usu->usu_id == 700 || $usu->usu_id==854){
		$fecha_anterior = calcularFecha(-7);
		$query = "select count(*) from pedido where \"ped_fechaSolicitud\" >= '$fecha_anterior' and \"ped_despachado\" is null";
		$table->search($query);
		//echo $query;
		$pedido=$table->sql_fetch_object();
		//echo ".....".$pedido->count."...";
		if ($pedido->count > 0){
			$query2 = "select \"bod_nombreBodega\" from bodega b, pedido p where b.bod_id = p.bod_id and p.ped_despachado is null 
			and ped_acentado='1' and \"ped_fechaSolicitud\" >= '$fecha_anterior' order by ped_id";
			//echo $query2;
			$table->search($query2);
			while ($pedido2=$table->sql_fetch_object()){
				$npedidos+=1;
				$nompedidos.=($pedido2->bod_nombreBodega.", ");
			}
		}
	}?>
    	Bienvenido(a)&nbsp;<?="$usu->usu_nombres  $usu->usu_apellidos"?>
	<?if($npedidos>0){
		?><span class="mensaje1">
		<a href="index.php?opc=pedido&s_opc=semanal">Despachar <?=$npedidos?> pedidos pendientes en <?=substr($nompedidos,0,40)."..."?><img src="images/mensaje.png" /></a></span>
		<?
	}
	if($usuario->tiu_id==1 && $usu->usu_id==854){
		include("funcionesAdmin.php");
		$tabla2=tieneBackupPendiente($usuario->usu_id);
		if($tabla2->nfound>0){
			?>			
			<span class="mensaje1"><a href="index.php?opc=backup&s_opc=backup">Tiene solicitudes de Backup
			<img src="images/mensaje.png" /></a></span>
			<?
		}
	}
	?>
    </div>
	<!--FIN Contenido mensaje a usuario
	********************************************************************-->
	
    

	<!--********************************************************************
	Contenido de la pagina-->
	<div class="cuerpo" id="contenido">
	<noscript>
		<h1>Debe habilitar Javascript para una correcta visualizaci&oacute;n</h1>
	</noscript>
		<?php
		
		/*if($usu->usu_id==854){
			echo '<a href="index.php?xxx=probando&idBorrar=32949">Prueba Borrando Paciente</a><br />';
			echo '<a href="index.php?yyy=probando&idBorrar=38665934-3&idBeneficiario=1114954543">Cambiar pagos</a><br />';
			echo '<a href="index.php?zzz=probando&pac_idBorrar=30623&pac_idBeneficiario=20726">Cambiar pagos</a><br />';
			if($xxx=='probando'){
				include('_borrarPaciente.php');
			}
			if($yyy){
				include('_cambioPagos.php');
			}
			if($zzz){
				include('_cambioPagos2.php');
			}
			unset($xxx);
			unset($yyy);
			unset($zzz);
		}*/
		if ($opc){
			include("./modules/$opc.php");
		}else{
			include("bienvenida.php");
		}
		?>

            <!--********************************************************************
            Pie de pagina-->
            <div id="piepagina">
              <hr color="#FF0000" />
                      Universidad del Valle - Sede San Fernando<br/>
                      Escuela de Odontolog&iacute;a<br/>
                      Cali - Colombia<br/>

            </div>
            <!--FIN Pie de pagina
            ********************************************************************-->
	</div>
	<!--FIN Contenido de la pagina
	********************************************************************-->
    
	<!--********************************************************************
	Menu Barra Superior-->
    <div id="menubar">
			<ul id="navmenu">
				<li><a href="javascript:void()" target="_self">Cuenta</a>
					<ul>
						<li class="expand"><a href="#" target="_self">HV</a>
							<ul>
								<li class="start"><a href="index.php?opc=personal&s_opc=busqueda" target="_self">Registrar</a></li>
								<li><a href="index.php?opc=personal&s_opc=busqueda1" target="_self">Actualizar</a></li>
								<li class="end"><a href="index.php?opc=personal&s_opc=busqueda2" target="_self">Consultar</a></li>
							</ul>
						</li>
						<li class="end"><a href="index.php?opc=usuario&s_opc=cambiopassword" target="_self">Cambiar password</a></li>
					</ul>
				</li>
                <li><a href="#">Sistema</a>
                    <ul>
                        <li class="expand"><a href="#">Usuarios</a>
                        	<ul>
                            	<li class="start"><a href="index.php?opc=usuario&s_opc=default" target="_self">Crear</a></li>
                            	<li><a href="index.php?opc=usuario&s_opc=busqueda" target="_self">Consultar</a></li>
				<li class="end"><a href="index.php?opc=usuario&s_opc=autorizarfact" target="_self">Autorizar Fact.</a></li>
                            </ul>
                        </li>
                        <li class="expand"><a href="#">Procedimiento</a>
                        	<ul>
                            	<li class="start"><a href="index.php?opc=procedimiento&s_opc=default" target="_self">Crear</a></li>
                            	<li class="end"><a href="index.php?opc=procedimiento&s_opc=busqueda" target="_self">Consultar</a></li>
                            </ul>
                        </li>
                        <li class="expand"><a href="#">Diagn&oacute;stico</a>
                        	<ul>
                            	<li class="start"><a href="index.php?opc=codDiagnostico&s_opc=default" target="_self">Crear</a></li>
                            	<li class="end"><a href="index.php?opc=codDiagnostico&s_opc=busqueda" target="_self">Consultar</a></li>
                            </ul>
                        </li>
                        <li><a href="index.php?opc=procedimiento&s_opc=busqueda1">Consultar CIE</a></li>
                        <li><a href="index.php?opc=rips&s_opc=default">Generar RIPS</a></li>
			<?
			//Si el tipo de usuario es un administrador poder mostrar esta opcion
			if($usuario->tiu_id==1){
             			?>
                        <li class="expand"><a href="#">Backup</a>
                        	<ul>
                            	<li class="start"><a href="index.php?opc=backup&s_opc=backup" target="_self">Realizar Backups</a></li>
                            	<li class="end"><a href="index.php?opc=backup&s_opc=visualizarbackups" target="_self">Ver backups</a></li>
                            </ul>
                        </li>
                        <li><a href="index.php?opc=materiales&s_opc=vaciarbodega">Vaciar Bodegas</a></li>
				<?
         		}?>
                        <li><a href="Manual.pdf" target="_blank">Ayuda<img src="images/pregunta.gif"/></a></li>
                        <li><a href="ayudaSIAAP.pdf" target="_blank">Ayuda <font color="#FF0000">(new)</font><img src="images/pregunta.gif"/></a></li>
                        <li class="end"><a href="logout.php">Salir<img src="images/salir2.gif"/></a></li>
                    </ul>
            	</li>		
            </ul>
            
		</div>
	<!--FIN Menu Barra Superior
	********************************************************************-->
	
	


    
    <!--********************************************************************
	Desarrollado-->
    <div id="supder">
	Desarrollado por Damian
    </div>
    <!--FIN Desarrollado
    ********************************************************************-->
    
    
	
<!--*********************************************************************************-->

</div>
</body>
</html>