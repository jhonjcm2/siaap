<!--********************************************************************
	Menu izquierdo de opciones, depende si es de HC o general-->
    <div id="divmenu">
        <ul id="menu">
            <li><a href="#">Paciente</a>
                <ul>
                    <li><a href="index.php?opc=paciente&s_opc=validacion">Registrar Paciente</a></li>
                    <li><a href="index.php?opc=paciente&s_opc=busqueda1">Consultar Paciente</a></li>
                    <li><a href="index.php?opc=paciente&s_opc=busqueda3">Consultar X &Aacute;rea</a></li>
                </ul>
            </li>
            <li><a href="#">Cita Odont&oacute;logo</a>
                <ul>
                    <li><a href="index.php?opc=cita_odont&s_opc=validacion" >Asignar Cita </a></li>
                    <li><a href="index.php?opc=cita_odont&s_opc=busqueda">Consultar Cita x Fecha</a></li>
                    <li><a href="index.php?opc=cita_odont&s_opc=busqueda2" >Consultar Cita x Estado</a></li>
                    <li><a href="index.php?opc=cita_odont&s_opc=busqueda1" >Consultar Cita x &Aacute;rea</a></li>
                    <li><a href="index.php?opc=cita_odont&s_opc=busqueda3" >Consultar Cita x Odont</a></li>
                    <li><a href="index.php?opc=cita_odont&s_opc=busqueda4" >Consultar Cita Paciente</a></li>
                    
                </ul>
            </li>
            <li><a href="#">Citas Pregrado</a>
                <ul>
                    <li><a href="index.php?opc=horarios&s_opc=default" > Ingresar Horario </a></li>
                    <li><a href='#'  > Consultar Horario</a></li>
                    <li><a href="index.php?opc=cita&s_opc=busqueda1" > Consultar Cita</a></li>
                    <li><a href="index.php?opc=cita&s_opc=busqueda2" > Consultar No. Citas</a></li>
                    
                </ul>
            </li>
            <li><a href="#">Facturaci&oacute;n</a>
            	<ul>
                	<li><a href="index.php?opc=pagos&s_opc=validacion" >Realizar Pagos</a></li>
                    <li><a href="index.php?opc=pagos&s_opc=busqueda" >Consultar Pagos</a></li>
                    <li><a href="index.php?opc=pagos&s_opc=busqueda1" >Estado Cuenta  Paciente</a></li>
                    <li><a href="index.php?opc=pagos&s_opc=busqueda2" >Estado Cuenta Odont&oacute;logo</a></li>
                    <li><a href="index.php?opc=pagos&s_opc=busqueda3" >Pagos Por &Aacute;rea</a></li>
                    <li><a href="index.php?opc=pagos&s_opc=busqueda4" >Pagos Por Per&iacute;odo</a></li>
                    <li><a href="index.php?opc=pagos&s_opc=validacion1" >Realizar Pagos1</a></li>
                    
                </ul>
            </li>
            <li><a href="#">Materiales</a>
            	<ul>
                	<li><a href="index.php?opc=materiales" >Registrar Materiales</a></li>
                    <li><a href="index.php?opc=materiales&s_opc=busqueda" >Consultar Material </a></li>
                    <li><a href="index.php?opc=materiales&s_opc=busqueda1" >Despachar Control. Mat.</a></li>
                    <li><a href="index.php?opc=pedido&s_opc=default" >Realizar Pedido</a></li>
                    <li><a href="index.php?opc=pedido&s_opc=busqueda1" >Consultar Pedido</a></li>
                    <li><a href="index.php?opc=pedido&s_opc=busqueda2" >Consultar Pedido x Per&iacute;odo</a></li>
                    <li><a href="index.php?opc=pedido&s_opc=busqueda" >Despachar Pedido</a></li>
                    <li><a href="index.php?opc=abastecimiento&s_opc=default" >Abastecer Almac&eacute;n</a></li>
                    <li><a href="index.php?opc=abastecimiento&s_opc=busqueda1" >Consultar Mat x Bod</a></li>
                    <li><a href="index.php?opc=abastecimiento&s_opc=busqueda" >Consultar Mat x Bod x &Aacute;rea</a></li>
                </ul>
            </li>
            <li><a href="#">Laboratorio</a>
            	<ul>
                	<li><a href="index.php?opc=laboratorio&s_opc=crearpld" >Registrar Pto. Lab.</a></li>
                    <li><a href="index.php?opc=laboratorio&s_opc=busqueda2" >Consultar Pto. Lab.</a></li>
                    <li><a href="index.php?opc=laboratorio&s_opc=busqueda1" >Consultar Orden de Lab.</a></li>
                    <li><a href="index.php?opc=laboratorio&s_opc=busqueda3" >Ordenes X Odont/Est.</a></li>
                    
                </ul>
            </li>
            <li><a href="#">Radiolog&iacute;a</a>
            	<ul>
                	<li><a href="index.php?opc=radiologia&s_opc=crearest" >Registrar Estudio</a></li>
                    <li><a href="index.php?opc=radiologia&s_opc=busqueda3" >Consultar Estudio</a></li>
                    <li><a href="index.php?opc=radiologia&s_opc=validacion" >Asignar Cita</a></li>
                    <li><a href="index.php?opc=radiologia&s_opc=busqueda" >Consultar Cita</a></li>
                    <li><a href="index.php?opc=radiologia&s_opc=busqueda1" >Consultar Est. Realizados</a></li>
                	<li><a href="index.php?opc=radiologia&s_opc=busqueda2" >Generar Informe</a></li>
                </ul>
            </li>
            <li><a href="#">Comunitaria</a>
            	<ul>
                	<li><a href="index.php?opc=comunitaria&s_opc=entidad" >Registrar Entidad</a></li>
                    <li><a href="index.php?opc=comunitaria&s_opc=busqueda" >Consultar Entidad</a></li>
                    <li><a href="index.php?opc=comunitaria&s_opc=cmtescuela" >Registrar Datos Escuela</a></li>
                	<li><a href="index.php?opc=comunitaria&s_opc=busqueda1" >Consultar Datos Escuela</a></li>
                    <li><a href="index.php?opc=comunitaria&s_opc=cmtladanc" >Registrar Datos ESE / Ancianato</a></li>
                    <li><a href="index.php?opc=comunitaria&s_opc=busqueda2" >Consultar Datos ESE / Ancianato</a></li>
                </ul>
            </li>
            <li><a href="#">Auxiliares</a>
            	<ul>
                	<li><a href="index.php?opc=auxiliares&s_opc=default" >Registrar Asignatura</a></li>
                    <li><a href="index.php?opc=auxiliares&s_opc=busqueda" >Consultar Asignatura</a></li>
                    <li><a href="index.php?opc=auxiliares&s_opc=busqueda1" >Consultar Eva. Asigt</a></li>
                	<li><a href="index.php?opc=auxiliares&s_opc=busqueda3" >Consultar Eva. X Docente</a></li>
                    <li><a href="index.php?opc=auxiliares&s_opc=busqueda4" >Consultar Eva. X Periodo</a></li>
                    <li><a href="index.php?opc=auxiliares&s_opc=busqueda5" >Consultar Eva. X Estudiante</a></li>
                </ul>
            </li>
            <li><a href="#">Saldo</a>
            	<ul>
                	<li><a href="index.php?opc=saldo&s_opc=busqueda" >Consultar Saldo paciente</a></li>
                    <li><a href="index.php?opc=saldo&s_opc=insertarsn" >Insertar Deuda(Temporal)</a></li>
                </ul>
            </li>
        </ul>
    </div>
	<!--FIN Menu izquierdo de opciones
	********************************************************************-->