<?//$datos=$user->datos;
	$pac = new paciente($pac_id);
	$paciente = $pac->data;
	
	$query= "SELECT * FROM paciente WHERE pac_id=$pac_id";
	$table->search($query);
	$paciente = $table->sql_fetch_object();
	$usuario=$user->datos;
?>
<!--********************************************************************
	Menu izquierdo de opciones, depende si es de HC o general-->
    <div id="divmenu">
        <ul id="menu">
            <li><a href="#">Datos Paciente</a>
                <ul>
                    <li><a href="index.php?opc=paciente&s_opc=datospersonales&pac_id=<?=$pac_id?>">Ingresar</a></li>
                    <li><a href="index.php?opc=paciente&s_opc=visualizardp&pac_id=<?=$pac_id?>">Consultar</a></li>
                    <li><a href="index.php?opc=urgencias&s_opc=listar&pac_id=<?=$pac_id?>">Consultar Emergencia</a></li>
                </ul>
            </li>
            <li><a href="#">Anamnesis</a>
                <ul>
                    <li><a href="index.php?opc=historia&s_opc=default&pac_id=<?=$pac_id ?>">Ingresar</a></li>
                    <li><a href="index.php?opc=historia&s_opc=visualizar&pac_id=<?=$pac_id ?>">Consultar</a></li>
                    
                </ul>
            </li>
            <li><a href="#">Antecedentes Grales.</a>
                <ul>
                    <li><a href="index.php?opc=antecedente&s_opc=antecedentes&pac_id=<?=$pac_id ?>"  >Ant. Sociales</a></li>
                    <li><a href="index.php?opc=antecedente&s_opc=antecedentes1&pac_id=<?=$pac_id ?>"  >Ant. Med. Personales</a></li>
                    <li><a href="index.php?opc=enf_nino&s_opc=default&pac_id=<?=$pac_id ?>"  >Enf. Propias Ni&ntilde;ez</a></li>
                    <li><a href="index.php?opc=enf_adulto&s_opc=default&pac_id=<?=$pac_id ?>"  >Enf. Propias Adulto</a></li>
                    <li><a href="index.php?opc=enf_familia&s_opc=default&pac_id=<?=$pac_id ?>" >Ant. Familiares</a></li>
                    <li><a href="index.php?opc=antecedente&s_opc=antecedentes4&pac_id=<?=$pac_id ?>"  >Ant. Odontol&oacute;gicos</a></li>
                    <li><a href="index.php?opc=antecedente&s_opc=antecedentes5&pac_id=<?=$pac_id ?>"  >Ant. Alergicos y Habitos</a></li>
                    <li><a href="index.php?opc=antecedente&s_opc=visualizar&pac_id=<?=$pac_id ?>"  > Consultar</a></li>
                    
                </ul>
            </li>
            <li><a href="#">Revisi&oacute;n por Sistema</a>
            	<ul>
                	
                    <li><a href="index.php?opc=historia&s_opc=historia1&pac_id=<?=$pac_id ?>"> Ingresar</a></li>
                    <li><a href="index.php?opc=historia&s_opc=visualizar1&pac_id=<?=$pac_id ?>"> Consultar</a></li>
                    <li></li>
                    
                </ul>
            </li>
            <li><a href="#">Ex&aacute;men F&iacute;s. Gral.</a>
            	<ul>
                    <li><a href="index.php?opc=historia&s_opc=historia2&pac_id=<?=$pac_id ?>">Ingresar</a></li>
                    <li><a href="index.php?opc=historia&s_opc=visualizar2&pac_id=<?=$pac_id ?>">Consultar</a></li>
                </ul>
            </li>
            <li><a href="#">Ex&aacute;men Estomatol&oacute;gico</a>
            	<ul>
                    <li><a href="index.php?opc=historia&s_opc=historia3&pac_id=<?=$pac_id ?>"  >Ingresar Exa. Extra Oral</a></li>
                    <li><a href="index.php?opc=historia&s_opc=visualizar3&pac_id=<?=$pac_id ?>"  >Consultar Extra Oral</a></li>
                    <li><a href="index.php?opc=historia&s_opc=historia4&pac_id=<?=$pac_id ?>"  >Ingresar Exa. Intra Oral</a></li>
					<li><a href="index.php?opc=historia&s_opc=visualizar4&pac_id=<?=$pac_id ?>"  >Consultar Intra Oral</a></li>
                </ul>
            </li>
            <li><a href="#">Odontograma</a>
            	<ul>
                    <li><a href="index.php?opc=odontograma&s_opc=inicial&pac_id=<?=$pac_id ?>"  >Ingresar Odont. Inicial </a></li>
                    <li><a href="index.php?opc=odontograma&s_opc=listar&pac_id=<?=$pac_id ?>&tio_id=1"  >Ingresar Odont. desarrollo </a></li>
                </ul>
            </li>
            <li><a href="#">Periodontograma</a>
            	<ul>
                    <li><a href="index.php?opc=periodontograma&s_opc=default&pac_id=<?=$pac_id ?>"  >Ingresar Periodontograma</a></li>
                    <li><a href="index.php?opc=periodontograma&s_opc=reevaluacion&pac_id=<?=$pac_id ?>"  >Reevaluaci&oacute;n Periodontal</a></li>
                    <li><a href="index.php?opc=periodontograma&s_opc=listar&pac_id=<?=$pac_id ?>"  >Consultar Periodontograma </a></li>
                </ul>
            </li>
            <li><a href="#">Diagn&oacute;stico</a>
            	<ul>
                    <li><a href="index.php?opc=diagnostico&s_opc=default&pac_id=<?=$pac_id ?>"  >Ingresar</a></li>
                    <li><a href="index.php?opc=diagnostico&s_opc=listar&pac_id=<?=$pac_id ?>"  >Consultar</a></li>
                </ul>
            </li>
            <li><a href="#">Presupuesto</a>
            	<ul>
                    <li><a href="index.php?opc=presupuesto&s_opc=default&pac_id=<?=$pac_id ?>"  >Ingresar</a></li>
                    <li><a href="index.php?opc=presupuesto&s_opc=listar&pac_id=<?=$pac_id ?>"  >Consultar</a></li>
                    <li></li>
                </ul>
            </li>
            <li><a href="#">Control de Materiales</a>
            	<ul>
                    <li><a href="index.php?opc=control_materiales&s_opc=default&pac_id=<?=$pac_id ?>">Ingresar</a></li>
                    <li><a href="index.php?opc=control_materiales&s_opc=listar&pac_id=<?=$pac_id ?>">Consultar</a></li>
                    <li><a href="index.php?opc=control_materiales&s_opc=devolucion&pac_id=<?=$pac_id ?>"> Devolver</a></li>
                </ul>
            </li>
            <li><a href="#">Evoluci&oacute;n</a>
            	<ul>
                    <li><a href="index.php?opc=evolucion&s_opc=default&pac_id=<?=$pac_id ?>"  >Ingresar </a></li>
                    <li><a href="index.php?opc=evolucion&s_opc=listar&pac_id=<?=$pac_id ?>"  >Consultar </a></li>
                </ul>
            </li>
            <li><a href="#">Laboratorio Dental</a>
            	<ul>
                    <li><a href="index.php?opc=laboratorio&s_opc=default&pac_id=<?=$pac_id ?>"  >Elaborar Orden</a></li>
                    <li><a href="index.php?opc=laboratorio&s_opc=listar&pac_id=<?=$pac_id ?>"  >Consultar Orden</a></li>
                </ul>
            </li>
            <li><a href="index.php">Men&uacute; Principal</a>
            </li>
        </ul>
    </div>
	<!--FIN Menu izquierdo de opciones
	********************************************************************-->