<?
/*
--------------ESTA PAGINA MUESTRA LOS RESULTADOS DE LOS SALDOS DE UN PACIENTE
--------------RECIBE EL ID DEL PACIENTE (pac_id)
--------------SE UBICA EN MODULES/SALDO/DEFAULT.PHP
*/
?>

<?
$pac_fechaNacimiento="$fn_dia-$fn_mes-$fn_anio";
if (!$table){
?><p>Error en la conexi&oacute;n con la base de datos<br>
Consulte con el administrador del sistema</p>
	<?include('busqueda.php');
}else{
	$miSaldo = new Saldo();
	if($miSaldo->calcularSaldos($pac_id)>0){
?>
<p class="titulo1">Consulta de Saldo de pacientes</p>
<div style="width: 80%; margin: 20px 20px 20px 20px">
<table>
	<tr>
		<td><b>Paciente:</b></td>
		<td><?=$miSaldo->paciente["nombres"]." ".$miSaldo->paciente["apellidos"]?></td>
	</tr>
	<tr>
		<td><b>Identificaci&oacute;n:</b></td>
		<td><?=$miSaldo->paciente["pac_numeroIdentificacion"]?></td>
	</tr>
	<tr>
		<td><b>Cargos del Paciente(Deudas):</b></td>
		<td>$<?echo number_format($miSaldo->saldoNegativoPaciente, 0, '', '.')?></td>
	</tr>
	<tr>
		<td><b>Pagos del Paciente(Pagos):</b></td>
		<td>$<?echo number_format($miSaldo->pagoTotalPaciente, 0, '', '.')?></td>
	</tr>
	<tr>
		<td><b>Saldo neto del Paciente:</b></td>
		<td>$<b><font <?if(($miSaldo->saldoNetoPaciente)<0){echo 'color="Red"';}else if(($miSaldo->saldoNetoPaciente)>0){echo 'color="Green"';}?>>
		<?echo number_format($miSaldo->saldoNetoPaciente, 0, '', '.')?>
			<?if(($miSaldo->saldoNetoPaciente)<0){echo '(Paciente debe)';}else if(($miSaldo->saldoNetoPaciente)>0){echo '(Paciente con saldo a favor)';}?></font></b>
		</td>
	</tr>
	<tr>
		<td><a href="index.php?opc=saldo&s_opc=visualizardeudas&pac_id=<?=$miSaldo->paciente["pac_id"];?>" target="_self">Ver todas las deudas</a></td>
		<td></td>
	</tr>
	<tr>
		<td><a href="index.php?opc=pagos&s_opc=listar3&pac_numeroIdentificacion=<?=$miSaldo->paciente["pac_numeroIdentificacion"];?>" target="_self">Ver todos los pagos</a></td>
		<td></td>
	</tr>
</table>
</div>
<?if($usu->usu_id==854){echo '<p class="mensaje1">ID del paciente en sistema:'.$pac_id.'</p>';}?>
<?
	$query='SELECT evo.evo_id, evo.evo_fecha, sn.sn_valor, sn.sn_id , pac.pac_id, pac.pac_nombres, pac.pac_apellidos, pac."pac_numeroIdentificacion"
	FROM saldo_negativo as sn, evolucion as evo, paciente as pac
	WHERE sn.evo_id=evo.evo_id AND pac.pac_id=evo.pac_id AND pac.pac_id='.$pac_id.
	'ORDER BY evo_fecha DESC,sn.sn_id DESC';
	$table->search($query);
	$data = array();
	$num=0;
	$i=1;
	while($object = $table->sql_fetch_object() ){
		array_push($data,$object);
		$i++;
		if($i>10)
			break;
	}
	if($table->nfound>0){
?>
	<script language="javascript" >
	<!--
	function traerUsuOdl(id_evolucion){
	
	url="aux.php?opc=saldo&s_opc=detallevo&evo_id="+id_evolucion;
	open(url,'usuario', "width=700, height=300, resizable=no, scrollbars=yes, menubar=no, toolbar=no, location=no, status=no");
	
	
	}
	-->
	</script>
		&Uacute;ltimas 10 deudas ingresadas
		<table class="listado">
				<tr class="cabecera">
					<td>N&uacute;mero</br> Evoluci&oacute;n</td>
					<td>Valor</td>
					<td>Fecha de ingreso</td>
					<td>Mostrar detalles</td>
				</tr>
					<? $num=0;$No=0;
					foreach($data as $obj){
						$num = ($num) ? 0 : 1;

					?>
					<tr <?=($num)?' class="par"':' class="impar"'?>>
						
						<td class="centrado">
							<?php echo $obj->evo_id?>
						</td>
						<td class="centrado">$
							<?php echo number_format($obj->sn_valor, 0, '', '.')?>
						</td>
						<td>
							<?php echo $obj->evo_fecha?>
						</td>
						<td class="centrado"><a href="#" onclick="javascript:traerUsuOdl(<?=$obj->evo_id;?>)">Visualizar</a>
						</td>
						
					</tr>
					<? } ?>
			</table>
<?
		}else{
			echo "No se encontraron deudas ingresadas a este paciente<br />";
		}
	}else{
		echo "El paciente NO EXISTE";
	}
}
?>





