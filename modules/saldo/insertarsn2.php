<?
/*
MODULO AL QUE PUEDEN INGRESAR SOLO los del archivo y que sean recaudadores
(bod_id=26 y tiu_id=9)

nombre del formulario: paciente
campo1: pac_numeroIdentificacion
monto: sn_valor
descripcion de la deuda: descripcion
numero del recibo: num_recibo
*/
$usuario = $user->datos;
	//if($user->estaAutorizadoFact($usuario->usu_id) && $usuario->bod_id==26){
	if($usuario->tiu_id==1 || ($user->estaAutorizadoFact($usuario->usu_id) && $usuario->bod_id==26) ){
?>


<script language="JavaScript1.2"><!--

function asignarValoresOcultos(idPac,nom, ape, valor, descr, nRecibo){
	document.paciente.pac_nombre.value=nom;
	document.paciente.pac_apellido.value=ape;
	document.paciente.pac_id.value = idPac;
	document.paciente.sn_valor2.value = valor;
	document.paciente.sn_valor.value = valor;
	document.paciente.descripcion2.value = "PROCEDIMIENTOS:"+descr+" (RECIBO No. "+nRecibo.substring(1,nRecibo.length)+")";
	document.paciente.descripcion.value = document.paciente.descripcion2.value;
	document.paciente.num_recibo.value = nRecibo;
	document.getElementById('datos').innerHTML="<b>Nombre del paciente: </b>"+nom+"<br/><b>Apellidos:</b>"+ape+"<br/>";
}
function traerPac(){
	url="busquedapacxid.php?opc=pagos&s_opc=pac&pac_numeroIdentificacion="+document.paciente.pac_numeroIdentificacion.value;
	open(url,'Paciente', "width=600, height=500, resizable=no, scrollbars=yes, menubar=no, toolbar=no, location=no, status=no");
}

function traerPago(){
	url="aux.php?opc=saldo&s_opc=busquedapacxpago&pac_numeroIdentificacion="+document.paciente.pac_numeroIdentificacion.value+
	"&num_recibo="+document.paciente.num_recibo.value;
	open(url,'Pagos', "width=720, height=220, resizable=no, scrollbars=yes, menubar=no, toolbar=no, location=no, status=no");
}

function enviarDatos() {
	//pac_numeroIdentificacion,sn_valor,descripcion
	pac_id=document.getElementById("pac_id");
	pac_numeroIdentificacion=document.getElementById("pac_numeroIdentificacion");
	sn_valor=document.getElementById("sn_valor");
	descripcion=document.getElementById("descripcion");

	if(document.paciente.descripcion.value != document.paciente.descripcion2.value ||
	document.paciente.sn_valor.value != document.paciente.sn_valor2.value ){
		alert("ERROR: Valores no coincidentes.");
		return false;
	}
	
	if(pac_id.value==null || pac_numeroIdentificacion.value==null || sn_valor.value==null || descripcion.value==null){
		alert("ERROR: Alguno de los valores son nulos");
		return false;
	}else if(pac_numeroIdentificacion.value.length == 0 || sn_valor.value.length == 0 || descripcion.value.length == 0 || pac_id.value==""){
		alert("ERROR: Algun valor esta vacio");
		return false;
	}else if(/^\s+$/.test(pac_numeroIdentificacion.value) || /^\s+$/.test(descripcion.value) || /^\s+$/.test(sn_valor.value)){
		alert("ERROR: Solo ha ingresado espacios en blanco en algun campo");
		return false;
	}else if( isNaN(sn_valor.value) ) {
		alert("ERROR: Valor no numerico en el monto");
		return false;
	}else {
		if(confirm("Identificaci\xf3n: "+pac_numeroIdentificacion.value+"\n"+
			"Monto de la deuda:"+sn_valor.value+"\n \xbfDesea ingresar la deuda?")){
			return true;
		}else{
			return false;
		}
	}
}
// -->
</script>
		<table width="70%" border="0" cellspacing="8" cellpadding="0" align="left">
			<tr>
				<td>
					<form action="<?=$PHP_SELF?>" method="post" name="paciente" onsubmit="return enviarDatos()">
						<div align="center">
							<table border="0" cellspacing="0" cellpadding="0" align="center">
								<tr>
									<td>
										<div align="center">
											<b>Registro de Deuda<br>
												<br>
												<div align="center" id="datos">
													
												</div>
											</b></div>
										<table class="black" border="0" cellspacing="5" cellpadding="0">
											<tr>
												<td class="mensaje1" colspan="2">Debe digitar el monto, el n&uacute;mero de Indentificaci&oacute;n del paciente y el n&uacute;mero del recibo. Si los datos no son correctos no se encontrar&aacute;n resultados.</td>
											</tr>
											<tr height="12%">
												<td class="black" width="20%" height="12%"></td>
												<td class="black" nowrap height="12%"></td>
											</tr>
											<tr>
												<td class="black" nowrap width="20%">
													<p class="titulo2">N&uacute;mero Identificaci&oacute;n:</p>
												</td>
												<td>	<input type="text" name="pac_numeroIdentificacion" id="pac_numeroIdentificacion" size="31" border="0">
													<input type="button" value="..." onclick="javascript:traerPac()"></td>
											</tr>
											<tr>
												<td class="black" width="20%">
													<p class="titulo2">Recibo: </p>
												</td>
												<td class="black" nowrap><input type="text" name="num_recibo" id="num_recibo" size="31" border="0">
												<input type="button" value="..." onclick="javascript:traerPago()"></td>
											</tr>
											<tr>
												<td class="black" width="20%">
													<p class="titulo2">Monto: </p>
												</td>
												<td class="black" nowrap><input type="text" name="sn_valor2" id="sn_valor2" size="31" border="0" disabled="disabled" value=""><input type="hidden" name="sn_valor" id="sn_valor" value=""></td>
											</tr>
											<tr>
												<td class="black" width="20%">
												</td>
												<td class="black" nowrap>
													<p class="titulo2">Descripci&oacute;n de la deuda (si se desea especificar): </p>
														<textarea name="descripcion2" id="descripcion2" cols="50" rows="5" disabled="disabled">PROCEDIMIENTO </textarea><input type="hidden" name="descripcion" id="descripcion" value=""></td>
											</tr>
											<tr>
												<td class="black" width="20%"></td>
												<td class="black" align="right">
													<div align="center">
														<p><br>
															<input type="hidden" name="envio" id="envio" value="ok" border="0">
															<input type="hidden" name="pac_id" id="pac_id" value="" border="0">
															<input type="hidden" name="pac_nombre" id="pac_nombre" value="" border="0">
															<input type="hidden" name="pac_apellido" id="pac_apellido" value="" border="0">
															<input type="hidden" name="s_opc" id="s_opc" value="crearsn" border="0">
															<input type="hidden" name="opc" id="opc" value="<?=$opc?>" border="0">
															<input type="image" src="images/confrimar.gif" alt="" align="right" border="0"></p>
													</div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</div>
					</form>
				</td>
			</tr>
		</table>
<? 
}else{
		show_mess("<b>Ud, no est&aacute; autorizado para ingresar a &eacute;ste m&oacute;dulo</b>","alert");
	}
?>