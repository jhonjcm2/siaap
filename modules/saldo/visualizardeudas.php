<?
//visualizardeudas.php

$query="SELECT * FROM paciente WHERE pac_id=".$pac_id;
$table->search($query);
$paciente=$table->sql_fetch_object();


	$query='SELECT evo.evo_id, evo.evo_fecha, sn.sn_valor, sn.sn_id , pac.pac_id, pac.pac_nombres, pac.pac_apellidos, pac."pac_numeroIdentificacion"
	FROM saldo_negativo as sn, evolucion as evo, paciente as pac
	WHERE sn.evo_id=evo.evo_id AND pac.pac_id=evo.pac_id AND pac.pac_id='.$pac_id.
	' ORDER BY evo_fecha DESC,sn.sn_id DESC';
	$table->search($query);
	$data = array();
	//echo $query ;
	while($object = $table->sql_fetch_object() ){
		array_push($data,$object);
	}
?>

<script language="javascript" >
<!--
function traerUsuOdl(id_evolucion){

	url="aux.php?opc=saldo&s_opc=detallevo&evo_id="+id_evolucion;
	open(url,'usuario', "width=720, height=300, resizable=no, scrollbars=yes, menubar=no, toolbar=no, location=no, status=no");
}
-->
</script>
<div style="width: 80%; margin: 20px 20px 20px 20px">
<p class="titulo1">Deudas del Paciente</p>

<p><span class="titulo2">Nombre del Paciente: </span><?=($paciente->pac_nombres)." ".($paciente->pac_apellidos);?></p>

<p><span class="titulo2">N&uacute;mero de Identificaci&oacute;n: </span><?=$paciente->pac_numeroIdentificacion;?></p>

<p>A continuaci&oacute;n se muestra una lista con todas las obligaciones que ha contraido el paciente debido procedimientos practicados. Cada procedimiento est&aacute; amarrado a una evoluci&oacute;n.</p>

<?
if($table->nfound>0){
?>
		
		<table class="listado">
				<tr class="cabecera">
					<td>N&uacute;mero</br> Evoluci&oacute;n</td>
					<td>Valor</td>
					<td>Fecha de ingreso</td>
					<td>Mostrar detalles</td>
				</tr>
					<?
					foreach($data as $obj){
						$num = ($num) ? 0 : 1;

					?>
					<tr <?=($num)?' class="par"':' class="impar"'?>>
						
						<td class="centrado">
							<?php echo $obj->evo_id?>
						</td>
						<td class="centrado">$
							<?php echo number_format($obj->sn_valor, 0, '', '.')?>
						</td>
						<td>
							<?php echo $obj->evo_fecha?>
						</td>
						<td class="centrado"><a href="#" onclick="javascript:traerUsuOdl(<?=$obj->evo_id;?>)">Visualizar</a>
						</td>
					</tr>
					<? } ?>
			</table>
<?
		}else{
			show_mess("No se encontraron deudas ingresadas a este paciente<br />","info");
		}
?>
</div>