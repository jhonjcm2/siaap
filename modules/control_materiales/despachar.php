<?
/**	Permite despachar un control de materiales validando que sea un auxiliar(5)
	o que sea el administrador(1)*/
if ($envio) {
	$miusuario = new usuario();
	//Validar que sea Auxiliar
	$result = $miusuario->validarUsuario($login,$password,$pac_id,5);

	//Para el administrador
	if(!$result['valido'])
		$result=$miusuario->validarUsuario($login,$password,$pac_id,1,5);

	$usu_id = $result['usu_id'];

	if($result['valido']){
		//print_r ($GLOBALS);
		$com = new control_materiales($com_id);
		$com_data= $com->data;
		if ($com_data->com_devolucion=="f"){
			$result = $com->despachar($bod_id);
		}else{
			$result = $com->recibir();
		}
		if($result["mess_cod"]!="alert"){
			$query="UPDATE control_materiales SET com_despachado=$usu_id WHERE com_id=$com_id";
			$table->sql_query($query);
			?>
				<script language="Javascript">
				<!--
					alert("Los materiales de la devoluci\xf3n fueron recibidos con \xe9xito");
					window.opener.location.reload();
					close();
					
				-->
			</script>
			<?
		}else{
			$mess=$result['mess'];
		}
	}else{
		$mess = $result['mess'];
	}
}
 ?>
<html>

	<head>
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<title> Administrador</title>
		<script language="JavaScript1.2"><!--
function verificar_ingreso(aform) {
        // validar que no esten vacios los campos

        if (aform.login.value == "") {
                alert("Por favor escriba su Login");
                aform.login.focus();
                return false;
        }
        if (aform.password.value == "") {
                alert("Por favor escriba su Password");
                aform.password.focus();
                return false;
        }
}
// -->
		</script>
	   <link rel="stylesheet" href="estilos.css">
	</head>	
	<body bgcolor="white">

		<center>
			<br>
			<table border="0" cellpadding="0" cellspacing="1" bgcolor="#e30707">

				<tr>

					<td align="center" bgcolor="#e30707"><span class="white1"><b> Odontologia Univalle </b></span></td>

				</tr>

				<tr>

					<td align="center">

						<table border="0" cellpadding="1" cellspacing="1" width="100%">

							<tr>

								<td bgcolor="white" align="center" valign="middle" colspan="2">

									<table border="0" cellpadding="0" cellspacing="10" width="100%">

										<tr>

											<td align="center" valign="middle"><img src='images/logo.jpg' border="0" alt="Logo"></td>

											<td align="center" valign="middle">

												<p><?
if ($mess) {

 ?>	
																	<table border="1" cellpadding="3" cellspacing="0" width="100%">
																		<tr>
																			<td align="center"><span class="black"><?=$mess?></span></td>
																		</tr>
																	</table>
												<? 
 }
 ?><br>
												<span class="red"><b><span class="navy">Por favor ingrese el Login y el Password</span><br>
													</b></span>
												<form name="form" action="<?=$PHP_SELF?>" method="post" onsubmit="return verificar_ingreso(form)">
													<p></p>
													<table border="0" cellpadding="0" cellspacing="2">
														<tr>
															<td width="60"><span class="black1">Login</span></td>
															<td width="10"></td>
															<td><span class="normal2"><span class="general"><input type="text" name="login" size="30"></span></span></td>
															<td></td>
														</tr>
														<tr>
															<td width="60"><span class="black1">Password</span></td>
															<td width="10"></td>
															<td><span class="normal2"><span class="general"><input type="password" name="password" size="30"></span></span></td>
															<td></td>
														</tr>
													</table>
													<div align="right">
														<input type="hidden" value="Entrar" name="envio">
														<input type="hidden" value="control_materiales" name="opc">
														<input type="hidden" value="despachar" name="s_opc">
														<input type="hidden" value="<?=$tipo?>" name="tipo">
														<input type="hidden" value="<?=$pac_id?>" name="pac_id">
														<input type="hidden" value="<?=$com_id?>" name="com_id"> <input type="hidden" value="<?=$usuario->bod_id?>" name="bod_id"> <input src="images/entrar.gif" width="62" height="22" border="0" type="image" alt="Entrar"></div>
												</form>
											</td>

										</tr>

										<tr>

											<td align="center" valign="middle" colspan="2">

												<p><img src="images/PiePagina2.GIF" alt="" height="36" width="230" border="0"></p>

											</td>

										</tr>

									</table>

								</td>

							</tr>

						</table>

					</td>

				</tr>

			</table>
		</center>
	</body>
</html>