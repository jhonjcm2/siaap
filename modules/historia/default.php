
<?
if (!$table){
?>Error en la conexi&oacute;n con la base de datos<br>
     Consulte con el administrador del sistema</p>
	            
	<?include('historia.php');
}else{
	$usuario=$user->datos;			
	if($usuario->tiu_id!=1&& $usuario->tiu_id!=3){
             	show_mess("<b>Ud, no est&aacute; autorizado para ingresar a &eacute;ste m&oacute;dulo</b>","alert");
      }else{
	
   	$query= "SELECT * FROM paciente WHERE pac_id=$pac_id";
   	$table->search($query);
   	$paciente = $table->sql_fetch_object();
   	$historial=1;
   	if($paciente->ara_id==7){
   		$mess="<center><b>Una historia de urgencia no requiere estos datos.</b> <br><br>Contin&uacute;e en presupuesto.";
	   	show_mess($mess,"alert");
	}else{
   	 	$query= "SELECT * FROM datos_per_pac WHERE pac_id=$pac_id";
   		$table->search($query);
   		$estado=$table->sql_fetch_object();
   		if ($estado->dpp_estado<3){
   			$mess="<center><b>Los datos de la identificacion del paciente NO han sido ingresados.</b> <br><br>
   			Por favor ingreselos primero </center>";
   	 		show_mess($mess,"alert");
   	 		$historial=0;
		}
	}// else tip_id
	if ($historial){// Si el historial ya ha sido ingresado
	
		$query= "SELECT * FROM historia_clinica WHERE pac_id=$pac_id";
	   	$table->search($query);
	   	if ($table->nfound){
		   	$mess="<center><b>Los datos de la Anamnesis ya fueron ingresados.</b> <br><br>
		   	Por favor continue ingresando los datos de los Ant. Generales</center>";
		   	 	show_mess($mess,"alert");
	 	
		}else{
	   
				$pac = new paciente($pac_id);
				$obj = $pac->data;
	
				
				?>
		<table border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<div align="center">
						<h2 class="black1"></h2>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<form action="<?=$PHP_SELF?>" method="post" name="historia">
						<p></p>
						<table width="70%" border="0" cellspacing="5" cellpadding="0">
							<tr>
								<td width="10%"></td>
								<td width="30%"></td>
								<td class="black1" width="30%">
									<div align="right">Historia cl&iacute;nica No : <?=$paciente->pac_numeroIdentificacion?> </div>
								</td>
							</tr>
							<tr>
								<td width="10%"></td>
								<td width="30%"></td>
								<td class="black1" align="right" width="30%"><input type="hidden" name="hic_fechaAnamnesis" value="<?=date("Y-m-d",time())?>"><?=fecha(date("Y-m-d",time()))?></td>
							</tr>
							<tr>
								<td class="black1" nowrap width="10%">Paciente :</td>
								<td class="black1" width="30%"><?="$paciente->pac_nombres   $paciente->pac_apellidos"?></td>
								<td width="30%"></td>
							</tr>
							<tr>
								<td class="black1" nowrap width="10%">Odont&oacute;logo :  </td>
								<td class="black1" nowrap width="30%">&nbsp;&nbsp;<? 
											$query= "select  MAX(oeh_id) FROM odontologos_encargados_historia WHERE pac_id='$paciente->pac_id'";
											$table->search($query); 
											$maxo=$table->sql_fetch_object();
											//echo $query;
												$query="select * from usuario u,  odontologos_encargados_historia  d   
 												where u.usu_id=d.usu_id and d.pac_id = '$paciente->pac_id' and oeh_id='$maxo->max' order by oeh_id ASC";							
												$table->search($query); 
												//echo $query;
												if ($table->nfound >= 1) {
												$data=$table->sql_fetch_object();
												?><?=$data->usu_nombres." ".$data->usu_apellidos?> <?}?></td>
								<td width="30%"></td>
							</tr>
							<tr>
								<td class="black1" nowrap width="10%">Docente :</td>
								<td class="black1" width="30%">&nbsp;&nbsp;<? 
											$query= "select  MAX(deh_id) FROM docentes_encargados_historia WHERE pac_id='$paciente->pac_id'";
											$table->search($query); 
											$maxd=$table->sql_fetch_object();
												$query="select * from usuario u,  docentes_encargados_historia  d   
 												where u.usu_id=d.usu_id and d.pac_id = '$paciente->pac_id' and deh_id='$maxd->max' order by deh_id ASC";							
												$table->search($query); 
												if ($table->nfound >= 1) {
												$data=$table->sql_fetch_object();
												?><?=$data->usu_nombres." ".$data->usu_apellidos?> <?}?></td>
								<td width="30%"></td>
							</tr>
							<tr>
								<td colspan="3" nowrap>
									<div align="center">
										<br>
										<span class="black1">ANAMNESIS<br>
											<br>
										</span></div>
								</td>
							</tr>
							<tr>
								<td class="black1" colspan="3" nowrap>Motivo de la Consulta : (Descripci&oacute;n en t&eacute;rminos del paciente)</td>
							</tr>
							<tr>
								<td colspan="3" nowrap><textarea class="black" name="hic_anamMotivoConsulta" rows="4" cols="115"><?=$hic_anamMotivoConsulta?></textarea><br>
									<br>
								</td>
							</tr>
							<tr>
								<td colspan="3" nowrap><span class="black1">Historia de la enfermedad actual : </td>
							</tr>
							<tr>
								<td class="black" colspan="3" nowrap>
									<p><textarea class="black" name="hic_anamHistoriaEnfActual" rows="4" cols="115"><?=$hic_anamHistoriaEnfActual?></textarea><br>
										<br>
									</p>
								</td>
							</tr>
							<tr>
								<td class="black" width="10%">
									<p>
									</p>
								</td>
								<td class="black" width="30%">
									<p>
									</p>
								</td>
								<td class="black" width="30%">
									<p><br>
										<br>
									</p>
									<p><input type="image" src="images/crear2.gif" alt="" border="0"> <input type="hidden" name="s_opc" value="crear" border="0"><input type="hidden" name="opc" value="historia" border="0"><input type="hidden" name="pac_id" value="<?=$pac_id?>" border="0"><input type="hidden" name="hic_id" value="<?=$hic_id?>" border="0"><input type="hidden" name="origen" value="default" border="0"></p>
								</td>
							</tr>
						</table>
						<p></p>
					</form>
				</td>
			</tr>
		</table>
<?    	
			}
     		}    
	}
}
?>
