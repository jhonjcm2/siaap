
<? if (!$table){ ?>	
	Error en la conexi&oacute;n con la base de datos<br>
	Consulte con el administrador del sistema</p>
		            
	<p class='black'><?include('historia.php');
	}
	else{
	$usuario=$user->datos;
	$pac=new paciente();
	$ret =$pac->validarOdo($pac_id, $usuario->usu_id);
	
	if($ret==false)
		$ret =$pac->validarDoc($pac_id, $usuario->usu_id);
		if($ret==false){
			$mess= "<b>Ud, no es el encargado de esta historia.</b>";
			show_mess($mess,"alert");
		}else{
	
		$query= "SELECT * FROM historia_clinica WHERE pac_id=$pac_id and hic_rxs=1";
	 	$table->search($query);
	 	if(!$table->nfound){
	 		$mess="<center><b>La revisi&oacute;n por sistema no ha sido ingresada.</b> <br><br>";
   	 		show_mess($mess,"alert");
   	 	}
   	 	else{
   	$query= "SELECT * FROM paciente WHERE pac_id=$pac_id";
   	$table->search($query);
   	$paciente = $table->sql_fetch_object();
	

$pac = new historia($pac_id);
$obj = $pac->data;
$usuario=$user->datos;	

if($usuario->tiu_id!=1&& $usuario->tiu_id!=3){
             	show_mess("<b>Ud, no est&aacute; autorizado para ingresar a &eacute;ste m&oacute;dulo</b>","alert");
         }
	else{
?>


		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td>
					<form action="<?=$PHP_SELF?>" method="get" name="historia">
						<table width="100%" border="0" cellspacing="2" cellpadding="0">
							<tr>
								<td width="10%"></td>
								<td width="50%"></td>
								<td class="black1" align="right" nowrap width="30%">Historia Clinica No. <?=$paciente->pac_numeroIdentificacion?></td>
							</tr>
							<tr>
								<td width="10%"></td>
								<td width="50%"></td>
								<td class="black1" align="right" nowrap width="30%">Fecha de Ingreso : <?=$obj->hic_fechaElaboracionRxs?>  </td>
							</tr>
							<tr>
								<td class="black1" width="10%">Paciente :</td>
								<td class="black1" width="50%">&nbsp;&nbsp;<?="$paciente->pac_nombres   $paciente->pac_apellidos"?></td>
								<td width="30%"></td>
							</tr>
							<tr>
								<td class="black1" nowrap width="10%">Odontologo : &nbsp;&nbsp;</td>
								<td class="black1" width="50%">&nbsp;&nbsp;<? 
											$query= "select  MAX(oeh_id) FROM odontologos_encargados_historia WHERE pac_id='$paciente->pac_id'";
											$table->search($query); 
											$maxo=$table->sql_fetch_object();
											//echo $query;
												$query="select * from usuario u,  odontologos_encargados_historia  d   
 												where u.usu_id=d.usu_id and d.pac_id = '$paciente->pac_id' and oeh_id='$maxo->max' order by oeh_id ASC";							
												$table->search($query); 
												//echo $query;
												if ($table->nfound >= 1) {
												$data=$table->sql_fetch_object();
												?><?=$data->usu_nombres." ".$data->usu_apellidos?> <?}?></td>
								<td width="30%"></td>
							</tr>
							<tr>
								<td class="black1" width="10%">Docente :</td>
								<td class="black1" width="50%">&nbsp;&nbsp;<? 
											$query= "select  MAX(deh_id) FROM docentes_encargados_historia WHERE pac_id='$paciente->pac_id'";
											$table->search($query); 
											$maxd=$table->sql_fetch_object();
												$query="select * from usuario u,  docentes_encargados_historia  d   
 												where u.usu_id=d.usu_id and d.pac_id = '$paciente->pac_id' and deh_id='$maxd->max' order by deh_id ASC";							
												$table->search($query); 
												if ($table->nfound >= 1) {
												$data=$table->sql_fetch_object();
												?><?=$data->usu_nombres." ".$data->usu_apellidos?> <?}?></td>
								<td width="30%"></td>
							</tr>
						</table>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="black1" colspan="3" align="center"><br>REVISION POR SISTEMA<br>
									<br>
								</td>
							</tr>
							<tr>
								<td class="black1" width="45%">- Nervioso :</td>
								<td width="10%"></td>
								<td class="black1" width="45%">- Genito Urinario :</td>
							</tr>
							<tr>
								<td rowspan="7">
									<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
										<tr>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Ansiedad :</td>
											<td class="black" nowrap bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_rxsNerviosoAnsiedad=="t")?"SI":"NO"?></td>
											<td class="black" nowrap bgcolor="white" width="30%">&nbsp;&nbsp;Depresi&oacute;n :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_depresion=="t")?"SI":"NO"?></td>
										</tr>
										<tr>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Irritabilidad :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_irritabilidad=="t")?"SI":"NO"?></td>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Lipotimias :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_lipotimias=="t")?"SI":"NO"?></td>
										</tr>
										<tr>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Sincopes :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_sincopes=="t")?"SI":"NO"?></td>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Epilepsia :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_epilepsias=="t")?"SI":"NO"?></td>
										</tr>
										<tr>
											<td class="black" nowrap bgcolor="white" width="30%">&nbsp;&nbsp;Convulsiones :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_convulsiones=="t")?"SI":"NO"?></td>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Neuralgias:</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_neuralgia=="t")?"SI":"NO"?></td>
										</tr>
										<tr>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Paresias :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_paresias=="t")?"SI":"NO"?></td>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Par&aacute;lisis:</td>
											<td class="black" nowrap bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_paralisis=="t")?"SI":"NO"?></td>
										</tr>
										<tr>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Cefaleas :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_cefaleas=="t")?"SI":"NO"?></td>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Migra&ntilde;a :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_migrana=="t")?"SI":"NO"?></td>
										</tr>
										<tr>
											<td class="black" colspan="3" nowrap bgcolor="white">&nbsp;&nbsp;Accidente cerebro vascular :</td>
											<td class="black"bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_accidenteCerebroVascular=="t")?"SI":"NO"?></td>
										</tr>
										<tr>
											<td class="black" colspan="3" nowrap bgcolor="white"></td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_nerviosoOtro=="t")?"SI":"NO"?></td>
										</tr>
									</table>
								</td>
								<td width="10%"></td>
								<td rowspan="4">
									<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
										<tr>
											<td class="black" nowrap bgcolor="white" width="30%">&nbsp;&nbsp;Infecciones Vaginales :</td>
											<td class="black" nowrap bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_rxsGenitoUrinarioInfeccion=="t")?"SI":"NO"?></td>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Embarazo :</td>
											<td class="black" nowrap bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_embarazo=="t")?"SI":"NO"?></td>
										</tr>
										<tr>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Insuficiencia Renal :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_insuficienciaRenal=="t")?"SI":"NO"?></td>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Abortos :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_abortos=="t")?"SI":"NO"?></td>
										</tr>
										<tr>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Cistitis :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_cistitis=="t")?"SI":"NO"?></td>
											<td class="black" nowrap bgcolor="white" width="30%">&nbsp;&nbsp;Planificacion :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_planificacion=="t")?"SI":"NO"?></td>
										</tr>
										<tr>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Dismenorrea :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_dismenorrea=="t")?"SI":"NO"?></td>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;ETS :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_ets=="t")?"SI":"NO"?></td>
										</tr>
										<tr>
											<td class="black" colspan="3" bgcolor="white">&nbsp;&nbsp;Otras : </td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_genitoOtro=="t")?"SI":"NO"?></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td width="10%"></td>
							</tr>
							<tr>
								<td width="10%"></td>
							</tr>
							<tr>
								<td width="10%"></td>
							</tr>
							<tr>
								<td width="10%"></td>
								<td class="black1" width="45%">&nbsp;&nbsp;Observaciones :</td>
							</tr>
							<tr>
								<td width="10%"></td>
								<td width="45%">
									<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
										<tr>
											<td class="black" bgcolor="white">&nbsp;&nbsp;<?=$obj->hic_obsGenito?></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td width="10%"></td>
								<td class="black1" width="45%">- Endocrino :</td>
							</tr>
							<tr>
								<td class ="black1" width="45%">&nbsp;&nbsp;Observaciones :</td>
								<td width="10%"></td>
								<td rowspan="3">
									<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
										<tr>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Enanismo :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_rxsEndocrinoEnanismo=="t")?"SI":"NO"?></td>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Cretinismo :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_cretinismo=="t")?"SI":"NO"?></td>
										</tr>
										<tr>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Gigantismo :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_gigantismo=="t")?"SI":"NO"?></td>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Deabetes :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_diabetes=="t")?"SI":"NO"?></td>
										</tr>
										<tr>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Acromegalia :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_acromegalia=="t")?"SI":"NO"?></td>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Otras : </td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_endocrinoOtro=="t")?"SI":"NO"?></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td rowspan="2">
									<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
										<tr>
											<td class="black" bgcolor="white">&nbsp;&nbsp;<?=$obj->hic_obsNervioso?></td>
										</tr>
									</table>
								</td>
								<td width="10%"></td>
							</tr>
							<tr>
								<td width="10%"></td>
							</tr>
							<tr>
								<td class="black1" width="45%">- Respiratorio :</td>
								<td width="10%"></td>
								<td class="black1" width="45%">&nbsp;&nbsp;Observaciones :</td>
							</tr>
							<tr>
								<td rowspan="4">
									<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
										<tr>
											<td class="black" nowrap bgcolor="white" width="30%">&nbsp;&nbsp;Tos :</td>
											<td class="black" nowrap bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_rxsRespiratorioTos=="t")?"SI":"NO"?></td>
											<td class="black" nowrap bgcolor="white" width="30%">&nbsp;&nbsp;Hemoptisis :</td>
											<td class="black" nowrap bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_hemoptisis=="t")?"SI":"NO"?></td>
										</tr>
										<tr>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Disnea :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_disnea=="t")?"SI":"NO"?></td>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Asma :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_asma=="t")?"SI":"NO"?></td>
										</tr>
										<tr>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Rin&iacute;tis:</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_rinitis=="t")?"SI":"NO"?></td>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;TBC :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_tbc=="t")?"SI":"NO"?></td>
										</tr>
										<tr>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Pulmonares :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_pulmonares=="t")?"SI":"NO"?></td>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Micosis :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_micosis=="t")?"SI":"NO"?></td>
										</tr>
										<tr>
											<td class="black" colspan="3" bgcolor="white">&nbsp;&nbsp;Otras : </td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_resOtro=="t")?"SI":"NO"?></td>
										</tr>
									</table>
								</td>
								<td width="10%"></td>
								<td width="45%">
									<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
										<tr>
											<td class="black" bgcolor="white">&nbsp;&nbsp;<?=$obj->hic_obsEndocrino?></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td width="10%"></td>
								<td class="black1" width="45%">- Osteo Articular :</td>
							</tr>
							<tr>
								<td width="10%"></td>
								<td rowspan="3">
									<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
										<tr>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Artritis :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_rxsOsteoarticularArtritis=="t")?"SI":"NO"?></td>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Artrosis :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_artrosis=="t")?"SI":"NO"?></td>
										</tr>
										<tr>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Espasmos :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_espasmos=="t")?"SI":"NO"?></td>
											<td class="black" nowrap bgcolor="white" width="30%">&nbsp;&nbsp;Dolores Articulares :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_doloresArticulares=="t")?"SI":"NO"?></td>
										</tr>
										<tr>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Tics :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_tics=="t")?"SI":"NO"?></td>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Otras : </td>
											<td class="black"bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_osteoOtro=="t")?"SI":"NO"?></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td width="10%"></td>
							</tr>
							<tr>
								<td  class="black1" width="45%">&nbsp;&nbsp;Observaciones :</td>
								<td width="10%"></td>
							</tr>
							<tr>
								<td rowspan="2">
									<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
										<tr>
											<td class="black" bgcolor="white">&nbsp;&nbsp;<?=$obj->hic_obsRespiratorio?></td>
										</tr>
									</table>
								</td>
								<td width="10%"></td>
								<td class="black1" width="45%">&nbsp;&nbsp;Observaciones :</td>
							</tr>
							<tr>
								<td width="10%"></td>
								<td rowspan="2">
									<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
										<tr>
											<td class="black" bgcolor="white">&nbsp;&nbsp;<?=$obj->hic_obsOsteoArt?></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td class= "black1" width="45%">-&nbsp;&nbsp;Cardiovascualr :</td>
								<td width="10%"></td>
							</tr>
							<tr>
								<td rowspan="4">
									<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
										<tr>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Arritmias :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_rxsCardiovascularArritmias=="t")?"SI":"NO"?></td>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Cianosis :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_cianosis=="t")?"SI":"NO"?></td>
										</tr>
										<tr>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Anasarca :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_anasarca=="t")?"SI":"NO"?></td>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;HTA :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_hta=="t")?"SI":"NO"?></td>
										</tr>
										<tr>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Cardiopat&iacute;as Congenitas :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_cardiopatiasCongenitas=="t")?"SI":"NO"?></td>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Varices :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_varices=="t")?"SI":"NO"?></td>
										</tr>
										<tr>
											<td class="black" colspan="3" bgcolor="white">&nbsp;&nbsp;Otras :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_cardioOtro=="t")?"SI":"NO"?></td>
										</tr>
									</table>
								</td>
								<td width="10%"></td>
								<td class= "black1" width="45%">- Piel :</td>
							</tr>
							<tr>
								<td width="10%"></td>
								<td rowspan="2">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td class="black" nowrap width="30%">&nbsp;&nbsp;Erupciones Cut&aacute;neas :</td>
											<td class="black" width="20%">&nbsp;&nbsp;<?=($obj->hic_rxsPielErupcionesCut=="t")?"SI":"NO"?></td>
											<td class="black" width="30%">&nbsp;&nbsp;Psoriasis :</td>
											<td class="black" width="20%">&nbsp;&nbsp;<?=($obj->hic_psoriasis=="t")?"SI":"NO"?></td>
										</tr>
										<tr>
											<td class="black" width="30%">&nbsp;&nbsp;Micosis :</td>
											<td class="black" width="20%">&nbsp;&nbsp;<?=($obj->hic_micosis1=="t")?"SI":"NO"?></td>
											<td class="black" width="30%">&nbsp;&nbsp;Otras : </td>
											<td class="black" width="20%">&nbsp;&nbsp;<?=($obj->hic_pielOtro=="t")?"SI":"NO"?></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td width="10%"></td>
							</tr>
							<tr>
								<td width="10%"></td>
								<td class="black1" width="45%">&nbsp;&nbsp;Observaciones :</td>
							</tr>
							<tr>
								<td class="black1" width="45%">&nbsp;&nbsp;Observaciones :</td>
								<td width="10%"></td>
								<td rowspan="2">
									<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
										<tr>
											<td class="black" bgcolor="white">&nbsp;&nbsp;<?=$obj->hic_obsPiel?></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td rowspan="2">
									<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
										<tr>
											<td class="black" bgcolor="white">&nbsp;&nbsp;<?=$obj->hic_obsCardiovascular?></td>
										</tr>
									</table>
								</td>
								<td width="10%"></td>
							</tr>
							<tr>
								<td width="10%"></td>
								<td class="black1" width="45%">- Inmunol&oacute;gico</td>
							</tr>
							<tr>
								<td class="black1" width="45%">- Gastroinstestinales :</td>
								<td width="10%"></td>
								<td rowspan="5">
									<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
										<tr>
											<td class="black" colspan="3" bgcolor="white">&nbsp;&nbsp;Enf. Autoinmunol&oacute;gicas (Deabetes Juvenil, Artritis Reumatoidea, Anemia, etc):</td>
											<td class="black" bgcolor="white" width="10%">&nbsp;&nbsp;<?=($obj->hic_rxsInmAut=="t")?"SI":"NO"?></td>
										</tr>
										<tr>
											<td class="black" colspan="3" bgcolor="white">&nbsp;&nbsp;Enf. Complejas Inmunol&oacute;gicas (Hepatitis Viral, Malaria, etc.) :</td>
											<td class="black" bgcolor="white" width="10%">&nbsp;&nbsp;<?=($obj->hic_rxsInmCom=="t")?"SI":"NO"?></td>
										</tr>
										<tr>
											<td class="black" colspan="3" bgcolor="white">&nbsp;&nbsp;Enf. de Inmunodeficiencia (Sida, etc.)</td>
											<td class="black" bgcolor="white" width="10%">&nbsp;&nbsp;<?=($obj->hic_rxsInmDef=="t")?"SI":"NO"?></td>
										</tr>
										<tr>
											<td class="black" colspan="3" bgcolor="white">&nbsp;&nbsp;Alergias :</td>
											<td class="black" bgcolor="white" width="10%">&nbsp;&nbsp;<?=($obj->hic_rxsInmAle=="t")?"SI":"NO"?></td>
										</tr>
										<tr>
											<td class="black" colspan="3" bgcolor="white">&nbsp;&nbsp;Otras : </td>
											<td class="black" bgcolor="white" width="10%">&nbsp;&nbsp;<?=($obj->hic_inmuOtro=="t")?"SI":"NO"?></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td rowspan="3">
									<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
										<tr>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Diarrea :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_rxsGastrointestinalDiarrea=="t")?"SI":"NO"?></td>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Parasitos :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_parasitos=="t")?"SI":"NO"?></td>
										</tr>
										<tr>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Gastritis :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_gastritis=="t")?"SI":"NO"?></td>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Ulceras :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_ulceras=="t")?"SI":"NO"?></td>
										</tr>
										<tr>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Hepatitis :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_hepatitis=="t")?"SI":"NO"?></td>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;Ictericia :</td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_ictericia=="t")?"SI":"NO"?></td>
										</tr>
										<tr>
											<td class="black" colspan="3" bgcolor="white">&nbsp;&nbsp;Otras : </td>
											<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;<?=($obj->hic_gastroOtro=="t")?"SI":"NO"?></td>
										</tr>
									</table>
								</td>
								<td width="10%"></td>
							</tr>
							<tr>
								<td width="10%"></td>
							</tr>
							<tr>
								<td width="10%"></td>
							</tr>
							<tr>
								<td class="black1" width="45%">&nbsp;&nbsp;Observaciones :</td>
								<td width="10%"></td>
							</tr>
							<tr>
								<td rowspan="2">
									<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
										<tr>
											<td class="black" bgcolor="white">&nbsp;&nbsp;<?=$obj->hic_obsGastrointestinal?></td>
										</tr>
									</table>
								</td>
								<td width="10%"></td>
								<td class="black1" width="45%">&nbsp;&nbsp;Observaciones :</td>
							</tr>
							<tr>
								<td width="10%"></td>
								<td rowspan="2">
									<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
										<tr>
											<td class="black" bgcolor="white">&nbsp;&nbsp;<?=$obj->hic_rxsObsInm?></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td width="45%"></td>
								<td width="10%"></td>
							</tr>
						</table>
						<p></p>
					</form>
					<p></p>
				</td>
			</tr>
		</table>
		<p></p>
<?
			}
		}
	}
}
?>