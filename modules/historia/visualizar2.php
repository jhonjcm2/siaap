<?
if (!$table){
?>Error en la conexi&oacute;n con la base de datos<br>
     Consulte con el administrador del sistema</p>
		            
	<?include('historia.php');
}else{
	$usuario=$user->datos;
	$pac=new paciente();
	$ret =$pac->validarOdo($pac_id, $usuario->usu_id);
	
	if($ret==false)
		$ret =$pac->validarDoc($pac_id, $usuario->usu_id);
		if($ret==false){
			$mess= "<b>Ud, no es el encargado de esta historia.</b>";
			show_mess($mess,"alert");
		}else{

	$query= "SELECT * FROM historia_clinica WHERE pac_id=$pac_id and hic_efg=1";
	 $table->search($query);
	 if(!$table->nfound){
	 	$mess="<center><b>El examen f&iacute;sico general no ha sido ingresado.</b> <br><br>";
   	 	show_mess($mess,"alert");
   	 	}
   	 	else{
   $query= "SELECT * FROM paciente WHERE pac_id=$pac_id";
   $table->search($query);
   $paciente = $table->sql_fetch_object();

$pac = new historia($pac_id);
$obj = $pac->data;
$usuario=$user->datos;	

if($usuario->tiu_id!=1&& $usuario->tiu_id!=3){
             	show_mess("<b>Ud, no est&aacute; autorizado para ingresar a &eacute;ste m&oacute;dulo</b>","alert");
         }
	else{
?>

	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<p></p>
				<p></p>
				<form action="<?=$PHP_SELF?>" method="post" name="historia">
					<p></p>
					<p></p>
					<table class="black" width="90%" border="0" cellspacing="5" cellpadding="0" align="center">
						<tr>
							<td class="black" width="10%"></td>
							<td class="black" align="right" nowrap width="50%"></td>
							<td class="black1" nowrap width="30%">
								<div align="right">
									Historia cl&iacute;nica No : <?=$paciente->pac_numeroIdentificacion?> </div>
							</td>
						</tr>
						<tr>
							<td class="black" width="10%"></td>
							<td class="black" align="right" nowrap width="50%"></td>
							<td class="black1" align="right" nowrap width="30%">Fecha de Elaboraci&oacute;n : <?=$obj->hic_fechaElaboracionEfg?></td>
						</tr>
						<tr>
							<td class="black1" width="10%">Paciente :</td>
							<td class="black1" nowrap width="50%">&nbsp;&nbsp;<?=$paciente->pac_nombres?> <?=$paciente->pac_apellidos?></td>
							<td class="black" nowrap width="30%"></td>
						</tr>
						<tr>
							<td class="black1" nowrap width="10%">Odont&oacute;logo :</td>
							<td class="black1" nowrap width="50%">&nbsp;&nbsp;<? 
											$query= "select  MAX(oeh_id) FROM odontologos_encargados_historia WHERE pac_id='$paciente->pac_id'";
											$table->search($query); 
											$maxo=$table->sql_fetch_object();
											//echo $query;
												$query="select * from usuario u,  odontologos_encargados_historia  d   
 												where u.usu_id=d.usu_id and d.pac_id = '$paciente->pac_id' and oeh_id='$maxo->max' order by oeh_id ASC";							
												$table->search($query); 
												//echo $query;
												if ($table->nfound >= 1) {
												$data=$table->sql_fetch_object();
												?><?=$data->usu_nombres." ".$data->usu_apellidos?> <?}?></td>
							<td class="black" nowrap width="30%"></td>
						</tr>
						<tr>
							<td class="black1" width="10%">Docente :  </td>
							<td class="black1" nowrap width="50%">&nbsp;&nbsp;<? 
											$query= "select  MAX(deh_id) FROM docentes_encargados_historia WHERE pac_id='$paciente->pac_id'";
											$table->search($query); 
											$maxd=$table->sql_fetch_object();
												$query="select * from usuario u,  docentes_encargados_historia  d   
 												where u.usu_id=d.usu_id and d.pac_id = '$paciente->pac_id' and deh_id='$maxd->max' order by deh_id ASC";							
												$table->search($query); 
												if ($table->nfound >= 1) {
												$data=$table->sql_fetch_object();
												?><?=$data->usu_nombres." ".$data->usu_apellidos?> <?}?></td>
							<td class="black" nowrap width="30%"></td>
						</tr>
						<tr>
							<td class="black1" colspan="3">
								<div align="center">
									<br>Examen F&iacute;sico General<br>
									<br>
								</div>
							</td>
						</tr>
						<tr>
							<td class="black" colspan="3" align="center">
								<p></p>
								<table width="70%" border="0" cellspacing="5" cellpadding="0">
									<tr>
										<td class="black1" colspan="2">- Caracteristicas Fisicas: <br>
											<br>
										</td>
										<td width="35%"></td>
									</tr>
									<tr>
										<td width="10%">
										<td class="black" width="25%">Contextura :</td>
										<td class="black" width="35%"><?if($obj->hic_contextura==1){echo "Grande";
																	}elseif($obj->hic_contextura==2){echo "Mediana";
																	}elseif($obj->hic_contextura==3){echo "Peque&ntilde;a";}?>
									</tr>
									<tr>
										<td width="10%"></td>
										<td class="black" nowrap width="25%">Picos de Crecimiento (ultimamente)  :</td>
										<td class="black" width="35%"><?if($obj->hic_picosCrecimiento==1){ echo "Estable";
																	}elseif($obj->hic_picosCrecimiento==2){ echo "Acelerado";
																	}elseif($obj->hic_picosCrecimiento==3){ echo "Decreciente";}?></td>
									</tr>
									<tr>
										<td width="10%"></td>
										<td class="black" width="25%">Apariencia Nutricional :</td>
										<td class="black" nowrap width="35%"> <?=($obj->hic_aparienciaNutricionalNormal =="t")?"Normal":"Anormal"?>  </td>
									</tr>
									<tr>
										<td width="10%"></td>
										<td class="black" width="25%">Orientaci&oacute;n en tiempo y espacio :</td>
										<td class="black" width="35%"><?=($obj->hic_caracteristicasFisicasOrien =="t")?"Normal":"Anormal"?> </td>
									</tr>
								</table>
								<p></p>
								<table width="70%" border="0" cellspacing="5" cellpadding="0">
									<tr>
										<td class="black1" colspan="3">- Signos Vitales :<br>
											<br>
										</td>
										<td class="black" width="15%"></td>
										<td class="black" nowrap width="20%"></td>
									</tr>
									<tr>
										<td width="10%"></td>
										<td class="black" width="7%">Peso :</td>
										<td class="black" nowrap width="35%"><?=$obj->hic_exFiSignosVitalesPeso?> kg</td>
										<td class="black" width="15%">
											<div align="right">
												Tension Arterial :</div>
										</td>
										<td class="black" nowrap width="20%"><?=$obj->hic_tensionArterial?> mm/hg</td>
									</tr>
									<tr>
										<td width="10%"></td>
										<td class="black" width="7%">Talla :</td>
										<td class="black" nowrap width="35%"><?=$obj->hic_talla?>Cms</td>
										<td class="black" nowrap width="15%">Frecuencia Respiratoria :</td>
										<td class="black" width="20%"><?=$obj->hic_frecuenciaRespiratoria?> p/min</td>
									</tr>
									<tr>
										<td width="10%"></td>
										<td class="black" width="7%">Pulso :</td>
										<td class="black" nowrap width="35%"><?=$obj->hic_pulso?> p/min</td>
										<td class="black" width="15%">
											<div align="right">
												Temperatura :</div>
										</td>
										<td class="black" width="20%"><?=$obj->hic_temperatura?> &ordm;C</td>
									</tr>
								</table>
								<p>
								</p>
								<table width="70%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td class="black1">- Observaciones :</td>
										</tr>
									<tr>
										<td class="black" >
											<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
												<tr>
													<td class="black" bgcolor="white">&nbsp;&nbsp;<?=$obj->hic_exaFisObs?></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
								<p></p>
								<p class="black1"></p>
								<p></p>
								<p class="black1"></p>
								<p></p>
								<p class="black1"></p>
								<p></p>
							</td>
						</tr>
						<tr>
							<td class="black" width="10%">
								<p>
								</p>
							</td>
							<td class="black" width="50%">
								<p>
								</p>
								<p><input type="hidden" name="s_opc" value="historia3" border="0"><input type="hidden" name="pac_id" value="<?=$pac_id?>" border="0"><input type="hidden" name="opc" value="<?=$opc?>" border="0"></p>
							</td>
							<td class="black" align="right" width="30%">
								<div align="center">
									<p></p>
								</div>
							</td>
						</tr>
					</table>
				</form>
			</td>
		</tr>
	</table>
<?
			}
		}
	}
}
?>
