<%	if (!$table){%>	
		Error en la conexi&oacute;n con la base de datos<br>
		Consulte con el administrador del sistema</p>
		<p class='black'><%include('modules/adulto/adulto1.php');
		}
	else{
	$usuario=$user->datos;
	$pac=new paciente();
	$ret =$pac->validarOdo($pac_id, $usuario->usu_id);
	
	if($ret==false)
		$ret =$pac->validarDoc($pac_id, $usuario->usu_id);
		if($ret==false){
			$mess= "<b>Ud, no es el encargado de �sta historia.</b>";
			show_mess($mess,"alert");
		}else{
		
		$query= "SELECT * FROM datos_personales_adulto WHERE pac_id=$pac_id and dpa_estado=2";
	 	$table->search($query);
	 	if(!$table->nfound){
	 	$mess="<center><b>Los antecedentes generales no han sido ingresados.</b> <br><br>";
   	 	show_mess($mess,"alert");
   	}else{	
		$query= "SELECT * FROM paciente WHERE pac_id=$pac_id";
   		$table->search($query);
   		$paciente = $table->sql_fetch_object();

$pac = new adulto($pac_id);
$obj = $pac->data;
$usuario=$user->datos;

%>


<html>
	<body bgcolor="#ffffff">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td>
					<div align="center">
						<h2 class="black1"></h2>
						<form action="<%=$PHP_SELF%>" method="post" name="adulto1">
							<table class="black" width="80%" border="0" cellspacing="10" cellpadding="0" align="center">
								<tr>
									<td class="black" width="40%"></td>
									<td class="black" width="5%"></td>
									<td class="black1" width="40%">
										<div align="right">
											Historia Cl&iacute;nica No: <%=$paciente->pac_numeroIdentificacion%><br>
											Fecha de Elaboraci&oacute;n : <%=$obj->dpa_fechaElaboracion%><br>
										</div>
									</td>
								</tr>
								<tr>
									<td class="black" width="40%">
										<table class="black" width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
											<tr>
												<td class="black1" nowrap width="10%">Paciente :</td>
												<td class="black1" width="25%"><%="$paciente->pac_nombres  $paciente->pac_apellidos"%></td>
											</tr>
											<tr>
												<td class="black1" nowrap width="10%">Odont&oacute;logo :</td>
												<td class="black1" width="25%"><% 
												       $query="select * from usuario u,  odontologos_encargados_historia  d   
 													where u.usu_id=d.usu_id and d.hic_id = '$obj->pac_id' order by oeh_id ASC";							
												       $table->search($query); 
												       if ($table->nfound >= 1) {
													$data=$table->sql_fetch_object();
													%><%=$data->usu_nombres." ".$data->usu_apellidos%> <%}%></td>
											</tr>
											<tr>
												<td class="black1" nowrap width="10%">Docente :</td>
												<td class="black1" width="25%"><% 
												       $query="select * from usuario u,  docentes_encargados_historia  d   
 													where u.usu_id=d.usu_id and d.hic_id = '$obj->pac_id' order by deh_id ASC";							
												       $table->search($query); 
												       if ($table->nfound >= 1) {
													$data=$table->sql_fetch_object();
													%><%=$data->usu_nombres." ".$data->usu_apellidos%> <%}%></td>
											</tr>
										</table>
									</td>
									<td class="black" width="5%"></td>
									<td class="black" width="40%"></td>
								</tr>
								<tr>
									<td class="black1" colspan="3">
										<div align="center">
											<br>
											<b>ANTECEDENTES GENERALES<br>
												<br>
											</b></div>
									</td>
								</tr>
								<tr>
									<td class="black" width="40%">
										<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="black">
											<tr>
												<td class="black1" bgcolor="white">&nbsp;SOCIALES:</td>
											</tr>
										</table>
										<table width="100%" border="0" cellspacing="2" cellpadding="0">
											<tr>
												<td class="black1" nowrap width="40%">- Tipo de Motivaci&oacute;n :</td>
												<td class="black" >&nbsp;&nbsp;<%=($obj->dpa_antSocMotivacionInterna=="t")?"Interna":"Externa"%></td>
											</tr>
										</table>
										<br>
										<table class="black1" width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td>- Expetactivas Frente al Tratamiento :</td>
											</tr>
										</table>
										<table class="black" width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
											<tr>
												<td bgcolor="white">&nbsp;&nbsp;&nbsp;<%=$obj->dpa_espectativasTratamiento%><br>
												</td>
											</tr>
										</table>
										<table class="black1" width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td><br>- Entorno Socio-Econ&oacute;mico :</td>
											</tr>
										</table>
										<table class="black" width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
											<tr>
												<td bgcolor="white">&nbsp;&nbsp;&nbsp;<%=$obj->dpa_entornoSocioeconomico%></td>
											</tr>
										</table>
										<table class="black1" width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td><br>- Hobbies :</td>
											</tr>
										</table>
										<table class="black" width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
											<tr>
												<td bgcolor="white">&nbsp;&nbsp;&nbsp;<%=$obj->dpa_hobbies%></td>
											</tr>
										</table>
										<p></p>
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class="black1">PATOLOGICOS :</td>
											</tr>
										</table>
										<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
											<tr>
												<td class="black" bgcolor="white" width="25%">&nbsp;&nbsp;&nbsp;Gastrointestinal :</td>
												<td class="black" bgcolor="white" width="20%"> &nbsp;&nbsp;&nbsp;<%=($obj->dpa_gastrointestinales=="t")?"SI":"NO"%></td>
												<td class="black" bgcolor="white" width="25%">&nbsp;&nbsp;&nbsp;Neuril&oacute;gicos :</td>
												<td class="black" bgcolor="white">&nbsp;&nbsp;&nbsp;<%=($obj->dpa_neurologicos=="t")?"SI":"NO"%>&nbsp;&nbsp;&nbsp;</td>
											</tr>
											<tr>
												<td class="black" nowrap bgcolor="white" width="25%">&nbsp;&nbsp;&nbsp;Cardiovasculares :</td>
												<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;&nbsp;<%=($obj->dpa_cardiovasculares=="t")?"SI":"NO"%></td>
												<td class="black" bgcolor="white" width="25%">&nbsp;&nbsp;&nbsp;Siqui&aacute;tricos :</td>
												<td class="black" bgcolor="white">&nbsp;&nbsp;&nbsp;<%=($obj->dpa_siquiatricos=="t")?"SI":"NO"%>&nbsp;&nbsp;&nbsp;</td>
											</tr>
											<tr>
												<td class="black" bgcolor="white" width="25%">&nbsp;&nbsp;&nbsp;Pulmunares :</td>
												<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;&nbsp;<%=($obj->dpa_pulmonares=="t")?"SI":"NO"%></td>
												<td class="black" nowrap bgcolor="white" width="25%">&nbsp;&nbsp;&nbsp;Neopl&aacute;sicos :</td>
												<td class="black" bgcolor="white">&nbsp;&nbsp;&nbsp;<%=($obj->dpa_neoplasicos=="t")?"SI":"NO"%>&nbsp;&nbsp;&nbsp;</td>
											</tr>
											<tr>
												<td class="black" bgcolor="white" width="25%">&nbsp;&nbsp;&nbsp;Eruptivas :</td>
												<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;&nbsp;<%=($obj->dpa_antMedPatologicosEruptivas=="t")?"SI":"NO"%></td>
												<td class="black" bgcolor="white" width="25%">&nbsp;&nbsp;&nbsp;Endocrinos</td>
												<td class="black" bgcolor="white">&nbsp;&nbsp;&nbsp;<%=($obj->dpa_endocrinos)?"SI":"NO"%>&nbsp;&nbsp;&nbsp;</td>
											</tr>
											<tr>
												<td class="black" bgcolor="white" width="25%">&nbsp;&nbsp;&nbsp;Genitourinarios :</td>
												<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;&nbsp;<%=($obj->dpa_genitourinarios=="t")?"SI":"NO"%></td>
												<td class="black" bgcolor="white" width="25%">&nbsp;&nbsp;&nbsp;Ven&eacute;reos :</td>
												<td class="black" bgcolor="white">&nbsp;&nbsp;&nbsp;<%=($obj->dpa_venereos=="t")?"SI":"NO"%>&nbsp;&nbsp;&nbsp;</td>
											</tr>
											<tr>
												<td class="black" bgcolor="white" width="25%">&nbsp;&nbsp;&nbsp;Inmunol&oacute;gicos :</td>
												<td class="black" colspan="3" bgcolor="white">&nbsp;&nbsp;&nbsp;<%=($obj->dpa_inmunologicos=="t")?"SI":"NO"%></td>
											</tr>
										</table>
										<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="black">
											<tr>
												<td class="black1" bgcolor="white">&nbsp;<br>
													 Observaciones Patol&oacute;gias :</td>
											</tr>
										</table>
										<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
											<tr>
												<td class="black" bgcolor="white">&nbsp;&nbsp;&nbsp;<%=$obj->dpa_comentariosPatologicos%></td>
											</tr>
										</table>
										<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="black">
											<tr>
												<td class="black1" bgcolor="white">&nbsp;<br>ODONTOLOGICOS :</td>
											</tr>
										</table>
										<table class="black" width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
											<tr>
												<td class="black" bgcolor="white" width="25%">&nbsp;&nbsp;&nbsp;Prevenci&oacute;n :</td>
												<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;&nbsp;<%=($obj->dpa_antOdonPrevencion=="t")?"SI":"NO"%></td>
												<td class="black" bgcolor="white" width="25%">&nbsp;&nbsp;&nbsp;Pr&oacute;tesis :</td>
												<td class="black" bgcolor="white">&nbsp;&nbsp;&nbsp;<%=($obj->dpa_protesis=="t")?"SI":"NO"%></td>
											</tr>
											<tr>
												<td class="black" bgcolor="white" width="25%">&nbsp;&nbsp;&nbsp;Operatoria :</td>
												<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;&nbsp;<%=($obj->dpa_operatoria=="t")?"SI":"NO"%></td>
												<td class="black" bgcolor="white" width="25%">&nbsp;&nbsp;&nbsp;Ortodoncia :</td>
												<td class="black" bgcolor="white">&nbsp;&nbsp;&nbsp;<%=($obj->dpa_ortodoncia=="t")?"SI":"NO"%></td>
											</tr>
											<tr>
												<td class="black" nowrap bgcolor="white" width="25%">&nbsp;&nbsp;&nbsp;Periodoncia :</td>
												<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;&nbsp;<%=($obj->dpa_periodoncia=="t")?"SI":"NO"%></td>
												<td class="black" bgcolor="white" width="25%">&nbsp;&nbsp;&nbsp;Ortopedia :</td>
												<td class="black" bgcolor="white">&nbsp;&nbsp;&nbsp;<%=($obj->dpa_ortopedia=="t")?"SI":"NO"%></td>
											</tr>
											<tr>
												<td class="black" bgcolor="white" width="25%">&nbsp;&nbsp;&nbsp;Endodoncia :</td>
												<td class="black" bgcolor="white" width="20%">&nbsp;&nbsp;&nbsp;<%=($obj->dpa_endodoncia=="t")?"SI":"NO"%></td>
												<td class="black" nowrap bgcolor="white" width="25%">&nbsp;&nbsp;&nbsp;Cirug&iacute;a Oral :</td>
												<td class="black" bgcolor="white">&nbsp;&nbsp;&nbsp;<%=($obj->dpa_cirugiaOral=="t")?"SI":"NO"%></td>
											</tr>
										</table>
										<p></p>
									</td>
									<td class="black" width="5%"></td>
									<td class="black" valign="top" width="40%">
										<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="black">
											<tr>
												<td class="black1" bgcolor="white">&nbsp;OTROS :</td>
											</tr>
										</table>
										<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
											<tr>
												<td class="black" nowrap bgcolor="white" width="30%">&nbsp;&nbsp;&nbsp;Hospitalarios :</td>
												<td class="black" bgcolor="white">&nbsp;&nbsp;&nbsp;<%=($obj->dpa_hospitalarios=="t")?"SI":"NO"%></td>
											</tr>
											<tr>
												<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;&nbsp;quir&uacute;rguicos :</td>
												<td class="black" bgcolor="white">&nbsp;&nbsp;&nbsp;<%=($obj->dpa_quirurgicos=="t")?"SI":"NO"%></td>
											</tr>
											<tr>
												<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;&nbsp;Traum&aacute;ticos :</td>
												<td class="black" bgcolor="white">&nbsp;&nbsp;&nbsp;<%=($obj->dpa_traumaticos=="t")?"SI":"NO"%></td>
											</tr>
										</table>
										<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="black">
											<tr>
												<td class="black1" bgcolor="white">&nbsp;<br>Observaciones  :</td>
											</tr>
										</table>
										<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
											<tr>
												<td class="black" bgcolor="white">&nbsp;&nbsp;&nbsp;<%=$obj->dpa_comentariosTrauma%></td>
											</tr>
										</table>
										<br>
										<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
											<tr>
												<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;&nbsp;F&aacute;rmacos :</td>
												<td class="black" bgcolor="white">&nbsp;&nbsp;&nbsp;<%=($obj->dpa_farmacos=="t")?"SI":"NO"%></td>
											</tr>
											<tr>
												<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;&nbsp;Radiaci&oacute;n :</td>
												<td class="black" bgcolor="white">&nbsp;&nbsp;&nbsp;<%=($obj->dpa_radiacion=="t")?"SI":"NO"%></td>
											</tr>
											<tr>
												<td class="black" nowrap bgcolor="white" width="30%">&nbsp;&nbsp;&nbsp;Toxicoal&eacute;rgicos :</td>
												<td class="black" bgcolor="white">&nbsp;&nbsp;&nbsp;<%=($obj->dpa_toxicoalergicos=="t")?"SI":"NO"%></td>
											</tr>
										</table>
										<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="black">
											<tr>
												<td class="black1" bgcolor="white">&nbsp;<br>Observaciones  :</td>
											</tr>
										</table>
										<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
											<tr>
												<td class="black" bgcolor="white">&nbsp;&nbsp;&nbsp;<%=$obj->dpa_comentariosRadiacion%></td>
											</tr>
										</table>
										<br>
										<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
											<tr>
												<td class="black" bgcolor="white" width="40%">&nbsp;&nbsp;&nbsp;Hemorr&aacute;gicos :</td>
												<td class="black" bgcolor="white">&nbsp;&nbsp;&nbsp;<%=($obj->dpa_hemorragicos=="t")?"SI":"NO"%></td>
											</tr>
											<tr>
												<td class="black" bgcolor="white" width="40%">&nbsp;&nbsp;&nbsp;Ginecobst&eacute;tricos :</td>
												<td class="black" bgcolor="white">&nbsp;&nbsp;&nbsp;<%=($obj->dpa_ginecobstetricos=="t")?"SI":"NO"%></td>
											</tr>
											<tr>
												<td class="black" nowrap bgcolor="white" width="40%">&nbsp;&nbsp;&nbsp;Problemas Articulares :</td>
												<td class="black" bgcolor="white">&nbsp;&nbsp;&nbsp;<%=($obj->dpa_ginecobstetricos=="t")?"SI":"NO"%></td>
											</tr>
										</table>
										<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="black">
											<tr>
												<td class="black1" bgcolor="white">&nbsp;<br>Observaciones  :</td>
											</tr>
										</table>
										<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
											<tr>
												<td class="black" nowrap bgcolor="white" width="40%">&nbsp;&nbsp;&nbsp;<%=$obj->dpa_comentariosProblemas%></td>
											</tr>
										</table>
										<p></p>
										<p></p>
										<p></p>
										<p></p>
									</td>
								</tr>
								<tr>
									<td width="40%"></td>
									<td width="5%"></td>
									<td align="right" width="40%"><input type="hidden" name="s_opc" value="buscar" border="0"> 
									<input type="hidden" name="opc" value="<%=$opc%>" border="0"> 
									<input type="hidden" name="pac_id" value="<%=$pac_id%>" border="0"> 
									<input type="hidden" name="hic_id" value="<%=$paciente->pac_numeroIdentificacion%>" border="0"> 
									<input type="hidden" name="deh_fechaEncargado" value='<%=date("Y-m-d",time())%>' border="0"> 
									<input type="hidden" name="oeh_fechaEncargado" value='<%=date("Y-m-d",time())%>' border="0"> 
									<input type="hidden" name="dpa_id" value="<%=$dpa_id%>" border="0"> <input type="hidden" name="dpa_estado" value="2" border="0"> <input type="hidden" name="odo_id" value="<%=$user->datos->usu_id%>" border="0"> 
									<input type="hidden" name="doc_id" value="<%=$user->datos->usu_id%>" border="0"></td>
								</tr>
							</table>
						</form>
						<h2 class="black1"></h2>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					
				</td>
			</tr>
		</table>
		<p></p>
	</body>

</html>
<%
		}
	}
}
%>