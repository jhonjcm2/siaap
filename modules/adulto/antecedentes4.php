<%
if (!$table){
%>Error en la conexi&oacute;n con la base de datos<br>
    Consulte con el administrador del sistema
    <%include('adulto.php');
	//exit(0);
}else{
	$usuario=$user->datos;
	$query= "SELECT hic_ant3 FROM historia_clinica WHERE pac_id=$pac_id";
	$table->search($query);
	$obj = $table->sql_fetch_object();
	if (!$table->nfound){
		$mess="<center><b>Los datos de los Ant. Familiares NO han sido ingresados.</b> <br><br>
      		Por favor ingreselos primero</center>";
   	 	show_mess($mess,"alert");
	}else{
		$query="select * from datos_personales_adulto where pac_id=$pac_id";
		//echo $query;
		$table->sql_query($query);
		$obj = $table->sql_fetch_object();
		$dpa_id = $obj->dpa_id;		
      
     		if($obj->dpa_estado==3){
      			$mess="<center><b>Los Antecedentes Odontol&oacute;gicos YA han sido ingresados.</b> <br><br>
      			Por favor continue ingresando los Ant. Alergicos y Habitos</center>";
   			show_mess($mess,"alert"); 	 		
       	}else{
   
/*******************/

$pac = new adulto( $pac_id);
$obj=$pac->data;

  $query= "SELECT * FROM paciente WHERE pac_id=$pac_id";
   $table->search($query);
   $paciente = $table->sql_fetch_object();

   $usuario=$user->datos;
%>


<html>
	<body bgcolor="#ffffff">
		<table width="90%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<div align="center">
						<h2 class="black1"></h2>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<form action="<%=$PHP_SELF%>" method="post" name="adulto1">
						<p></p>
						<table class="black" width="80%" border="0" cellspacing="2" cellpadding="0" align="center">
							<tr>
								<td class="black" width="35%"></td>
								<td class="black" width="10%"></td>
								<td class="black1" align="right" width="35%">
									
										Historia Cl&iacute;nica No: <%=$paciente->pac_numeroIdentificacion%><br>
									Fecha : <%=$obj->dpa_fechaElaboracion%><br>
									
								</td>
							</tr>
							<tr>
								<td class="black" width="35%">
									<table class="black" width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
										<tr>
											<td class="black1" nowrap width="10%">Paciente : </td>
											<td class="black1" width="25%"><%="$paciente->pac_nombres $paciente->pac_apellidos"%></td>
										</tr>
										<tr>
											<td class="black1" nowrap width="10%">Odont&oacute;logo  :  </td>
											<td class="black1" width="25%"><% 
												$query="select * from usuario u,  odontologos_encargados_historia  d   
 												where u.usu_id=d.usu_id and d.hic_id = '$obj->pac_id' order by oeh_id ASC";							
												$table->search($query); 
												if ($table->nfound >= 1) {
												$data=$table->sql_fetch_object();
												%><%=$data->usu_nombres." ".$data->usu_apellidos%> <%}%></td>
										</tr>
										<tr>
											<td class="black1" nowrap width="10%">Docente  :</td>
											<td class="black1" nowrap width="25%"><% 
											$query="select * from usuario u,  docentes_encargados_historia  d   
 											where u.usu_id=d.usu_id and d.hic_id = '$obj->pac_id' order by deh_id ASC";							
											$table->search($query); 
											if ($table->nfound >= 1) {
											$data=$table->sql_fetch_object();
											%><%=$data->usu_nombres." ".$data->usu_apellidos%> <%}%></td>
										</tr>
									</table>
								</td>
								<td class="black" width="10%"></td>
								<td class="black" width="35%"></td>
							</tr>
							<tr>
								<td class="black1" colspan="3"><br>
									<b>ANTECEDENTES ESTOMATOLOGICOS U ODONTOLOGICOS<br>
										<br>
									</b></td>
							</tr>
							<tr>
								<td class="black1" colspan="3">
									<table width="100%" border="0" cellspacing="2" cellpadding="0">
										<tr>
											<td width="10%"></td>
											<td class="black" nowrap width="25%">Prevenci&oacute;n :</td>
											<td width="20%"><input type="checkbox" name="dpa_antOdonPrevencion" value="1" <%=($dpa_antOdonPrevencion)?"checked":""%> border="0"></td>
											<td class="black" width="25%">Pr&oacute;tesis :</td>
											<td><input type="checkbox" name="dpa_protesis" value="1" <%=($dpa_protesis)?"checked":""%> border="0"></td>
										</tr>
										<tr>
											<td width="10%"></td>
											<td class="black" width="25%">Operatoria :</td>
											<td width="20%"><input type="checkbox" name="dpa_operatoria" value="1" <%=($dpa_operatoria)?"checked":""%> border="0"></td>
											<td class="black" nowrap width="25%">Ortodoncia :</td>
											<td><input type="checkbox" name="dpa_ortodoncia" value="1" <%=($dpa_ortodoncia)?"checked":""%> border="0"></td>
										</tr>
										<tr>
											<td width="10%"></td>
											<td class="black" width="25%">Periodoncia :</td>
											<td width="20%"><input type="checkbox" name="dpa_periodoncia" value="1" <%=($dpa_periodoncia)?"checked":""%> border="0"></td>
											<td class="black" width="25%">Ortopedia :</td>
											<td><input type="checkbox" name="dpa_ortopedia" value="1" <%=($dpa_ortopedia)?"checked":""%> border="0"></td>
										</tr>
										<tr>
											<td width="10%"></td>
											<td class="black" nowrap width="25%">Endodoncia :</td>
											<td width="20%"><input type="checkbox" name="dpa_endodoncia" value="1" <%=($dpa_endodoncia)?"checked":""%> border="0"></td>
											<td class="black" nowrap width="25%">Cirug&iacute;a Oral :</td>
											<td><input type="checkbox" name="dpa_cirugiaOral" value="1" <%=($dpa_cirugiaOral)?"checked":""%> border="0"></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td class="black1" colspan="3"><br>
									Observaciones  : <br>
									<textarea name="dpa_comentariosAntOdo" rows="4" cols="100"><%=$dpa_comentariosTrauma%></textarea></td>
							</tr>
							<tr>
								<td class="black1" colspan="3">
									<table width="100%" border="1" cellspacing="2" cellpadding="0">
										<tr>
											<td class="black"colspan="3">Fecha &uacute;ltima visita al odont&oacute;logo: (DD/MM/AAAA) <select class="black" name="fvo_dia" size="1">
													<option value="-1">Seleccione</option>
													<option <%=($fvo_dia==01)?"selected":""%> value="01">01</option>
													<option <%=($fvo_dia==02)?"selected":""%> value="02">02</option>
													<option <%=($fvo_dia==03)?"selected":""%> value="03">03</option>
													<option <%=($fvo_dia==04)?"selected":""%> value="04">04</option>
													<option <%=($fvo_dia==05)?"selected":""%> value="05">05</option>
													<option <%=($fvo_dia==06)?"selected":""%> value="06">06</option>
													<option <%=($fvo_dia==07)?"selected":""%> value="07">07</option>
													<option <%=($fvo_dia==08)?"selected":""%> value="08">08</option>
													<option <%=($fvo_dia==09)?"selected":""%> value="09">09</option>
													<option <%=($fvo_dia==10)?"selected":""%> value="10">10</option>
													<option <%=($fvo_dia==11)?"selected":""%> value="11">11</option>
													<option <%=($fvo_dia==12)?"selected":""%> value="12">12</option>
													<option <%=($fvo_dia==13)?"selected":""%> value="13">13</option>
													<option <%=($fvo_dia==14)?"selected":""%> value="14">14</option>
													<option <%=($fvo_dia==15)?"selected":""%> value="15">15</option>
													<option <%=($fvo_dia==16)?"selected":""%> value="16">16</option>
													<option <%=($fvo_dia==17)?"selected":""%> value="17">17</option>
													<option <%=($fvo_dia==18)?"selected":""%> value="18">18</option>
													<option <%=($fvo_dia==19)?"selected":""%> value="19">19</option>
													<option <%=($fvo_dia==20)?"selected":""%> value="20">20</option>
													<option <%=($fvo_dia==21)?"selected":""%> value="21">21</option>
													<option <%=($fvo_dia==22)?"selected":""%> value="22">22</option>
													<option <%=($fvo_dia==23)?"selected":""%> value="23">23</option>
													<option <%=($fvo_dia==24)?"selected":""%> value="24">24</option>
													<option <%=($fvo_dia==25)?"selected":""%> value="25">25</option>
													<option <%=($fvo_dia==26)?"selected":""%> value="26">26</option>
													<option <%=($fvo_dia==27)?"selected":""%> value="27">27</option>
													<option <%=($fvo_dia==28)?"selected":""%> value="28">28</option>
													<option <%=($fvo_dia==29)?"selected":""%> value="29">29</option>
													<option <%=($fvo_dia==30)?"selected":""%> value="30">30</option>
													<option <%=($fvo_dia==31)?"selected":""%> value="31">31</option>
												</select> / <select class="black" name="fvo_mes" size="1">
													<option value="-1">Seleccione</option>
													<option <%=($fvo_mes==01)?"selected":""%> value="01">Enero</option>
													<option <%=($fvo_mes==02)?"selected":""%> value="02">Febrero</option>
													<option <%=($fvo_mes==03)?"selected":""%> value="03">Marzo</option>
													<option <%=($fvo_mes==04)?"selected":""%> value="04">Abril</option>
													<option <%=($fvo_mes==05)?"selected":""%> value="05">Mayo</option>
													<option <%=($fvo_mes==06)?"selected":""%> value="06">Junio</option>
													<option <%=($fvo_mes==07)?"selected":""%> value="07">Julio</option>
													<option <%=($fvo_mes==08)?"selected":""%> value="08">Agosto</option>
													<option <%=($fvo_mes==09)?"selected":""%> value="09">Septiembre</option>
													<option <%=($fvo_mes==10)?"selected":""%> value="10">Octubre</option>
													<option <%=($fvo_mes==11)?"selected":""%> value="11">Noviembre</option>
													<option <%=($fvo_mes==12)?"selected":""%> value="12">Diciembre</option>
												</select> / <input type="text" class="black" name="fvo_anio" value="<%=$fvo_anio%>" size="6" border="0"><br>
												<br>
											</td>
										</tr>
										<tr>
											<td class="black" width="45%">Motivo de &uacute;ltima visita:<br>
												<textarea class="black" name="dpn_motivo" rows="4" cols="52"><%=$dpn_motivo%></textarea></td>
											<td width="10%"></td>
											<td class="black" valign="top">Ha presentado alguna reacci&oacute;n desfavorable  hacia el tratamiento odontol&oacute;gico: <input type="checkbox" name="dpn_reaccionDesfavorableTratam" value="1" <%=($dpn_reaccionDesfavorableTratam)?"checked":""%> border="0"><br>
												<br>Hay en la familia historia de condiciones dentales raras, como: dientes ausentes o extras: <input type="checkbox" name="dpn_condicionesDentalesRaras" value="1" <%=($dpn_condicionesDentalesRaras)?"checked":""%> border="0"></td>
										</tr>
										<tr>
											<td class="black" width="45%">Que trauma oral o dental ha presentado el ni&ntilde;o:<br>
												<textarea class="black" name="dpn_tiposTraumaOralDental" rows="4" cols="52"><%=$dpn_tiposTraumaOralDental%></textarea></td>
											<td width="10%"></td>
											<td class="black" valign="top">Hubo cambios de color, movilidad: <input type="checkbox" name="dpn_cambiosColorMovilidad" value="1" <%=($dpn_cambiosColorMovilidad)?"checked":""%> border="0"><br>
												<br>Ha sido cooperador con el m&eacute;dico y odont&oacute;logo del pasado: <input type="checkbox" name="dpn_ninoCooperadorOdontologo" value="1" <%=($dpn_ninoCooperadorOdontologo)?"checked":""%> border="0"></td>
										</tr>
										<tr>
											<td class="black" width="45%">Qu&eacute; tratamientos caseros o aplicados por el odont&oacute;logo a utilizado en el pasado:<br>
												<textarea class="black" name="dpn_tratamientosCaseros" rows="4" cols="52"><%=$dpn_tratamientosCaseros%></textarea></td>
											<td width="10%"></td>
											<td class="black" valign="top">Observaciones:<br>
												<textarea class="black" name="dpn_observacionesOdontologicas" rows="4" cols="52"><%=$dpn_observacionesOdontologicas%></textarea></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td width="35%"></td>
								<td nowrap width="10%">
									<div align="right">
										<br>
										<input type="hidden" name="s_opc" value="crear4" border="0">  <input type="hidden" name="origen" value="antGenerales" border="0"> <input type="hidden" name="opc" value="<%=$opc%>" border="0">
										 <input type="hidden" name="dpa_id" value="<%=$dpa_id%>" border="0"> 
										 <input type="hidden" name="pac_id" value="<%=$pac_id%>" border="0"> 
										 <input type="hidden" name="hic_id" value="<%=$paciente->pac_id%>" border="0"> 
										<p>  <input type="hidden" name="odl_id" value="<%=$usuario->usu_id%>" border="0"> 
										<input type="hidden"  name="doc_id" value="<%=$usu->usu_id%>"> 
										<input type="hidden" name="dpa_estado" value="3" border="0"> 
										<input type="image" src="images/crear2.gif" alt="" align="right" border="1"><br>
											<br>
											<br>
											<br>
										</p>
									</div>
								</td>
								<td align="right" width="35%"></td>
							</tr>
						</table>
					</form>
				</td>
			</tr>
		</table>
	</body>

</html>
<%
		}
	}
}
%>
