<%
if (!$table){
%>Error en la conexi&oacute;n con la base de datos<br>
    Consulte con el administrador del sistema
    <%include('adulto.php');
	//exit(0);
}else{
	$usuario=$user->datos;
	$query= "SELECT hic_id FROM historia_clinica WHERE pac_id=$pac_id";
	$table->search($query);
	$obj = $table->sql_fetch_object();
	if (!$table->nfound){

	$mess="<center><b>Los datos de la Anamnesis NO han sido ingresados.</b> <br><br>
      			Por favor ingreselos primero</center>";
   	 		show_mess($mess,"alert");
	}else{
	$query="select * from datos_personales_adulto where pac_id=$pac_id";
	//echo $query;
	$table->sql_query($query);
	$obj = $table->sql_fetch_object();
	$dpa_id = $obj->dpa_id;		
      
     	if($obj->dpa_estado==2){
      	$mess="<center><b>Los Antecedentes Genereales YA han sido ingresados.</b> <br><br>
      			Por favor continue ingresando los datos de la Revisi&oacute;n por Sistema</center>";
   	show_mess($mess,"alert"); 	 		
       }else{
   
/*******************/

$pac = new adulto( $pac_id);
$obj=$pac->data;

  $query= "SELECT * FROM paciente WHERE pac_id=$pac_id";
   $table->search($query);
   $paciente = $table->sql_fetch_object();

   $usuario=$user->datos;
%>


<html>
	<body bgcolor="#ffffff">
		<table width="90%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<div align="center">
						<h2 class="black1"></h2>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<form action="<%=$PHP_SELF%>" method="post" name="adulto1">
						<p></p>
						<table class="black" width="80%" border="0" cellspacing="2" cellpadding="0" align="center">
							<tr>
								<td class="black" width="35%"></td>
								<td class="black" width="10%"></td>
								<td class="black1" align="right" width="35%">
									
										Historia Cl&iacute;nica No: <%=$paciente->pac_numeroIdentificacion%><br>
									Fecha : <%=$obj->dpa_fechaElaboracion%><br>
									
								</td>
							</tr>
							<tr>
								<td class="black" width="35%">
									<table class="black" width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
										<tr>
											<td class="black1" nowrap width="10%">Paciente : </td>
											<td class="black1" width="25%"><%="$paciente->pac_nombres $paciente->pac_apellidos"%></td>
										</tr>
										<tr>
											<td class="black1" nowrap width="10%">Odont&oacute;logo  :  </td>
											<td class="black1" width="25%"><% 
												$query="select * from usuario u,  odontologos_encargados_historia  d   
 												where u.usu_id=d.usu_id and d.hic_id = '$obj->pac_id' order by oeh_id ASC";							
												$table->search($query); 
												if ($table->nfound >= 1) {
												$data=$table->sql_fetch_object();
												%><%=$data->usu_nombres." ".$data->usu_apellidos%> <%}%></td>
										</tr>
										<tr>
											<td class="black1" nowrap width="10%">Docente  :</td>
											<td class="black1" nowrap width="25%"><% 
											$query="select * from usuario u,  docentes_encargados_historia  d   
 											where u.usu_id=d.usu_id and d.hic_id = '$obj->pac_id' order by deh_id ASC";							
											$table->search($query); 
											if ($table->nfound >= 1) {
											$data=$table->sql_fetch_object();
											%><%=$data->usu_nombres." ".$data->usu_apellidos%> <%}%></td>
										</tr>
									</table>
								</td>
								<td class="black" width="10%"></td>
								<td class="black" width="35%"></td>
							</tr>
							<tr>
								<td class="black1" colspan="3">
									<div align="center">
										<b>ANTECEDENTES GENERALES <br>
											<br>
										</b></div>
								</td>
							</tr>
							<tr>
								<td class="black" valign="top" width="35%">
									<p class="black1">SOCIALES :</p>
									<table  width="100%" border="0" cellspacing="2" cellpadding="0">
										<tr>
											<td class="black1" width="40%">-  Motivaci&oacute;n :</td>
											<td></td>
										</tr>
										<tr>
											<td class="black" width="40%">Interna<input type="radio" name="dpa_antSocMotivacionInterna" value="1" <%=($dpa_antSocMotivacionInterna=="1")?"checked":""%> checked border="0"></td>
											<td class="black">Externa<input type="radio" name="dpa_antSocMotivacionInterna" value="0" <%=($dpa_antSocMotivacionInterna=="0")?"checked":""%> border="0"></td>
										</tr>
									</table>
									<br><span class="black1">-  Expetactivas Frente al Tratamiento: </span>
									<textarea name="dpa_espectativasTratamiento" rows="4" cols="43"><%=$dpa_espectativasTratamiento%></textarea><br>
									<br><span class="black1">-  Entorno Socio-Econ&oacute;mico :</span><br>
									<textarea name="dpa_entornoSocioeconomico" rows="4" cols="43"><%=$dpa_entornoSocioeconomico%></textarea>
									<br><span class="black1">	-  Hobbies:</span><br>
									<textarea name="dpa_hobbies" rows="4" cols="43"><%=$dpa_hobbies%></textarea><br>
									<br>
									<br>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td class="black1">PATOLOGICOS :</td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="2" cellpadding="0">
										<tr>
											<td width="10%"></td>
											<td class="black" nowrap width="35%">Gastrointestinales :</td>
											<td width="20%"><input type="checkbox" name="dpa_gastrointestinales" value="1" <%=($dpa_gastrointestinales)?"checked":""%> border="0"></td>
											<td class="black" width="30%">Neurol&oacute;gicos :</td>
											<td width="4%"><input type="checkbox" name="dpa_neurologicos" value="1" <%=($dpa_neurologicos)?"checked":""%> border="0"></td>
										</tr>
										<tr>
											<td width="10%"></td>
											<td class="black" width="35%">Cardiovasculares :</td>
											<td width="20%"><input type="checkbox" name="dpa_cardiovasculares" value="1" <%=($dpa_cardiovasculares)?"checked":""%> border="0"></td>
											<td class="black" width="30%">Siqui&aacute;tricos :</td>
											<td width="4%"><input type="checkbox" name="dpa_siquiatricos" value="1" <%=($dpa_siquiatricos)?"checked":""%> border="0"></td>
										</tr>
										<tr>
											<td width="10%"></td>
											<td class="black" width="35%">Pulmonares :</td>
											<td width="20%"><input type="checkbox" name="dpa_pulmonares" value="1" <%=($dpa_pulmonares)?"checked":""%> border="0"></td>
											<td class="black" width="30%">Neopl&aacute;sicos :</td>
											<td width="4%"><input type="checkbox" name="dpa_neoplasicos" value="1" <%=($dpa_neoplasicos)?"checked":""%> border="0"></td>
										</tr>
										<tr>
											<td width="10%"></td>
											<td class="black" width="35%">Eruptivas :</td>
											<td width="20%"><input type="checkbox" name="dpa_antMedPatologicosEruptivas" value="1" <%=($dpa_antMedPatologicosEruptivas)?"checked":""%> border="0"></td>
											<td class="black" width="30%">Endocrinos</td>
											<td width="4%"><input type="checkbox" name="dpa_endocrinos" value="1" <%=($dpa_endocrinos)?"checked":""%> border="0"></td>
										</tr>
										<tr>
											<td width="10%"></td>
											<td class="black" width="35%">Genitourinarios :</td>
											<td width="20%"><input type="checkbox" name="dpa_genitourinarios" value="1" <%=($dpa_genitourinarios)?"checked":""%> border="0"></td>
											<td class="black" width="30%">Ven&eacute;reos :</td>
											<td width="4%"><input type="checkbox" name="dpa_venereos" value="1" <%=($dpa_venereos)?"checked":""%> border="0"></td>
										</tr>
										<tr>
											<td width="10%"></td>
											<td class="black" width="35%">Inmunol&oacute;gicos :</td>
											<td width="20%"><input type="checkbox" name="dpa_inmunologicos" value="1" <%=($dpa_inmunologicos)?"checked":""%> border="0"></td>
											<td width="30%"></td>
											<td width="4%"></td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="2" cellpadding="0">
										<tr>
											<td class="black1"><br>Observaciones Patol&oacute;gicas: <textarea name="dpa_comentariosPatologicos" rows="4" cols="42"><%=$dpa_comentariosPatologicos%></textarea></td>
										</tr>
									</table>
								</td>
								<td class="black" width="10%"></td>
								<td class="black" width="35%">
									<p class="black1">ODONTOLOGICOS :</p>
									<table width="100%" border="0" cellspacing="2" cellpadding="0">
										<tr>
											<td width="10%"></td>
											<td class="black" width="25%">Prevenci&oacute;n :</td>
											<td width="20%"><input type="checkbox" name="dpa_antOdonPrevencion" value="1" <%=($dpa_antOdonPrevencion)?"checked":""%> border="0"></td>
											<td class="black" width="25%">Pr&oacute;tesis :</td>
											<td><input type="checkbox" name="dpa_protesis" value="1" <%=($dpa_protesis)?"checked":""%> border="0"></td>
										</tr>
										<tr>
											<td width="10%"></td>
											<td class="black" width="25%">Operatoria :</td>
											<td width="20%"><input type="checkbox" name="dpa_operatoria" value="1" <%=($dpa_operatoria)?"checked":""%> border="0"></td>
											<td class="black" width="25%">Ortodoncia :</td>
											<td><input type="checkbox" name="dpa_ortodoncia" value="1" <%=($dpa_ortodoncia)?"checked":""%> border="0"></td>
										</tr>
										<tr>
											<td width="10%"></td>
											<td class="black" width="25%">Periodoncia :</td>
											<td width="20%"><input type="checkbox" name="dpa_periodoncia" value="1" <%=($dpa_periodoncia)?"checked":""%> border="0"></td>
											<td class="black" width="25%">Ortopedia :</td>
											<td><input type="checkbox" name="dpa_ortopedia" value="1" <%=($dpa_ortopedia)?"checked":""%> border="0"></td>
										</tr>
										<tr>
											<td width="10%"></td>
											<td class="black" width="25%">Endodoncia :</td>
											<td width="20%"><input type="checkbox" name="dpa_endodoncia" value="1" <%=($dpa_endodoncia)?"checked":""%> border="0"></td>
											<td class="black" width="25%">Cirug&iacute;a Oral :</td>
											<td><input type="checkbox" name="dpa_cirugiaOral" value="1" <%=($dpa_cirugiaOral)?"checked":""%> border="0"></td>
										</tr>
									</table>
									<p class="black1">OTROS :</p>
									<table width="100%" border="0" cellspacing="2" cellpadding="0">
										<tr>
											<td width="10%"></td>
											<td class="black" nowrap width="30%">Hospitalarios :</td>
											<td><input type="checkbox" name="dpa_hospitalarios" value="1" <%=($dpa_hospitalarios)?"checked":""%> border="0"></td>
										</tr>
										<tr>
											<td width="10%"></td>
											<td class="black" width="30%">Quir&uacute;rgicos :</td>
											<td><input type="checkbox" name="dpa_quirurgicos" value="1" <%=($dpa_quirurgicos)?"checked":""%> border="0"></td>
										</tr>
										<tr>
											<td width="10%"></td>
											<td class="black" width="30%">Traum&aacute;ticos :</td>
											<td><input type="checkbox" name="dpa_traumaticos" value="1" <%=($dpa_traumaticos)?"checked":""%> border="0"></td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="2" cellpadding="0">
										<tr>
											<td width="10%"></td>
											<td class="black1">Observaciones  : <textarea name="dpa_comentariosTrauma" rows="4" cols="40"><%=$dpa_comentariosTrauma%></textarea><br>
												<br>
											</td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="2" cellpadding="0">
										<tr>
											<td width="10%"></td>
											<td class="black" width="30%">F&aacute;rmacos :</td>
											<td><input type="checkbox" name="dpa_farmacos" value="1" <%=($dpa_farmacos)?"checked":""%> border="0"></td>
										</tr>
										<tr>
											<td width="10%"></td>
											<td class="black" width="30%">Radiaci&oacute;n :</td>
											<td><input type="checkbox" name="dpa_radiacion" value="1" <%=($dpa_radiacion)?"checked":""%> border="0"></td>
										</tr>
										<tr>
											<td width="10%"></td>
											<td class="black" width="30%">Toxicoal&eacute;rgicos :</td>
											<td><input type="checkbox" name="dpa_toxicoalergicos" value="1" <%=($dpa_toxicoalergicos)?"checked":""%> border="0"></td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="2" cellpadding="0">
										<tr>
											<td class="black1">Observaciones  : <textarea name="dpa_comentariosRadiacion" rows="4" cols="40"><%=$dpa_comentariosRadiacion%></textarea><br>
												<br>
											</td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="2" cellpadding="0">
										<tr>
											<td width="10%"></td>
											<td class="black" width="40%">Hemorr&aacute;gicos :</td>
											<td><input type="checkbox" name="dpa_hemorragicos" value="1" <%=($dpa_hemorragicos)?"checked":""%> border="0"></td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="2" cellpadding="0">
										<tr>
											<td width="10%"></td>
											<td class="black" width="40%">Ginecobst&eacute;tricos :</td>
											<td><input type="checkbox" name="dpa_ginecobstetricos" value="1" <%=($dpa_ginecobstetricos)?"checked":""%> border="0"></td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="2" cellpadding="0">
										<tr>
											<td width="10%"></td>
											<td class="black" width="40%">Problemas Articulares :</td>
											<td><input type="checkbox" name="dpa_problemasArticulares" value="1" <%=($dpa_problemasArticulares)?"checked":""%> border="0"></td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="2" cellpadding="0">
										<tr>
											<td width="10%"></td>
											<td class="black1" width="40%">Observaciones  :<br>
												<textarea name="dpa_comentariosProblemas" rows="4" cols="40"><%=$dpa_comentariosProblemas%></textarea><br>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td width="35%"></td>
								<td nowrap width="10%">
									<div align="right">
										<br>
										<input type="hidden" name="s_opc" value="crear2" border="0">  <input type="hidden" name="origen" value="antGenerales" border="0"> <input type="hidden" name="opc" value="<%=$opc%>" border="0">
										 <input type="hidden" name="dpa_id" value="<%=$dpa_id%>" border="0"> 
										 <input type="hidden" name="pac_id" value="<%=$pac_id%>" border="0"> 
										 <input type="hidden" name="hic_id" value="<%=$paciente->pac_id%>" border="0"> 
										<p>  <input type="hidden" name="odl_id" value="<%=$usuario->usu_id%>" border="0"> 
										<input type="hidden"  name="doc_id" value="<%=$usu->usu_id%>"> 
										<input type="hidden" name="dpa_estado" value="2" border="0"> 
										<input type="image" src="images/crear2.gif" alt="" align="right" border="1"><br>
											<br>
											<br>
											<br>
										</p>
									</div>
								</td>
								<td align="right" width="35%"></td>
							</tr>
						</table>
					</form>
				</td>
			</tr>
		</table>
	</body>

</html>
<%
		}
	}
}
%>
