<?
	$query = "SELECT * FROM paciente  WHERE pac_id=$pac_id";
 	$table->search($query);
 	$obj = $table->sql_fetch_object();
	
	$query = "SELECT * FROM datos_per_pac  WHERE pac_id=$pac_id";
 	$table->search($query);
	$dpp = $table->sql_fetch_object();
	//echo $query;
	
	if (!$table->nfound ){
  		$mess="<center><b>Estos datos a&uacute;n no han sido ingresados en su totalidad.</b> <br><br>";
   		$mess_cod="info";
   		show_mess($mess,"alert");
   	}else{
		

?>

		<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td>
					<div align="center">
						<p class="black1"></p>
					</div>
					<form action="<?=$PHPSELF?>" method="post" name="paciente">
						<table width="100%" border="0" cellspacing="2" cellpadding="0" align="center">
							<tr>
								<td class="black1" colspan="4" align="center"><b>IDENTIFICACION DEL PACIENTE<br>
										<br>
									</b></td>
							</tr>
							<tr>
								<td width="20%"></td>
								<td width="25%"></td>
								<td class="black1" align="right" nowrap width="35%">NO. HISTORIA CL&Iacute;NICA : &nbsp;&nbsp;<?=$obj->pac_numeroIdentificacion?></td>
								<td class="black" align="left" nowrap width="20%" width="25%"></td>
							</tr>
							<tr>
								<td width="20%"></td>
								<td width="25%"></td>
								<td class="black" align="right" nowrap width="35%">Fecha de ingreso:&nbsp;&nbsp;<?=$obj->pac_fechaIngreso?></td>
								<td class="black" align="left" nowrap width="20%"></td>
							</tr>
							<tr>
								<td class="black1" colspan="2"><br>Datos Basicos</td>
								<td width="35%"></td>
								<td width="20%"></td>
							</tr>
							<tr>
								<td class="black1" colspan="4">
									<table width="100%" border="0" cellspacing="4" cellpadding="0" align="center">
										<tr>
											<td class="black" nowrap bgcolor="white" width="15%">Nombres y Apellidos : </td>
											<td class="black" nowrap bgcolor="white" width="30%">&nbsp;&nbsp;<?="$obj->pac_nombres $obj->pac_apellidos"?></td>
											<td class="black" nowrap bgcolor="white" width="15%"></td>
											<td class="black" nowrap bgcolor="white" width="40%">&nbsp;&nbsp;</td>
										</tr>
										<tr>
											<td class="black" nowrap bgcolor="white" width="15%">Identificaci&oacute;n :</td>
											<td class="black" nowrap bgcolor="white" width="30%">&nbsp;&nbsp;<? 
												 $query="select * from tipo_documento
												 where tid_id = '$obj->tid_id'";							
												 $table->search($query); 
												 if ($table->nfound >= 1) {
													$data=$table->sql_fetch_object();
													?><?=$data->tid_tipo?>
												<?}?><?=$obj->pac_numeroIdentificacion?></td>
											<td class="black" nowrap bgcolor="white" width="15%">Expedida en: </td>
											<td class="black" nowrap bgcolor="white" width="40%">&nbsp;&nbsp; <? 
												$query="select * from ciudad
												where ciu_id = '$obj->pac_lugarExpedicion'";							
												$table->search($query); 
												if ($table->nfound >= 1) {
													$data=$table->sql_fetch_object();
													?><?=$data->ciu_nombreCiudad?>
												<?}?></td>
										</tr>
										<tr>
											<td class="black" nowrap bgcolor="white" width="15%">Tipo de Paciente : </td>
											<td class="black" nowrap bgcolor="white" width="30%">&nbsp;&nbsp;<? 
												 $query="select * from tipo_paciente
												 where tip_id = '$obj->tip_id'";							
												 $table->search($query); 
												 if ($table->nfound >= 1) {
													$data=$table->sql_fetch_object();
													?><?=$data->tip_categoriaPaciente?>
												<?}?></td>
											<td class="black" nowrap bgcolor="white" width="15%">&Aacute;rea de Atenci&oacute;n:</td>
											<td class="black" nowrap bgcolor="white" width="40%">&nbsp;&nbsp;<? 
												$query="select * from area_atencion
												where ara_id = '$obj->ara_id'";							
												$table->search($query); 
												if ($table->nfound >= 1) {
													$data=$table->sql_fetch_object();
													?><?=$data->ara_nombreArea?>
												<?}?></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr height="3%">
								<td width="20%" height="3%"></td>
								<td width="25%" height="3%"></td>
								<td width="35%" height="3%"></td>
								<td width="20%" height="3%"></td>
							</tr>
							<tr>
								<td class="black1" colspan="2" nowrap>
									Datos de los Padres.</td>
								<td width="35%"></td>
								<td width="20%"></td>
							</tr>
							<tr>
								<td class="black1" colspan="4">
									<table width="100%" border="0" cellspacing="1" cellpadding="2" align="center" bgcolor="black">
										<tr>
											<td class="black" nowrap bgcolor="white" width="10%">Nom Pap&aacute; : </td>
											<td class="black" nowrap bgcolor="white" width="30%">&nbsp;&nbsp;<?=$dpp->dpp_nombrePadre?></td>
											<td class="black" bgcolor="white" width="15%">Ocupaci&oacute;n : </td>
											<td class="black" bgcolor="white" width="40%">&nbsp;&nbsp;<?=$dpp->dpp_ocupacionPadre?></td>
										</tr>
										<tr>
											<td class="black" bgcolor="white" width="10%">Empresa :</td>
											<td class="black" nowrap bgcolor="white" width="30%">&nbsp;&nbsp;<?=$dpp->dpp_institucionPadre?></td>
											<td class="black" nowrap bgcolor="white" width="15%">Direcci&oacute;n : </td>
											<td class="black" bgcolor="white" width="40%">&nbsp;&nbsp;<?=$dpp->dpp_dirPadre?></td>
										</tr>
										<tr>
											<td class="black" nowrap bgcolor="white" width="10%">Tel&eacute;fonos :</td>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;<?=$dpp->dpp_telefonoInstitucionPadre?></td>
											<td class="black" nowrap bgcolor="white" width="15%"></td>
											<td class="black" bgcolor="white" width="40%"></td>
										</tr>
										<tr>
											<td class="black" nowrap bgcolor="white" width="10%">Nom Mam&aacute; : </td>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;<?=$dpp->dpp_nombreMadre?></td>
											<td class="black" bgcolor="white" width="15%">Ocupaci&oacute;n : </td>
											<td class="black" bgcolor="white" width="40%">&nbsp;&nbsp;<?=$dpp->dpp_ocupacionMadre?></td>
										</tr>
										<tr>
											<td class="black" nowrap bgcolor="white" width="10%">Empresa :</td>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;<?=$dpp->dpp_institucionMadre?></td>
											<td class="black" bgcolor="white" width="15%">Direcci&oacute;n : </td>
											<td class="black" bgcolor="white" width="40%">&nbsp;&nbsp;<?=$dpp->dpp_dirMadre?></td>
										</tr>
										<tr>
											<td class="black" nowrap bgcolor="white" width="10%">Tel&eacute;fonos :</td>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;<?=$dpp->dpp_telefonoInstitucionMadre?></td>
											<td class="black" bgcolor="white" width="15%"></td>
											<td class="black" bgcolor="white" width="40%"></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td class="black1" colspan="2" nowrap><br>
									Sistema de Seguridad Social. </td>
								<td width="35%"></td>
								<td width="20%"></td>
							</tr>
							<tr>
								<td class="black1" colspan="4">
									<table width="100%" border="0" cellspacing="1" cellpadding="2" align="center" bgcolor="black">
										<tr>
											<td class="black" bgcolor="white" width="10%">E.P.S : </td>
											<td class="black" nowrap bgcolor="white" width="30%">&nbsp;&nbsp;<? 
												       $query="select * from eps
												      	 where eps_id = '$dpp->eps_id'";							
												       $table->search($query); 
												       if ($table->nfound >= 1) {
													  $data=$table->sql_fetch_object();
													 ?><?=$data->eps_nombreEps?>
<?}?></td>
											<td class="black" bgcolor="white" width="15%"> I.P.S : </td>
											<td class="black" bgcolor="white" width="40%">&nbsp;&nbsp;<? 
												       $query="select * from ips
												      	 where ips_id = '$dpp->ips_id'";							
												       $table->search($query); 
												       if ($table->nfound >= 1) {
													  $data=$table->sql_fetch_object();
													 ?><?=$data->ips_nombreIps?>
<?}?></td>
										</tr>
										<tr>
											<td class="black" bgcolor="white" width="10%">Plan de Atenci&oacute;n:</td>
											<td class="black" nowrap bgcolor="white" width="30%">&nbsp;&nbsp;<? 
												       $query="select * from plan_atencion
												      	 where pla_id = '$dpp->pla_id'";							
												       $table->search($query); 
												       if ($table->nfound >= 1) {
													  $data=$table->sql_fetch_object();
													 ?><?=$data->pla_nombrePlan?> <?}?></td>
											<td class="black" nowrap bgcolor="white" width="15%">M&eacute;dico Tratante:</td>
											<td class="black" bgcolor="white" width="40%">&nbsp;&nbsp;<?=$dpp->dpp_medicoTratante?></td>
										</tr>
										<tr>
											<td class="black" nowrap bgcolor="white" width="10%">Tipo Vinculacion :</td>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;<? 
												       $query="select * from tipo_vinculacion
												      	 where tvu_id = '$dpp->tvu_id'";							
												       $table->search($query); 
												       if ($table->nfound >= 1) {
													  $data=$table->sql_fetch_object();
													 ?><?=$data->tvu_nombreVinculacion?>
<?}?></td>
											<td class="black" nowrap bgcolor="white" width="15%">Remitido Por :</td>
											<td class="black" bgcolor="white" width="40%">&nbsp;&nbsp;<?=$dpp->dpp_remitidoPor?></td>
										</tr>
										<tr>
											<td class="black" nowrap bgcolor="white" width="10%">Regimen Afiliaci&oacute;n:</td>
											<td class="black" bgcolor="white" width="30%">&nbsp;&nbsp;<? 
												       $query="select * from regimen_afiliacion
												      	 where rea_id = '$dpp->rea_id'";							
												       $table->search($query); 
												       if ($table->nfound >= 1) {
													  $data=$table->sql_fetch_object();
													 ?><?=$data->rea_nombre?>
<?}?></td>
											<td class="black" bgcolor="white" width="15%">Tel&eacute;fono:</td>
											<td class="black" bgcolor="white" width="40%">&nbsp;&nbsp;<?=$dpp->dpp_telefonoMedicoTratante?></td>
										</tr>
										<tr>
											<td class="black" colspan="3" bgcolor="white">Cooperativa a la que pertenece :&nbsp;&nbsp;<? 
												       $query="select * from cooperativa
												      	 where coo_id = '$dpp->coo_id'";							
												       $table->search($query); 
												       if ($table->nfound >= 1) {
													  $data=$table->sql_fetch_object();
													 ?><?=$data->coo_nombre?>
<?}?></td>
											<td bgcolor="white" width="40%"></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr height="6%">
								<td width="20%" height="6%"></td>
								<td width="25%" height="6%"></td>
								<td width="35%" height="6%"></td>
								<td width="20%" height="6%"></td>
							</tr>
							<tr>
							<?$pac = new paciente($pac_id);
								$obj = $pac->data;
								if ($obj->tip_id==1)
		   							$opc_dp="nino";
								else 
		   							$opc_dp="adulto";?>
								<td class="black" colspan="4" align="center" bgcolor="#d1d1d1"><a href='<?="$PHPSELF?opc=paciente&s_opc=visualizardp&pac_id=$pac_id"?>' >Regresar   </a>   
								&nbsp;&nbsp;&nbsp;<a href='<?="$PHPSELF?opc=paciente&s_opc=submenu&pac_id=$pac_id"?>' >Salir  </a></td>
							</tr>
							<tr>
								<td width="20%">
								</td>
								<td colspan="3"></td>
							</tr>
						</table>
						<p></p>
						<p></p>
					</form>
					<p></p>
					<p></p>
				</td>
			</tr>
		</table>
<?
}
?>
