<?
if (!$table){
?>Error en la conexi&oacute;n con la base de datos<br>
		            Consulte con el administrador del sistema</p>
		            
	<p class='black'><?include('nino.php');
}else{
   $query= "SELECT * FROM paciente WHERE pac_id=$pac_id";
   $table->search($query);
   $paciente = $table->sql_fetch_object();
}
$pac = new nino($pac_id);
$obj = $pac->data;
$usuario = $user ->datos;
if (!isset($dpn_id)){
	$dpn_id=$obj->dpn_id;
}
?>
		<table width="100%" border="1" cellspacing="2" cellpadding="0" align="center">
			<tr>
				<td align="center" width="90%">
				<p class="black1">
				</p>
				<form action="<?=$PHP_SELF?>" method="post" name="antPsicNino">
						<div align="center">
							<br>
							<br>
							<p class="black1">EDITANDO LOS DATOS PSICOLOGICOS Y ODONTOLOGICOS<br>
							<br>
							<br>
						</div>
						<div align="left">
							<table width="100%" border="0" cellspacing="2" cellpadding="0">
								<tr>
									<td width="10%"></td>
									<td width="35%">
										<div align="right">
											<p class="black1"></p>
										</div>
									</td>
									<td width="10%"></td>
									<td width="35%">
										<p class="black1">No. HISTORIA CLINICA:<?=$paciente->pac_numeroIdentificacion?><br>
											<br>
										</p>
									</td>
									<td width="10%"></td>
								</tr>
								<tr>
									<td width="10%"></td>
									<td colspan="3">
										<div align="left">
											<p class="black1">Paciente: <?=$paciente->pac_nombres?> <?=$paciente->pac_apellidos?></p>
										</div>
									</td>
									<td width="10%"></td>
								</tr>
								<tr>
									<td width="10%"></td>
									<td colspan="3">
										<p class="black1">Odont&oacute;logo:  <?="$usuario->usu_nombres  $usuario->usu_apellidos"?></p>
									</td>
									<td width="10%"></td>
								</tr>
								<tr>
									<td width="10%"></td>
									<td colspan="3"><p class="black1">Docente:  <? 
											$query="select * from usuario u,  docentes_encargados_historia  d   
 											where u.usu_id=d.usu_id and d.hic_id = '$obj->pac_id' order by deh_id ASC";							
											$table->search($query); 
											if ($table->nfound >= 1) {
											$data=$table->sql_fetch_object();
											?>  <?=$data->usu_nombres." ".$data->usu_apellidos?>  <?}?><br>
											<br>
											<br>
										
										
										</td>
									<td width="10%"></td>
								</tr>
								<tr>
									<td width="10%"></td>
									<td colspan="3">
										<p class="black1">Psicol&oacute;gicos:<br>
											<br>
										</p>
									</td>
									<td width="10%"></td>
								</tr>
								<tr>
									<td width="10%"></td>
									<td colspan="3">
										<table width="100%" border="0" cellspacing="5" cellpadding="0">
											<tr>
												<td nowrap width="20%">
													<p class="black">Vive con los padres: <input type="checkbox" name="dpn_antPsicVivePadres" value="1" <?=($obj->dpn_antPsicVivePadres=="t")?"checked":""?> border="0"></p>
												</td>
												<td nowrap width="33%">
													<div align="left">
														<p class="black">Cu&aacute;ntos hermanos tiene el ni&ntilde;o: <input type="text" name="dpn_cuantosHermanosTiene" value="<?=$obj->dpn_cuantosHermanosTiene?>" size="2" maxlength="2" border="0"></p>
													</div>
												</td>
												<td nowrap width="40%">
													<div align="left">
														<p class="black">N&uacute;mero que ocupa entre hermanos: <input type="text" name="dpn_numeroOcupa" value="<?=$obj->dpn_numeroOcupa?>" size="2" maxlength="2" border="0"></p>
													</div>
												</td>
											</tr>
										</table>
									</td>
									<td width="10%"></td>
								</tr>
								<tr>
									<td width="10%"></td>
									<td rowspan="4" width="35%">
										<p class="black">Como es el trato con el ni&ntilde;o por parte de los padres:<br>
											<textarea name="dpn_comoTratanNino" rows="4" cols="32"><?=$obj->dpn_comoTratanNino?></textarea></p>
									</td>
									<td width="10%"></td>
									<td rowspan="4" width="35%">
										<div align="left">
											<p class="black">Como es el trato con el ni&ntilde;o por parte de los hermanos:<br>
												<textarea name="dpn_comoTratanHermanos" rows="4" cols="32"><?=$obj->dpn_comoTratanHermanos?></textarea></p>
										</div>
									</td>
									<td width="10%"></td>
								</tr>
								<tr>
									<td width="10%"></td>
									<td width="10%"></td>
									<td width="10%"></td>
								</tr>
								<tr>
									<td width="10%"></td>
									<td width="10%"></td>
									<td width="10%"></td>
								</tr>
								<tr>
									<td width="10%"></td>
									<td width="10%"></td>
									<td width="10%"></td>
								</tr>
								<tr>
									<td width="10%"></td>
									<td width="35%">
										<p class="black">Qui&eacute;n cuida del ni&ntilde;o: <? 
												       $query="select * from cuida_del_nino";								
												       $table->search($query);
												       if ($table->nfound >= 1) {												 
													 ?><select name="cdn_id" size="1">
												<? echo "\n";
													 while ($data=$table->sql_fetch_object()){  
													   			?>
												<option value="<?=$data->cdn_id?>" <?=($obj->cdn_id==$data->cdn_id)?"selected":""?>><?=$data->cdn_nombreEncargado; echo "\n"; ?></option><?}?>
											</select><?}?></p>
									</td>
									<td width="10%"></td>
									<td rowspan="4" width="35%">
										<div align="left">
											<p class="black">Observaciones:<br>
												<textarea name="dpn_observacionesPsicologicas" rows="4" cols="32"><?=$obj->dpn_observacionesPsicologicas?></textarea></p>
										</div>
									</td>
									<td width="10%"></td>
								</tr>
								<tr>
									<td width="10%"></td>
									<td width="35%"></td>
									<td width="10%"></td>
									<td width="10%"></td>
								</tr>
								<tr>
									<td width="10%"></td>
									<td width="35%"></td>
									<td width="10%"></td>
									<td width="10%"></td>
								</tr>
								<tr>
									<td width="10%"></td>
									<td width="35%"></td>
									<td width="10%"></td>
									<td width="10%"></td>
								</tr>
								<tr>
									<td width="10%"></td>
									<td colspan="3">
										<p class="black1"><br>
											Odontol&oacute;gicos:<br>
										</p>
									</td>
									<td width="10%"></td>
								</tr>
								<tr>
									<td nowrap width="10%"></td>
									<td colspan="3" nowrap>
										<p class="black">Fecha &uacute;ltima visita al odont&oacute;logo: (DD/MM/AAAA) <select name="fvo_dia" size="1">
												<? echo "\n";
													 while ($data=$table->sql_fetch_object()){  
													   			?><?}?>
												<option value="01">1</option>
												<option value="02">2</option>
											</select> / <select name="fvo_mes" size="1">
												<? echo "\n";
													 while ($data=$table->sql_fetch_object()){  
													   			?><?}?>
												<option value="01">enero</option>
												<option value="02">febrero</option>
											</select> / <select name="fvo_anio" size="1">
												<option value="01">2000</option>
												<option value="02">2001</option>
											</select><br>
											<br>
										</p>
									</td>
									<td nowrap width="10%"></td>
								</tr>
								<tr>
									<td width="10%"></td>
									<td rowspan="2">
										<p class="black">Motivo de &uacute;ltima visita:<br>
											<textarea name="dpn_motivo" rows="4" cols="32"><?=$obj->dpn_motivo?></textarea></p>
									</td>
									<td width="10%"></td>
									<td width="35%">
										<p class="black"><br>
											Ha presentado alguna reacci&oacute;n desfavorable el ni&ntilde;o hacia el tratamiento odontol&oacute;gico: <input type="checkbox" name="dpn_reaccionDesfavorableTratam" value="1" <?=($obj->dpn_reaccionDesfavorableTratam=="t")?"checked":""?> border="0"></p>
									</td>
									<td width="10%"></td>
								</tr>
								<tr>
									<td width="10%"></td>
									<td width="10%"></td>
									<td width="35%">
										<p class="black"><br>
											Hay en la familia historia de condiciones dentales raras, como: dientes ausentes o extras: <input type="checkbox" name="dpn_condicionesDentalesRaras" value="1" <?=($obj->dpn_condicionesDentalesRaras=="t")?"checked":""?> border="0"></p>
									</td>
									<td nowrap width="10%"></td>
								</tr>
								<tr>
									<td width="10%"></td>
									<td width="35%"></td>
									<td width="10%"></td>
									<td nowrap width="35%"></td>
									<td nowrap width="10%"></td>
								</tr>
								<tr>
									<td width="10%"></td>
									<td rowspan="3">
										<p class="black">Que trauma oral o dental ha presentado el ni&ntilde;o:<br>
											<textarea name="dpn_tiposTraumaOralDental" rows="4" cols="32"><?=$obj->dpn_tiposTraumaOralDental?></textarea></p>
									</td>
									<td width="10%"></td>
									<td width="35%">
										<p class="black"><br>
											Hubo: cambios de color, movilidad: <input type="checkbox" name="dpn_cambiosColorMovilidad" value="1" <?=($obj->dpn_cambiosColorMovilidad=="t")?"checked":""?> border="0"></p>
									</td>
									<td width="10%"></td>
								</tr>
								<tr>
									<td width="10%"></td>
									<td width="10%"></td>
									<td width="35%">
										<p class="black"></p>
									</td>
									<td width="10%"></td>
								</tr>
								<tr>
									<td width="10%"></td>
									<td width="10%"></td>
									<td width="35%">
										<p class="black"><br>
											Su ni&ntilde;o ha sido cooperador con el m&eacute;dico y odont&oacute;logo del pasado: 
											<input type="checkbox" name="dpn_ninoCooperadorOdontologo" value="1" <?=($obj->dpn_ninoCooperadorOdontologo=="t")?"checked":""?> border="0"></p>
									</td>
									<td width="10%"></td>
								</tr>
								<tr>
									<td width="10%"></td>
									<td width="35%"></td>
									<td width="10%"></td>
									<td width="35%"></td>
									<td width="10%"></td>
								</tr>
								<tr>
									<td width="10%"></td>
									<td width="35%">
										<p class="black">Qu&eacute; tratamientos caseros o aplicados por el odont&oacute;logo</p>
									</td>
									<td width="10%"></td>
									<td width="35%">
										<p class="black"></p>
									</td>
									<td width="10%"></td>
								</tr>
								<tr>
									<td width="10%"></td>
									<td rowspan="4" width="35%">
										<p class="black">a utilizado en el pasado:<br>
											<textarea name="dpn_tratamientosCaseros" rows="4" cols="32"><?=$obj->dpn_tratamientosCaseros?></textarea></p>
									</td>
									<td width="10%"></td>
									<td rowspan="4" width="35%">
										<p class="black">Observaciones:<br>
											<textarea name="dpn_observacionesOdontologicas" rows="4" cols="32"><?=$obj->dpn_observacionesOdontologicas?></textarea></p>
									</td>
									<td width="10%"></td>
								</tr>
								<tr>
									<td width="10%"></td>
									<td width="10%"></td>
									<td width="10%"></td>
								</tr>
								<tr>
									<td width="10%"></td>
									<td width="10%"></td>
									<td width="10%"></td>
								</tr>
								<tr>
									<td width="10%"></td>
									<td width="10%"></td>
									<td width="10%"></td>
								</tr>
								<tr>
									<td width="10%"></td>
									<td width="35%"></td>
									<td width="10%"></td>
									<td width="35%"></td>
									<td width="10%"></td>
								</tr>
								<tr>
									<td width="10%"></td>
									<td colspan="3">
										<p class="black1"><br>
											Habitos: Dieta<br>
										</p>
									</td>
									<td width="10%"></td>
								</tr>
								<tr>
									<td width="10%"></td>
									<td colspan="3">
										<table width="50%" border="0" cellspacing="2" cellpadding="1" align="center" bgcolor="black">
											<tr>
												<td bgcolor="white" width="27%">
													<div align="center">
														<p class="<p class=" black1"></p>
													</div>
												</td>
												<td bgcolor="white" width="39%">
													<div align="center">
														<p class="black1">Desayuno</p>
													</div>
												</td>
												<td bgcolor="white" width="39%">
													<div align="center">
														<p class="black1">Almuerzo</p>
													</div>
												</td>
												<td bgcolor="white" width="39%">
													<div align="center">
														<p class="black1">Comida</p>
													</div>
												</td>
											</tr>
											<tr>
												<td align="center" bgcolor="white" width="27%">
													<div align="center">
														<p class="black1">1Control</p>
													</div>
												</td>
												<td bgcolor="white" width="39%"><textarea name="dpn_habitosD1controlDesayuno" rows="4" cols="20"><?=$obj->dpn_habitosD1controlDesayuno?></textarea></td>
												<td bgcolor="white" width="39%"><textarea name="dpn_1controlAlmuerzo" rows="4" cols="20"><?=$obj->dpn_1controlAlmuerzo?></textarea></td>
												<td bgcolor="white" width="39%"><textarea name="dpn_1controlComida" rows="4" cols="20"><?=$obj->dpn_1controlComida?></textarea></td>
											</tr>
											<tr>
												<td align="center" bgcolor="white" width="27%">
													<div align="center">
														<p class="black1">2Control</p>
													</div>
												</td>
												<td bgcolor="white" width="39%"><textarea name="dpn_2controlDesayuno" rows="4" cols="20"><?=$obj->dpn_2controlDesayuno?></textarea></td>
												<td bgcolor="white" width="39%"><textarea name="dpn_2controlAlmuerzo" rows="4" cols="20"><?=$obj->dpn_2controlAlmuerzo?></textarea></td>
												<td bgcolor="white" width="39%"><textarea name="dpn_2controlComida" rows="4" cols="20"><?=$obj->dpn_2controlComida?></textarea></td>
											</tr>
											<tr>
												<td align="center" nowrap bgcolor="white" width="27%">
													<div align="center">
														<p class="black1">3 Control</p>
													</div>
												</td>
												<td bgcolor="white" width="39%"><textarea name="dpn_3controlDesayuno" rows="4" cols="20"><?=$obj->dpn_3controlDesayuno?></textarea></td>
												<td bgcolor="white" width="39%"><textarea name="dpn_3controlAlmuerzo" rows="4" cols="20"><?=$obj->dpn_3controlAlmuerzo?></textarea></td>
												<td bgcolor="white" width="39%"><textarea name="dpn_3controlComida" rows="4" cols="20"><?=$obj->dpn_3controlComida?></textarea></td>
											</tr>
										</table>
									</td>
									<td width="10%"></td>
								</tr>
								<tr>
									<td width="10%"></td>
									<td width="35%">
										<p class="black1"><br>
											Su ni&ntilde;o presenta alguna de las siguientes situaciones:<br>
										</p>
									</td>
									<td width="10%"></td>
									<td width="35%"></td>
									<td width="10%"></td>
								</tr>
								<tr>
									<td width="10%"></td>
									<td rowspan="5" width="35%">
										<table width="80%" border="0" cellspacing="2" cellpadding="0">
											<tr>
												<td width="60%">
													<p class="black">Rechina los dientes</p>
												</td>
												<td width="40%"><input type="checkbox" name="dpn_rechinaDientes" value="1" <?=($obj->dpn_rechinaDientes=="t")?"checked":""?> border="0"></td>
											</tr>
											<tr>
												<td width="60%">
													<p class="black">Se chupa los dedos</p>
												</td>
												<td width="40%"><input type="checkbox" name="dpn_chupaDedo" value="1" <?=($obj->dpn_chupaDedo=="t")?"checked":""?> border="0"></td>
											</tr>
											<tr>
												<td width="60%">
													<p class="black">Se come las u&ntilde;as</p>
												</td>
												<td width="40%"><input type="checkbox" name="dpn_comeUnas" value="1" <?=($obj->dpn_comeUnas=="t")?"checked":""?> border="0"></td>
											</tr>
											<tr>
												<td width="60%">
													<p class="black">Respira por la boca</p>
												</td>
												<td width="40%"><input type="checkbox" name="dpn_respiraBoca" value="1" <?=($obj->dpn_respiraBoca=="t")?"checked":""?> border="0"></td>
											</tr>
											<tr>
												<td width="60%">
													<p class="black">Toma tetero</p>
												</td>
												<td width="40%"><input type="checkbox" name="dpn_tomaTetero" value="1" <?=($obj->dpn_tomaTetero=="t")?"checked":""?> border="0"></td>
											</tr>
										</table>
									</td>
									<td width="10%"></td>
									<td rowspan="4" width="35%">
										<p class="black">Que otros h&aacute;bitos orales tiene:<br>
											<textarea name="dpn_otrosHabitos" rows="4" cols="32"><?=$obj->dpn_otrosHabitos?></textarea></p>
									</td>
									<td width="10%"></td>
								</tr>
								<tr>
									<td width="10%"></td>
									<td width="10%"></td>
									<td width="10%"></td>
								</tr>
								<tr>
									<td width="10%"></td>
									<td width="10%"></td>
									<td width="10%"></td>
								</tr>
								<tr>
									<td width="10%"></td>
									<td width="10%"></td>
									<td width="10%"></td>
								</tr>
								<tr>
									<td width="10%"></td>
									<td width="10%"></td>
									<td width="35%"></td>
									<td width="10%"></td>
								</tr>
								<tr>
									<td width="10%"></td>
									<td width="35%"></td>
									<td width="10%"></td>
									<td width="35%"></td>
									<td width="10%"></td>
								</tr>
								<tr>
									<td width="10%"></td>
									<td width="35%"></td>
									<td width="10%"></td>
									<td width="35%">
										<div align="center">
											<input type="hidden" name="s_opc" value="modificar" border="0"><input type="hidden" name="opc" value="<?=$opc?>" border="0"><input type="hidden" name="dpn_id" value="<?=$dpn_id?>" border="0"> <input type="hidden" name="pac_id" value="<?=$pac_id?>" border="0"><br>
											<input type="image" src="images/editar.gif" alt="" border="0"></div>
									</td>
									<td width="10%"></td>
								</tr>
							</table>
						</div>
					</form>
			</td>
			</tr>
		</table>