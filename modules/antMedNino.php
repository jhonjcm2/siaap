<?
if (!$table){
?>Error en la conexi&oacute;n con la base de datos<br>
    Consulte con el administrador del sistema
    <?include('nino.php');
	exit(0);
}else{
	$nino = new nino ( );
	$ret = $nino->crear( );
	//print_r ($ret);
	show_mess($ret['mess'],$ret['mess_cod']);
        if($ret['mess_cod']!="info"){
		include("modules/nino/default.php"); exit(0);
	}else{
	       
	$query="select last_value from datos_personales_nin_dpn_id_seq";
	$table->search($query);
	$obj = $table->sql_fetch_object();
	$dpn_id = $obj->last_value;
		

/*******************/

$nino = new nino( $pac_id);


   $query= "SELECT * FROM paciente WHERE pac_id=$pac_id";
   $table->search($query);
   $paciente = $table->sql_fetch_object();

?>
		<table width="100%" border="1" cellspacing="5" cellpadding="0" align="center">
			<tr>
				<td align="center" width="90%">
					<p class="black1"><br>
						<br>
						ANTECEDENTES GENERALES</p>
					<p class="black1"><br>
					</p>
					<form action="<?=$PHP_SELF?>" method="post" name="antMedNino">
						<div align="left">
							<table width="100%" border="0" cellspacing="2" cellpadding="0">
								<tr>
								<td width="10%"></td>
								<td colspan="2">
										<div align="right">
											<p class="black1"> </p>
										</div>
									</td>
								<td class="black1" width="35%">No. HISTORIA CLINICA:  <?=$paciente->pac_numeroIdentificacion?><br>
									<br>
									<br>
								</td>
								<td width="10%"></td>
							</tr>
								<tr>
								<td width="10%"></td>
								<td><p class="black1">Paciente: <?=$paciente->pac_nombres?> <?=$paciente->pac_apellidos?><br>
											<br>
											<br>
										
										
										</td>
								<td></td>
								<td width="35%"></td>
								<td width="10%"></td>
							</tr>
								<tr>
								<td width="10%"></td>
								<td colspan="3">
										<p class="black1">MEDICOS GENERALES DE NI&Ntilde;EZ<br>
											<br>
										</p>
									</td>
								<td width="10%"></td>
							</tr>
								<tr>
								<td width="10%"></td>
								<td colspan="3">
										<p class="black1">- Prenatal<br>
											<br>
											<br>
										</p>
									</td>
								<td width="10%"></td>
							</tr>
								<tr>
								<td width="10%"></td>
								<td rowspan="4" width="35%">
										<p class="black">Enfermedades graves durante el embarazo: <textarea name="dpn_antMedPrenatalEnferEmbarazo" rows="4" cols="32"></textarea></p>
									</td>
									<td width="10%"></td>
									<td rowspan="4" width="35%">
										<p class="black">Medicamentos tomados durante el embarazo: <textarea name="dpn_medicamentosTomadosEmb" rows="4" cols="34"></textarea><br>
										</p>
									</td>
								<td width="10%"></td>
							</tr>
								<tr>
								<td width="10%"></td>
								<td width="10%"></td>
								<td width="10%"></td>
							</tr>
								<tr>
								<td width="10%"></td>
								<td width="10%"></td>
								<td width="10%"></td>
							</tr>
								<tr>
								<td width="10%"></td>
								<td width="10%"></td>
								<td width="10%"></td>
							</tr>
								<tr>
								<td width="10%"></td>
								<td colspan="3">
										<p class="black1"><br>
											<br>
											- Natal<br>
										</p>
									</td>
								<td width="10%"></td>
							</tr>
								<tr>
								<td width="10%"></td>
								<td colspan="3">
										<table width="100%" border="0" cellspacing="5" cellpadding="0">
											<tr>
												<td nowrap width="30%">
													<p class="black">Tipo de parto: <? 
												       $query="select * from parto";								
												       $table->search($query);
												       if ($table->nfound >= 1) {												 
													 ?><select name="par_id" size="1">
															<option value="-1">seleccione</option><? echo "\n";
													 while ($data=$table->sql_fetch_object()){  
													   			?>
															<option value="<?=$data->par_id?>"><?=$data->par_tipoParto; echo "\n"; ?></option><?}?>
														</select><?}?></p>
												</td>
												<td nowrap width="40%">
													<p class="black">Emplearon f&oacute;rceps en el parto: <input type="checkbox" name="dpn_antMedNatalEmplearonForceps" value="1" border="0"></p>
												</td>
												<td nowrap width="30%">
													<div align="left">
														<p class="black">
														El ni&ntilde;o naci&oacute; cian&oacute;tico: <input type="checkbox" name="dpn_ninoCianotico" value="1" border="0"><br>
														</p>
													</div>
												</td>
											</tr>
										</table>
									</td>
								<td width="10%"></td>
							</tr>
								<tr>
								<td width="10%"></td>
								<td colspan="3">
										<p class="black1"><br>
											<br>
											- Postnatal<br>
										</p>
									</td>
								<td width="10%"></td>
							</tr>
								<tr>
								<td nowrap width="10%"></td>
								<td colspan="3" nowrap>
										<p class="black">Fecha &uacute;ltimo ex&aacute;men m&eacute;dico: (DD/MM/AAAA) <select name="fex_dia" size="1">
												<option value="01">1</option>
											</select>/ <select name="fex_mes" size="1">
												<option value="01">1</option>
											</select>/<select name="fex_anio" size="1">
												<option value="1974">1975</option>
											</select><br>
										<br>
									</p>
									</td>
								<td nowrap width="10%"></td>
							</tr>
								<tr>
								<td width="10%"></td>
								<td rowspan="4" width="35%">
										<p class="black">Motivo de &uacute;ltimo ex&aacute;men m&eacute;dico:<br>
											<textarea name="dpn_motivoUltimoExamen" rows="4" cols="32"></textarea></p>
									</td>
									<td width="10%"></td>
									<td rowspan="4" width="35%">
										<p class="black">Qu&eacute; tratamiento m&eacute;dico actual tiene:<br>
											<textarea name="dpn_tratamientoMedicoActual" rows="4" cols="34"></textarea></p>
									</td>
								<td width="10%"></td>
							</tr>
								<tr>
								<td width="10%"></td>
								<td width="10%"></td>
								<td width="10%"></td>
							</tr>
								<tr>
								<td width="10%"></td>
								<td width="10%"></td>
								<td width="10%"></td>
							</tr>
								<tr>
								<td width="10%"></td>
								<td width="10%"></td>
								<td width="10%"></td>
							</tr>
								<tr>
								<td width="10%"></td>
								<td colspan="3">
										<p class="black"><br>
											
										Medicamentos tomados actualmente: <input type="text" name="dpn_medicTomActual" size="24" border="0"><br>
											<br>
										<br>
									</p>
									</td>
								<td width="10%"></td>
							</tr>
								<tr>
								<td width="10%"></td>
								<td colspan="3">
										<p class="black1">Su ni&ntilde;o ha tenido alguna vez reacciones al&eacute;rgicas a lo siguiente:<br>
										</p>
									</td>
								<td width="10%"></td>
							</tr>
								<tr>
								<td width="10%"></td>
								<td colspan="3">
										<table width="100%" border="0" cellspacing="5" cellpadding="0">
											<tr>
												<td width="20%">
													<p class="black">Anest&eacute;sico Local:</p>
												</td>
												<td width="13%"><input type="checkbox" name="dpn_sufreAlergiaAnestesicolocal" value="1" border="0"></td>
												<td width="20%">
													<div align="center">
														<p class="black">
														Penicilina:</p>
													</div>
												</td>
												<td width="13%"><input type="checkbox" name="dpn_alergiaPenicilina" value="1" border="0"></td>
												<td nowrap width="25%">
													<p class="black">Otras drogas o comidas:</p>
												</td>
												<td><input type="checkbox" name="dpn_alergiaDrogasComidas" value="1" border="0"></td>
											</tr>
										<tr>
											<td colspan="6"><br>
											</td>
										</tr>
									</table>
									</td>
								<td width="10%"></td>
							</tr>
								<tr>
								<td width="10%"></td>
								<td rowspan="4" width="35%">
										<p class="black">Por qu&eacute; ha sido hospitalizado: <br>
										<textarea name="dpn_porqueHaSidoHospitalizado" rows="4" cols="32"></textarea></p>
									</td>
									<td width="10%"></td>
									<td rowspan="4" width="35%">
										<p class="black">Qu&eacute; impedimentos f&iacute;sicos tiene:<br>
											<textarea name="dpn_impedimentosFisicos" rows="4" cols="34"></textarea></p>
									</td>
								<td width="10%"></td>
							</tr>
								<tr>
								<td width="10%"></td>
								<td width="10%"></td>
								<td width="10%"></td>
							</tr>
								<tr>
								<td width="10%"></td>
								<td width="10%"></td>
								<td width="10%"></td>
							</tr>
								<tr>
								<td width="10%"></td>
								<td width="10%"></td>
								<td width="10%"></td>
							</tr>
								<tr>
								<td width="10%"></td>
								<td width="35%">
										<p class="black">Ha tenido tendencia a sangrar exageradamente: <input type="checkbox" name="dpn_sangraExageradamente" value="1" border="0"><br>
											<br>
										<br>
									</p>
									</td>
									<td width="10%"></td>
									<td width="35%"></td>
								<td width="10%"></td>
							</tr>
								<tr>
								<td width="10%"></td>
								<td colspan="3">
										<p class="black1">Su ni&ntilde;o ha sido diagnosticado alguna vez con algunas de las siguientes enfermedades:<br>
											<br>
										</p>
									</td>
								<td width="10%"></td>
							</tr>
								<tr>
								<td width="10%"></td>
								<td colspan="2">
										<table width="80%" border="0" cellspacing="2" cellpadding="1" align="left" bgcolor="black">
											<tr>
												<td colspan="2" bgcolor="white">
													<div align="center">
														<p class="black1"><font color="black">Enfermedad<br>
																<br>
															</font></p>
													</div>
												</td>
											</tr>
											<tr>
												<td bgcolor="white" width="80%">
													<p class="black">Fiebre Escarlatina</p>
												</td>
												<td bgcolor="white" width="39%"><input type="checkbox" name="dpn_haTenidoFiebreEscarlatina" value="1" border="0"></td>
											</tr>
											<tr>
												<td bgcolor="white" width="80%">
													<p class="black">Fiebre Reum&aacute;tica</p>
												</td>
												<td bgcolor="white" width="39%"><input type="checkbox" name="dpn_fiebreReumatica" value="1" border="0"></td>
											</tr>
											<tr>
												<td bgcolor="white" width="80%">
													<p class="black">Polio</p>
												</td>
												<td bgcolor="white" width="39%"><input type="checkbox" name="dpn_polio" value="1" border="0"></td>
											</tr>
											<tr>
												<td bgcolor="white" width="80%">
													<p class="black">Rub&eacute;ola</p>
												</td>
												<td bgcolor="white" width="39%"><input type="checkbox" name="dpn_rubeola" value="1" border="0"></td>
											</tr>
											<tr>
												<td bgcolor="white" width="80%">
													<p class="black">T&eacute;tano</p>
												</td>
												<td bgcolor="white" width="39%"><input type="checkbox" name="dpn_tetano" value="1" border="0"></td>
											</tr>
											<tr>
												<td bgcolor="white" width="80%">
													<p class="black">Difteria</p>
												</td>
												<td bgcolor="white" width="39%"><input type="checkbox" name="dpn_difteria" value="1" border="0"></td>
											</tr>
											<tr>
												<td bgcolor="white" width="80%">
													<p class="black">Tos ferina</p>
												</td>
												<td bgcolor="white" width="39%"><input type="checkbox" name="dpn_tosFerina" value="1" border="0"></td>
											</tr>
										</table>
									</td>
									<td width="35%">
										<table width="80%" border="0" cellspacing="2" cellpadding="0" align="left" bgcolor="black">
											<tr>
												<td bgcolor="white" width="80%">
													<p class="black">Asma</p>
												</td>
												<td bgcolor="white"><input type="checkbox" name="dpn_asma" value="1" border="0"></td>
											</tr>
											<tr>
												<td bgcolor="white" width="80%">
													<p class="black">Sarampi&oacute;n</p>
												</td>
												<td bgcolor="white"><input type="checkbox" name="dpn_sarampion" value="1" border="0"></td>
											</tr>
											<tr>
												<td bgcolor="white" width="80%">
													<p class="black">Convulsiones</p>
												</td>
												<td bgcolor="white"><input type="checkbox" name="dpn_convulsiones" value="1" border="0"></td>
											</tr>
											<tr>
												<td bgcolor="white" width="80%">
													<p class="black">Paperas</p>
												</td>
												<td bgcolor="white"><input type="checkbox" name="dpn_paperas" value="1" border="0"></td>
											</tr>
											<tr>
												<td bgcolor="white" width="80%">
													<p class="black">Hepatitis</p>
												</td>
												<td bgcolor="white"><input type="checkbox" name="dpn_hepatitis" value="1" border="0"></td>
											</tr>
											<tr>
												<td bgcolor="white" width="80%">
													<p class="black">Retardo mental</p>
												</td>
												<td bgcolor="white"><input type="checkbox" name="dpn_retardoMental" value="1" border="0"></td>
											</tr>
											<tr>
												<td bgcolor="white" width="80%">
													<p class="black">Autismo</p>
												</td>
												<td bgcolor="white"><input type="checkbox" name="dpn_autismo" value="1" border="0"></td>
											</tr>
											<tr>
												<td bgcolor="white" width="80%">
													<p class="black">Par&aacute;lisis cerebral</p>
												</td>
												<td bgcolor="white"><input type="checkbox" name="dpn_paralisisCerebral" value="1" border="0"></td>
											</tr>
											<tr>
												<td bgcolor="white" width="80%">
													<p class="black">diabetes</p>
												</td>
												<td bgcolor="white"><input type="checkbox" name="dpn_diabetes" value="1" border="0"></td>
											</tr>
											<tr>
												<td bgcolor="white" width="80%">
													<p class="black">Anemia</p>
												</td>
												<td bgcolor="white"><input type="checkbox" name="dpn_anemia" value="1" border="0"></td>
											</tr>
										</table>
									</td>
								<td width="10%"></td>
							</tr>
								<tr>
								<td width="10%"></td>
								<td width="35%">
										<p class="black"><br>
											
										Observaciones:<br>
											<textarea name="dpn_observacionesEnfermedades" rows="4" cols="40"></textarea></p>
									</td>
									<td width="10%"></td>
									<td width="35%"></td>
								<td width="10%"></td>
							</tr>
								<tr>
								<td width="10%"></td>
								<td width="35%"></td>
									<td width="10%"></td>
									<td align="center" width="35%"><input type="hidden" name="dpn_id" value="<?=$dpn_id?>" border="0"><input type="hidden" name="pac_id" value="<?=$pac_id?>" border="0"><input type="hidden" name="s_opc" value="antPsicNino" border="0"><input type="hidden" name="opc" value="<?=$opc?>" border="0"><br>
										<br>
										<input type="image" src="images/crear2.gif" alt="" align="left" border="0"></td>
								<td align="center" width="10%"></td>
							</tr>
								<tr>
									<td width="10%"></td>
									<td width="35%"><br>
										<br>
									</td>
									<td width="10%"></td>
									<td align="center" width="35%"></td>
									<td align="center" width="10%"></td>
								</tr>
								<tr>
									<td align="center" width="10%">
										<div align="center">
											<p class="black">
										</div>
									</td>
									<td class="black" colspan="3" align="center" bgcolor="#c8c8c8"><a href="index.php?opc=nino&s_opc=visualizar&pac_id=<?=$obj->pac_id?>">Visualizar Datos Personales </a>, <a href="index.php">Salir</a></td>
									<td width="10%"></td>
								</tr>
							</table>
						</div>
					</form>
				</td>
			</tr>
		</table>
<? }
}

?>
