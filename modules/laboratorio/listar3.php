<?

if (!$table){
?>Error en la conexi&oacute;n con la base de datos<br>
		            Consulte con el administrador del sistema</p>
		            
	<p class='black'><?include('busqueda.php');
}else{

	$usuario=$user->datos;


     // Recuperacion de datos enviados por formularios

 	$query = "SELECT * FROM orden_lab";
      // Arreglos para datos del query y el url para paginacion
      $where_array = array();
      $url_array = array();

      $fields = array("usu_id");

      // Adicionando los campos para el query
      foreach($fields as $v)
	if(${$v}!=""){
	  array_push($where_array,' "'.$v.'" '." LIKE '%".${$v}."%' ");
	  array_push($url_array,"$v = ".${$v});
	}
	
      if(sizeof($where_array))
	$query .= " WHERE ". implode(" AND ",$where_array);

      $query .= " ORDER BY old_id";
      $url = "&opc=radiologia&s_opc=listar&";
      $url .= implode("&",$url_array);
      $url .= "&".SID;
  //echo $query;
      $table->search($query,$skip,$PHP_SELF,$url);

      $data = array();
      
      while($object = $table->sql_fetch_object())
	array_push($data,$object);
	//print_r($data);
      if($table->nfound){
	$mess = " Se encontraron <b> ".$table->nfound."</b> registros <br> 
                mostrando desde el <b>".$table->begnum."</b>
                hasta el <b>".$table->endnum."</b>"; 

	
?>
	<p class="black">
		
	</p>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td class="black1" align="center" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="black1" align="center" valign="middle" width="80%">ORDENES DE LABORATORIO<br>
							<br>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="black1" align="left" valign="top">
				<table width="50%" border="0" cellspacing="0" cellpadding="0" align="left">
					<tr>
						<td class="black1" nowrap width="30%">Odontologo o Estudiante:</td>
						<td class="black1" ><?$query = "select * from usuario where usu_id=$usu_id";
						$table ->search($query);
						$usuario= $table->sql_fetch_object();?>&nbsp;&nbsp;
						<?=$usuario->usu_nombres?>&nbsp;<?=$usuario->usu_apellidos?></td>
					</tr>
					<tr>
						<td class="black1" width="30%">No. Identificaci&oacute;n:</td>
						<td class="black1">&nbsp;&nbsp;<?=$usu->usu_numeroIdentificacion?></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
		<td class="black1" align="center" valign="top">
				<p></p>
				<br>
				<br>
				<table class="listado"><tr class="cabecera">
						<td>No de Orden</td>
						<td>Paciente</td>
						<td>Fecha de Ingreso</td>
						<td>Docente que autoriz&oacute;&nbsp;</td>
						<td>Estado</td>
						<td>Valor</td>
						<td>Pagos Realizados</td>
						<td>Saldo</td>
						<td>Acci&oacute;n</td>
					</tr>
					
					<? foreach($data as $obj){ 
						$num = ($num) ? 0 : 1;
						$No=$No + 1;
						
						$query = "select * from paciente where pac_id=$obj->pac_id";
						$table ->search($query);
						$pac= $table->sql_fetch_object();
						$query = "select * from usuario where usu_id=$obj->old_autoriza";
						$table ->search($query);
						$usu= $table->sql_fetch_object();
						
						$query = "select sum(oxp_valor*\"oxp_canPld\") as valor from oldxpld where old_id=$obj->old_id";
						$table ->search($query);
						$oxp= $table->sql_fetch_object();
						
						$query = "select sum(esc_pago) as pago  from estado_cuenta where pac_id=$obj->pac_id and \"pag_centroInfo\"='1' ";
						$table ->search($query);
						$pago= $table->sql_fetch_object();
						
						$saldo= ($oxp->valor - $pago->pago);
						$total +=$pago->pago;
						$saldoT +=$saldo;
					?>
					<tr <?=($num)?' class="par"':' class="impar"'?>>
						<td><?=$obj->old_id?></td>
						<td>&nbsp;&nbsp;&nbsp;<?=$pac->pac_nombres?>&nbsp;<?=$pac->pac_apellidos?></td>
						<td>&nbsp;&nbsp;&nbsp;<?=$obj->old_fechaElaboracion?></td>
						<td>&nbsp;&nbsp;&nbsp;<?=$usu->usu_nombres?>&nbsp;<?=$usu->usu_apellidos?></td>
						<td>&nbsp;<?if($obj->old_legalizado=='t'){ echo "LEGALIZADO"; }else echo "PENDIENTE";?></td>
						<td><?=number_format($oxp->valor,0)?>&nbsp;</td>
						<td><?=number_format($pago->pago,0)?>&nbsp;</td>
						<td><?=number_format($saldo,0)?></td>
						<td><a href="index.php?opc=laboratorio&s_opc=visualizarevo2&old_id=<?=$obj->old_id?>">Visualizar</a></td>
					</tr>

					<? } ?>

			</table>
				<br>
				<br>
				<table width="50%" border="0" cellspacing="2" cellpadding="0" bgcolor="black">
					<tr>
						<td class="black1" colspan="2" align="center" nowrap bgcolor="#fdfdfd">Estado de Cuenta del Odontol&oacute;go o Estudiante :</td>
					</tr>
					<tr>
						<td class="black1" nowrap bgcolor="#fdfdfd" width="20%">&nbsp;&nbsp; Total Pagos Realizados:</td>
						<td class="red" align="right" bgcolor="#fdfdfd" width="20%">
							<div align="right">
								<b><?=number_format($total,0)?>&nbsp;&nbsp;</b></div>
						</td>
					</tr>
					<tr>
						<td class="black1" bgcolor="#fdfdfd" width="20%">&nbsp;&nbsp;Total Saldo : </td>
						<td class="red" align="right" bgcolor="#fdfdfd" width="20%">
							<div align="right">
								<b><?=number_format($saldoT,0)?>&nbsp;&nbsp;</b></div>
						</td>
					</tr>
				</table>
				<p align="center"><span class="navy"><?= $prev.$pages.$next ?>&nbsp;&nbsp;&nbsp;&nbsp;	</span></p>
				<p align="center"> </p>
				<p align="center"></p>
			</td>
	</tr>
	</table>
	<?			
	}
	else {
		$mess = "<b>No se encontraron registros en la b&uacute;squeda.</b>";
		show_mess($mess,"info");
		
  		}

}
?>
