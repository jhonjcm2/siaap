<?
if (!$table){
?>Error en la conexi&oacute;n con la base de datos<br>
    Consulte con el administrador del sistema
    <?include('nino.php');
	exit(0);
}else{
	$usuario=$user->datos;
	
	$query= "SELECT * FROM paciente WHERE pac_id=$pac_id";
   	$table->search($query);
   	$paciente = $table->sql_fetch_object();
	
	if($usuario->tiu_id!=1&& $usuario->tiu_id!=3){
             	show_mess("<b>Ud, no est&aacute; autorizado para ingresar a &eacute;ste m&oacute;dulo</b>","alert");
       }else{
		$query="select * from antecedente where pac_id=$pac_id";
		$table->sql_query($query);
		$ant = $table->sql_fetch_object();
		
		if($ant->ant_estado < 5){
			$mess="<center><b>Los datos de los Ant. Alergicos y Habitos NO  han sido ingresados.</b> <br><br>
  			Por favor ingreselos primero</center>";
    			show_mess($mess,"alert");
		}else{	
$pac = new adulto( $pac_id);
$obj=$pac->data;
?>
		<table width="90%" border="0" cellspacing="2" cellpadding="0" align="center">
		<tr>
			<td align="center" width="90%" class="black1">
				<form action="<?=$PHP_SELF?>" method="post" name="antecedente">
					<div align="left">
						<table width="100%" border="0" cellspacing="2" cellpadding="0" align="center">
							<tr>
								<td width="45%"><div align="right"><p class="black1"> </p></div></td>
								<td width="10%"></td>
									<td align="right" nowrap width="45%"><p class="black1">No. HISTORIA CLINICA:&nbsp;&nbsp;<?=$paciente->pac_numeroIdentificacion?><br>
											 Fecha de Ingreso : <?=$ant->ant_fechaIngresoAnt5?>
								</td>
								</tr>
							<tr>
									<td colspan="3" align="left">
									<div align="left">
										<table class="black1" width="80%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td nowrap width="10%">Paciente :</td>
												<td width="70%">&nbsp;&nbsp;<?="$paciente->pac_nombres $paciente->pac_apellidos"?></td>
											</tr>
											<tr>
												<td nowrap width="10%">Odont&oacute;logo :</td>
												<td width="70%">&nbsp;&nbsp;<? 
											$query= "select  MAX(oeh_id) FROM odontologos_encargados_historia WHERE pac_id='$paciente->pac_id'";
											$table->search($query); 
											$maxo=$table->sql_fetch_object();
											//echo $query;
												$query="select * from usuario u,  odontologos_encargados_historia  d   
 												where u.usu_id=d.usu_id and d.pac_id = '$paciente->pac_id' and oeh_id='$maxo->max' order by oeh_id ASC";							
												$table->search($query); 
												//echo $query;
												if ($table->nfound >= 1) {
												$data=$table->sql_fetch_object();
												?><?=$data->usu_nombres." ".$data->usu_apellidos?> <?}?></td>
											</tr>
											<tr>
												<td nowrap width="10%">Docente :</td>
												<td nowrap width="70%">&nbsp;&nbsp;<? 
											$query= "select  MAX(deh_id) FROM docentes_encargados_historia WHERE pac_id='$paciente->pac_id'";
											$table->search($query); 
											$maxd=$table->sql_fetch_object();
												$query="select * from usuario u,  docentes_encargados_historia  d   
 												where u.usu_id=d.usu_id and d.pac_id = '$paciente->pac_id' and deh_id='$maxd->max' order by deh_id ASC";							
												$table->search($query); 
												if ($table->nfound >= 1) {
												$data=$table->sql_fetch_object();
												?><?=$data->usu_nombres." ".$data->usu_apellidos?> <?}?></td>
											</tr>
										</table>
									</div>
								</td>
								</tr>
							<tr>
									<td class="black1" colspan="3" align="center"><p><br>
									ANTECEDENTES  </p>
									  <p>TOXICOS - ALERGICOS - QUIRURGICOS - HEMORRAGIAS<br>
								        <br>
							              </p></td>
							</tr>
							<tr>
								<td class="black1" colspan="3" nowrap>Ha tenido alguna vez reacciones al&eacute;rgicas a :</td>
							</tr>
							<tr>
								<td colspan="3"><br>
									<table class="black1" width="70%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
									<tr>
										<td nowrap bgcolor="white" width="20%" class="black">&nbsp;&nbsp;Anest&eacute;sico Local :</td>
										<td class="black" bgcolor="white" width="13%">&nbsp;&nbsp;<?=($ant->ant_sufreAlergiaAnestesicolocal=="t")?"SI":"NO"?></td>
									</tr>
									<tr>
										<td nowrap bgcolor="white" width="20%" class="black">&nbsp;&nbsp;Penicilina :</td>
										<td class="black" bgcolor="white" width="13%">&nbsp;&nbsp;<?=($ant->ant_alergiaPenicilina=="t")?"SI":"NO"?></td>
									</tr>
									<tr>
										<td nowrap bgcolor="white" width="20%" class="black">&nbsp;&nbsp;Otros medicamentos o comidas :</td>
										<td class="black" bgcolor="white" width="13%">&nbsp;&nbsp;<?=($ant->ant_alergiaDrogasComidas=="t")?"SI":"NO"?></td>
									</tr>
									</table>
								</td>
							</tr>
							<tr>
									<td class="black1" colspan="3" nowrap><br>
										Observaciones :<br>
										<table width="80%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
											<tr>
												<td class="black" bgcolor="white">&nbsp;&nbsp;<?=$ant->ant_comentarioAntAler?></td>
											</tr>
										</table>
									</td>
							</tr>
							<tr>
								<td colspan="3" class="black1" align="center"><br>
									<br>
									HABITOS </td>
							</tr>
							<tr>
								<td class="black1" colspan="2" nowrap><br>Presenta alguna de las siguientes situaciones:<br>
									<br>
								</td>
								<td width="45%"></td>
							</tr>
							<tr>
								<td align="right" width="45%">
									<table width="90%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
										<tr>
											<td nowrap bgcolor="white" width="60%"class="black">Rechina los dientes :</td>
											<td class="black" align="center" bgcolor="white" width="30%"><?=($ant->ant_rechinaDientes=="t")?"SI":"NO"?></td>
										</tr>
										<tr>
											<td nowrap bgcolor="white" width="60%" class="black">Se chupa los dedos :</td>
											<td class="black" align="center" bgcolor="white" width="30%"><?=($ant->ant_chupaDedo=="t")?"SI":"NO"?></td>
										</tr>
										<tr>
											<td bgcolor="white" width="60%" class="black">Se come las u&ntilde;as</td>
											<td class="black" align="center" bgcolor="white" width="30%"><?=($ant->ant_comeUnas=="t")?"SI":"NO"?></td>
										</tr>
										<tr>
											<td nowrap bgcolor="white" width="60%" class="black">Respira por la boca :  </td>
											<td class="black" align="center" bgcolor="white" width="30%"><?=($ant->ant_respiraBoca=="t")?"SI":"NO"?></td>
										</tr>
										<tr>
											<td bgcolor="white" width="60%" class="black">Apieta los Dientes :</td>
											<td class="black" align="center" bgcolor="white" width="30%"><?=($ant->ant_aprietaDiente=="t")?"SI":"NO"?></td>
										</tr>
									</table>
								</td>
								<td width="10%"></td>
								<td valign="top" class="black1" width="45%">Otros h&aacute;bitos orales :<br>
									<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
										<tr>
											<td class="black" bgcolor="white">&nbsp;&nbsp;<?=$ant->ant_otrosHabitos?></td>
										</tr>
									</table>
								</td>
							</tr>
								<tr height="8%">
								<td width="45%" height="8%"></td>
								<td nowrap width="10%" height="8%"></td>
									<td width="45%" height="8%"><div align="center">
											    </div></td>
								</tr>
								<tr>
									<td class="black" colspan="3" align="center" nowrap bgcolor="#e1e1e1"><a href="index.php?opc=antecedente&s_opc=visualizar&pac_id=<?=$paciente->pac_id?>" >Consultar Ant. Sociales,</a> 
									<a href="index.php?opc=antecedente&s_opc=visualizar1&pac_id=<?=$paciente->pac_id?>" >Consultar Ant. M&eacute;dicos Personales,</a><br>
										 <a href="index.php?opc=enf_nino&s_opc=visualizar&pac_id=<?=$paciente->pac_id?>" >Consultar Enfermedades Propias de la Ni&ntilde;ez,</a>  
										 <a href="index.php?opc=enf_adulto&s_opc=visualizar&pac_id=<?=$paciente->pac_id?>" >Consultar Enfermedades Propias del Adulto,<br></a> 
										 <a href="index.php?opc=enf_familia&s_opc=visualizar&pac_id=<?=$paciente->pac_id?>" >Consultar Ant Familiares,</a>
										 <a href="index.php?opc=antecedente&s_opc=visualizar4&pac_id=<?=$paciente->pac_id?>" >Consultar Ant Odontol&oacute;gicos</a></td>
								</tr>
							</table>
							<p></p>
						</div>
				</form>
			</td>
		</tr>
	</table>
<?
		}
	}
}
?>