<?
if (!$table){
?>Error en la conexi&oacute;n con la base de datos<br>
    Consulte con el administrador del sistema
    <?include('adulto.php');
	//exit(0);
}else{
	$usuario=$user->datos;
	$query= "SELECT * FROM paciente WHERE pac_id=$pac_id";
  	$table->search($query);
   	$paciente = $table->sql_fetch_object();

   	$query="select * from antecedente where pac_id=$pac_id";
	$table->sql_query($query);
	$ant = $table->sql_fetch_object();

	
	if ($ant->ant_estado < 3){
		$mess="<center><b>Los datos de los Ant Familiares NO han sido ingresados.</b> <br><br>
      		Por favor ingreselos primero</center>";
   	 	show_mess($mess,"alert");
	}else{
$pac = new adulto( $pac_id);
$obj=$pac->data;

?>

<table border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td><div align="center"><h2 class="black1"></h2></div></td>
		</tr>
		<tr>
			<td><form action="<?=$PHP_SELF?>" method="post" name="antecedente">
				<table class="black" width="100%" border="0" cellspacing="2" cellpadding="0" align="center">
					<tr>
						<td  width="45%"></td>
						<td width="10%"></td>
						<td class="black1" align="right" nowrap width="45%">Historia Cl&iacute;nica No: <?=$paciente->pac_numeroIdentificacion?><br>
									 Fecha de Ingreso: <?=$ant->ant_fechaIngresoAnt3?><br>
						</td>
					</tr>
					<tr>
								<td class="black" colspan="2">
							<table class="black" width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
								<tr>
									<td class="black1" nowrap width="10%">Paciente : </td>
									<td class="black1" nowrap width="25%"><?="$paciente->pac_nombres $paciente->pac_apellidos"?></td>
								</tr>
								<tr>
									<td class="black1" nowrap width="10%">Odont&oacute;logo  :  </td>
									<td class="black1" nowrap width="25%">&nbsp;&nbsp;<? 
											$query= "select  MAX(oeh_id) FROM odontologos_encargados_historia WHERE pac_id='$paciente->pac_id'";
											$table->search($query); 
											$maxo=$table->sql_fetch_object();
											//echo $query;
												$query="select * from usuario u,  odontologos_encargados_historia  d   
 												where u.usu_id=d.usu_id and d.pac_id = '$paciente->pac_id' and oeh_id='$maxo->max' order by oeh_id ASC";							
												$table->search($query); 
												//echo $query;
												if ($table->nfound >= 1) {
												$data=$table->sql_fetch_object();
												?><?=$data->usu_nombres." ".$data->usu_apellidos?> <?}?></td>
								</tr>
								<tr>
									<td class="black1" nowrap width="10%">Docente  :</td>
									<td class="black1" nowrap width="25%">&nbsp;&nbsp;<? 
											$query= "select  MAX(deh_id) FROM docentes_encargados_historia WHERE pac_id='$paciente->pac_id'";
											$table->search($query); 
											$maxd=$table->sql_fetch_object();
												$query="select * from usuario u,  docentes_encargados_historia  d   
 												where u.usu_id=d.usu_id and d.pac_id = '$paciente->pac_id' and deh_id='$maxd->max' order by deh_id ASC";							
												$table->search($query); 
												if ($table->nfound >= 1) {
												$data=$table->sql_fetch_object();
												?><?=$data->usu_nombres." ".$data->usu_apellidos?> <?}?></td>
								</tr>
							</table>
						</td>
								<td  width="45%"></td>
							</tr>
					<tr>
						<td class="black" colspan="3" align="center"><br><b>ANTECEDENTES MEDICOS FAMILIARES<br><br></b></td>
					</tr>
					<tr>
						<td class="black" colspan="3" valign="top">
							<table width="80%" border="0" cellspacing="2" cellpadding="0">
								<tr>
										<td class="black" nowrap width="25%">Anomalias Cong&eacute;nitas :</td>
										<td class="black" width="20%"><?=($ant->ant_anoCong=="t")?"SI":"NO"?></td>
									<td class="black" width="30%">Neoplasias</td>
									<td class="black" width="5%"><?=($ant->ant_neoplasias=="t")?"SI":"NO"?></td>
								</tr>
								<tr>
										<td class="black" width="25%">Enf. Mentales : </td>
										<td class="black" width="20%"><?=($ant->ant_enfMen=="t")?"SI":"NO"?></td>
									<td class="black" width="30%">Endocrinas</td>
									<td class="black" width="5%"><?=($ant->ant_endocrinas=="t")?"SI":"NO"?></td>
								</tr>
								<tr>
										<td class="black" width="25%">Enf. Cardiovasculares : </td>
										<td class="black" width="20%"><?=($ant->ant_enfCar=="t")?"SI":"NO"?></td>
									<td class="black" nowrap width="30%">Enf. de Origen Sanguineo&nbsp;:</td>
									<td class="black" width="5%"><?=($ant->ant_enfOriSan=="t")?"SI":"NO"?></td>
								</tr>
								<tr>
										<td class="black" width="25%">Enf. Dermatol&oacute;gicas : </td>
										<td class="black" width="20%"><?=($ant->ant_enfDer=="t")?"SI":"NO"?></td>
									<td class="black" width="30%">Enf. Infecciosas</td>
									<td class="black" width="5%"><?=($ant->ant_enfInf=="t")?"SI":"NO"?></td>
								</tr>
								<tr>
										<td class="black" width="25%">Enf. de Transmisi&oacute;n Sexual : </td>
										<td class="black" width="20%"><?=($ant->ant_enfTranSex=="t")?"SI":"NO"?></td>
									<td class="black" width="30%">Otras : </td>
									<td class="black" width="5%"><?=($ant->ant_otrasEnf=="t")?"SI":"NO"?></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="black" colspan="3" valign="top"><br>Observaciones :<br>
									<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="black">
										<tr>
											<td class="black" bgcolor="white">&nbsp;&nbsp;<?=$ant->ant_antFamiliares?></td>
										</tr>
									</table>
								</td>
					</tr>
							<tr height="8%">
								<td width="45%" height="8%"></td>
								<td height="8%"></td>
								<td width="45%" height="8%"></td>
							</tr>
							<tr>
								<td colspan="3" align="center" bgcolor="#e9e9e9"><a href="index.php?opc=antecedente&s_opc=visualizar&pac_id=<?=$paciente->pac_id?>" >Consultar Ant. Sociales,</a> 
								<a href="index.php?opc=antecedente&s_opc=visualizar1&pac_id=<?=$paciente->pac_id?>" >Consultar Ant. M&eacute;dicos Personales,</a><br>
								<a href="index.php?opc=antecedente&s_opc=visualizar2&pac_id=<?=$paciente->pac_id?>" >Consultar Enfermedades Propias del Adulto,</a> 
								<a href="index.php?opc=antecedente&s_opc=visualizar4&pac_id=<?=$paciente->pac_id?>" >Consultar Ant Odontol&oacute;gicos, </a> 
								<a href="index.php?opc=antecedente&s_opc=visualizar5&pac_id=<?=$paciente->pac_id?>" >Consultar Ant Alergicos y Habitos </a></td>
							</tr>
				</table>
						<p></p>
					</form></td>
		</tr>
	</table>
<?
	}
}
?>
