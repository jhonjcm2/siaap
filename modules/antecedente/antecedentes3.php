<?
if (!$table){
?>Error en la conexi&oacute;n con la base de datos<br>
    Consulte con el administrador del sistema
    <?include('adulto.php');
	//exit(0);
}else{
	$usuario=$user->datos;
	$query= "SELECT * FROM paciente WHERE pac_id=$pac_id";
  	$table->search($query);
   	$paciente = $table->sql_fetch_object();

   	$query="select * from antecedente where pac_id=$pac_id";
	$table->sql_query($query);
	$obj = $table->sql_fetch_object();
	$ant_id=$obj->ant_id;
	
	if($obj->ant_estado < 2){
 	     	$mess="<center><b>Los Antecedentes M&eacute;dicos Personales NO han sido ingresados.</b> <br><br>
  		Por favor ingreselos primero</center>";
   		show_mess($mess,"alert"); 	 		
   	}else{
             	if ($obj->ant_estado >= 3){
			$mess="<center><b>Los datos de los Ant Familiares YA han sido ingresados.</b> <br><br>
      			Por favor continue ingresando los Ant. Odontol&oacute;gicos</center>";
   	 		show_mess($mess,"alert");
		}else{
$pac = new adulto( $pac_id);
$obj=$pac->data;

?>
<table border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td><div align="center"><h2 class="black1"></h2></div></td>
		</tr>
		<tr>
			<td><form action="<?=$PHP_SELF?>" method="post" name="antecedente">
				<table class="black" width="90%" border="0" cellspacing="2" cellpadding="0" align="center">
					<tr>
						<td  width="35%"></td>
						<td width="10%"></td>
						<td class="black1" align="right" nowrap width="35%">Historia Cl&iacute;nica No: <?=$paciente->pac_numeroIdentificacion?><br>
							<input type="hidden" name=ant_fechaIngresoAnt3 value="<?=date("Y-m-d",time())?>"> Fecha de Ingreso: <?=fecha(date("Y-m-d",time()))?><br>
						</td>
					</tr>
					<tr>
						<td class="black" width="35%">
							<table class="black" width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
								<tr>
									<td class="black1" nowrap width="10%">Paciente : </td>
									<td class="black1" width="25%"><?="$paciente->pac_nombres $paciente->pac_apellidos"?></td>
								</tr>
								<tr>
									<td class="black1" nowrap width="10%">Odont&oacute;logo  :  </td>
									<td class="black1" width="25%">&nbsp;&nbsp;<? 
											$query= "select  MAX(oeh_id) FROM odontologos_encargados_historia WHERE pac_id='$paciente->pac_id'";
											$table->search($query); 
											$maxo=$table->sql_fetch_object();
											//echo $query;
												$query="select * from usuario u,  odontologos_encargados_historia  d   
 												where u.usu_id=d.usu_id and d.pac_id = '$paciente->pac_id' and oeh_id='$maxo->max' order by oeh_id ASC";							
												$table->search($query); 
												//echo $query;
												if ($table->nfound >= 1) {
												$data=$table->sql_fetch_object();
												?><?=$data->usu_nombres." ".$data->usu_apellidos?> <?}?></td>
								</tr>
								<tr>
									<td class="black1" nowrap width="10%">Docente  :</td>
									<td class="black1" nowrap width="25%">&nbsp;&nbsp;<? 
											$query= "select  MAX(deh_id) FROM docentes_encargados_historia WHERE pac_id='$paciente->pac_id'";
											$table->search($query); 
											$maxd=$table->sql_fetch_object();
												$query="select * from usuario u,  docentes_encargados_historia  d   
 												where u.usu_id=d.usu_id and d.pac_id = '$paciente->pac_id' and deh_id='$maxd->max' order by deh_id ASC";							
												$table->search($query); 
												if ($table->nfound >= 1) {
												$data=$table->sql_fetch_object();
												?><?=$data->usu_nombres." ".$data->usu_apellidos?> <?}?></td>
								</tr>
							</table>
						</td>
						<td  width="10%"></td>
						<td  width="35%"></td>
					</tr>
					<tr>
						<td class="black" colspan="3" align="center"><br><b>ANTECEDENTES MEDICOS FAMILIARES<br><br></b></td>
					</tr>
					<tr>
						<td class="black" colspan="3" valign="top">
							<table width="80%" border="0" cellspacing="2" cellpadding="0">
								<tr>
										<td class="black" nowrap width="25%">Anomalias Cong&eacute;nitas :</td>
										<td width="20%"><input type="checkbox" name="ant_anoCong" value="1" <?=($ant_anoCong)?"checked":""?> border="0"></td>
									<td class="black" width="30%">Neoplasias</td>
									<td width="5%"><input type="checkbox" name="ant_neoplasias" value="1" <?=($ant_neoplasias)?"checked":""?> border="0"></td>
								</tr>
								<tr>
										<td class="black" width="25%">Enf. Mentales : </td>
										<td width="20%"><input type="checkbox" name="ant_enfMen" value="1" <?=($ant_enfMen)?"checked":""?> border="0"></td>
									<td class="black" width="30%">Endocrinas</td>
									<td width="5%"><input type="checkbox" name="ant_endocrinas" value="1" <?=($ant_endocrinas)?"checked":""?> border="0"></td>
								</tr>
								<tr>
										<td class="black" width="25%">Enf. Cardiovasculares : </td>
										<td width="20%"><input type="checkbox" name="ant_enfCar" value="1" <?=($ant_enfCar)?"checked":""?> border="0"></td>
									<td class="black" nowrap width="30%">Enf. de Origen Sangu&iacute;neo&nbsp;:</td>
									<td width="5%"><input type="checkbox" name="ant_enfOriSan" value="1" <?=($ant_enfOriSan)?"checked":""?> border="0"></td>
								</tr>
								<tr>
										<td class="black" width="25%">Enf. Dermatol&oacute;gicas : </td>
										<td width="20%"><input type="checkbox" name="ant_enfDer" value="1" <?=($ant_enfDer)?"checked":""?> border="0"></td>
									<td class="black" width="30%">Enf. Infecciosas</td>
									<td width="5%"><input type="checkbox" name="ant_enfInf" value="1" <?=($ant_enfInf)?"checked":""?> border="0"></td>
								</tr>
								<tr>
										<td class="black" width="25%">Enf. de Transmisi&oacute;n Sexual : </td>
										<td width="20%"><input type="checkbox" name="ant_enfTranSex" value="1" <?=($ant_enfTranSex)?"checked":""?> border="0"></td>
									<td class="black" width="30%">Otras : </td>
									<td width="5%"><input type="checkbox" name="ant_otrasEnf" value="1" <?=($ant_otrasEnf)?"checked":""?> border="0"></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="black" colspan="3" valign="top"><br>Observaciones :<br>
							<textarea name="ant_antFamiliares" rows="4" cols="71"><?=$ant_antFamiliares?></textarea>
						</td>
					</tr>
					<tr>
						<td width="35%"></td>
						<td nowrap width="10%"><div align="right"><br>
							<input type="hidden" name="s_opc" value="crear3" border="0"> 
							<input type="hidden" name="origen" value="ant" border="0"> 
							<input type="hidden" name="opc" value="<?=$opc?>" border="0">
							<input type="hidden" name="ant_id" value="<?=$ant_id?>" border="0"> 
							<input type="hidden" name="pac_id" value="<?=$pac_id?>" border="0"> 
							<input type="hidden" name="ant_estado" value="3" border="0">    
							<input type="image" src="images/crear2.gif" alt="" align="right" border="1"><br><br><br><br></div>
						</td>
						<td align="right" width="35%"></td>
					</tr>
				</table>
			</form></td>
		</tr>
	</table>
<?
		}
	}
}
?>
