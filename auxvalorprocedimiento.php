<?
/*
auxvalorprocedimiento.php

Puede recibir el valor del procedimiento en pto_codigoProcedimiento
*/
include("config.inc.php");
standar_header();
session_start();
$user = validate();
$table = new my_db;

//recibo por metodo get la variable pagina
//examino la página a mostrar y el inicio del registro a mostrar
$pagina = $_GET["pagina"];
$TAMANO_PAGINA=15;//Numero de resultados a mostrar
//Si la variable no existe entonces es primera vez
if (!$pagina) {
	$inicio = 0;//Es el offset
	$pagina=1;//Pagina en la que nos encontramos
}
else {
    	$inicio = ($pagina - 1) * $TAMANO_PAGINA;
} 
//--------------------------------------------------------
//miro a ver el número total de campos que hay en la tabla con esa búsqueda
	$tablaini = new my_db;
	$queryini = "SELECT * FROM procedimiento";
	if($pto_codigoProcedimiento=$_GET["pto_codigoProcedimiento"]){
		$queryini.=" WHERE \"pto_codigoProcedimiento\" = '$pto_codigoProcedimiento' ";
	}
	$tablaini->search($queryini);
	//$num_total_registros = $tablaini->nfound;
	//calculo el total de páginas
	$total_paginas = ceil($tablaini->nfound / $TAMANO_PAGINA);
	//echo "primero<br>".$queryini;
//--------------------------------------------------------
	//Creamos el objeto de base de datos
	$table2 = new my_DB();
	
	//Hacemos la consulta
	$query = "SELECT * FROM \"procedimiento\" ";
	if($pto_codigoProcedimiento){
		$query.=" WHERE \"pto_codigoProcedimiento\" = '$pto_codigoProcedimiento' ";
	}
	$query.="ORDER BY \"pto_nombreProcedimiento\" ASC LIMIT ". $TAMANO_PAGINA ." OFFSET ".$inicio;

	//Hacemos la consulta
	//$table->sql_query($query);
	$table2->search($query);

	//echo "segundo<br>".$query;
	

?>
<html>

        <head>
                <meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
                <meta name="generator" content="Adobe GoLive 6">
                <title>Registro pacientes</title>
                <!-- link href="estilos.css" rel="stylesheet" media="screen" -->
                <link href="estilos1.css" rel="stylesheet" media="screen">
        </head>
<body>

	
<script language="javascript">
<!--
//Funcion para quien abrio esta ventana
function setOdo(valor_procedimiento){
	//Del documento quien abre le pasamos al formulario los valores ingresados a esta funcion
	opener.document.forms['busca_proc'].deu_valor.value = valor_procedimiento;
	//opener.document.forms['busca_proc'].pto_id.value = id_procedimiento;
	close();
}

-->
</script>

<body>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td class="black1" align="center" width="100%">Probando busqueda<br>
				<br>
				<br>
				<br>
			</td>
		</tr>
		<tr>
			<td nowrap width="100%">
				<table width="80%" border="0" cellpadding="2" cellspacing="1" align="center" bgcolor="black">
					<tr>
						<td  align="center" bgcolor="#e30707" class="white" width="30%"><b>C&oacute;digo procedimiento</b></td>
						<td align="center" bgcolor="#e30707" class="white" width="10%"><b>Nombre Procedimiento</b></td>
						<td align="center" bgcolor="#e30707" class="white" width="10%"><b>&Aacute;rea</b></td>
						<td align="center" bgcolor="#e30707" class="white" width="10%"><b>Valor Estudiante Pregrado</b></td>
						<td align="center" bgcolor="#e30707" class="white" width="10%"><b>Valor Estudiante Postgrado</b></td>
						<td align="center" bgcolor="#e30707" class="white" width="10%"><b>Valor Odont. Gral.</b></td>
						<td align="center" bgcolor="#e30707" class="white" width="10%"><b>Valor Odont. Especialista</b></td>
					</tr>
					
					<?
					if ($table2->nfound >= 1) {
					//En data almacenamos la fila en la que esta
						echo("Registros encontrados: ");
						echo($tablaini->nfound);
						echo("<BR>");
						while ($ara = $table2->sql_fetch_object() )
						{
							//$ara=$table2->sql_fetch_object();
							
							
							?>
							<tr bgcolor="<?=repetition()?>">

								<td class="black" width="10%"><?=$ara->pto_codigoProcedimiento?>
								</td>
								<td class="black" width="35%"><?=$ara->pto_nombreProcedimiento?>
								</td>
								<td class="black" width="15%"><?=$ara->pto_areaProcedimiento?>
								</td>
								<td class="black" width="10%"><a href="#" onClick= "javascript:setOdo('<?=$ara->pto_valorEstPre?>')"> <?=$ara->pto_valorEstPre?>
								</a></td>
								<td class="black" width="10%"><a href="#" onClick= "javascript:setOdo('<?=$ara->pto_valorEstPost?>')"> <?=$ara->pto_valorEstPost?>
								</a></td>
								<td class="black" width="10%"><a href="#" onClick= "javascript:setOdo('<?=$ara->pto_valorAsit?>')"> <?=$ara->pto_valorAsit?>
								</a></td>
								<td class="black" width="10%"><a href="#" onClick= "javascript:setOdo('<?=$ara->	pto_valorEsp?>')"> <?=$ara->pto_valorEsp?>
								</a></td>
							</tr>
							<?
						}
					}
					?>
				</table>
			</td>
		</tr>
		<tr>
			<?
			//muestro los distintos índices de las páginas, si es que hay varias páginas
			if ($total_paginas > 1){
				for ($i=1;$i<=$total_paginas;$i++){
					if ($pagina == $i){
						//si muestro el índice de la página actual, no coloco enlace
						echo "<font size='+1' color='red'>".$pagina . "</font> ";
					}
					else{
						//si el índice no corresponde con la página mostrada actualmente, coloco el enlace para ir a esa página
						echo "<a href='auxvalorprocedimiento.php?pto_codigoProcedimiento=$pto_codigoProcedimiento&pagina=" . $i . "'>" . $i . "</a> ";
					}
				}
			}
			?>
		</tr>
	</table>
</body>
</html>