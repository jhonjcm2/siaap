<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>

	<head>
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<meta name="author" content="Nestor Fabian">
		<title>Calendario</title>
		<LINK REL="stylesheet" TYPE="text/css" HREF="estilo2.css">
	</head>
<body>
<script type="text/javascript" src="ManResp.js" language="javascript"></script>
<br>
<?php
$dias = array("Domingo","Lunes","Martes","Mi�rcoles","Jueves","Viernes","S�bado");
$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

function formatearNum($i, $tipo) {
//    $f = ($i < 10 ? '0' : '') . $i;
    $f = str_pad($i,2,'0', STR_PAD_LEFT);
    if ($tipo && $tipo != '') {
        switch($tipo) {
            case 'mes':
                $f = ($f > 12 ? 12 : $f);
                break;

            case 'dia':
                $f = ($f > 31 ? 31 : $f);
        }
    }
    return $f;
    }

if($HTTP_GET_VARS)
	{
	$mes = $HTTP_GET_VARS['mes'];
	$anio = $HTTP_GET_VARS['anio'];
	}
else	{
	$mes = date('m');
	$anio = date('Y');
	}

$horas = date('H');
$minutos = date('i');
$segundos = date('s');
$dia = date('d');

$diasmes = cal_days_in_month(0,$mes,$anio);

$primerdia = jddayofweek(GregorianToJD($mes,1,$anio), 0);

echo "<center>\n";

echo "<form action='calendario.php' name='calend' method='get'>";
echo "<select name='mes' size='1' onchange='javascript:document.calend.submit();'>\n";
for($cm=1;$cm<=12;$cm++)
	{
	if($mes==$cm)
		echo "<option selected value='$cm'>",$meses[$cm-1],"</option>\n";
	else echo "<option value='$cm'>",$meses[$cm-1],"</option>\n";
	}
echo "</select>\n&nbsp;&nbsp;\n";

echo "<select name='anio' size='1' onchange='javascript:document.calend.submit();'>\n";
for($ca=$anio-50;$ca<$anio+50;$ca++)
	{
	if($anio==$ca)
		echo "<option selected value='$ca'>$ca</option>\n";
	else echo "<option value='$ca'>$ca</option>\n";
	}
echo "</select>\n&nbsp;&nbsp;\n";

echo "<br><br>\n";

echo "<table class='calendario'><tr>\n";

$diadelasemana = 0;
    for($i=0; $i<7; $i++) {
	echo " <th>$dias[$i]</th>";
	}
echo "</tr><tr>\n";

    for ($i = 0; $i < $primerdia; $i++) {
        echo "	<td>&nbsp;</td>";
        $diadelasemana++;
    	}
    for ($i=1; $i<=$diasmes;$i++) {
        if ($diadelasemana == 7) {
        	echo "\n</tr><tr>\n";
	        $diadelasemana = 0;
		}
	$fmes=formatearNum($mes,'mes');
	$fdia=formatearNum($i,'dia');
	if($i==$dia && $anio==date('Y') && $mes==date('m'))
		echo "	<td class='activo'><a href=\"javascript:retornaDateTime('$anio-$fmes-$fdia')\">$i</a></td>";
	else echo "	<td><a href=\"javascript:retornaDateTime('$anio-$fmes-$fdia')\">$i</a></td>";
        $diadelasemana++;
        }
    for ($i=$diadelasemana; $i<7;$i++) {
        echo "	<td>&nbsp;</td>";
    	}
echo "</table>\n<br>";

echo "<input type='text' name='horas' maxlength='2' size='1' border='0' value='$horas'> : \n";
echo "<input type='text' name='minutos' maxlength='2' size='1' border='0' value='$minutos'> : \n";
echo "<input type='text' name='segundos' maxlength='2' size='1' border='0' value='$segundos'>\n";

echo "</form>\n";
echo "</center>\n";

?>
</body>
</html>
