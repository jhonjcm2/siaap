<?
/*
funcionensayolistar.php

Puede recibir el valor del procedimiento en pto_codigoProcedimiento

include("config.inc.php");
standar_header();
session_start();
$user = validate();
$table = new my_db;*/


/*$TAMANO_PAGINA Numero de resultados a mostrar
$pagina Pagina en la que nos encontramos
$queryini Consulta que se va a hacer
$orden arreglo con campos con los que se va a ordenar
$campos arreglo con los campos de la consulta que se van a mostrar
$titulosCampos arreglo con los titulos de los campos
$nombrePagina nombre de la pagina completo con las variables via GET
*/
function listaBusqueda($TAMANO_PAGINA,$pagina,$queryini, $orden, $campos, $titulosCampos,$nombrePagina,$campoDo=0,$nombreForm=""){
	if (!$pagina) {
		$inicio = 0;//Es el offset
		$pagina=1;//Pagina en la que nos encontramos
	}
	else {
		$inicio = ($pagina - 1) * $TAMANO_PAGINA;
	}


//--------------------------------------------------------
//miro a ver el número total de campos que hay en la tabla con esa búsqueda
	$tablaini = new my_db;
	//echo "queryini es $queryini<br />";
	$tablaini->search($queryini);
	//$num_total_registros = $tablaini->nfound;
	//calculo el total de páginas
	$total_paginas = ceil($tablaini->nfound / $TAMANO_PAGINA);
	//echo "primero<br>".$queryini;

//--------------------------------------------------------
//Obtenemos los que se van a mostrar
	//Creamos el objeto de base de datos
	$table2 = new my_DB();
	
	//Hacemos la consulta
	$query = $queryini.' ORDER BY "'.implode(",",$orden).'" ASC LIMIT '.$TAMANO_PAGINA." OFFSET ".$inicio;
	//echo "query es $query<br />";
	//Hacemos la consulta
	//$table->sql_query($query);
	$table2->search($query);

	//echo "segundo<br>".$query;

?>
<html>

        <head>
                <meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
                <meta name="generator" content="Adobe GoLive 6">
                <title>Registro pacientes</title>
                <!-- link href="estilos.css" rel="stylesheet" media="screen" -->
                <!--****************************************************-->
		<LINK rel="stylesheet" type="text/css" href="images/estilo.css">
		<LINK rel="stylesheet" type="text/css" href="images/sdmenu.css">
		<LINK rel="stylesheet" type="text/css" href="files_acordeon/estilo.css">
		<link rel="stylesheet" type="text/css" href="estilos1.css">
		<!--****************************************************-->
        </head>
<body>

	
<script language="javascript">
<!--
//Funcion para quien abrio esta ventana
function setOdo(valor_procedimiento){
	//Del documento quien abre le pasamos al formulario los valores ingresados a esta funcion
	opener.document.forms['<?=$nombreForm?>'].<?=$campos[$campoDo]?>.value = valor_procedimiento;
	//opener.document.forms['busca_proc'].pto_id.value = id_procedimiento;
	close();
}

-->
</script>
<form action="<?=$PHP_SELF?>" method="post" name="listado">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td class="black1" align="center" width="100%">B&uacute;squeda<br>
				<br>
				<br>
				<br>
			</td>
		</tr>
		<tr>
			<td nowrap width="100%">
				<table class="listado">
					<tr class="cabecera">
						<?foreach($titulosCampos as $k){
							?>
							<td  align="center" bgcolor="#e30707" class="white" width="30%"><b><?=$k?></b></td>
							<?
						}?>
					</tr>
					
					<?
					if ($table2->nfound >= 1) {
					//En data almacenamos la fila en la que esta
						echo("Registros encontrados: ");
						echo($tablaini->nfound);
						echo("<BR>");
						while ($ara=$table2->sql_fetch_object())
						{$num = ($num) ? 0 : 1;
					?>
							<tr <?=($num)?' class="par"':' class="impar"'?>>
								<?for($indice=0; $indice<sizeof($campos); $indice++){
									if($indice==$campoDo){?>
										<td>
										<a href="#" onClick= "javascript:setOdo('<?=$ara->$campos[$indice]?>')"><?=$ara->$campos[$indice]?>
										</a>
										</td>
									<?}else{?>
										<td class="black" width="10%"><?=$ara->$campos[$indice]?></td>
									<?}
								}?>
							</tr>
							<?
						}
					}else{
						echo("No se encontraron registros en su b&uacute;squeda!!");
					}
					?>
				</table>
			</td>
		</tr>
		<tr>
			<?
			//muestro los distintos índices de las páginas, si es que hay varias páginas
			if ($total_paginas > 1){
				for ($i=1;$i<=$total_paginas;$i++){
					if ($pagina == $i){
						//si muestro el índice de la página actual, no coloco enlace
						echo "<font size='+1' color='red'>".$pagina."</font> ";
					}
					else{
						//si el índice no corresponde con la página mostrada actualmente, coloco el enlace para ir a esa página
						echo "<a href='$nombrePagina&pagina=".$i."'>".$i."</a> ";
					}
				}
			}
			?>
		</tr>
	</table>
</form>
</body>
</html>
<?}?>